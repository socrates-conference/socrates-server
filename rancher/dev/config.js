// @flow
/* eslint-disable */
import type {Config} from '../../src/ConfigType';

const config: Config = {
  'pricingTable': {
    'single': 62.5,
    'juniorExclusively': 62.5,
    'juniorShared': 45.45,
    'bedInDouble': 47.95,
    'Thursday Flat': 11.9,
    'Thursday Dinner': 13.3,
    'Friday Flat': 52.9,
    'Friday Dinner': 13.3,
    'Saturday Flat': 52.9,
    'Saturday Dinner': 13.3,
    'Sunday Flat': 45,
    'Sunday Dinner': 13.3
  },
  'sponsoringAmount': 26750,
  'environment': 'dev',
  'jwtSecret': '%jwt_secret%',
  'database': {
    'host': 'socrates-db',
    'port': 3306,
    'name': 'socrates_db',
    'user': '%username%',
    'password': '%password%',
    'debug': false
  },
  'server': {
    'port': 4444
  },
  'mail': {
    'user': '%email_user%',
    'password': '%email_password%',
    'host': '%email_provider%'
  },
  'kafka': {
    'host': 'zookeeper',
    'port': 2181
  }
};
export default config;

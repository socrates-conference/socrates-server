CREATE TABLE `sponsor`
(
    `id`             int(11)      NOT NULL AUTO_INCREMENT,
    `conferenceId`   int(11)      NOT NULL,
    `name`           varchar(255) NOT NULL,
    `nameAdditional` varchar(255) DEFAULT NULL,
    `responsible`    varchar(100) NOT NULL,
    `address1`       varchar(255) NOT NULL,
    `address2`       varchar(255) DEFAULT NULL,
    `province`       varchar(255) DEFAULT NULL,
    `postal`         varchar(20)  NOT NULL,
    `city`           varchar(255) NOT NULL,
    `country`        varchar(255) NOT NULL,
    `information`    varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_sponsor_conference_idx` (`conferenceId`),
    CONSTRAINT `FK_sponsor_conference` FOREIGN KEY (`conferenceId`) REFERENCES `conference` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

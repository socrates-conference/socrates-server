/*
Sponsoring state:
0 - not contacted
1 - contacted
2 - interested
3 - confirmed
4 - rejected
5 - no answer
 */


INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('1', '1', '1', '3', 'mozaicworks', '1', '6');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`)
VALUES ('2', '1', '2', '1', 'codurance', '0');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('3', '1', '3', '3', 'demandflow.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('4', '1', '4', '3', 'huf-group.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('5', '1', '5', '3', 'ISOGruppe', '1', '1');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('6', '1', '6', '3', 'klosebrothers', '1', '1');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('7', '1', '7', '3', 'leanovateBerlin', '1', '1');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`)
VALUES ('8', '1', '8', '1', 'mercateo', '0');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('9', '1', '9', '3', 'codecentric', '1', '2');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`)
VALUES ('10', '1', '10', '1', 'connexxo', '0');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('11', '1', '11', '3', 'innoq', '1', '3');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('12', '1', '12', '3', 'ooseNews', '1', '3');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('13', '1', '13', '3', 'rewedigital', '1', '2');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('14', '1', '14', '3', 'MaibornWolff', '1', '3');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('15', '1', '15', '3', 'methodpark_DE', '1', '3');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `packageId`)
VALUES ('16', '1', '16', '3', 'tngtecg', '3');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('17', '1', '17', '3', 'DATEV', '1', '3');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('18', '1', '18', '3', 'itagile', '1', '4');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('19', '1', '19', '3', 'novatecgmbh', '1', '5');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('20', '1', '27', '3', 'andreanaobjects', '1', '1');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('21', '1', '31', '3', 'seibertmedia', '1', '1');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('22', '1', '32', '3', 'NordicTweets', '1', '1');
INSERT INTO `socrates_db`.`sponsor_conference_event` (`id`, `conferenceEditionId`, `sponsorId`,
                                                      `stateOfRequestForSponsoring`, `twitter`, `isLogoOnHomepage`,
                                                      `packageId`)
VALUES ('23', '1', '33', '3', 'BeeGFS', '1', '1');

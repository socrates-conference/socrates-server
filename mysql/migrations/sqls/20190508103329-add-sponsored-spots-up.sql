INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('1', '3', ' ', 'single', 'no', 'Andreas', 'Klein');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('2', '4', 'Philipp.Tusch@huf-group.com', 'single', 'no', 'Philipp', 'Tusch');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('3', '5', 'Dominik.Schlosser@iso-gruppe.com', 'single', 'no', 'Dominik', 'Schlösser');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('4', '6', 'jaroslaw.klose@klosebrothers.de', 'single', 'no', 'Jaroslav', 'Klose');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('5', '9', ' ', 'single', 'no', 'Christoph', 'Walter');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('6', '9', ' ', 'single', 'no', 'Thomas', 'Traude');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('7', '11', 'vera@peuntinger.de', 'single', 'yes', 'Vera', 'Peuntinger');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('8', '11', 'michael.vitz@innoq.com', 'single', 'no', 'Michael', 'Witz');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('9', '11', 'martin.weck@outlook.com', 'single', 'no', 'Martin', 'Weck');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('10', '12', 'stephan.roth@oose.de', 'single', 'no', 'Stephan', 'Roth');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('11', '12', 'markus.lang@oose.de', 'single', 'no', 'Markus', 'Lang');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('12', '12', 'stefan.rieger@oose.de', 'single', 'no', 'Stefan', 'Rieger');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('13', '13', 'ansgar.himmel@rewe-digital.com', 'single', 'no', 'Ansgar', 'Himmel');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('14', '13', 'stefan.scheidt@rewe-digital.com', 'single', 'no', 'Stefan', 'Scheidt');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('15', '14', 'richard.gross@maibornwolff.de', 'single', 'no', 'Richard', 'Gross');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('16', '14', 'katja.boehnke@maibornwolff.de', 'single', 'yes', 'Katja', 'Böhnke');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('17', '14', 'damir.porobic@maibornwolff.de', 'single', 'no', 'Damir', 'Porobic');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('18', '15', ' ', 'single', 'no', 'Benjamin', 'Klüglein');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('19', '15', ' ', 'single', 'no', 'Andreas', 'Lindsteding');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('20', '15', ' ', 'single', 'no', 'Christian', 'Schlumberger');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('21', '16', 'ruben.straube@tngtech.com', 'single', 'no', 'Ruben', 'Straube');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('22', '16', 'matthias.klass@tngtech.com', 'single', 'no', 'Matthias', 'Klaas');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('23', '16', 'christoph.wersal@tngtech.com', 'single', 'no', 'Christoph', 'Wersal');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('24', '17', '', 'single', 'no', 'Christoph', 'Welcz');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('25', '17', ' ', 'single', 'no', 'Oliver', 'Roth');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('26', '17', ' ', 'single', 'no', 'Thomas', 'Scholz');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('27', '18', 'nb@it-agile.de', 'single', 'no', 'Nico', ' ');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('28', '18', 'mgaertne@gmail.com', 'single', 'no', 'Markus', 'Gärtner');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('29', '18', 'steven.collins@it-agile.de', 'single', 'no', 'Steven', 'Collins');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('30', '18', 'tim.mueller@it-agile.de', 'single', 'no', 'Tim', 'Müller');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('31', '19', ' ', 'single', 'no', 'Timo', 'Freiberg');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('32', '19', ' ', 'single', 'no', 'Alexandre', 'Miller');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('33', '19', ' ', 'single', 'no', 'Sebastian', 'Letzel');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('34', '19', ' ', 'single', 'no', 'Dominik', 'Kriese');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('35', '19', ' ', 'single', 'no', 'Alex', 'Schüssler');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('36', '20', 'eike.kohnert@andrena.de', 'single', 'no', 'Eike', 'Kohnert');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('37', '21', 'jrost@seibert-media.net', 'single', 'no', 'Julian Felix', 'Rost');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('38', '22', ' ', 'single', 'no', 'Markus', 'Tacker');
INSERT INTO `socrates_db`.`sponsored_spot` (`id`, `sponsorConferenceEventId`, `email`, `room_type_id`, `diversity`,
                                            `firstname`, `lastname`)
VALUES ('39', '23', ' ', 'single', 'no', 'Oliver', 'Steffen');

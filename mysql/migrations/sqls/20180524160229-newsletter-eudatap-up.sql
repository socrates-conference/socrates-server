ALTER TABLE `socrates_db`.`person`
    ADD COLUMN `newsletter_eudatap_consent_key` VARCHAR(36) NULL,
    ADD COLUMN `newsletter_eudatap_consent_date` DATETIME NULL,
    ADD COLUMN `is_newsletter_consent_pending` TINYINT(1) NOT NULL DEFAULT '0',
    ADD COLUMN `newsletter_request_date` DATETIME NULL;

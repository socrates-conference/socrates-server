INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('1', '1', 'mozaicworks', 'Dimitry', 'paying at hotel', ' ', ' ', ' ');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `address2`, `postal`,
                                     `city`, `country`)
VALUES ('2', '1', 'Codurance Ltd', 'Tobias M.', '15 Northburgh Street', '', 'EC1V 0JR', 'London', 'UK');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('3', '1', 'DemandFlow GmbH', 'Tobias M.', 'Besselstr. 26', '68219', 'Mannheim', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('4', '1', 'Huf Secure Mobile GmbH', 'Tobias M.', 'Steeger Str. 17', '42551', 'Velbert', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('5', '1', 'ISO Gruppe', 'Tobias M.', ' ', ' ', ' ', ' ');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('6', '1', 'klose brothers gmbh', 'Tobias M.', 'sudbrackstrasse 17', '33611', 'Bielefeld', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `address2`, `postal`,
                                     `city`, `country`)
VALUES ('7', '1', 'leanovate GmbH', 'Tobias M.', 'Blücherstr. 22', 'Hof V Aufgang 5, 2. Stock', '10961', 'Berlin',
        'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `nameAdditional`, `responsible`, `address1`,
                                     `postal`, `city`, `country`)
VALUES ('8', '1', 'Mercateo Services GmbH', ' z. H. Herrn Marcus Hendel', 'Dimitry', 'Musseumsgasse 4-5', '06366',
        'Köthen', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `address2`, `postal`,
                                     `city`, `country`)
VALUES ('9', '1', 'codecentric AG', 'Tobias G.', 'Hochstr. 11', 'finance@codecentric.de', '42697', 'Solingen',
        'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('10', '1', 'Connexxo GmbH', 'Tobias M.', 'Bärenwaldstrasse 13', '81737', 'München', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `nameAdditional`, `responsible`, `address1`,
                                     `postal`, `city`, `country`)
VALUES ('11', '1', 'innoQ Deutschland GmbH', ' z.H. Herrn Stefan Tilkov', 'Tobias M.', 'Krischerstr. 100', '40789',
        'Monheim', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `nameAdditional`, `responsible`, `address1`,
                                     `postal`, `city`, `country`)
VALUES ('12', '1', 'oose Innovative Informatik eG', ' z.H. Frau Angelika Sonntag', 'Tobias M.', 'Schulterblatt 36',
        '20357', 'Hamburg', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `nameAdditional`, `responsible`, `address1`,
                                     `postal`, `city`, `country`, `information`)
VALUES ('13', '1', 'REWE Digital GmbH', ' c/o REWE Deutscher Supermarkt AG & Co. KGaA Zentrales Rechnungswesen – BWO',
        'Tobias M.', 'Raiffeisenstr.5-9', '61191', 'Rosbach v. d. H.', 'Deutschland',
        'Ansprechpartner Lasse Hofmann, Kostenstelle 3168010011.');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('14', '1', 'MaibornWolff GmbH', 'Tobias M.', 'Theresienhöhe 13', '80339', 'München', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('15', '1', 'Method Park Holding AG', 'Tobias M.', 'Wetterkreuz 19a', '91058', 'Erlangen', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('16', '1', 'TNG Technology Consulting GmbH', 'Tobias M.', 'Betastr 13 a', '85774', 'Unterföhring',
        'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('17', '1', 'DATEV eG', 'Alex', 'Paumgartnerstrasse 6-14', '90329', 'Nürnberg', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `nameAdditional`, `responsible`, `address1`,
                                     `postal`, `city`, `country`)
VALUES ('18', '1', 'it-agile GmbH', 'Monespenny', 'Tobias M.', 'Willy-Brandt-Straße 1', '20457', 'Hamburg',
        'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('19', '1', 'NOVATEC Consulting GmbH', 'Tobias M.', 'Dieselstraße 18/1', '70771', 'Leinfelden-Ecterdingen',
        'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('20', '1', 'Instana (sponsoring instana account)', 'Tobias G.', ' ', ' ', ' ', ' ');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('21', '1', 'Valtech GmbH', 'Tobias M.', 'Bahnstraße 16', '40212', 'Düsseldorf', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('22', '1', 'ePages GmbH', 'Tobias M.', 'Pilatuspool 2', '20355', 'Hamburg', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('23', '1', 'Zweitag GmbH', 'Tobias M.', 'Alter Fischmarkt 12', '48143', 'Münster', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`, `information`)
VALUES ('24', '1', 'GitHub Inc.', 'Tobias M.', '88 Colin P Kelly Jr St', 'CA 94107', 'San Francisco', 'USA',
        'VAT registrant number EU528001391');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('25', '1', 'Wikimedia Deutschland e. V.', 'Tobias M.', 'Tempelhofer Ufer 23-24', '10963', 'Berlin',
        'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `nameAdditional`, `responsible`, `address1`,
                                     `postal`, `city`, `country`)
VALUES ('26', '1', 'QT Mobilitätsservice GmbH', 'c/o WeWork', 'Tobias M.', 'Neue Schönhauser Str. 3-5', '10178',
        'Berlin', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `nameAdditional`, `responsible`, `address1`,
                                     `postal`, `city`, `country`)
VALUES ('27', '1', 'Andrena Objects AG', 'z. H. Frau Elisabeth Bieneck', 'Tobias M.', 'Albert-Nestler-Str. 9', '76131',
        'Karlsruhe', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('28', '1', 'dreamIt GmbH', 'Tobias M.', 'Gaußstr. 126', '22765', 'Hamburg', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `address2`, `postal`,
                                     `city`, `country`)
VALUES ('29', '1', 'Holisticon AG', 'Tobias M.', 'Griegstr. 75', 'Haus 25', '22763', 'Hamburg', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('30', '1', 'improuv GmbH', 'Tobias M.', 'Brecherspitzstr. 8', '81541', 'München', 'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `nameAdditional`, `responsible`, `address1`,
                                     `postal`, `city`, `country`)
VALUES ('31', '1', '//SEIBERT/MEDIA GmbH', 'LuisenForum', 'Tobias M.', 'Kirchgasse 6', '65185', 'Wiesbaden',
        'Deutschland');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `nameAdditional`, `responsible`, `address1`,
                                     `postal`, `city`, `country`)
VALUES ('32', '1', 'Nordic Semiconductor ASA', 'c/o Markus Tacker', 'Tobias M.', 'Otto Nielsens veg 12', '7052',
        'Trondheim', 'Norway');
INSERT INTO `socrates_db`.`sponsor` (`id`, `conferenceId`, `name`, `responsible`, `address1`, `postal`, `city`,
                                     `country`)
VALUES ('33', '1', 'ThinkParQ GmbH', 'Tobias M.', 'Trippstadter Str. 110', '67663', 'Kaiserslauten', 'Deutschland');

INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('1', '1', 'Maria Diaconu', 'maria.diaconu@mozaicworks.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('2', '2', 'Sandro Mancuso', 'sandro@codurance.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('3', '3', 'Andreas Klein', 'andreas.klein@demandflow.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('4', '4', 'Verena Ehlenbruch', 'Verena.Ehlenbruch@huf-group.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('5', '5', 'Marina Pelz', 'Marina.Pelz@iso-gruppe.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('6', '6', 'Martin Klose', 'info@klosebrothers.de (Martin Klose)', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('7', '6', 'Jaroslav Klose', 'info@klosebrothers.de (Martin Klose)', '0', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('8', '7', 'Susanne Walter', 'susanne.walter@leanovate.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('9', '8', 'Marcus Hendel', 'Marcus.Hendel@mercateo.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('10', '9', 'Claudia Fröhling', 'claudia.froehling@codecentric.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('11', '10', 'Pierluigi Pugliese', 'ppugliese@connexxo.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('12', '11', 'Eberhard Wolf', 'eberhard.wolff@innoq.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('13', '12', 'Angelika Sonntag', 'angelika.sonntag@oose.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('14', '12', 'Stephan Roth', 'stephan.roth@oose.de', '0', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('15', '13', 'Lasse Hoffman', 'lasse.hofmann@rewe-digital.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('16', '14', 'Thomas Pilot', 'thomas.pilot@maibornwolff.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('17', '15', 'Anna Zeidler', 'anna.zeidler@methodpark.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('18', '16', 'Ruben Straube', 'ruben.straube@tngtech.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('19', '16', 'Postbox', 'office@tngtech.com', '0', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('20', '17', 'Andy Fischer', 'andreas-chr.fischer@datev.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('21', '18', 'Markus Gärtner', 'info@it-agile.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('22', '18', 'Wolf Gideon Bleek', 'wolf-gideon.bleek@it-agile.de', '0', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('23', '19', 'Axel Schüssler', 'Axel.Schuessler@novatec-gmbh.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('24', '20', 'Fabian Lange', 'fabian.lange@instana.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('25', '21', 'Gerd Rosenacker', 'gerd.rosenacker@valtech.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('26', '22', 'Benjamin Nothdurf', 'b.nothdurft@gmail.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('27', '23', 'Sebastian Tuke', 'sebastian.tuke@zweitag.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('28', '24', 'Mike Adolphs', 'mike@github.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('29', '25', 'Sandra Müllrick', 'sandra.muellrick@wikimedia.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('30', '26', 'Markus Krogemann', 'markus@qixxit.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('31', '27', 'Elisabeth Bieneck', 'elisabeth.bieneck@andrena.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('32', '28', 'Unbekannt', 'info@dreamit.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('33', '29', 'Stefan Heldt', 'stefan.heldt@holisticon.de', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('34', '30', 'Sabastian Hegelmeier', 'sebastian.heglmeier@improuv.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('35', '31', 'Joachim Seibert', 'jseibert@seibert-media.net', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('36', '32', 'Markus Tacker', 'Markus.Tacker@nordicsemi.no', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('37', '33', 'Toni Rheker', 'toni.rheker@thinkparq.com', '1', '1');
INSERT INTO `socrates_db`.`sponsor_contact` (`id`, `sponsorId`, `name`, `email`, `isMainContact`, `isActive`)
VALUES ('38', '33', 'Troy Patterson', 'troy.patterson@thinkparq.com', '0', '1');


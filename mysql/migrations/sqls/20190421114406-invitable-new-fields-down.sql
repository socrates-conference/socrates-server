ALTER TABLE `socrates_db`.`invitables`
    DROP COLUMN `introduction`,
    DROP COLUMN `room_type`,
    ADD COLUMN `age`;

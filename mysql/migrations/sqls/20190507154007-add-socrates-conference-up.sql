INSERT INTO `socrates_db`.`conference` (`id`, `name`, `longName`)
VALUES ('1', 'SoCraTes', 'Software Craft and Testing Conference');

INSERT INTO `socrates_db`.`conference_edition` (`id`, `conferenceId`, `year`, `startDate`, `endDate`,
                                                `startApplications`, `endApplications`, `lotteryDay`, `lunchPrice`,
                                                `dinnerPrice`)
VALUES ('1', '1', '2019', '2019-08-22 17:00:00', '2019-08-25 17:00:00', '2019-04-10 19:00:00', '2019-08-01 19:00:00',
        '2019-05-01 19:00:00', '0', '13.30');

INSERT INTO `socrates_db`.`conference_day` (`id`, `conferenceEditionId`, `name`, `conferenceDayNumber`, `fee`)
VALUES ('1', '1', 'Thursday', '1',  '11.90');
INSERT INTO `socrates_db`.`conference_day` (`id`, `conferenceEditionId`, `name`, `conferenceDayNumber`, `fee`)
VALUES ('2', '1', 'Friday', '2', '52.90');
INSERT INTO `socrates_db`.`conference_day` (`id`, `conferenceEditionId`, `name`, `conferenceDayNumber`, `fee`)
VALUES ('3', '1', 'Saturday', '3', '52.90');
INSERT INTO `socrates_db`.`conference_day` (`id`, `conferenceEditionId`, `name`, `conferenceDayNumber`, `fee`)
VALUES ('4', '1', 'Sunday', '4', '45.00');
INSERT INTO `socrates_db`.`conference_day` (`id`, `conferenceEditionId`, `name`, `conferenceDayNumber`, `fee`)
VALUES ('5', '1', 'Monday', '5', '0');

INSERT INTO `socrates_db`.`room_type` (`id`, `conferenceEditionId`, `name`, `nightPricePerPerson`, `numberOfRooms`,
                                       `peoplePerRoom`, `reservedOrganization`, `reservedSponsors`, `lotterySortOrder`)
VALUES ('single', '1', 'Single', '62.50', '120', '1', '3', '6', '4');
INSERT INTO `socrates_db`.`room_type` (`id`, `conferenceEditionId`, `name`, `nightPricePerPerson`, `numberOfRooms`,
                                       `peoplePerRoom`, `reservedOrganization`, `reservedSponsors`, `lotterySortOrder`)
VALUES ('bedInDouble', '1', 'Double shared', '47.95', '16', '2', '4', '0', '3');
INSERT INTO `socrates_db`.`room_type` (`id`, `conferenceEditionId`, `name`, `nightPricePerPerson`, `numberOfRooms`,
                                       `peoplePerRoom`, `reservedOrganization`, `reservedSponsors`, `lotterySortOrder`)
VALUES ('juniorExclusively', '1', 'Junior (exclusively)', '62.50', '42', '1', '0', '0', '2');
INSERT INTO `socrates_db`.`room_type` (`id`, `conferenceEditionId`, `name`, `nightPricePerPerson`, `numberOfRooms`,
                                       `peoplePerRoom`, `reservedOrganization`, `reservedSponsors`, `lotterySortOrder`)
VALUES ('juniorShared', '1', 'Junior (shared)', '45.45', '14', '2', '0', '0', '1');

INSERT INTO `socrates_db`.`arrival_time` (`id`, `legacyId`, `conferenceEditionId`, `conferenceDayId`, `name`, `hint`)
VALUES ('1', '0', '1', '1', 'Thursday afternoon', 'Means that the participant takes dinner on Thursday.');
INSERT INTO `socrates_db`.`arrival_time` (`id`, `legacyId`, `conferenceEditionId`, `conferenceDayId`, `name`, `hint`)
VALUES ('2', '1', '1', '1', 'Thursday evening', 'Means that the participant will NOT have dinner on Thursday.');

INSERT INTO `socrates_db`.`departure_time` (`id`, `legacyId`, `conferenceEditionId`, `conferenceDayId`, `name`, `hint`,
                                            `hasToPayConferenceDayFee`)
VALUES ('1', '0', '1', '3', 'Saturday afternoon', 'Means that the participant will NOT have dinner on Saturday.', '1');
INSERT INTO `socrates_db`.`departure_time` (`id`, `legacyId`, `conferenceEditionId`, `conferenceDayId`, `name`, `hint`,
                                            `hasToPayConferenceDayFee`)
VALUES ('2', '1', '1', '3', 'Saturday evening', 'Means that the participant takes dinner on Saturday.', '1');
INSERT INTO `socrates_db`.`departure_time` (`id`, `legacyId`, `conferenceEditionId`, `conferenceDayId`, `name`, `hint`,
                                            `hasToPayConferenceDayFee`)
VALUES ('3', '2', '1', '4', 'Sunday morning', 'Nothing special to note.', '0');
INSERT INTO `socrates_db`.`departure_time` (`id`, `legacyId`, `conferenceEditionId`, `conferenceDayId`, `name`, `hint`,
                                            `hasToPayConferenceDayFee`)
VALUES ('4', '3', '1', '4', 'Sunday afternoon', 'Means that the participant will NOT have dinner on Sunday.', '1');
INSERT INTO `socrates_db`.`departure_time` (`id`, `legacyId`, `conferenceEditionId`, `conferenceDayId`, `name`, `hint`,
                                            `hasToPayConferenceDayFee`)
VALUES ('5', '4', '1', '4', 'Sunday evening', 'Means that the participant takes dinner on Sunday.', '1');
INSERT INTO `socrates_db`.`departure_time` (`id`, `legacyId`, `conferenceEditionId`, `conferenceDayId`, `name`, `hint`,
                                            `hasToPayConferenceDayFee`)
VALUES ('6', '5', '1', '5', 'Monday morning', 'Nothing special to note.', '0');

INSERT INTO `socrates_db`.`stay_length` (`id`, `arrivalId`, `departureId`, `nights`, `lunches`, `dinners`)
VALUES ('1', '1', '1', '2', '0', '2');
INSERT INTO `socrates_db`.`stay_length` (`id`, `arrivalId`, `departureId`, `nights`, `lunches`, `dinners`)
VALUES ('2', '1', '2', '2', '0', '3');
INSERT INTO `socrates_db`.`stay_length` (`id`, `arrivalId`, `departureId`, `nights`, `lunches`, `dinners`)
VALUES ('3', '1', '3', '3', '0', '3');
INSERT INTO `socrates_db`.`stay_length` (`id`, `arrivalId`, `departureId`, `nights`, `lunches`, `dinners`)
VALUES ('4', '1', '4', '3', '0', '3');
INSERT INTO `socrates_db`.`stay_length` (`id`, `arrivalId`, `departureId`, `nights`, `lunches`, `dinners`)
VALUES ('5', '1', '5', '3', '0', '4');
INSERT INTO `socrates_db`.`stay_length` (`id`, `arrivalId`, `departureId`, `nights`, `lunches`, `dinners`)
VALUES ('6', '1', '6', '4', '0', '4');
INSERT INTO `socrates_db`.`stay_length` (`id`, `arrivalId`, `departureId`, `nights`, `lunches`, `dinners`)
VALUES ('7', '2', '1', '2', '0', '1');
INSERT INTO `socrates_db`.`stay_length` (`id`, `arrivalId`, `departureId`, `nights`, `lunches`, `dinners`)
VALUES ('8', '2', '2', '2', '0', '2');
INSERT INTO `socrates_db`.`stay_length` (`id`, `arrivalId`, `departureId`, `nights`, `lunches`, `dinners`)
VALUES ('9', '2', '3', '3', '0', '2');
INSERT INTO `socrates_db`.`stay_length` (`id`, `arrivalId`, `departureId`, `nights`, `lunches`, `dinners`)
VALUES ('10', '2', '4', '3', '0', '2');
INSERT INTO `socrates_db`.`stay_length` (`id`, `arrivalId`, `departureId`, `nights`, `lunches`, `dinners`)
VALUES ('11', '2', '5', '3', '0', '3');
INSERT INTO `socrates_db`.`stay_length` (`id`, `arrivalId`, `departureId`, `nights`, `lunches`, `dinners`)
VALUES ('12', '2', '6', '4', '0', '3');

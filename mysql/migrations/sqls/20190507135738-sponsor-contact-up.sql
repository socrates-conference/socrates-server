CREATE TABLE `sponsor_contact`
(
    `id`            int(11)      NOT NULL AUTO_INCREMENT,
    `sponsorId`     int(11)      NOT NULL,
    `name`          varchar(45)  NOT NULL,
    `email`         varchar(100) NOT NULL,
    `phone`         varchar(50)  NOT NULL DEFAULT '',
    `isMainContact` tinyint(1)   NOT NULL DEFAULT '0',
    `isActive`      tinyint(1)   NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `FK_contact_sponsor_idx` (`sponsorId`),
    CONSTRAINT `FK_contact_sponsor` FOREIGN KEY (`sponsorId`) REFERENCES `sponsor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `sponsor_conference_event`
(
    `id`                           int(11)        NOT NULL AUTO_INCREMENT,
    `conferenceEditionId`          int(11)        NOT NULL,
    `sponsorId`                    int(11)        NOT NULL,
    `stateOfRequestForSponsoring`  varchar(45)    NOT NULL,
    `twitter`                      varchar(45)    NOT NULL DEFAULT '',
    `isLogoOnHomepage`             tinyint(1)     NOT NULL DEFAULT '0',
    `amountForOrganisationalStuff` decimal(10, 2) NOT NULL DEFAULT '0',
    `packageId`                    int(11)                 DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_sponsor_conference_idx` (`conferenceEditionId`),
    KEY `FK_sponsor_conference_sponsor_idx` (`sponsorId`),
    KEY `FK_sponsor_conference_package_idx` (`packageId`),
    CONSTRAINT `FK_sponsor_conference_conference` FOREIGN KEY (`conferenceEditionId`) REFERENCES `conference_edition` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_sponsor_conference_package` FOREIGN KEY (`packageId`) REFERENCES `sponsoring_package` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_sponsor_conference_sponsor` FOREIGN KEY (`sponsorId`) REFERENCES `sponsor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

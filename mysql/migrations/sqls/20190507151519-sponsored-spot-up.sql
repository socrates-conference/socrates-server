CREATE TABLE `sponsored_spot`
(
    `id`                       int(11)      NOT NULL AUTO_INCREMENT,
    `sponsorConferenceEventId` int(11)      NOT NULL,
    `email`                    varchar(100) NOT NULL,
    `room_type_id`             varchar(45)  NOT NULL,
    `diversity`                varchar(45)  NOT NULL DEFAULT '0',
    `firstname`                varchar(45)  NOT NULL,
    `lastname`                 varchar(45)  NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_spot_sponsor_idx` (`sponsorConferenceEventId`),
    KEY `FK_sponsored_spot_room_type_idx` (`room_type_id`),
    CONSTRAINT `FK_sponsored_spot_room_type` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_sponsored_spot_sponsor` FOREIGN KEY (`sponsorConferenceEventId`) REFERENCES `sponsor_conference_event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

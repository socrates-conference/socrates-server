CREATE TABLE `event_log`
(
    `id`         int(11)       NOT NULL AUTO_INCREMENT,
    `type`       varchar(45)   NOT NULL,
    `date`       datetime      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `old_values` varchar(5000) NOT NULL,
    `new_values` varchar(5000) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `IX_DATE` (`date`),
    KEY `IX_TYPE` (`type`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

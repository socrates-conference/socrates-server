ALTER TABLE `socrates_db`.`registration`
    ADD COLUMN `confirmation_reminder_counter` INT NOT NULL DEFAULT '0',
    ADD COLUMN `confirmation_last_reminder` DATETIME NULL DEFAULT NULL;

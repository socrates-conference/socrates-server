CREATE TABLE `application`
(
    `id`               int(11)      NOT NULL AUTO_INCREMENT,
    `person_id`        int(11)      NOT NULL,
    `room_type`        varchar(30)  NOT NULL,
    `diversity_reason` varchar(255) NOT NULL,
    `date`             datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `FK_application_person_idx` (`person_id`),
    CONSTRAINT `FK_application_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

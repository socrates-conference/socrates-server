CREATE TABLE `conference_day`
(
    `id`                  int(11)       NOT NULL AUTO_INCREMENT,
    `conferenceEditionId` int(11)       NOT NULL,
    `name`                varchar(45)   NOT NULL,
    `conferenceDayNumber` int(11)       NOT NULL,
    `fee`                 decimal(5, 2) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_day_conference_idx` (`conferenceEditionId`),
    CONSTRAINT `FK_day_conference` FOREIGN KEY (`conferenceEditionId`) REFERENCES `conference_edition` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

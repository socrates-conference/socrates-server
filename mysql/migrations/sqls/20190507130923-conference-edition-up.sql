CREATE TABLE `conference`
(
    `id`       int(11)      NOT NULL AUTO_INCREMENT,
    `name`     varchar(50)  NOT NULL,
    `longName` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `conference_edition`
(
    `id`                int(11)       NOT NULL AUTO_INCREMENT,
    `conferenceId`      int(11)       NOT NULL,
    `year`              int(11)       NOT NULL,
    `startDate`         datetime      NOT NULL,
    `endDate`           datetime      NOT NULL,
    `startApplications` datetime      NOT NULL,
    `endApplications`   datetime      NOT NULL,
    `lotteryDay`        datetime      NOT NULL,
    `lunchPrice`        decimal(5, 2) NOT NULL DEFAULT '0.00',
    `dinnerPrice`       decimal(5, 2) NOT NULL DEFAULT '0.00',
    PRIMARY KEY (`id`),
    KEY `FK_edition_conference_idx` (`conferenceId`),
    CONSTRAINT `FK_edition_conference` FOREIGN KEY (`conferenceId`) REFERENCES `conference` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

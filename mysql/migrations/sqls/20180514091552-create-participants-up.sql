CREATE TABLE `participants`
(
    `id`           int(11)      NOT NULL AUTO_INCREMENT,
    `person_id`    int(11)      NOT NULL,
    `firstname`    varchar(255) NOT NULL,
    `lastname`     varchar(255) NOT NULL,
    `company`      varchar(255),
    `address1`     varchar(255) NOT NULL,
    `address2`     varchar(255),
    `province`     varchar(255),
    `postal`       varchar(20)  NOT NULL,
    `city`         varchar(255) NOT NULL,
    `country`      varchar(255) NOT NULL,
    `arrival`      varchar(255) NOT NULL,
    `departure`    varchar(255) NOT NULL,
    `dietary`      tinyint(1)   NOT NULL DEFAULT '0',
    `dietary_info` varchar(255),
    `family`       tinyint(1)   NOT NULL DEFAULT '0',
    `family_info`  varchar(1023),
    `gender`       varchar(255),
    `tshirt`       varchar(31)  NOT NULL,
    `labelname`    varchar(63)  NOT NULL,
    `social`       varchar(63)  NOT NULL DEFAULT '',
    `pronoun`      varchar(31)  NOT NULL DEFAULT '',
    PRIMARY KEY (`id`),
    KEY `FK_participant_person_idx` (`person_id`),
    CONSTRAINT `FK_participant_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

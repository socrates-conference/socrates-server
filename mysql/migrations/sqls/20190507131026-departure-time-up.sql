CREATE TABLE `departure_time`
(
    `id`                       int(11)      NOT NULL AUTO_INCREMENT,
    `legacyId`                 varchar(5)   NOT NULL,
    `conferenceEditionId`      int(11)      NOT NULL,
    `conferenceDayId`          int(11)      NOT NULL,
    `name`                     varchar(45)  NOT NULL,
    `hint`                     varchar(255) NOT NULL DEFAULT '',
    `hasToPayConferenceDayFee` tinyint(1)   NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`),
    KEY `FK_departure_conference_day_idx` (`conferenceDayId`),
    KEY `FK_departure_conference_edition_idx` (`conferenceEditionId`),
    CONSTRAINT `FK_departure_conference_day` FOREIGN KEY (`conferenceDayId`) REFERENCES `conference_day` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_departure_conference_edition` FOREIGN KEY (`conferenceEditionId`) REFERENCES `conference_edition` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

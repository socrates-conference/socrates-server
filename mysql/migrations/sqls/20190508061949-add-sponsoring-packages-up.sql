INSERT INTO `socrates_db`.`sponsoring_package` (`id`, `conferenceEditionId`, `name`, `includesLogoOnHomepage`,
                                                `includesLogoForMarketing`, `includesMentioning`, `includesSwag`,
                                                `includesSocialMedia`, `includesNamedRoom`, `numberOfSlots`, `price`)
VALUES ('1', '1', 'One', '1', '1', '1', '0', '0', '0', '1', '800.00');
INSERT INTO `socrates_db`.`sponsoring_package` (`id`, `conferenceEditionId`, `name`, `includesLogoOnHomepage`,
                                                `includesLogoForMarketing`, `includesMentioning`, `includesSwag`,
                                                `includesSocialMedia`, `includesNamedRoom`, `numberOfSlots`, `price`)
VALUES ('2', '1', 'Two', '1', '1', '1', '0', '1', '0', '2', '1600.00');
INSERT INTO `socrates_db`.`sponsoring_package` (`id`, `conferenceEditionId`, `name`, `includesLogoOnHomepage`,
                                                `includesLogoForMarketing`, `includesMentioning`, `includesSwag`,
                                                `includesSocialMedia`, `includesNamedRoom`, `numberOfSlots`, `price`)
VALUES ('3', '1', 'Three', '1', '1', '1', '0', '1', '0', '3', '2400.00');
INSERT INTO `socrates_db`.`sponsoring_package` (`id`, `conferenceEditionId`, `name`, `includesLogoOnHomepage`,
                                                `includesLogoForMarketing`, `includesMentioning`, `includesSwag`,
                                                `includesSocialMedia`, `includesNamedRoom`, `numberOfSlots`, `price`)
VALUES ('4', '1', 'Four', '1', '1', '1', '0', '1', '0', '4', '3200.00');
INSERT INTO `socrates_db`.`sponsoring_package` (`id`, `conferenceEditionId`, `name`, `includesLogoOnHomepage`,
                                                `includesLogoForMarketing`, `includesMentioning`, `includesSwag`,
                                                `includesSocialMedia`, `includesNamedRoom`, `numberOfSlots`, `price`)
VALUES ('5', '1', 'Five', '1', '1', '1', '0', '1', '0', '5', '4000.00');
INSERT INTO `socrates_db`.`sponsoring_package` (`id`, `conferenceEditionId`, `name`, `price`, `numberOfSlots`,
                                                `includesLogoOnHomepage`, `includesLogoForMarketing`,
                                                `includesMentioning`, `includesSocialMedia`, `includesSwag`,
                                                `includesNamedRoom`)
VALUES ('6', '1', 'MozaicWorks', '400.00', '1', '1', '1', '1', '1', '0', '0');


ALTER TABLE `socrates_db`.`registration`
    DROP COLUMN `confirmation_last_reminder`,
    DROP COLUMN `confirmation_reminder_counter`;

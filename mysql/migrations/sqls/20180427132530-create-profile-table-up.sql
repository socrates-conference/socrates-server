CREATE TABLE `profile`
(
    `id`                     int(11)     NOT NULL AUTO_INCREMENT,
    `person_id`              int(11)     NOT NULL,
    `first_name`             varchar(45) NOT NULL,
    `last_name`              varchar(45) NOT NULL,
    `dietary_restrictions`   varchar(255) DEFAULT NULL,
    `needs_assistance`       tinyint(1)   DEFAULT NULL,
    `can_provide_assistance` tinyint(1)   DEFAULT NULL,
    `t_shirt_size`           varchar(20)  DEFAULT NULL,
    `remarks`                varchar(255) DEFAULT NULL,
    `pronoun`                varchar(45)  DEFAULT NULL,
    `home_address`           int(11)      DEFAULT NULL,
    `billing_address`        int(11)      DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_profile_person_idx` (`person_id`),
    CONSTRAINT `FK_profile_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


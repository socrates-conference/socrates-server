CREATE TABLE `sponsoring_package`
(
    `id`                       int(11)        NOT NULL AUTO_INCREMENT,
    `conferenceEditionId`      int(11)        NOT NULL,
    `name`                     varchar(45)    NOT NULL,
    `price`                    decimal(10, 2) NOT NULL,
    `numberOfSlots`            int(11)        NOT NULL,
    `includesLogoOnHomepage`   tinyint(1)     NOT NULL DEFAULT '1',
    `includesLogoForMarketing` tinyint(1)     NOT NULL DEFAULT '1',
    `includesMentioning`       tinyint(1)     NOT NULL DEFAULT '1',
    `includesSocialMedia`      tinyint(1)     NOT NULL DEFAULT '1',
    `includesSwag`             tinyint(1)     NOT NULL DEFAULT '0',
    `includesNamedRoom`        tinyint(1)     NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `FK_package_conference_idx` (`conferenceEditionId`),
    CONSTRAINT `FK_package_conference` FOREIGN KEY (`conferenceEditionId`) REFERENCES `conference_edition` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

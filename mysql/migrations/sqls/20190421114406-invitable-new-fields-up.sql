ALTER TABLE `socrates_db`.`invitables`
    ADD COLUMN `room_type` VARCHAR(30) NOT NULL AFTER `gender`,
    ADD COLUMN `introduction` VARCHAR(2000) NULL DEFAULT '' AFTER `room_type`,
    DROP COLUMN `age`;

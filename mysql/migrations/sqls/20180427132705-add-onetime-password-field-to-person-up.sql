ALTER TABLE `person`
    ADD COLUMN `is_onetime_pass` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_administrator`;

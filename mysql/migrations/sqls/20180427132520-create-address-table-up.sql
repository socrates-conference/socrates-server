CREATE TABLE `address`
(
    `id`                int(11)      NOT NULL AUTO_INCREMENT,
    `person_id`         int(11)      NOT NULL,
    `street`            varchar(100) NOT NULL COMMENT 'street address, c/o',
    `street_additional` varchar(100) DEFAULT NULL COMMENT 'Apartment, suite, unit, building, flor, etc...',
    `post_code`         varchar(30)  NOT NULL COMMENT 'Postal code or zip',
    `city`              varchar(30)  NOT NULL,
    `country`           varchar(30)  NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_address_person_idx` (`person_id`),
    CONSTRAINT `FK_address_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE `registration`
(
    `id`                    int(11)      NOT NULL AUTO_INCREMENT,
    `person_id`             int(11)      NOT NULL,
    `room_type`             varchar(30)  NOT NULL,
    `diversity_reason`      varchar(255) NOT NULL,
    `date`                  datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `stay_length`           varchar(20)  NOT NULL,
    `confirmed`             tinyint(1)   NOT NULL DEFAULT '0',
    `confirmation_date`     datetime              DEFAULT NULL,
    `confirmation_deadline` datetime     NOT NULL,
    `confirmation_key`      varchar(36)  NOT NULL COMMENT 'uuid or something like that.',
    PRIMARY KEY (`id`),
    KEY `FK_registration_person_idx` (`person_id`),
    CONSTRAINT `FK_attendant_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `stay_length`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `arrivalId`   int(11) NOT NULL,
    `departureId` int(11) NOT NULL,
    `nights`      int(11) NOT NULL,
    `lunches`     int(11) NOT NULL,
    `dinners`     int(11) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_stay_arrival_idx` (`arrivalId`),
    KEY `FK_stay_departure_idx` (`departureId`),
    CONSTRAINT `FK_stay_arrival` FOREIGN KEY (`arrivalId`) REFERENCES `arrival_time` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_stay_departure` FOREIGN KEY (`departureId`) REFERENCES `departure_time` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

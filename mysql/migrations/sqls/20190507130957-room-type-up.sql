CREATE TABLE `room_type`
(
    `id`                   varchar(45)   NOT NULL,
    `conferenceEditionId`  int(11)       NOT NULL,
    `name`                 varchar(45)   NOT NULL,
    `nightPricePerPerson`  decimal(5, 2) NOT NULL,
    `numberOfRooms`        int(11)       NOT NULL,
    `peoplePerRoom`        int(11)       NOT NULL,
    `reservedOrganization` int(11)       NOT NULL,
    `reservedSponsors`     int(11)       NOT NULL,
    `lotterySortOrder`     int(11)       NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_room_type_conference_idx` (`conferenceEditionId`),
    CONSTRAINT `FK_room_type_conference` FOREIGN KEY (`conferenceEditionId`) REFERENCES `conference_edition` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

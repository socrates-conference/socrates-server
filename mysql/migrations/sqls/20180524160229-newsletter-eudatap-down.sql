ALTER TABLE `socrates_db`.`person`
    DROP COLUMN `newsletter_eudatap_consent_date`,
    DROP COLUMN `newsletter_eudatap_consent_key`,
    DROP COLUMN `is_newsletter_consent_pending`,
    DROP COLUMN `newsletter_request_date`;

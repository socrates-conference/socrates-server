ALTER TABLE `socrates_db`.`person`
ADD COLUMN `is_hotel` TINYINT(1) NOT NULL DEFAULT '0' AFTER `is_administrator`;

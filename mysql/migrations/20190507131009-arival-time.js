/* eslint-disable no-unused-vars */
'use strict';

let dbm;
let type;
let seed;
const fs = require('fs');
const path = require('path');
let Promise;

exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
  Promise = options.Promise;
};

exports.up = function(db) {
  const filePath = path.join(__dirname, 'sqls', '20190507131009-arival-time-up.sql');
  return new Promise( function( resolve, reject ) {
    fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
      if (err) {return reject(err);}
      console.log('received data: ' + data);

      resolve(data);
    });
  })
    .then(function(data) {
      return db.runSql(data);
    });
};

exports.down = function(db) {
  const filePath = path.join(__dirname, 'sqls', '20190507131009-arival-time-down.sql');
  return new Promise( function( resolve, reject ) {
    fs.readFile(filePath, {encoding: 'utf-8'}, function(err, data){
      if (err) {return reject(err);}
      console.log('received data: ' + data);

      resolve(data);
    });
  })
    .then(function(data) {
      return db.runSql(data);
    });
};

exports._meta = {
  'version': 1
};

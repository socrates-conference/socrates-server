// @flow
import Aggregate from './aggregate';
import {EventDispatcher} from './eventDispatcher';
import NewsletterRepository from '../db/newsletterRepository';
import {validateEmail} from '../utilities/Utilities';
import uuidv1 from 'uuid/v1';

export type NewsletterSubscriber = {
  id: number,
  name: string,
  email: string
}

export const INTERESTED_PERSON_CREATED = 'INTERESTED_PERSON_CREATED';
export const INTERESTED_PERSON_CONFIRMED = 'INTERESTED_PERSON_CONFIRMED';
export const INTERESTED_PERSON_REMOVED = 'INTERESTED_PERSON_REMOVED';

export default class Newsletter extends Aggregate {
  _repository: NewsletterRepository;

  constructor(repository: NewsletterRepository, dispatcher: EventDispatcher) {
    super(dispatcher);
    this._repository = repository;
  }

  async signup(name: string, email: string): Promise<NewsletterSubscriber> {
    validatePerson(name, email);
    const consentKey = uuidv1();
    const person = await this._repository.signup(name, email, consentKey);
    if (person) {
      this.dispatch({type: INTERESTED_PERSON_CREATED, payload: {person, consentKey}});
      return person;
    } else {
      throw new Error('Registration failed. Interested person could not be added.');
    }
  }

  async confirm(consentKey: string): Promise<boolean> {
    if(!consentKey){
      throw new Error('The information provided is not valid. The consent key is missing.');
    }
    const subscriber = await this._repository.readPersonByConsentKey(consentKey);
    if (subscriber) {
      const success = await this._repository.confirm(consentKey);
      if (success) {
        this.dispatch({type: INTERESTED_PERSON_CONFIRMED, payload: subscriber});
      }
      return success;
    } else {
      throw new Error('Confirmation failed. Interested person could not be found.');
    }
  }

  async unsubscribe(email: string): Promise<string> {
    const result = await this._repository.unsubscribe(email);
    if (result) {
      this.dispatch({type: INTERESTED_PERSON_REMOVED, payload: email});
      return result;
    } else {
      throw new Error('Unsubscribe failed. Newsletter subscription could not be deleted, please try again later.');
    }
  }
}

function validatePerson(name: string, email: string) {
  if (!name) {
    throw new Error('The information provided is not valid: The name is missing.');
  } else if (!email) {
    throw new Error('The information provided is not valid: The email address is missing.');
  } else if (!validateEmail(email)) {
    throw new Error('The information provided is not valid: The email address is malformed.');
  }
}
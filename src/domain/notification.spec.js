// @flow
import Email from '../adapter/email';
import DomainEventDispatcher from './domainEventDispatcher';
import Notification from './notification';
import {INTERESTED_PERSON_CREATED} from './newsletter';
import {APPLICATION_ADDED} from './application';
import {PARTICIPANT_CONFIRMED} from './participant';

jest.mock('../adapter/email');

describe('Notification:', () => {
// eslint-disable-next-line no-unused-vars
  let mail, notification, dispatcher;

  const mockSend = jest.fn((title, body, ...email) => {
    expect(email[0]).toEqual('test@domain.com');
    expect(title).toBeDefined();
    expect(body).toBeDefined();
    return Promise.resolve('ok');
  });

  const mockFail = jest.fn((title, body, ...email) => {
    return Promise.reject({error: new Error(), mailOptions: {title, body, email}});
  });

  describe('and email can be sent', () => {
    beforeEach(() => {
      mail = new Email();
      dispatcher = new DomainEventDispatcher();
      notification = new Notification(mail, dispatcher);
      mail.send.mockImplementation(mockSend);
    });

    afterEach(() => {
      mockSend.mockClear();
      mail.send.mockReset();
    });

    it('when an interested person was created should send an email to that person to confirm the subscription', () => {
      dispatcher.dispatchEvent(
        {
          type: INTERESTED_PERSON_CREATED,
          payload: {person: {name: 'test', email: 'test@domain.com'}, consentKey: 'someKey'}
        });
      expect(mockSend.mock.calls.length).toEqual(1);
      expect(mockSend.mock.calls[0][1]).toContain('SoCraTes Organization Team');
    });
    it('when an application was added should send an email to the applicant', () => {
      dispatcher.dispatchEvent(
        {type: APPLICATION_ADDED, payload: {name: 'test', email: 'test@domain.com'}});
      expect(mockSend.mock.calls.length).toEqual(1);
      expect(mockSend.mock.calls[0][1]).toContain('SoCraTes Organization Team');
    });
    it('when a participant confirms the participation', () => {
      dispatcher.dispatchEvent(
        {type: PARTICIPANT_CONFIRMED, payload: {name: 'test', email: 'test@domain.com'}});
      expect(mockSend.mock.calls.length).toEqual(1);
      expect(mockSend.mock.calls[0][1]).toContain('SoCraTes Organization Team');
    });
  });
  describe('and email cannot be sent', () => {
    beforeEach(() => {
      mail = new Email();
      dispatcher = new DomainEventDispatcher();
      notification = new Notification(mail, dispatcher);
      mail.send.mockReset();
      mail.send.mockImplementation(mockFail);
    });

    afterEach(() => {
      mockFail.mockClear();
      mail.send.mockReset();
    });

    describe('when an application was added', () => {
      it('should throw an error', () => {
        dispatcher.dispatchEvent(
          {type: APPLICATION_ADDED, payload: {name: 'test', email: 'test@domain.com'}});
        expect(mockFail.mock.calls.length).toEqual(1);
      });
    });
    describe('when an interested person was created', () => {
      it('should throw an error', () => {
        dispatcher.dispatchEvent(
          {
            type: INTERESTED_PERSON_CREATED,
            payload: {person: {name: 'test', email: 'test@domain.com'}, consentKey: 'someKey'}
          });
        expect(mockFail.mock.calls.length).toEqual(1);
      });
    });
  });
});

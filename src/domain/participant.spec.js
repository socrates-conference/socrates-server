// @flow

import MySql from '../db/MySql';
import SqlTestHelper from '../db/SqlTestHelper';
import Participants, {
  PARTICIPANT_CANCELED,
  PARTICIPANT_CONFIRMED,
  PARTICIPANT_UPDATED
} from './participant';
import DomainEventDispatcher from './domainEventDispatcher';
import ParticipantRepository from '../db/participation/participantRepository';
import type {Participant, ParticipantArrival, ParticipantDeparture} from './participant';
import {conferenceDaysStubbed} from '../db/conference/conferenceTestStubs';

jest.mock('../db/MySql');

const testHelper = new SqlTestHelper();
const mysql = new MySql(testHelper.testConfig);
const dispatcher = new DomainEventDispatcher();
const participant: Participant = {
  personId: 1,
  nickname: 'tester',
  email: 'test@test.de',
  firstname: 'tester',
  lastname: 'test',
  company: 'testco',
  address1: 'test 123',
  address2: 'apt. test',
  province: 'test',
  postal: '12345',
  city: 'testcity',
  country: 'testcountry',
  gender: 'female',
  arrival: 'Thursday afternoon',
  departure: 'Sunday evening',
  dietary: false,
  family: false,
  tshirt: 'Unisex S',
  labelname: 'tester',
  social: '@tester',
  pronoun: 'she',
  id: 0
};
const participantFromDb: any = {
  // eslint-disable-next-line camelcase
  person_id: 1,
  ...participant
};

const participantArrivals: ParticipantArrival[] = [
  {arrival: '0', arrivalText: 'arrival One', count: 20},
  {arrival: '1', arrivalText: 'arrival Two', count: 10}
];
const participantDepartures: ParticipantDeparture[] = [
  {departure: '0', departureText: 'arrival One', count: 13},
  {departure: '1', departureText: 'arrival Two', count: 17},
  {departure: '1', departureText: 'arrival Two', count: 55}
];


describe('Participants:', () => {
  let participants;
  let repository;

  beforeEach(() => {
    mysql.insert.mockImplementation(() => {
      return Promise.resolve({numberOfRowsAffected: 0, lastInsertedId: 1});
    });

    mysql.delete.mockImplementation(() => {
      return Promise.resolve({numberOfRowsAffected: 1});
    });

    repository = new ParticipantRepository(mysql);
    participants = new Participants(repository, dispatcher);
  });

  describe('confirming a participant should', () => {
    let event;

    beforeEach(async () => {
      mysql.selectWithParams.mockImplementation(
        () => Promise.resolve([{id: 1, email: 'test@test.de', nickname: 'tester'}]));
      mysql.update.mockImplementation(
        () => Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0}));
      dispatcher.subscribeOnce(PARTICIPANT_CONFIRMED, ev => {event = ev;});
      await participants.confirm({...participant, confirmed: true});
    });

    afterEach(() => {event = undefined;});

    it('add participant in the db', () => {
      expect(mysql.insert).toHaveBeenCalledTimes(1);
    });

    it('update the registration participant in the db', () => {
      expect(mysql.update).toHaveBeenCalledTimes(1);
    });

    it(`dispatch ${PARTICIPANT_CONFIRMED} event`, () => {
      if (!event) {
        throw new Error('Event should have been dispatched');
      }
      expect(event.type).toEqual(PARTICIPANT_CONFIRMED);
      expectMatchesPartial(event.payload, {...participant, id: 1});
    });
  });

  describe('canceling a participant should', () => {
    let event;

    beforeEach(() => {
      dispatcher.subscribeOnce(PARTICIPANT_CANCELED, ev => {event = ev;});
    });

    afterEach(() => {event = undefined;});

    describe('when participant exists', () => {
      beforeEach(async () => {
        mysql.selectWithParams.mockImplementationOnce(() => Promise.resolve([participantFromDb]));
        mysql.selectWithParams.mockImplementationOnce(() =>
          Promise.resolve([{id: 1, nickname: 'Test', email: 'test@test.de'}]));
        await participants.cancel(1);
      });

      it('read existing participant from the db', () => {
        expect(mysql.selectWithParams).toHaveBeenCalledTimes(2);
      });

      it('remove participant from the db', () => {
        expect(mysql.delete).toHaveBeenCalledTimes(1);
      });

      it(`dispatch ${PARTICIPANT_CANCELED} event`, () => {
        if (!event) {
          throw new Error('Event should have been dispatched');
        }
        expect(event.type).toEqual(PARTICIPANT_CANCELED);
        expect(event.payload).toEqual({id: 1, email: 'test@test.de', nickname: 'Test'});
      });
    });

    describe('when participant does not exist', () => {
      let success;

      beforeEach(async () => {
        mysql.selectWithParams.mockImplementation(() => {});
        success = await participants.cancel(1);
      });

      it('unsuccessfully read participant from the db', () => {
        expect(mysql.selectWithParams).toHaveBeenCalledTimes(1);
      });

      it('return true', () => {
        expect(success).toBe(true);
      });

      it(`not dispatch ${PARTICIPANT_CANCELED} event`, () => {
        expect(event).toBeUndefined();
      });
    });

  });

  describe('updating a participant should', () => {
    let event;

    beforeEach(() => {
      dispatcher.subscribeOnce(PARTICIPANT_UPDATED, ev => {event = ev;});
    });

    afterEach(() => {event = undefined;});

    describe('when participant exists', () => {
      beforeEach(async () => {
        mysql.selectWithParams.mockImplementation(() => Promise.resolve([participantFromDb]));
        mysql.update.mockImplementation(() => Promise.resolve({numberOfRowsAffected: 1}));
        await participants.update({...participant, lastname: 'newTest'});
      });

      it('read existing participant from the db', () => {
        expect(mysql.selectWithParams).toHaveBeenCalledTimes(1);
      });

      it('write updated participant into the db', () => {
        expect(mysql.update).toHaveBeenCalledTimes(1);
        expect(mysql.update.mock.calls[0][1]).toEqual([
          1, 'tester', 'newTest', 'testco', 'test 123', 'apt. test', 'test', '12345', 'testcity', 'testcountry',
          'Thursday afternoon', 'Sunday evening', false, undefined, false, undefined, 'female', 'Unisex S', 'tester',
          '@tester', 'she', 0]);
      });

      it(`dispatch ${PARTICIPANT_UPDATED} event`, () => {
        if (!event) {
          throw new Error('Event should have been dispatched');
        }
        expect(event.type).toEqual(PARTICIPANT_UPDATED);
        expect(event.payload).toEqual({...participant, lastname: 'newTest'});
      });
    });

    describe('when participant does not exist', () => {
      beforeEach(async () => {
        mysql.selectWithParams.mockImplementation(() => {});
      });

      it('unsuccessfully read participant from the db', async () => {
        try {
          await participants.update(participant);
        } catch (_) {
        }
        expect(mysql.selectWithParams).toHaveBeenCalledTimes(1);
      });

      it('throw an error', async () => {
        try {
          await participants.update(participant);
        } catch (e) {
          expect(e).toBeDefined();
        }
      });

      it(`not dispatch ${PARTICIPANT_UPDATED} event`, () => {
        expect(event).toBeUndefined();
      });
    });

  });

  describe('can read participant arrivals and departures', () => {
    beforeEach(() => {
      mysql.select.mockImplementationOnce(
        () => Promise.resolve(participantArrivals));
      mysql.select.mockImplementationOnce(
        () => Promise.resolve(participantDepartures));
    });
    afterEach(() => {
      mysql.select.mockReset();
    });
    it('receiving an object containing both', async () => {
      const result = await participants.getArrivalsAndDepartures();
      expect(result).toEqual({arrivals: participantArrivals, departures: participantDepartures});
    });
  });

  describe('can read total amount of participants', () => {
    beforeEach(() => {
      mysql.select.mockImplementationOnce(
        () => Promise.resolve([{amount: 222}]));
    });
    afterEach(() => {
      mysql.select.mockReset();
    });
    it('receiving an object containing both', async () => {
      const result = await participants.getTotalAmountOfParticipants();
      expect(result).toBe(222);
    });
  });

  describe('calculates amount of daily fees per conference day', () => {
    const participantsDepartures = [
      {amount: 5, departureDayNumber: 2, hasToPayDepartureDayConferenceFee: true},
      {amount: 2, departureDayNumber: 2, hasToPayDepartureDayConferenceFee: false},
      {amount: 10, departureDayNumber: 3, hasToPayDepartureDayConferenceFee: true},
      {amount: 5, departureDayNumber: 3, hasToPayDepartureDayConferenceFee: false},
      {amount: 30, departureDayNumber: 4, hasToPayDepartureDayConferenceFee: true},
      {amount: 15, departureDayNumber: 4, hasToPayDepartureDayConferenceFee: false}
    ];
    it('on empty conference days, gets empty result', async () => {

      const result = await participants.calculateDailyConferenceFees([], participantsDepartures);
      expect(result).toEqual([]);
    });
    it('on empty participant departures, gets array containing days with zero daily fee amount for each day',
      async () => {
        const result = await participants.calculateDailyConferenceFees(conferenceDaysStubbed, []);
        expect(result.length).toBe(conferenceDaysStubbed.length);
        result.forEach(item => expect(item.amount).toBe(0));
      });
    it('receiving non empty parameters, gets calculated result', async () => {
      const result = await participants.calculateDailyConferenceFees(conferenceDaysStubbed, participantsDepartures);
      expect(result).toHaveLength(4);
      expect(result[0]).toEqual({conferenceDayId: 21, conferenceDayNumber: 1, conferenceDayName: 'Thursday',
        amount: 67});
      expect(result[1]).toEqual({conferenceDayId: 22, conferenceDayNumber: 2, conferenceDayName: 'Friday',
        amount: 65});
      expect(result[2]).toEqual({conferenceDayId: 23, conferenceDayNumber: 3, conferenceDayName: 'Saturday',
        amount: 55});
      expect(result[3]).toEqual({conferenceDayId: 24, conferenceDayNumber: 4, conferenceDayName: 'Sunday',
        amount: 30});
    });
  });

  it('should list all participants', async () => {
    mysql.select.mockImplementation(() => Promise.resolve([participantFromDb]));
    expectContainsPartial(await participants.list(), participant);
    expect(mysql.select).toHaveBeenCalledTimes(1);
  });

  describe('can read participants by room type', () => {
    let promise;
    beforeEach(() => {
      mysql.selectWithParams.mockImplementationOnce(() => promise);
    });
    afterEach(() => {
      mysql.selectWithParams.mockReset();
    });
    it('and return them', async () => {
      promise = Promise.resolve([participantFromDb]);
      const result = await participants.readByRoomType('single');
      expect(result).toHaveLength(1);
      expect(mysql.selectWithParams).toHaveBeenCalledTimes(1);
    });
    it('and return an empty array if no participants available', async () => {
      promise = Promise.resolve([]);
      const result = await participants.readByRoomType('single');
      expect(result).toHaveLength(0);
      expect(mysql.selectWithParams).toHaveBeenCalledTimes(1);
    });
    it('and return an empty array if fails', async () => {
      promise = Promise.reject();
      const result = await participants.readByRoomType('single');
      expect(result).toHaveLength(0);
      expect(mysql.selectWithParams).toHaveBeenCalledTimes(1);
    });
  });

  it('should read participants departures', async () => {
    const participantsDepartures = [
      {amount: 10, departureDayNumber: 1, hasToPayDepartureDayConferenceFee: true},
      {amount: 5, departureDayNumber: 1, hasToPayDepartureDayConferenceFee: false},
      {amount: 30, departureDayNumber: 2, hasToPayDepartureDayConferenceFee: true},
      {amount: 15, departureDayNumber: 2, hasToPayDepartureDayConferenceFee: false},
      {amount: 8, departureDayNumber: 3, hasToPayDepartureDayConferenceFee: false}
    ];
    mysql.selectWithParams.mockImplementation(() => Promise.resolve(participantsDepartures));

    const result = await participants.readParticipantsDepartures(1);
    expect(result).toEqual(participantsDepartures);
  });

  afterEach(() => {
    mysql.select.mockReset();
    mysql.selectWithParams.mockReset();
    mysql.insert.mockReset();
    mysql.delete.mockReset();
    mysql.update.mockReset();
  });
});

const expectMatchesPartial = (obj: {}, partial: {}) => {
  const result = Object.entries(partial).reduce((acc, [key, value]) => acc && obj[key] === value, true);
  expect(result).toBe(true);
};

const expectContainsPartial = (list: any[], partial: {}) => {
  const result = list.some(item => Object.entries(partial).map(([key, value]) => item[key] === value));
  expect(result).toBe(true);
};

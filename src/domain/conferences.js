// @flow

import moment from 'moment';
import Aggregate from './aggregate';
import type {EventDispatcher} from './eventDispatcher';
import ConferenceEditionRepository from '../db/conference/conferenceEditionRepository';
import type {
  ArrivalTime,
  ConferenceDay,
  ConferenceEdition,
  Conference, ConferenceEditionSponsor,
  DepartureTime,
  LengthOfStay,
  RoomOption,
  StayPrice, RoomType
} from './conferenceTypes';

export const conferenceEditionIdForLegacyCalls = 1;

export default class Conferences extends Aggregate {
  _repository: ConferenceEditionRepository;

  constructor(repository: ConferenceEditionRepository, dispatcher: EventDispatcher) {
    super(dispatcher);
    this._repository = repository;
  }

  async readConferenceData(id: number): Promise<?Conference> {
    const conferenceEdition = await this._repository.readConferenceEdition(id);
    if (conferenceEdition) {
      const conferenceData = this._toConferenceDataType(conferenceEdition);
      conferenceData.days = await this._repository.readConferenceDays(conferenceData.editionId);
      conferenceData.roomTypes = await this._repository.readConferenceRoomTypes(conferenceData.editionId);
      conferenceData.arrivals = await this._repository.readConferenceArrivalTimes(conferenceData.editionId);
      conferenceData.departures = await this._repository.readConferenceDepartureTimes(conferenceData.editionId);
      conferenceData.lengthOfStay = await this._repository.readConferenceLengthsOfStay(conferenceData.editionId);
      return conferenceData;
    }
    return null;
  }

  async getRoomOptions(conferenceEditionId: number): Promise<RoomOption[]> {
    const conference: ?Conference = await this.readConferenceData(conferenceEditionId);
    if (conference) {
      return conference.roomTypes.map(roomType => ({
        value: roomType.id,
        label: roomType.display,
        roomPricePerNight: roomType.nightPricePerPerson,
        prices: this._calculatePrices(conference, roomType.nightPricePerPerson)
      }));
    }
    return [];
  }

  async getTotalSponsoring(conferenceEditionId: number): Promise<number> {
    return this._repository.getTotalSponsoring(conferenceEditionId);
  }

  async readArrivalsAnDeparturesHints
  (conferenceEditionId: number): Promise<{ arrivals: ArrivalTime[], departures: DepartureTime[] }> {
    const conference: ?Conference = await this.readConferenceData(conferenceEditionId);
    if (conference) {
      return {arrivals: conference.arrivals, departures: conference.departures};
    }
    return {arrivals: [], departures: []};
  }

  async readRoomTypes(conferenceEditionId: number): Promise<RoomType[]> {
    const roomTypes: RoomType[] = await this._repository.readConferenceRoomTypes(conferenceEditionId);
    return roomTypes ? roomTypes : [];
  }

  async readSponsorsForHotel(conferenceEditionId: number): Promise<ConferenceEditionSponsor[]> {
    return this._repository.readSponsorsForHotel(conferenceEditionId);
  }

  async readConferenceDays(conferenceEditionId: number): Promise<ConferenceDay[]> {
    return this._repository.readConferenceDays(conferenceEditionId);
  }

  _calculatePrices = (conferenceData: Conference, roomPricePerNight: number): StayPrice[] => {
    const calculateFee = (length: LengthOfStay): number => {
      let fee = (conferenceData.lunchPrice * length.lunches);
      fee += (conferenceData.dinnerPrice * length.dinners);
      const arrivalDay: ?ConferenceDay =
        conferenceData.days.find(x => x.id === length.arrivalDayId);

      const departureDay: ?ConferenceDay =
        conferenceData.days.find(x => x.id === length.departureDayId);

      const departureTime: ?DepartureTime =
        conferenceData.departures.find(x => x.id === length.departureId);

      if (arrivalDay && departureDay && departureTime) {
        const arrivalDayNumber = arrivalDay.conferenceDayNumber;
        const departureDayNumber = departureDay.conferenceDayNumber;
        for (let number = arrivalDayNumber; number < departureDayNumber; number++) {
          const currentDay: ?ConferenceDay =
            conferenceData.days.find(x => x.conferenceDayNumber === number);
          if (currentDay) {
            fee += currentDay.fee;
          }
        }
        if (departureTime.hasToPayConferenceDayFee) {
          fee += departureDay.fee;
        }
      } else {
        throw new Error('Cannot calculate prices.');
      }
      return fee;
    };
    return conferenceData.lengthOfStay.map((length: LengthOfStay) => {
      const roomPrice = (roomPricePerNight * length.nights);
      const fees = calculateFee(length);
      const display = length.from + ' - ' + length.to;
      const departureTime: ?DepartureTime =
        conferenceData.departures.find(x => x.id === length.departureId);

      const arrivalTime: ?ArrivalTime =
        conferenceData.departures.find(x => x.id === length.arrivalId);

      return {
        roomPrice,
        fees,
        nights: length.nights,
        amount: Math.round(roomPrice + fees) + 5, //convert to whole number and add 5 euro as puffer
        display: display,
        arrivalId: length.arrivalId,
        departureId: length.departureId,
        legacyArrivalId: arrivalTime ? arrivalTime.legacyId : '0',
        legacyDepartureId: departureTime ? departureTime.legacyId : '3'
      };
    });
  };

  // noinspection JSMethodCanBeStatic
  _toConferenceDataType(conferenceEdition: ConferenceEdition): Conference {
    return {
      ...conferenceEdition,
      editionId: conferenceEdition.id,
      startDate: moment(conferenceEdition.startDate),
      endDate: moment(conferenceEdition.endDate),
      startApplications: moment(conferenceEdition.startApplications),
      endApplications: moment(conferenceEdition.startApplications),
      lotteryDay: moment(conferenceEdition.lotteryDay),
      days: [],
      roomTypes: [],
      lengthOfStay: [],
      arrivals: [],
      departures: []
    };
  }
}

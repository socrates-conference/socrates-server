// @flow

import Aggregate from './aggregate';
import {Event, EventDispatcher} from './eventDispatcher';
import ParticipantRepository from '../db/participation/participantRepository';
import type {PersonResult} from '../db/application/personMySQL';
import {REGISTRATION_DELETED} from '../domain/management/registrationList';
import type {ConferenceDay, RoomOption, StayPrice} from './conferenceTypes';

export type Participant = {
  address1: string,
  address2?: string,
  arrival: string,
  city: string,
  company?: string,
  confirmed?: boolean,
  country: string,
  departure: string,
  dietary: boolean,
  dietaryInfo?: string,
  email?: string,
  family: boolean,
  familyInfo?: string,
  firstname: string,
  gender?: string,
  id: ?number,
  labelname: string,
  lastname: string,
  nickname?: string,
  personId: number,
  postal: string,
  price?: ?PriceCalculation,
  pronoun: string,
  province: string,
  social: string,
  tshirt: string
};

export type RoomInformation = {
  personId: number,
  room: string,
  arrival: number,
  departure: number
}

export type PriceCalculation = {
  nights: number,
  room: string,
  roomPricePerNight: number,
  totalRoomPrice: number,
  fees: string[],
  sponsored?: number,
  totalFeesPrice: number,
  totalPrice: number
};

export type PriceCalculationData = {
  roomOptions: RoomOption[],
  totalSponsoring: number,
  roomType: string,
  pricePerNight: number,
  defaultPrice: StayPrice
};

export const PARTICIPANT_CONFIRMED = 'PARTICIPANT_CONFIRMED';
export const LOG_PARTICIPANT_CONFIRMED = 'LOG_PARTICIPANT_CONFIRMED';
export const PARTICIPANT_CANCELED = 'PARTICIPANT_CANCELED';
export const LOG_PARTICIPANT_CANCELED = 'LOG_PARTICIPANT_CANCELED';
export const PARTICIPANT_UPDATED = 'PARTICIPANT_UPDATED';
export const LOG_PARTICIPANT_UPDATED = 'LOG_PARTICIPANT_UPDATED';
export const PARTICIPANT_DELETED = 'PARTICIPANT_DELETED';
export const LOG_PARTICIPANT_DELETED = 'LOG_PARTICIPANT_DELETED';

export type ParticipantArrival = {
  arrival: string,
  arrivalText: string,
  count: number
}
export type ParticipantDeparture = {
  departure: string,
  departureText: string,
  count: number
}
export type AmountOfConferenceDailyFees = {
  conferenceDayId: number,
  conferenceDayNumber: number,
  conferenceDayName: string,
  amount: number
}
export type ParticipantArrivalsAndDepartures = {
  arrivals: ParticipantArrival[],
  departures: ParticipantDeparture[]
}
export type ParticipantsDeparture = {
  amount: number,
  departureDayNumber: number,
  hasToPayDepartureDayConferenceFee: boolean
}
export default class Participants extends Aggregate {
  _repository: ParticipantRepository;

  constructor(repository: ParticipantRepository, eventDispatcher: EventDispatcher) {
    super(eventDispatcher);
    eventDispatcher.subscribe(REGISTRATION_DELETED, this._handleRegistrationDeleted);
    this._repository = repository;
  }

  async confirm(participant: Participant): Promise<?Participant> {
    if (participant.email) {
      const person: ?PersonResult = await this._repository.readPersonByEmail(participant.email);
      if (person) {
        const confirmed: ?boolean = await this._repository.confirmRegistration(person.id);
        if (confirmed) {
          const completeParticipant = {...participant, personId: person.id, nickname: person.nickname};
          const newParticipant = await this._repository.create(completeParticipant);
          this._dispatcher.dispatchEvent({type: PARTICIPANT_CONFIRMED, payload: newParticipant});
          this._dispatcher.dispatchEvent({
            type: LOG_PARTICIPANT_CONFIRMED, payload:
              {
                oldValue: {},
                newValue: newParticipant
              }
          });
          return newParticipant;
        }
      }
    }
  }

  async cancel(id: number): Promise<boolean> {
    const storedParticipant = await this._repository.readParticipant(id);
    if (storedParticipant) {
      const person = await this._repository.readPersonById(storedParticipant.personId);
      if (person) {
        const success = await this._repository.deleteParticipant(id);
        if (success) {
          this._dispatcher.dispatchEvent({type: PARTICIPANT_CANCELED, payload: person});
          this._dispatcher.dispatchEvent({
            type: LOG_PARTICIPANT_CANCELED, payload:
              {
                oldValue: storedParticipant, newValue: {}
              }
          });
        }
        return success;
      }
    }
    return true;
  }

  async update(participant: Participant) {
    const storedParticipant = await this._repository._readParticipantByPersonId(participant.personId);
    const updatedParticipant = {...storedParticipant, ...participant};
    const newStoredParticipant = await this._repository.update(updatedParticipant);
    this._dispatcher.dispatchEvent({type: PARTICIPANT_UPDATED, payload: newStoredParticipant});
    this._dispatcher.dispatchEvent({
      type: LOG_PARTICIPANT_UPDATED, payload: {
        oldValue: storedParticipant,
        newValue: newStoredParticipant
      }
    });
    return newStoredParticipant;
  }

  async list(): Promise<Array<Participant>> {
    return await this._repository.readAll();
  }

  async readByRoomType(roomTypeId: string): Promise<Array<Participant>> {
    try {
      return await this._repository.readByRoomType(roomTypeId);
    } catch (e) {
      console.log(`Cannot read participants per room. Room type: ${roomTypeId}`, e);
      return [];
    }
  }

  async find(id: number): Promise<?Participant> {
    return await this._repository.readParticipant(id);
  }

  async findByEmail(email: string): Promise<?Participant> {
    return await this._repository.readParticipantByEmail(email);
  }

  async findPersonIdByEmail(email: string): Promise<?number> {
    const person: ?PersonResult = await this._repository.readPersonByEmail(email);
    if (person) {
      return person.id;
    }
  }

  async calculatePrice(params: PriceCalculationData): Promise<?PriceCalculation> {
    const {roomOptions, totalSponsoring, roomType, pricePerNight, defaultPrice} = params;
    const allRoomInformation = await this._repository.readAllRoomInformation();
    const totalRoomFees = allRoomInformation.reduce((total: number, roomInformation) => {
      const roomData: ?RoomOption = roomOptions.find((r: RoomOption) => r.value === roomInformation.room);
      if (roomData) {
        const price = roomData.prices.find((p: StayPrice) =>
          p.legacyArrivalId === roomInformation.arrival.toString()
          && p.legacyDepartureId === roomInformation.departure.toString());
        if (price) {
          return total + price.fees;
        }
      }
      return total;
    }, 0);
    const sponsoringFactor = totalSponsoring < totalRoomFees ? (totalSponsoring / totalRoomFees) : 1;
    if (defaultPrice) {
      return {
        room: roomType,
        nights: defaultPrice.nights,
        roomPricePerNight: pricePerNight,
        totalRoomPrice: defaultPrice.roomPrice,
        fees: [],
        sponsored: sponsoringFactor * defaultPrice.fees,
        totalFeesPrice: defaultPrice.fees,
        totalPrice: defaultPrice.roomPrice + defaultPrice.fees
      };
    }
    return null;
  }

  async readAllRoomInformation(): Promise<Array<RoomInformation>> {
    return await this._repository.readAllRoomInformation();

  }

  async getRegistrationEmailByConfirmationKey(key: string): Promise<string> {
    return await this._repository.readRegistrationEmailByConfirmationKey(key);
  }

  async getRoomTypeByPersonId(personId: number): Promise<string> {
    return await this._repository.getRoomTypeByPersonId(personId);
  }

  async deleteConfirmationKey(key: string): Promise<boolean> {
    return await this._repository.deleteConfirmationKey(key);
  }

  async readParticipantsDepartures(conferenceEditionId: number): Promise<ParticipantsDeparture[]> {
    return await this._repository.readParticipantsDepartures(conferenceEditionId);
  }

  async getArrivalsAndDepartures(): Promise<ParticipantArrivalsAndDepartures> {
    const arrivals = await this._repository.readArrivalsForConfirmedParticipants();
    const departures = await this._repository.readDeparturesForConfirmedParticipants();
    return {arrivals, departures};
  }

  async getTotalAmountOfParticipants(): Promise<number> {
    return this._repository.getTotalAmountOfParticipants();
  }

  calculateDailyConferenceFees
  (conferenceDays: ConferenceDay[], participantsDeparture: ParticipantsDeparture[]): AmountOfConferenceDailyFees[] {
    const result: Map<number, AmountOfConferenceDailyFees> = new Map();
    conferenceDays.forEach((day: ConferenceDay) => {
      result.set(day.conferenceDayNumber, {
        conferenceDayId: day.id,
        conferenceDayNumber: day.conferenceDayNumber,
        conferenceDayName: day.name,
        amount: 0
      });
    });
    participantsDeparture.forEach((departure: ParticipantsDeparture) => {
      conferenceDays.forEach((day: ConferenceDay) => {
        if (this._hasToPayFeeForDay(departure, day)) {
          const dailyFeesForDay: ?AmountOfConferenceDailyFees = result.get(day.conferenceDayNumber);
          if (dailyFeesForDay) {
            dailyFeesForDay.amount += departure.amount;
          }
        }
      });
    });
    return Array.from(result.values());
  }

  _hasToPayFeeForDay = (departure: ParticipantsDeparture, day: ConferenceDay) =>
    (departure.departureDayNumber > day.conferenceDayNumber) ||
    (departure.departureDayNumber === day.conferenceDayNumber && departure.hasToPayDepartureDayConferenceFee);

  _handleRegistrationDeleted = (ev: Event): void => {
    this._repository._readParticipantByPersonId(ev.payload.id).then(async storedParticipant => {
      if (storedParticipant && storedParticipant.id) {
        const deleted = await this._repository.deleteParticipant(storedParticipant.id);
        if (deleted) {
          this.dispatch({type: PARTICIPANT_DELETED, payload: storedParticipant});
          this.dispatch({type: LOG_PARTICIPANT_DELETED, payload: {oldValue: storedParticipant, newValue: {}}});
        }
      }
    });
  };
}

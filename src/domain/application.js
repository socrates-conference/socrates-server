// @flow

import Aggregate from './aggregate';
import {EventDispatcher} from './eventDispatcher';
import ApplicationRepository from '../db/application/applicationRepository';
import {validateEmail} from '../utilities/Utilities';

export type ApplicationData = {
  id?: ?number;
  personId: number,
  nickname: string,
  email: string,
  roomTypes: Array<string>,
  diversityReason: string,
}

export const APPLICATION_ADDED = 'APPLICATION_ADDED';

export default class Application extends Aggregate {
  _repository: ApplicationRepository;

  constructor(repository: ApplicationRepository, dispatcher: EventDispatcher) {
    super(dispatcher);
    this._repository = repository;
  }

  async add(application: ApplicationData): Promise<ApplicationData> {
    validateApplication(application);

    const previousApplication = await this._repository.existsPreviousApplication(application);
    const alreadyRegistered = await this._repository.existsRegistration(application);
    if (!previousApplication && !alreadyRegistered) {
      const addedApplication = await this._repository.create(application);
      this.dispatch({type: APPLICATION_ADDED, payload: addedApplication});
      return addedApplication;
    }

    throw new Error('ALREADY_APPLIED');
  }
}

function validateApplication(application: ApplicationData) {
  if (!application.nickname) {
    throw new Error('The information provided is not valid: The nickname is missing.');
  } else if (!application.email) {
    throw new Error('The information provided is not valid: The email address is missing.');
  } else if (!validateEmail(application.email)) {
    throw new Error('The information provided is not valid: The email address is malformed.');
  } else if (!application.roomTypes || application.roomTypes.length === 0) {
    throw new Error('The information provided is not valid: The room types are not defined.');
  } else if (!application.diversityReason || application.diversityReason.length === 0) {
    throw new Error('The information provided is not valid: The diversity reason is not valid.');
  }
}

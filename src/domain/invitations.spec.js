import DomainEventDispatcher from './domainEventDispatcher';
import Invitations, {differentRoomTypes, hasAlreadyRoommateError} from './invitations';
import InvitationsMySqlRepository from '../db/invitations/invitationsRepository';
import {PARTICIPANT_CANCELED} from './participant';

jest.mock('../db/invitations/invitationsRepository');

const testPersonTest = {
  id: 1,
  nickname: 'Test',
  email: 'Test@test.de'

};
const testPersonTestTwo = {
  id: 2,
  nickname: 'Test2',
  email: 'test2@test.de'

};
const participantInvitationRequest = {
  inviterId: 1,
  inviterEmail: 'Test@test.de',
  invitee1Id: 1,
  invitee1Email: 'Test@test.de',
  invitee2Id: 2,
  invitee2Email: 'test2@test.de'
};
const adminInvitationRequest = {
  inviterId: 4711,
  inviterEmail: 'admin@test.de',
  invitee1Id: 1,
  invitee1Email: 'Test@test.de',
  invitee2Id: 2,
  invitee2Email: 'test2@test.de'
};

const participantInvitationStub = {
  id: 4711,
  inviter: {id: 1, email: 'Test@test.de'},
  invitee1: {id: 1, email: 'Test@test.de'},
  invitee2: {id: 2, email: 'test2@test.de'},
  status: 'OPEN',
  key: 'key'
};

const adminInvitationStub = {
  id: 4711,
  inviter: {id: 88, email: 'admin@test.de'},
  invitee1: {id: 1, email: 'Test@test.de'},
  invitee2: {id: 2, email: 'test2@test.de'},
  status: 'OPEN',
  key: 'key'
};

const rawRoomSharingData = [
  {
    P1Id: 1,
    P1FirstName: 'P1FirstName',
    P1LastName: 'P1LastName',
    P1FamilyInfo: 'P1FamilyInfo',
    P2Id: 2,
    P2FirstName: 'P2FirstName',
    P2LastName: 'P2LastName',
    P2FamilyInfo: 'P2FamilyInfo',
    RoomType: 'roomType'
  },
  {
    P1Id: 3,
    P1FirstName: 'P3FirstName',
    P1LastName: 'P3LastName',
    P1FamilyInfo: 'P3FamilyInfo',
    P2Id: 4,
    P2FirstName: 'P4FirstName',
    P2LastName: 'P4LastName',
    P2FamilyInfo: 'P4FamilyInfo',
    RoomType: 'roomType'
  }
];

describe('Invitations:', () => {
  let domainEventDispatcher;
  let invitations, events, repository;
  const listener = (event) => {
    events.push(event);
  };

  beforeEach(() => {
    domainEventDispatcher = new DomainEventDispatcher();
    events = [];
    repository = new InvitationsMySqlRepository();
    invitations = new Invitations(repository, domainEventDispatcher);
  });

  afterEach(() => {
  });

  describe('when sending an invitation', () => {
    describe('where participants have different room types', () => {
      let readPersonByEmailPromise, hasRoommatePromise, haveParticipantsSameRoomTypePromise;
      beforeEach(() => {
        repository.readPersonByEmail.mockImplementation(() => readPersonByEmailPromise);
        repository.hasRoommate.mockImplementation(() => hasRoommatePromise);
        repository.haveParticipantsSameRoomType.mockImplementation(() => haveParticipantsSameRoomTypePromise);
      });
      afterEach(() => {
        repository.readPersonByEmail.mockReset();
        repository.hasRoommate.mockReset();
        repository.haveParticipantsSameRoomType.mockReset();
      });
      it('should contain 0 invitation', (done) => {
        readPersonByEmailPromise = Promise.resolve(testPersonTestTwo);
        hasRoommatePromise = Promise.resolve(false);
        haveParticipantsSameRoomTypePromise = Promise.resolve(false);
        const newInvitationRequest = {
          ...participantInvitationRequest,
          invitee2Id: 5,
          invitee2Email: 'test5@test.de'
        };
        invitations.send(newInvitationRequest)
          .then(() => done('error expected'))
          .catch(error => {
            expect(error.message).toEqual(differentRoomTypes);
            done();
          });
      });
    });

    describe('where the person to be invited has already a roommate', () => {
      let readPersonByEmailPromise, hasRoommatePromise;
      beforeEach(() => {
        repository.readPersonByEmail.mockImplementation(() => readPersonByEmailPromise);
        repository.hasRoommate.mockImplementation(() => hasRoommatePromise);
      });
      afterEach(() => {
        repository.readPersonByEmail.mockReset();
        repository.hasRoommate.mockReset();
      });
      it('should contain 0 invitation', (done) => {
        readPersonByEmailPromise = Promise.resolve(testPersonTestTwo);
        hasRoommatePromise = Promise.resolve(true);
        const newInvitationRequest = {
          ...participantInvitationRequest,
          invitee2Id: 6,
          invitee2Email: 'test6@test.de'
        };
        invitations.send(newInvitationRequest)
          .then(() => done('error expected'))
          .catch(error => {
            expect(error.message).toEqual(hasAlreadyRoommateError);
            done();
          });
      });
    });

    describe('from participant to participant', () => {
      let stored;
      beforeEach(async() => {
        repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTestTwo));
        repository.hasRoommate.mockImplementation(() => Promise.resolve(false));
        repository.haveParticipantsSameRoomType.mockImplementation(() => Promise.resolve(true));
        repository.create.mockImplementation(() => Promise.resolve(participantInvitationStub));
        domainEventDispatcher.subscribe('INVITATION_CREATED_BY_PARTICIPANT', listener);
        stored = await invitations.send(participantInvitationRequest);
      });
      afterEach(() => {
        repository.readPersonByEmail.mockReset();
        repository.hasRoommate.mockReset();
        repository.haveParticipantsSameRoomType.mockReset();
        repository.create.mockReset();
        domainEventDispatcher.unsubscribe('INVITATION_CREATED_BY_PARTICIPANT', listener);
      });

      it('should contain 1 invitation', () => {
        expect(stored).toBeDefined();
      });
      it('should change invitation status to PARTIALLY_ACCEPTED:1', () => {
        expect(repository.create.mock.calls[0][0]).toEqual(participantInvitationRequest);
        expect(repository.create.mock.calls[0][2]).toEqual('PARTIALLY_ACCEPTED:1');
      });
      it('should dispatch INVITATION_CREATED_BY_PARTICIPANT event', () => {
        expect(events[0].type).toEqual('INVITATION_CREATED_BY_PARTICIPANT');
      });
    });

    describe('from admin to participants', () => {
      let stored;
      beforeEach(async() => {
        repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTestTwo));
        repository.hasRoommate.mockImplementation(() => Promise.resolve(false));
        repository.haveParticipantsSameRoomType.mockImplementation(() => Promise.resolve(true));
        repository.create.mockImplementation(() => Promise.resolve(participantInvitationStub));
        domainEventDispatcher.subscribe('INVITATION_CREATED_BY_ADMIN', listener);
        stored = await invitations.send(adminInvitationRequest);
      });
      afterEach(() => {
        repository.readPersonByEmail.mockReset();
        repository.hasRoommate.mockReset();
        repository.haveParticipantsSameRoomType.mockReset();
        repository.create.mockReset();
        domainEventDispatcher.unsubscribe('INVITATION_CREATED_BY_ADMIN', listener);
      });

      it('should contain 1 invitation', () => {
        expect(stored).toBeDefined();
      });
      it('should change invitation status to OPEN', () => {
        expect(repository.create.mock.calls[0][0]).toEqual(adminInvitationRequest);
        expect(repository.create.mock.calls[0][2]).toEqual('OPEN');
      });
      it('should dispatch INVITATION_CREATED_BY_PARTICIPANT event', () => {
        expect(events[0].type).toEqual('INVITATION_CREATED_BY_ADMIN');
      });
    });
  });

  describe('Given inviter is an attendee', () => {
    describe('and sender is the other invitee', () => {
      describe('when accepting an invitation', () => {
        let stored;

        beforeEach(async () => {
          repository.findByKey.mockImplementation(() =>
            Promise.resolve({...participantInvitationStub, status: 'PARTIALLY_ACCEPTED:1'}));
          repository.update.mockImplementation(() =>
            Promise.resolve({...participantInvitationStub, status: 'ACCEPTED'}));
          repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTest));

          domainEventDispatcher.subscribe('INVITATION_ACCEPTED', listener);
          stored = await invitations.accept('key', 'tesT2@test.de');
        });
        afterEach(() => {
          repository.findByKey.mockReset();
          repository.update.mockReset();
          repository.readPersonByEmail.mockReset();
          domainEventDispatcher.unsubscribe('INVITATION_ACCEPTED', listener);
        });

        it('should contain the accepted invitation', () => {
          expect(stored).toBeDefined();
        });

        it('should change invitation status to ACCEPTED', () => {
          const updatedInvitation = repository.update.mock.calls[0][0];
          expect(updatedInvitation.status).toEqual('ACCEPTED');
        });

        it('should dispatch INVITATION_ACCEPTED event', () => {
          expect(events[0].type).toEqual('INVITATION_ACCEPTED');
          expect(events[0].payload.emailToSentTo).toEqual(stored.invitee1.email);
          expect(events[0].payload.emailRoommate).toEqual(stored.invitee2.email);
        });
      });

      describe('when trying to accept an unknown invitation', () => {
        beforeEach(async () => {
          repository.findByKey.mockImplementation(() => Promise.resolve(null));
        });
        afterEach(() => {
          repository.findByKey.mockReset();
        });
        it('should throw an error', async (done) => {
          invitations.accept('nonAvailableKey', 'Test2@test.de')
            .then(() =>
              done('Accepting an invitation should throw an error, when the invitation cannot be found'))
            .catch(() => done());
        });
      });

      describe('when trying to accept an invitation with status ACCEPTED', () => {
        beforeEach(async () => {
          repository.findByKey.mockImplementation(() =>
            Promise.resolve({...participantInvitationStub, status: 'ACCEPTED'}));
        });
        afterEach(() => {
          repository.findByKey.mockReset();
        });
        it('should throw an error', async (done) => {
          invitations.accept('key', 'test2@test.de')
            .then(() =>
              done('Accepting an invitation should throw an error, when the invitation was already accepted'))
            .catch(() => done());
        });
      });

      describe('when declining an not already accepted invitation', () => {

        let stored;

        beforeEach(async () => {
          repository.findByKey.mockImplementation(() =>
            Promise.resolve({...participantInvitationStub, status: 'PARTIALLY_ACCEPTED:1'}));
          repository.update.mockImplementation(() =>
            Promise.resolve({...participantInvitationStub, status: 'DECLINED'}));
          repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTest));

          domainEventDispatcher.subscribe('INVITATION_DECLINED', listener);
          stored = await invitations.decline('key', 'test2@test.de');
        });
        afterEach(() => {
          repository.findByKey.mockReset();
          repository.update.mockReset();
          repository.readPersonByEmail.mockReset();
          domainEventDispatcher.unsubscribe('INVITATION_DECLINED', listener);
        });

        it('should contain the accepted invitation', () => {
          expect(stored).toBeDefined();
        });

        it('should change invitation status to DECLINED', () => {
          const updatedInvitation = repository.update.mock.calls[0][0];
          expect(updatedInvitation.status).toEqual('DECLINED');
        });

        it('should dispatch INVITATION_ACCEPTED event', () => {
          expect(events[0].type).toEqual('INVITATION_DECLINED');
          expect(events[0].payload.emailToSentTo).toEqual(stored.invitee1.email);
          expect(events[0].payload.emailRoommate).toEqual(stored.invitee2.email);
        });
      });

      describe('when trying to decline an unknown invitation', () => {
        beforeEach(async () => {
          repository.findByKey.mockImplementation(() => Promise.resolve(null));
        });
        afterEach(() => {
          repository.findByKey.mockReset();
        });
        it('should throw an error', async (done) => {
          invitations.accept('nonAvailableKey', 'test2@test.de')
            .then(() =>
              done('Declining an invitation should throw an error, when the invitation cannot be found'))
            .catch(() => done());
        });
      });
    });

    describe('and sender is not an invitee', () => {
      const sender = 'unknown@test.de';

      beforeEach(async () => {
        repository.findByKey.mockImplementation(() =>
          Promise.resolve({...participantInvitationStub, status: 'PARTIALLY_ACCEPTED:1'}));
      });
      afterEach(() => {
        repository.findByKey.mockReset();
      });

      describe('when accepting an invitation', () => {
        it('should throw an error', done => {
          invitations.accept(1, sender)
            .then(() => done('Accepting an invitation should throw an error when the sender is unknown'))
            .catch((e) => {
              if (e.message === 'Invitation cannot be accepted. Sender does not match any invitees.') {
                done();
              }
            });
        });
      });

      describe('when declining an invitation', () => {
        it('should throw an error', done => {
          invitations.decline(1, sender)
            .then(() => done('Accepting an invitation should throw an error when the sender is unknown'))
            .catch((e) => {
              if (e.message === 'Invitation cannot be declined. Sender does not match any invitees.') {
                done();
              }
            });
        });
      });
    });
  });

  describe('Given inviter is an admin', () => {

    describe('and sender is not an invitee', () => {
      const sender = 'unknown@test.de';

      beforeEach(async () => {
        repository.findByKey.mockImplementation(() =>
          Promise.resolve(adminInvitationStub));
      });
      afterEach(() => {
        repository.findByKey.mockReset();
      });

      describe('when accepting an invitation', () => {
        it('should throw an error', done => {
          invitations.accept(1, sender)
            .then(() => done('Accepting an invitation should throw an error when the sender is unknown'))
            .catch((e) => {
              if (e.message === 'Invitation cannot be accepted. Sender does not match any invitees.') {
                done();
              }
            });
        });
      });

      describe('when declining an invitation', () => {
        it('should throw an error', done => {
          invitations.decline(1, sender)
            .then(() => done('Accepting an invitation should throw an error when the sender is unknown'))
            .catch((e) => {
              if (e.message === 'Invitation cannot be declined. Sender does not match any invitees.') {
                done();
              }
            });
        });
      });
    });

    describe('and first invitee responds', () => {

      describe('when accepting an invitation', () => {
        let stored;

        beforeEach(async () => {
          repository.findByKey.mockImplementation(() => Promise.resolve(adminInvitationStub));
          repository.update.mockImplementation(() => Promise.resolve({...adminInvitationStub, status: 'ACCEPTED'}));
          repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTestTwo));

          domainEventDispatcher.subscribe('INVITATION_ACCEPTED', listener);
          stored = await invitations.accept('key', 'test@test.de');
        });
        afterEach(() => {
          repository.findByKey.mockReset();
          repository.update.mockReset();
          repository.readPersonByEmail.mockReset();
          domainEventDispatcher.unsubscribe('INVITATION_ACCEPTED', listener);
        });

        it('should contain the accepted invitation', () => {
          expect(stored).toBeDefined();
        });

        it('should change invitation status to PARTIALLY_ACCEPTED:1', () => {
          const updatedInvitation = repository.update.mock.calls[0][0];
          expect(updatedInvitation.status).toEqual('PARTIALLY_ACCEPTED:1');
        });
      });

      describe('when declining an invitation', () => {
        let stored;

        beforeEach(async () => {
          repository.findByKey.mockImplementation(() => Promise.resolve(adminInvitationStub));
          repository.update.mockImplementation(() => Promise.resolve({...adminInvitationStub, status: 'DECLINED'}));
          repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTestTwo));

          domainEventDispatcher.subscribe('INVITATION_DECLINED', listener);
          stored = await invitations.decline('key', 'test@test.de');
        });
        afterEach(() => {
          repository.findByKey.mockReset();
          repository.update.mockReset();
          repository.readPersonByEmail.mockReset();
          domainEventDispatcher.unsubscribe('INVITATION_DECLINED', listener);
        });

        it('should contain the accepted invitation', () => {
          expect(stored).toBeDefined();
        });

        it('should change invitation status to DECLINED', () => {
          const updatedInvitation = repository.update.mock.calls[0][0];
          expect(updatedInvitation.status).toEqual('DECLINED');
        });
        it('should dispatch INVITATION_DECLINED event', () => {
          expect(events[0].type).toEqual('INVITATION_DECLINED');
          expect(events[0].payload.emailToSentTo).toEqual(stored.invitee2.email);
          expect(events[0].payload.emailRoommate).toEqual(stored.invitee1.email);
        });
      });

      describe('when trying to accept an unknown invitation', () => {
        beforeEach(async () => {
          repository.findByKey.mockImplementation(() => Promise.resolve(null));
        });
        afterEach(() => {
          repository.findByKey.mockReset();
        });
        it('should throw an error', async (done) => {
          invitations.accept('nonAvailableKey', 'test@test.de')
            .then(() =>
              done('Accepting an invitation should throw an error, when the invitation cannot be found'))
            .catch(() => done());
        });
      });

      describe('when trying to accept an invitation with status ACCEPTED', () => {
        beforeEach(async () => {
          repository.findByKey.mockImplementation(() =>
            Promise.resolve({...participantInvitationStub, status: 'ACCEPTED'}));
        });
        afterEach(() => {
          repository.findByKey.mockReset();
        });
        it('should throw an error', async (done) => {
          invitations.accept('key', 'test@test.de')
            .then(() =>
              done('Accepting an invitation should throw an error, when the invitation was already accepted'))
            .catch(() => done());
        });
      });

      describe('when trying to decline an unknown invitation', () => {
        beforeEach(async () => {
          repository.findByKey.mockImplementation(() => Promise.resolve(null));
        });
        afterEach(() => {
          repository.findByKey.mockReset();
        });
        it('should throw an error', async (done) => {
          invitations.accept('nonAvailableKey', 'test@test.de')
            .then(() => done('Declining an invitation should throw an error, when the invitation cannot be found'))
            .catch(() => done());
        });
      });
    });

    describe('and second invitee responds', () => {
      describe('when accepting an invitation', () => {
        let stored;

        beforeEach(async () => {
          repository.findByKey.mockImplementation(() => Promise.resolve(adminInvitationStub));
          repository.update.mockImplementation(() =>
            Promise.resolve({...adminInvitationStub, status: 'PARTIALLY_ACCEPTED:2'}));
          repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTest));

          domainEventDispatcher.subscribe('PARTIALLY_ACCEPTED:2', listener);
          stored = await invitations.accept('key', 'test2@test.de');
        });
        afterEach(() => {
          repository.findByKey.mockReset();
          repository.update.mockReset();
          repository.readPersonByEmail.mockReset();
          domainEventDispatcher.unsubscribe('PARTIALLY_ACCEPTED:2', listener);
        });

        it('should contain the accepted invitation', () => {
          expect(stored).toBeDefined();
        });

        it('should change invitation status to PARTIALLY_ACCEPTED:2', () => {
          const updatedInvitation = repository.update.mock.calls[0][0];
          expect(updatedInvitation.status).toEqual('PARTIALLY_ACCEPTED:2');
        });
      });

      describe('when declining an invitation', () => {
        let stored;

        beforeEach(async () => {
          repository.findByKey.mockImplementation(() => Promise.resolve(adminInvitationStub));
          repository.update.mockImplementation(() => Promise.resolve({...adminInvitationStub, status: 'DECLINED'}));
          repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTest));

          domainEventDispatcher.subscribe('INVITATION_DECLINED', listener);
          stored = await invitations.decline('key', 'test2@test.de');
        });
        afterEach(() => {
          repository.findByKey.mockReset();
          repository.update.mockReset();
          repository.readPersonByEmail.mockReset();
          domainEventDispatcher.unsubscribe('INVITATION_DECLINED', listener);
        });

        it('should contain the accepted invitation', () => {
          expect(stored).toBeDefined();
        });

        it('should change invitation status to DECLINED', () => {
          const updatedInvitation = repository.update.mock.calls[0][0];
          expect(updatedInvitation.status).toEqual('DECLINED');
        });
        it('should dispatch INVITATION_DECLINED event', () => {
          expect(events[0].type).toEqual('INVITATION_DECLINED');
          expect(events[0].payload.emailToSentTo).toEqual(stored.invitee1.email);
          expect(events[0].payload.emailRoommate).toEqual(stored.invitee2.email);
        });
      });

      describe('when trying to accept an unknown invitation', () => {
        beforeEach(async () => {
          repository.findByKey.mockImplementation(() => Promise.resolve(null));
        });
        afterEach(() => {
          repository.findByKey.mockReset();
        });
        it('should throw an error', async (done) => {
          invitations.accept('nonAvailableKey', 'test2@test.de')
            .then(() =>
              done('Accepting an invitation should throw an error, when the invitation cannot be found'))
            .catch(() => done());
        });
      });

      describe('when trying to accept an invitation with status ACCEPTED', () => {
        beforeEach(async () => {
          repository.findByKey.mockImplementation(() =>
            Promise.resolve({...participantInvitationStub, status: 'ACCEPTED'}));
        });
        afterEach(() => {
          repository.findByKey.mockReset();
        });
        it('should throw an error', async (done) => {
          invitations.accept('key', 'test2@test.de')
            .then(() =>
              done('Accepting an invitation should throw an error, when the invitation was already accepted'))
            .catch(() => done());
        });
      });

      describe('when trying to decline an unknown invitation', () => {
        beforeEach(async () => {
          repository.findByKey.mockImplementation(() => Promise.resolve(null));
        });
        afterEach(() => {
          repository.findByKey.mockReset();
        });
        it('should throw an error', async (done) => {
          invitations.accept('nonAvailableKey', 'test2@test.de')
            .then(() => done('Declining an invitation should throw an error, when the invitation cannot be found'))
            .catch(() => done());
        });
      });
    });

    describe('and first invitee has accepted invitation', () => {
      let stored;
      beforeEach(async () => {
        repository.findByKey.mockImplementation(() =>
          Promise.resolve({...adminInvitationStub, status: 'PARTIALLY_ACCEPTED:1'}));
      });
      afterEach(() => {
        repository.findByKey.mockReset();
      });
      describe('and first invitee responds again', () => {
        describe('accepting an invitation', () => {
          beforeEach(async () => {
            repository.update.mockImplementation(() =>
              Promise.resolve({...adminInvitationStub, status: 'PARTIALLY_ACCEPTED:1'}));
            repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTestTwo));

            domainEventDispatcher.subscribe('INVITATION_ACCEPTED', listener);
            stored = await invitations.accept('key', 'test@test.de');
          });
          afterEach(() => {
            repository.update.mockReset();
            repository.readPersonByEmail.mockReset();
            domainEventDispatcher.unsubscribe('INVITATION_ACCEPTED', listener);
          });

          it('should contain the partially accepted invitation', () => {
            expect(stored).toBeDefined();
          });

          it('should not change the invitation status', () => {
            const updatedInvitation = repository.update.mock.calls[0][0];
            expect(updatedInvitation.status).toEqual('PARTIALLY_ACCEPTED:1');
          });

          it('should not dispatch INVITATION_ACCEPTED event', () => {
            expect(events.length).toEqual(0);
          });
        });

        describe('declining an invitation', () => {
          beforeEach(async () => {
            repository.update.mockImplementation(() => Promise.resolve({...adminInvitationStub, status: 'DECLINED'}));
            repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTestTwo));

            domainEventDispatcher.subscribe('INVITATION_DECLINED', listener);
            stored = await invitations.decline('key', 'test@test.de');
          });
          afterEach(() => {
            repository.update.mockReset();
            repository.readPersonByEmail.mockReset();
            domainEventDispatcher.unsubscribe('INVITATION_DECLINED', listener);
          });

          it('should contain the accepted invitation', () => {
            expect(stored).toBeDefined();
          });

          it('should change invitation status to DECLINED', () => {
            const updatedInvitation = repository.update.mock.calls[0][0];
            expect(updatedInvitation.status).toEqual('DECLINED');
          });
          it('should dispatch INVITATION_DECLINED event', () => {
            expect(events[0].type).toEqual('INVITATION_DECLINED');
            expect(events[0].payload.emailToSentTo).toEqual(stored.invitee2.email);
            expect(events[0].payload.emailRoommate).toEqual(stored.invitee1.email);
          });
        });
      });
      describe('and second invitee responds', () => {
        describe('accepting an invitation', () => {
          beforeEach(async () => {
            repository.update.mockImplementation(() =>
              Promise.resolve({...adminInvitationStub, status: 'PARTIALLY_ACCEPTED:1'}));
            repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTest));

            domainEventDispatcher.subscribe('INVITATION_ACCEPTED', listener);
            stored = await invitations.accept('key', 'test2@test.de');
          });
          afterEach(() => {
            repository.update.mockReset();
            repository.readPersonByEmail.mockReset();
            domainEventDispatcher.unsubscribe('INVITATION_ACCEPTED', listener);
          });

          it('should contain the partially accepted invitation', () => {
            expect(stored).toBeDefined();
          });

          it('should change the invitation status to ACCEPTED', () => {
            const updatedInvitation = repository.update.mock.calls[0][0];
            expect(updatedInvitation.status).toEqual('ACCEPTED');
          });

          it('should dispatch INVITATION_ACCEPTED event', () => {
            expect(events[0].type).toEqual('INVITATION_ACCEPTED');
            expect(events[0].payload.emailToSentTo).toEqual(stored.invitee1.email);
            expect(events[0].payload.emailRoommate).toEqual(stored.invitee2.email);
          });
        });

        describe('declining an invitation', () => {
          beforeEach(async () => {
            repository.update.mockImplementation(() => Promise.resolve({...adminInvitationStub, status: 'DECLINED'}));
            repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTest));

            domainEventDispatcher.subscribe('INVITATION_DECLINED', listener);
            stored = await invitations.decline('key', 'test2@test.de');
          });
          afterEach(() => {
            repository.update.mockReset();
            repository.readPersonByEmail.mockReset();
            domainEventDispatcher.unsubscribe('INVITATION_DECLINED', listener);
          });

          it('should contain the accepted invitation', () => {
            expect(stored).toBeDefined();
          });

          it('should change invitation status to DECLINED', () => {
            const updatedInvitation = repository.update.mock.calls[0][0];
            expect(updatedInvitation.status).toEqual('DECLINED');
          });
          it('should dispatch INVITATION_DECLINED event', () => {
            expect(events[0].type).toEqual('INVITATION_DECLINED');
            expect(events[0].payload.emailToSentTo).toEqual(stored.invitee1.email);
            expect(events[0].payload.emailRoommate).toEqual(stored.invitee2.email);
          });
        });
      });
    });

    describe('and second invitee has accepted invitation', () => {
      let stored;
      beforeEach(async () => {
        repository.findByKey.mockImplementation(() =>
          Promise.resolve({...adminInvitationStub, status: 'PARTIALLY_ACCEPTED:2'}));
      });
      afterEach(() => {
        repository.findByKey.mockReset();
      });
      describe('and second invitee responds again', () => {
        describe('accepting an invitation', () => {
          beforeEach(async () => {
            repository.update.mockImplementation(() =>
              Promise.resolve({...adminInvitationStub, status: 'PARTIALLY_ACCEPTED:2'}));
            repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTest));

            domainEventDispatcher.subscribe('INVITATION_ACCEPTED', listener);
            stored = await invitations.accept('key', 'test2@test.de');
          });
          afterEach(() => {
            repository.update.mockReset();
            repository.readPersonByEmail.mockReset();
            domainEventDispatcher.unsubscribe('INVITATION_ACCEPTED', listener);
          });

          it('should contain the partially accepted invitation', () => {
            expect(stored).toBeDefined();
          });

          it('should not change the invitation status', () => {
            const updatedInvitation = repository.update.mock.calls[0][0];
            expect(updatedInvitation.status).toEqual('PARTIALLY_ACCEPTED:2');
          });

          it('should not dispatch INVITATION_ACCEPTED event', () => {
            expect(events.length).toEqual(0);
          });
        });

        describe('declining an invitation', () => {
          beforeEach(async () => {
            repository.update.mockImplementation(() => Promise.resolve({...adminInvitationStub, status: 'DECLINED'}));
            repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTest));

            domainEventDispatcher.subscribe('INVITATION_DECLINED', listener);
            stored = await invitations.decline('key', 'test2@test.de');
          });
          afterEach(() => {
            repository.update.mockReset();
            repository.readPersonByEmail.mockReset();
            domainEventDispatcher.unsubscribe('INVITATION_DECLINED', listener);
          });

          it('should contain the accepted invitation', () => {
            expect(stored).toBeDefined();
          });

          it('should change invitation status to DECLINED', () => {
            const updatedInvitation = repository.update.mock.calls[0][0];
            expect(updatedInvitation.status).toEqual('DECLINED');
          });
          it('should dispatch INVITATION_DECLINED event', () => {
            expect(events[0].type).toEqual('INVITATION_DECLINED');
            expect(events[0].payload.emailToSentTo).toEqual(stored.invitee1.email);
            expect(events[0].payload.emailRoommate).toEqual(stored.invitee2.email);
          });
        });
      });
      describe('and first invitee responds', () => {
        describe('accepting an invitation', () => {
          beforeEach(async () => {
            repository.update.mockImplementation(() =>
              Promise.resolve({...adminInvitationStub, status: 'ACCEPTED'}));
            repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTestTwo));

            domainEventDispatcher.subscribe('INVITATION_ACCEPTED', listener);
            stored = await invitations.accept('key', 'test@test.de');
          });
          afterEach(() => {
            repository.update.mockReset();
            repository.readPersonByEmail.mockReset();
            domainEventDispatcher.unsubscribe('INVITATION_ACCEPTED', listener);
          });

          it('should contain the invitation', () => {
            expect(stored).toBeDefined();
          });

          it('should change the invitation status to ACCEPTED', () => {
            const updatedInvitation = repository.update.mock.calls[0][0];
            expect(updatedInvitation.status).toEqual('ACCEPTED');
          });

          it('should dispatch INVITATION_ACCEPTED event', () => {
            expect(events[0].type).toEqual('INVITATION_ACCEPTED');
            expect(events[0].payload.emailToSentTo).toEqual(stored.invitee2.email);
            expect(events[0].payload.emailRoommate).toEqual(stored.invitee1.email);
          });
        });

        describe('declining an invitation', () => {
          beforeEach(async () => {
            repository.update.mockImplementation(() => Promise.resolve({...adminInvitationStub, status: 'DECLINED'}));
            repository.readPersonByEmail.mockImplementation(() => Promise.resolve(testPersonTestTwo));

            domainEventDispatcher.subscribe('INVITATION_DECLINED', listener);
            stored = await invitations.decline('key', 'test@test.de');
          });
          afterEach(() => {
            repository.update.mockReset();
            repository.readPersonByEmail.mockReset();
            domainEventDispatcher.unsubscribe('INVITATION_DECLINED', listener);
          });

          it('should contain the accepted invitation', () => {
            expect(stored).toBeDefined();
          });

          it('should change invitation status to DECLINED', () => {
            const updatedInvitation = repository.update.mock.calls[0][0];
            expect(updatedInvitation.status).toEqual('DECLINED');
          });
          it('should dispatch INVITATION_DECLINED event', () => {
            expect(events[0].type).toEqual('INVITATION_DECLINED');
            expect(events[0].payload.emailToSentTo).toEqual(stored.invitee2.email);
            expect(events[0].payload.emailRoommate).toEqual(stored.invitee1.email);
          });
        });
      });
    });
  });

  describe('when getting sent invitation', () => {
    let stored;
    beforeEach(async() => {
      repository.readSentInvitationByPersonId.mockImplementation(() => Promise.resolve({}));
      stored = await invitations.readSentInvitationByPersonId(1);
    });
    afterEach(() => {
      repository.readSentInvitationByPersonId.mockReset();
    });

    it('readSentInvitationByPersonId is called', async () => {
      expect(stored).toBeDefined();
      expect(repository.readSentInvitationByPersonId.mock.calls).toHaveLength(1);
    });
  });

  describe('when getting received invitations', () => {
    let stored;
    beforeEach(async() => {
      repository.readReceivedInvitationsByPersonId.mockImplementation(() => Promise.resolve({}));
      stored = await invitations.readReceivedInvitationsByPersonId(1);
    });
    afterEach(() => {
      repository.readReceivedInvitationsByPersonId.mockReset();
    });

    it('readReceivedInvitationsByPersonId is called', async () => {
      expect(stored).toBeDefined();
      expect(repository.readReceivedInvitationsByPersonId.mock.calls).toHaveLength(1);
    });
  });

  describe('when searching invitation by key', () => {
    let stored;
    beforeEach(async() => {
      repository.findByKey.mockImplementation(() => Promise.resolve({}));
      stored = await invitations.readByKey('key');
    });
    afterEach(() => {
      repository.findByKey.mockReset();
    });

    it('findByKey is called', async () => {
      expect(stored).toBeDefined();
      expect(repository.findByKey.mock.calls).toHaveLength(1);
    });
  });

  describe('on participation cancelled event', () => {
    const waitForCancelEventAndCheckDeletion = (done, expectedCancellationEmail, expectedOtherEmail) => {
      return event => {
        const {cancelledEmail, cancelledInvitations} = event.payload;
        expect(cancelledEmail).toEqual(expectedCancellationEmail);
        expect(cancelledInvitations).toHaveLength(2);
        expect(cancelledInvitations[0].email).toEqual(expectedOtherEmail);
        expect(cancelledInvitations[1].email).toEqual(expectedOtherEmail);
        done();
      };
    };
    beforeEach(() => {
      repository.readPersonByEmail.mockImplementationOnce(() => Promise.resolve(testPersonTest));
      repository.readSentInvitationByPersonId.mockImplementation(() => Promise.resolve(participantInvitationStub));
      repository.readPersonByEmail.mockImplementationOnce(() => Promise.resolve(testPersonTestTwo));
      repository.readReceivedInvitationsByPersonId.mockImplementation(() => Promise.resolve([adminInvitationStub]));
      repository.readPersonByEmail.mockImplementationOnce(() => Promise.resolve(testPersonTestTwo));
      repository.deleteReceivedInvitationsByMail.mockImplementation(() => Promise.resolve());
      repository.deleteSentInvitationsByMail.mockImplementation(() => Promise.resolve());
    });
    afterEach(() => {
      repository.readPersonByEmail.mockReset();
      repository.readSentInvitationByPersonId.mockReset();
      repository.readReceivedInvitationsByPersonId.mockReset();
      repository.deleteReceivedInvitationsByMail.mockReset();
      repository.deleteSentInvitationsByMail.mockReset();
      domainEventDispatcher.unsubscribe('INVITATIONS_CANCELLED_FOR_CANCELLED_PARTICIPANT',
        waitForCancelEventAndCheckDeletion);
    });
    it('deletes sent invitations', async done => {
      domainEventDispatcher.subscribe('INVITATIONS_CANCELLED_FOR_CANCELLED_PARTICIPANT',
        waitForCancelEventAndCheckDeletion(done, 'test@test.de', 'test2@test.de'));
      domainEventDispatcher.dispatchEvent({type: PARTICIPANT_CANCELED, payload: {id: 1, email: 'test@test.de'}});
    });
  });

  describe('when getting all room sharing data', () => {
    let stored;
    beforeEach(async() => {
      repository.getAllRoomSharingData.mockImplementation(() => Promise.resolve(rawRoomSharingData));
      stored = await invitations.getAllRoomSharingData();
    });
    afterEach(() => {
      repository.getAllRoomSharingData.mockReset();
    });

    it('getAllRoomSharingData is called', async () => {
      expect(stored).toBeDefined();
      expect(repository.getAllRoomSharingData.mock.calls).toHaveLength(1);
    });
    it('result contains two room sharing data items', async () => {
      expect(stored).toHaveLength(2);
      const firstItem = stored[0];
      expect(firstItem.invitee1.firstName).toEqual('P1FirstName');
      expect(firstItem.invitee1.lastName).toEqual('P1LastName');
      expect(firstItem.invitee1.personId).toBe(1);
      expect(firstItem.invitee1.familyInfo).toEqual('P1FamilyInfo');
      expect(firstItem.invitee2.firstName).toEqual('P2FirstName');
      expect(firstItem.roomType).toEqual('roomType');
    });
  });
});

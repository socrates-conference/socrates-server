// @flow

import Aggregate from './aggregate';
import type {Event, EventDispatcher} from './eventDispatcher';
import type {InvitationsRepository} from '../db/invitations/invitationsRepository';
import {makeRandomKey} from '../utilities/Utilities';
import type {PersonResult} from '../db/application/personMySQL';
import {PARTICIPANT_CANCELED} from './participant';

export type InvitationRequest = {
    inviterId: number,
    inviterEmail: string,
    invitee1Id: number,
    invitee1Email: string,
    invitee2Id: number,
    invitee2Email: string,
}

export type Person = {
    id: number,
    email: string
}

export type InvitationStatus =
    'UNKNOWN'
    | 'OPEN'
    | 'ACCEPTED'
    | 'DECLINED'
    | 'PARTIALLY_ACCEPTED:1'
    | 'PARTIALLY_ACCEPTED:2';

export type Invitation = {
    id: number,
    invitee1: Person,
    invitee2: Person,
    inviter: Person,
    key: string,
    status: InvitationStatus
}

export type CancelledInvitation = {
    nickname: string,
    email: string,
    status: InvitationStatus
}

export type SharingPerson = {
  personId: number,
  firstName: string,
  lastName: string,
  familyInfo: string,
  company: string,
  address1: string,
  address2: string,
  postal: string,
  province: string,
  city: string,
  country: string
}

export type RoomSharing = {
  invitee1: SharingPerson,
  invitee2: SharingPerson,
  roomType: string
}

export const INVITATION_CREATED_BY_PARTICIPANT = 'INVITATION_CREATED_BY_PARTICIPANT';
export const INVITATION_CREATED_BY_ADMIN = 'INVITATION_CREATED_BY_ADMIN';
export const INVITATION_ACCEPTED = 'INVITATION_ACCEPTED';
export const INVITATION_DECLINED = 'INVITATION_DECLINED';
export const INVITATIONS_CANCELLED_FOR_CANCELLED_PARTICIPANT = 'INVITATIONS_CANCELLED_FOR_CANCELLED_PARTICIPANT';

const isInvitee1 = (inv: Invitation, sender) => inv.invitee1.email.toLowerCase() === sender.toLowerCase();
const isInvitee2 = (inv: Invitation, sender) => inv.invitee2.email.toLowerCase() === sender.toLowerCase();
const inviteeAccepted = (invitation: Invitation, n: number) => invitation.status.endsWith(String(n));
const isOtherInvitee = (invitation: Invitation, sender) =>
  (isInvitee1(invitation, sender) && inviteeAccepted(invitation, 2)) ||
    (isInvitee2(invitation, sender) && inviteeAccepted(invitation, 1));

const validateForAccept = (invitation: ?Invitation, key: string, sender: string) => {
  return validate(invitation, key, sender, 'Invitation cannot be accepted');
};

const validateForDecline = (invitation: ?Invitation, key: string, sender: string) => {
  return validate(invitation, key, sender, 'Invitation cannot be declined');
};
const validate = (invitation: ?Invitation, key: string, sender: string, message: string) => {
  if (!invitation) {
    throw new Error(`${message}. No such invitation: ${key}`);
  }
  if (!isInvitee1(invitation, sender) && !isInvitee2(invitation, sender)) {
    throw new Error(`${message}. Sender does not match any invitees.`);
  }
  return invitation;
};

function toRoomSharing(item): RoomSharing {
  return {
    invitee1: {
      personId: item.P1Id, firstName: item.P1FirstName, lastName: item.P1LastName, familyInfo: item.P1FamilyInfo,
      company: item.P1Company, address1: item.P1Address1, address2: item.P1Address2, postal: item.P1Postal,
      province: item.P1Province, city: item.P1City, country: item.P1Country
    },
    invitee2: {
      personId: item.P2Id, firstName: item.P2FirstName, lastName: item.P2LastName, familyInfo: item.P2FamilyInfo,
      company: item.P2Company, address1: item.P2Address1, address2: item.P2Address2, postal: item.P2Postal,
      province: item.P2Province, city: item.P2City, country: item.P2Country
    },
    roomType: item.RoomType
  };
}



export const differentRoomTypes = 'Participants have not the same room type.';
export const serverError = 'Cannot create invitation.';
export const hasAlreadyRoommateError = 'The person you want to invite, has already a roommate.';

export default class Invitations extends Aggregate {
    _invitations: Map<number, Invitation>;
    _repository: InvitationsRepository;

    constructor(repository: InvitationsRepository, dispatcher: EventDispatcher) {
      super(dispatcher);
      this._invitations = new Map();
      this._repository = repository;
      dispatcher.subscribe(PARTICIPANT_CANCELED, this._handleParticipantCanceled);
    }

    async _getEventPayload(invitation: Invitation, sender: string) {
      let recipientName = '';
      let emailToSentTo = '';
      let emailRoommate = '';
      if (isInvitee2(invitation, sender)) {
        const person: ?PersonResult = await this._repository.readPersonByEmail(invitation.invitee1.email);
        if (person) {
          recipientName = person.nickname;
          emailToSentTo = person.email;
          emailRoommate = invitation.invitee2.email;
        } else {
          throw new Error('Invitation cannot be accepted: Person cannot be found.');
        }
      } else {
        const person: ?PersonResult = await this._repository.readPersonByEmail(invitation.invitee2.email);
        if (person) {
          recipientName = person.nickname;
          emailToSentTo = person.email;
          emailRoommate = invitation.invitee1.email;
        } else {
          throw new Error('Invitation cannot be accepted: Person cannot be found.');
        }
      }
      return {recipientName, emailToSentTo, emailRoommate};
    }

    async accept(key: string, inviteeEmail: string): Promise<?Invitation> {
      const stored = await this._repository.findByKey(key);
      if (!stored) {
        throw new Error(`invitation cannot be found by key ${key}`);
      }
      const invitation = validateForAccept(stored, key, inviteeEmail);
      if (invitation) {
        let status = invitation.status;
        switch (invitation.status) {
          case 'OPEN':
            if (isInvitee2(invitation, inviteeEmail)) {
              status = 'PARTIALLY_ACCEPTED:2';
            } else {
              status = 'PARTIALLY_ACCEPTED:1';
            }
            break;
          case 'PARTIALLY_ACCEPTED:1':
          case 'PARTIALLY_ACCEPTED:2':
            if (isOtherInvitee(invitation, inviteeEmail)) {
              status = 'ACCEPTED';
            }
            break;
          default:
            throw new Error('Invitation cannot be accepted: Invalid action at current status.');
        }
        const next = {...invitation, status};
        const updatedInvitation = await this._repository.update(next);
        if (updatedInvitation) {
          if (status === 'ACCEPTED') {
            this._dispatcher.dispatchEvent({
              type: INVITATION_ACCEPTED,
              payload: await this._getEventPayload(updatedInvitation, inviteeEmail)
            });
          }
          return updatedInvitation;
        }
      }
      return null;
    }

    async decline(key: string, sender: string): Promise<?Invitation> {
      const stored = await this._repository.findByKey(key);
      const invitation = validateForDecline(stored, key, sender);
      if (invitation) {
        if (invitation.status !== 'DECLINED') {
          const status = 'DECLINED';
          const next = {...invitation, status};
          const updatedInvitation = await this._repository.update(next);
          if (updatedInvitation) {
            this._dispatcher.dispatchEvent(
              {
                type: INVITATION_DECLINED,
                payload: await this._getEventPayload(invitation, sender)
              });
            return updatedInvitation;
          }
        }
      }
      return null;
    }

    async send(data: Object): Promise<?Invitation> {
      const invitationRequest: InvitationRequest = {...data};
      const invitee2: ?PersonResult = await this._repository.readPersonByEmail(invitationRequest.invitee2Email);
      if (invitee2) {
        invitationRequest.invitee2Id = invitee2.id;
        const hasAlreadyRoommate = await this._repository.hasRoommate(invitee2.id);
        if (hasAlreadyRoommate) {
          throw new Error(hasAlreadyRoommateError);
        }
        const participantsHaveSameRoomType =
                await this._repository.haveParticipantsSameRoomType(invitationRequest.invitee1Id,
                  invitationRequest.invitee2Id);
        if (!participantsHaveSameRoomType) {
          throw new Error(differentRoomTypes);
        }
        const generatedKey = makeRandomKey();
        const status = invitationRequest.invitee1Id === invitationRequest.inviterId ? 'PARTIALLY_ACCEPTED:1' : 'OPEN';
        const stored = await this._repository.create(invitationRequest, generatedKey, status);
        if (stored) {
          this._dispatcher.dispatchEvent(
            {
              type: invitationRequest.invitee1Id === invitationRequest.inviterId
                ? INVITATION_CREATED_BY_PARTICIPANT
                : INVITATION_CREATED_BY_ADMIN,
              payload: {
                ...stored,
                recipientName: invitee2.nickname,
                key: generatedKey
              }
            });
          return stored;
        }
      }
      throw new Error(serverError);
    }

    async readSentInvitationByPersonId(personId: number): Promise<?Invitation> {
      return this._repository.readSentInvitationByPersonId(personId);
    }

    async readReceivedInvitationsByPersonId(personId: number): Promise<Invitation[]> {
      return this._repository.readReceivedInvitationsByPersonId(personId);
    }

    async list(): Promise<Invitation[]> {
      return this._repository.findAll();
    }

    async readByKey(key: string): Promise<?Invitation> {
      return this._repository.findByKey(key);
    }

    async getAllRoomSharingData(): Promise<RoomSharing[]> {
      const results: Object[] = await this._repository.getAllRoomSharingData();
      return results.map(item => toRoomSharing(item));
    }

    _handleParticipantCanceled = (ev: Event) => {
      const cancelledEmail = ev.payload.email;
      this.getInvitationsThatWillBeDeleted(cancelledEmail)
        .then(async invitations => {
          await this._repository.deleteReceivedInvitationsByMail(cancelledEmail);
          await this._repository.deleteSentInvitationsByMail(cancelledEmail);
          return invitations;
        })
        .then(async invitations => {
          await this.dispatchInvitationCancelled(invitations, cancelledEmail);
        })
        .catch(console.error);
    };

    async dispatchInvitationCancelled(cancelledInvitations: Array<CancelledInvitation>, cancelledEmail: string) {
      const event = {
        type: INVITATIONS_CANCELLED_FOR_CANCELLED_PARTICIPANT,
        payload: {cancelledEmail, cancelledInvitations}
      };
      this._dispatcher.dispatchEvent(event);

    }

    getInvitationsThatWillBeDeleted(cancelledEmail: string): Promise<Array<CancelledInvitation>> {
      return this._repository.readPersonByEmail(cancelledEmail).then(async person => {
        if (!person) {
          return [];
        }

        let cancelledInvitations: Array<CancelledInvitation> = [];
        cancelledInvitations = cancelledInvitations.concat(await this.getReceivedInvitationsToCancel(person));
        cancelledInvitations = cancelledInvitations.concat(await this.getSentInvitationToCancel(person));
        return cancelledInvitations;
      });
    }

    async getSentInvitationToCancel(person: PersonResult): Promise<Array<CancelledInvitation>> {
      const sentInvitation = await this._repository.readSentInvitationByPersonId(person.id);
      if (sentInvitation) {
        const receiver = (await this._repository.readPersonByEmail(sentInvitation.invitee2.email));
        if (receiver) {
          const {email, nickname} = receiver;
          return [{status: sentInvitation.status, nickname, email}];
        }
      }
      return [];
    }

    async getReceivedInvitationsToCancel(person: PersonResult): Promise<Array<CancelledInvitation>> {
      return await this._repository.readReceivedInvitationsByPersonId(person.id).then(async receivedInvitations => {
        const result = [];
        for (const invitation of receivedInvitations) {
          const sender: ?PersonResult = (await this._repository.readPersonByEmail(invitation.invitee1.email));
          if (sender) {
            const {email, nickname} = sender;
            result.push({status: invitation.status, nickname, email});
          }
        }
        return result;
      });
    }
}

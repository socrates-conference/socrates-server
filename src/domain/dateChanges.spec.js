// @flow

import DomainEventDispatcher from './domainEventDispatcher';
import TestHelper from '../db/SqlTestHelper';
import MySql from '../db/MySql';
import DataChangesRepository from '../db/dataChanges/dataChangesRepository';
import DataChanges from './dataChanges';


jest.mock('../db/dataChanges/dataChangesRepository');

const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);
const repository = new DataChangesRepository(mySql);

const dispatcher = new DomainEventDispatcher();

describe('Data changes:', () => {
  let dataChanges: DataChanges;
  let promise;
  beforeEach(() => {
    dataChanges = new DataChanges(repository, dispatcher);
    repository.readChangesFrom.mockImplementation(() => promise);
  });
  afterEach(() => {
    repository.readChangesFrom.mockReset();
  });
  it('can read changes', async () => {
    promise = Promise.resolve([{}]);
    const changes = await dataChanges.readChangesFrom('2005-01-01 00:00:00');
    console.log(changes);
    expect(repository.readChangesFrom.mock.calls).toHaveLength(1);
    expect(changes).toHaveLength(1);
  });
});

import {humanizeRoomType} from './roomTypes';

describe('Room types', () => {
  describe('humanizeRoomType', () => {
    it('returns necessary values', () => {
      expect(humanizeRoomType('single')).toBe('single');
      expect(humanizeRoomType('bedInDouble')).toBe('bed in a double');
      expect(humanizeRoomType('juniorExclusively')).toBe('junior (exclusively)');
      expect(humanizeRoomType('juniorShared')).toBe('junior (shared)');
      expect(humanizeRoomType('somethingElse')).toBe('');
    });
  });
});

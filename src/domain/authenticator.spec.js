// @flow

import DomainEventDispatcher from './domainEventDispatcher';
import MySql from '../db/MySql';
import TestHelper from '../db/SqlTestHelper';
import type {
  AccessCheckResult,
  LoginResult
} from './authenticator';
import Authenticator from './authenticator';
import AuthenticationRepository from '../db/authenticationRepository';
import config from '../../src/config';
import jwt from 'jsonwebtoken';

jest.mock('../db/authenticationRepository');

describe('Authenticator', () => {
  const dispatcher = new DomainEventDispatcher();
  const testHelper = new TestHelper();
  let mySql, authenticator, authenticationRepository;
  beforeEach(() => {
    mySql = new MySql(testHelper.testConfig);
    authenticationRepository = new AuthenticationRepository(mySql);
    authenticator = new Authenticator(authenticationRepository, dispatcher);
  });

  const theHash = '$2a$10$eEhkyHRz6dtEQEHnQdDkher2Lg2ZrzQOC2fxiw7vi8naXZIqHZ.re';
  const defaultPerson = {
    name: 'test', email: 'test@test.org', password: theHash, isAdministrator: false, isHotel: false,
    isOneTimePassword: false, personId: 4711
  };

  describe('when logging in', () => {
    describe('and right email and password are provided for normal user', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve({...defaultPerson});
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      describe('and  user does not have a ticket ', () => {
        beforeEach(() => {
          authenticationRepository.readRegistrationByPersonId.mockImplementation(() => {
            return Promise.resolve(null);
          });

        });
        afterEach(() => {
          authenticationRepository.readRegistrationByPersonId.mockReset();
        });

        it('return empty token', async () => {
          const requestToken = jwt.sign({email: 'test@test.org', hash: theHash}, config.jwtSecret);
          const result = await authenticator.login(requestToken);
          expect(result.token).toEqual('');
        });
      });

      describe('and  user has a ticket ', () => {
        beforeEach(() => {
          authenticationRepository.readRegistrationByPersonId.mockImplementation((id: string) => {
            return Promise.resolve({id, personId: id});
          });
          authenticationRepository.readPersonByEmail.mockImplementation((email: string) => {
            return Promise.resolve({id: 4711, personId: 4711, email: email,
              password: '$2a$10$nrapppt.y2T25Ier4E5RYeLJZOT78Ajj6BzvBljibhbqWl7es3izK'});
          });
        });
        afterEach(() => {
          authenticationRepository.readRegistrationByPersonId.mockReset();
          authenticationRepository.readPersonByEmail.mockReset();
        });

        it('return a result containing a token', () => {
          const requestToken = jwt.sign({email: 'test@test.org', hash: 'password'}, config.jwtSecret);
          return authenticator.login(requestToken)
            .then((result: LoginResult) => {
              expect(result.token !== '').toBe(true);
            });
        });

        it('dispatch USER_LOGGED_IN', async (done) => {
          const requestToken = jwt.sign({email: 'test@test.org', hash: 'password'}, config.jwtSecret);
          dispatcher.subscribeOnce('USER_LOGGED_IN', () => done());
          await authenticator.login(requestToken);
        });
      });
    });
    describe('and right email and password are provided for administrator', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve({...defaultPerson, isAdministrator: true});
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      describe('and user does not have a ticket ', () => {
        beforeEach(() => {
          authenticationRepository.readRegistrationByPersonId.mockImplementation(() => {
            return Promise.resolve(null);
          });

        });
        afterEach(() => {
          authenticationRepository.readRegistrationByPersonId.mockReset();
        });

        it('return a result containing a token', () => {
          const requestToken = jwt.sign({email: 'test@test.org', hash: 'myPassword'}, config.jwtSecret);
          return authenticator.login(requestToken)
            .then((result: LoginResult) => {
              expect(result.token !== '').toBe(true);
            });
        });

        it('dispatch USER_LOGGED_IN', async (done) => {
          const requestToken = jwt.sign({email: 'test@test.org', hash: 'myPassword'}, config.jwtSecret);
          dispatcher.subscribeOnce('USER_LOGGED_IN', () => done());
          await authenticator.login(requestToken);
        });
      });

      describe('and  user has a ticket ', () => {
        beforeEach(() => {
          authenticationRepository.readRegistrationByPersonId.mockImplementation((id: string) => {
            return Promise.resolve({id, personId: id});
          });
        });
        afterEach(() => {
          authenticationRepository.readRegistrationByPersonId.mockReset();
        });

        it('return a result containing a token', () => {
          const requestToken = jwt.sign({email: 'test@test.org', hash: 'myPassword'}, config.jwtSecret);
          return authenticator.login(requestToken)
            .then((result: LoginResult) => {
              expect(result.token !== '').toBe(true);
            });
        });

        it('dispatch USER_LOGGED_IN', async (done) => {
          const requestToken = jwt.sign({email: 'test@test.org', hash: 'myPassword'}, config.jwtSecret);
          dispatcher.subscribeOnce('USER_LOGGED_IN', () => done());
          await authenticator.login(requestToken);
        });
      });
    });

    describe('and false password is provided', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(
            {name: 'test', email: 'test@test.org', password: theHash}
          );
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });

      it('return a result containing an empty token', () => {
        const requestToken = jwt.sign({email: 'test@test.org', hash: 'anotherPassword'}, config.jwtSecret);
        return authenticator.login(requestToken)
          .then((result: LoginResult) => {
            expect(result.token === '').toBe(true);
          });
      });

      it('dispatch USER_LOGIN_FAILURE', async (done) => {
        const requestToken = jwt.sign({email: 'test@test.org', hash: 'anotherPassword'}, config.jwtSecret);
        dispatcher.subscribeOnce('USER_LOGIN_FAILURE', () => done());
        await authenticator.login(requestToken);
      });
    });

    describe('and no user for the email can be found', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(null);
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });

      it('return empty token', async () => {
        const requestToken = jwt.sign({email: 'nonexistentEmail@valid.com', hash: 'anotherPassword'}, config.jwtSecret);
        const result = await authenticator.login(requestToken);
        expect(result.token === '').toBe(true);
      });
    });

    describe('with invalid request data', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(
            {name: 'test', email: 'test@test.org', password: theHash}
          );
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });

      it(' if no email return a failure result with an empty token', async () => {
        const requestToken = jwt.sign({email: '', hash: 'anotherPassword'}, config.jwtSecret);
        const result = await authenticator.login(requestToken);
        expect(result.token === '').toBe(true);
      });
      it('if no password return a failure result', async () => {
        const requestToken = jwt.sign({email: 'test@test.org', hash: ''}, config.jwtSecret);
        const result = await authenticator.login(requestToken);
        expect(result.token === '').toBe(true);
      });
      it('if invalid email return a failure result', async () => {
        const requestToken = jwt.sign({email: 'invalidEmail', hash: 'anotherPassword'}, config.jwtSecret);
        const result = await authenticator.login(requestToken);
        expect(result.token === '').toBe(true);
      });
    });
  });

  describe('when checking access', () => {
    describe('to a non secured destination', () => {
      it('from a non-logged in user access is allowed', async () => {
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/nonsecured/', undefined);
        expect(result.allowed).toBe(true);
        expect(result.error).toBeNull();
      });
      it('from a logged in user access is allowed', async () => {
        const user = {...defaultPerson};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/nonsecured/', user);
        expect(result.allowed).toBe(true);
        expect(result.error).toBeNull();
      });
    });
    it('to a destination that requires authentication from a non-logged in user access is denied', async () => {
      const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/participants/', undefined);
      expect(result.allowed).toBe(false);
      expect(result.error).toBeDefined();
      if (result.error) {
        expect(result.error.message).toEqual('Authentication required.');
      }
    });
    describe('to a destination that requires authentication from a verifiable logged in user ', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve({...defaultPerson});
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is allowed', async () => {
        const user = {...defaultPerson};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/participants/', user);
        expect(result.allowed).toBe(true);
        expect(result.error).toBeNull();
      });
    });
    describe('to a destination that requires authentication from a non verifiable logged in user', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(null);
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is denied', async () => {
        const user = {...defaultPerson};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/participants/', user);
        expect(result.allowed).toBe(false);
        expect(result.error).toBeDefined();
        if (result.error) {
          expect(result.error.message).toEqual('Authentication required.');
        }
      });
    });
    it('to a destination that requires an administrator from a non-logged in user access is denied', async () => {
      const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/management/', undefined);
      expect(result.allowed).toBe(false);
      expect(result.error).toBeDefined();
      if (result.error) {
        expect(result.error.message).toEqual('Authentication required.');
      }
    });
    describe('to a destination that requires an administrator from a verifiable logged in non administrator ', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve({...defaultPerson});
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is denied', async () => {
        const user = {...defaultPerson};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/management/', user);
        expect(result.allowed).toBe(false);
        expect(result.error).toBeDefined();
        if (result.error) {
          expect(result.error.message).toEqual('Administrator required.');
        }
      });
    });
    describe('to a destination that requires an administrator from a verifiable logged in administrator ', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(
            {...defaultPerson, isAdministrator: true}
          );
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is allowed', async () => {
        const user = {...defaultPerson};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/management/', user);
        expect(result.allowed).toBe(true);
        expect(result.error).toBeNull();
      });
    });
    describe('to a destination that requires an administrator from a non verifiable logged in user', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(null);
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is denied', async () => {
        const user = {...defaultPerson};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/management/', user);
        expect(result.allowed).toBe(false);
        expect(result.error).toBeDefined();
        if (result.error) {
          expect(result.error.message).toEqual('Authentication required.');
        }
      });
    });

    it('to a destination that requires a hotel user from a non-logged in user access is denied', async () => {
      const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/hotel/', undefined);
      expect(result.allowed).toBe(false);
      expect(result.error).toBeDefined();
      if (result.error) {
        expect(result.error.message).toEqual('Authentication required.');
      }
    });
    describe('to a destination that requires a hotel user from a verifiable logged in default user ', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve({...defaultPerson});
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is denied', async () => {
        const user = {...defaultPerson};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/hotel/', user);
        expect(result.allowed).toBe(false);
        expect(result.error).toBeDefined();
        if (result.error) {
          expect(result.error.message).toEqual('Administrator or hotel user required.');
        }
      });
    });
    describe('to a destination that requires a hotel user from a verifiable logged in hotel user', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(
            {...defaultPerson, isHotel: true}
          );
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is allowed', async () => {
        const user = {...defaultPerson};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/hotel/', user);
        expect(result.allowed).toBe(true);
        expect(result.error).toBeNull();
      });
    });
    describe('to a destination that requires a hotel user from a verifiable logged in administrator', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve({...defaultPerson, isAdministrator: true});
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is allowed', async () => {
        const user = {...defaultPerson};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/hotel/', user);
        expect(result.allowed).toBe(true);
        expect(result.error).toBeNull();
      });
    });
    describe('to a destination that requires a hotel user from a non verifiable logged in user', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(null);
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is denied', async () => {
        const user = {...defaultPerson};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/hotel/', user);
        expect(result.allowed).toBe(false);
        expect(result.error).toBeDefined();
        if (result.error) {
          expect(result.error.message).toEqual('Authentication required.');
        }
      });
    });
  });
});

// @flow

import * as moment from 'moment';

export type ConferenceEdition = {
  id: number,
  conferenceId: number,
  name: string,
  longName: string,
  year: number,
  startDate: moment.Moment,
  endDate: moment.Moment,
  startApplications: moment.Moment,
  endApplications: moment.Moment,
  lotteryDay: moment.Moment,
  lunchPrice: number,
  dinnerPrice: number
}
export type ConferenceDay = {
  id: number,
  conferenceEditionId: number,
  name: string,
  conferenceDayNumber: number,
  fee: number
}
export type ArrivalTime = {
  id: number,
  legacyId: string,
  conferenceEditionId: number,
  conferenceDayId: number,
  name: string,
  hint: string
}
export type DepartureTime = {
  id: number,
  legacyId: string,
  conferenceEditionId: number,
  conferenceDayId: number,
  name: string,
  hint: string,
  hasToPayConferenceDayFee: boolean
}
export type RoomType = {
  id: string,
  conferenceEditionId: number,
  display: string,
  nightPricePerPerson: number,
  numberOfRooms: number,
  peoplePerRoom: number,
  reservedOrganization: number,
  reservedSponsors: number,
  lotterySortOrder: number
}
export type LengthOfStay = {
  id: string,
  arrivalId: number,
  departureId: number,
  from: string,
  to: string,
  nights: number,
  lunches: number,
  dinners: number,
  arrivalDayId: number,
  departureDayId: number,
  hasDepartureDayFee: boolean
}
export type Conference = {
  conferenceId: number,
  editionId: number,
  name: string,
  longName: string,
  year: number,
  startDate: moment.Moment,
  endDate: moment.Moment,
  startApplications: moment.Moment,
  endApplications: moment.Moment,
  lotteryDay: moment.Moment,
  lunchPrice: number,
  dinnerPrice: number,
  days: ConferenceDay[],
  roomTypes: RoomType[],
  arrivals: ArrivalTime[],
  departures: DepartureTime[],
  lengthOfStay: LengthOfStay[]
}
export type StayPrice = {
  roomPrice: number,
  fees: number,
  amount: number,
  display: string,
  arrivalId: number,
  departureId: number,
  legacyArrivalId: string,
  legacyDepartureId: string,
  nights: number
}
export type RoomOption = {
  value: string,
  label: string,
  roomPricePerNight: number,
  prices: StayPrice[]
}

export type ConferenceEditionSponsor = {
  sponsorId: number,
  name: string,
  nameAdditional: string,
  address1: string,
  address2: string,
  province: string,
  postal: string,
  city: string,
  country: string,
  amount: number
}

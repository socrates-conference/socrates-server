// @flow
import Aggregate from './aggregate';
import {EventDispatcher} from './eventDispatcher';
import AuthenticationRepository from '../db/authenticationRepository';
import bcryptjs from 'bcryptjs';
import {sign, verify} from 'jsonwebtoken';
import config from '../config';
import {validateEmail} from '../utilities/Utilities';

export type LoginResult = {
  token: string,
}

export type User = {
  personId: ?number,
  name: string,
  email: string,
  password: string,
  isAdministrator: boolean,
  isHotel: boolean,
  isOneTimePassword: boolean
}
export type AccessCheckResult = {
  allowed: boolean,
  error: ?Error
}

type UserInfo = {
  isAuthenticated: boolean,
  isAdministrator: boolean,
  isHotelUser: boolean
}

export const ONETIME_PASSWORD_CREATED = 'ONETIME_PASSWORD_CREATED';
export const PASSWORD_REVOKED = 'PASSWORD_REVOKED';

const requestNeedsAuthentication = ['/participants'];
const requestNeedsAdministrator = ['/management'];
const requestNeedsHotel = ['/hotel'];

const needsAuthentication = (url: string): boolean => {
  const pattern = new RegExp(requestNeedsAuthentication.join('|'));
  return pattern.test(url.toLowerCase());
};
const needsAdministrator = (url: string): boolean => {
  const pattern = new RegExp(requestNeedsAdministrator.join('|'));
  return pattern.test(url.toLowerCase());
};
const needsHotel = (url: string): boolean => {
  const pattern = new RegExp(requestNeedsHotel.join('|'));
  return pattern.test(url.toLowerCase());
};

export default class Authenticator extends Aggregate {
  _repository: AuthenticationRepository;

  constructor(repository: AuthenticationRepository, dispatcher: EventDispatcher) {
    super(dispatcher);
    this._repository = repository;
  }

  _notifyLoginSuccess(user: User): LoginResult {
    this.dispatch({type: 'USER_LOGGED_IN', payload: user});
    const payload = {
      email: user.email,
      name: user.name,
      isAdministrator: Boolean(user.isAdministrator),
      isHotel: Boolean(user.isHotel),
      isOneTimePassword: Boolean(user.isOneTimePassword)
    };
    const token = sign(payload, config.jwtSecret);

    return {token: token};
  }

  _notifyLoginFailure(email: string): LoginResult {
    this.dispatch({type: 'USER_LOGIN_FAILURE', payload: email});
    return {token: ''};
  }

  _notifyNotAParticipant(email: string): LoginResult {
    this.dispatch({type: 'LOGIN_NOT_ALLOW_WITHOUT_A_CONFERENCE_TICKET', payload: email});
    return {token: ''};
  }

  async login(requestToken: string): Promise<LoginResult> {
    const data: Object = verify(requestToken, config.jwtSecret);
    if (data) {
      if (!validateData(data.email, data.hash)) {
        return this._notifyLoginFailure(data.email);
      }
      const user: ?User = await this._repository.readPersonByEmail(data.email);
      if (user) {
        const hash = data.hash;
        const password = user.password;
        const isPasswordCorrect = bcryptjs.compareSync(hash, password);
        const hasTicket = user.personId
          ? (await this._repository.readRegistrationByPersonId(
            user.personId.toString()) || user.isHotel || user.isAdministrator)
          : false;
        if (isPasswordCorrect && (hasTicket || user.isAdministrator)) {
          return this._notifyLoginSuccess(user);
        } else if (isPasswordCorrect) {
          return this._notifyNotAParticipant(data.email);
        } else {
          return this._notifyLoginFailure(data.email);
        }
      }
      return this._notifyLoginFailure(data.email);
    }
    return this._notifyLoginFailure('invalid request token.');
  }

  async updatePassword(requestToken: string): Promise<LoginResult> {
    const data: Object = verify(requestToken, config.jwtSecret);
    if (data && bcryptjs.compareSync(data.oldPassword, data.oldPasswordHash)) {
      if (!validateData(data.email, data.newPassword)) {
        return this._notifyLoginFailure(data.email);
      }
      const user: ?User = await this._repository.readPersonByEmail(data.email);
      if (user) {
        const newPasswordHash = bcryptjs.hashSync(data.newPassword, 10);
        const updated = await this._repository.updatePassword(data.email, newPasswordHash);
        if (updated) {
          user.isOneTimePassword = false;
          return this._notifyLoginSuccess(user);
        }
      }
      return this._notifyLoginFailure(data.email);
    }
    return this._notifyLoginFailure('invalid request token.');
  }

  async revokePassword(requestToken: string): Promise<void> {
    const data: Object = verify(requestToken, config.jwtSecret);
    if (data) {
      const revoked = await this._repository.updatePassword(data.email, '');
      if (revoked) {
        this.dispatch({type: PASSWORD_REVOKED, payload: data.email});
      } else {
        throw new Error('Revoke password failed.');
      }
    } else {
      throw new Error('invalid request token.');
    }
  }

  async generateOneTimePassword(requestToken: string): Promise<void> {
    const data: Object = verify(requestToken, config.jwtSecret);
    if (data && bcryptjs.compareSync(data.email, data.emailHash)) {
      const user: ?User = await this._repository.readPersonByEmail(data.email);
      if (user) {
        const newPassword = await this._repository.updateWithOneTimePassword(data.email);
        if (newPassword) {
          this.dispatch({type: ONETIME_PASSWORD_CREATED, payload: {user, newPassword}});
          return;
        }
      }
    }
    throw new Error('Generate password failed.');
  }

  async checkAccess(url: string, determinedUser: ?User): Promise<AccessCheckResult> {
    const userInfo = await this.getUserInfoFromUser(determinedUser);
    return this._check(url, userInfo);
  }

  async getUserInfoFromUser(determinedUser: ?User): Promise<UserInfo> {
    let userInfo: UserInfo;
    if (determinedUser) {
      const user: ?User = await this._repository.readPersonByEmail(determinedUser.email);
      userInfo = this._calculateUserInfo(user);
    } else {
      userInfo = {isAuthenticated: false, isAdministrator: false, isHotelUser: false};
    }
    return userInfo;
  }

  // noinspection JSMethodCanBeStatic
  _calculateUserInfo(user: ?User): UserInfo {
    return {
      isAuthenticated: Boolean(user),
      isAdministrator: user ? user.isAdministrator : false,
      isHotelUser: user ? user.isHotel : false
    };
  }

  // noinspection JSMethodCanBeStatic
  _check(url: string, user: UserInfo) {
    const urlRequirements = this._getRequirementsForUrl(url);

    if (urlRequirements.needsUser && !user.isAuthenticated) {
      return {allowed: false, error: new Error('Authentication required.')};
    }
    if (urlRequirements.administrator && !user.isAdministrator) {
      return {allowed: false, error: new Error('Administrator required.')};
    }
    if (urlRequirements.hotel && !(user.isHotelUser || user.isAdministrator)) {
      return {allowed: false, error: new Error('Administrator or hotel user required.')};
    }

    return {allowed: true, error: null};
  }

  // noinspection JSMethodCanBeStatic
  _getRequirementsForUrl(url: string) {
    const authentication = needsAuthentication(url);
    const administrator = needsAdministrator(url);
    const hotel = needsHotel(url);
    return {
      authentication: authentication,
      administrator: administrator,
      hotel: hotel,
      needsUser: authentication || administrator || hotel
    };
  }
}

function isEmpty(variable) {
  return !variable || variable === '';
}

function validateData(email: string, password: string) {
  return !(isEmpty(password) || isEmpty(email) || !validateEmail(email));
}

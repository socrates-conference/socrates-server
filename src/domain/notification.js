// @flow

import Service from './service';
import type {Event, EventDispatcher} from './eventDispatcher';
import Email from '../adapter/email';
import * as fs from 'fs';
import {APPLICATION_ADDED} from './application';
import {INTERESTED_PERSON_CONFIRMED, INTERESTED_PERSON_CREATED} from './newsletter';
import type {RegistrationData} from './management/applicationList';
import {APPLICATION_REGISTERED, APPLICATION_DELETED} from './management/applicationList';
import {humanizeRoomType} from './roomTypes';
import {ONETIME_PASSWORD_CREATED} from './authenticator';
import {REGISTRATION_DELETED, SEND_CONFIRMATION_REMINDER} from './management/registrationList';
import moment from 'moment';
import {PARTICIPANT_CONFIRMED} from './participant';
import {
  INVITATION_ACCEPTED,
  INVITATION_CREATED_BY_PARTICIPANT,
  INVITATION_DECLINED,
  INVITATIONS_CANCELLED_FOR_CANCELLED_PARTICIPANT
} from './invitations';
import type {CancelledInvitation} from './invitations';

const dir = process.cwd();
// Needed for tests, otherwise the fs reads will fail.
if (dir.endsWith('domain')) {
  process.chdir('../../');
}

const APPLICATION_ADDED_BODY = fs.readFileSync('./email/applicationAdded.txt', 'utf-8');
const INTERESTED_PERSON_CREATED_BODY = fs.readFileSync('./email/interestedPersonCreated.txt', 'utf-8');
const INTERESTED_PERSON_CONFIRMED_BODY = fs.readFileSync('./email/interestedPersonConfirmed.txt', 'utf-8');
const APPLICATION_REGISTERED_BODY = fs.readFileSync('./email/applicationRegistered.txt', 'utf-8');
const PASSWORD_GENERATED_BODY = fs.readFileSync('./email/passwordGenerated.txt', 'utf-8');
const REGISTRATION_DELETED_BODY = fs.readFileSync('./email/registrationDeleted.txt', 'utf-8');
const APPLICATION_DELETED_BODY = fs.readFileSync('./email/applicationDeleted.txt', 'utf-8');
const CONFIRMATION_FIRST_REMINDER_BODY = fs.readFileSync('./email/confirmationFirstReminder.txt', 'utf-8');
const CONFIRMATION_SECOND_REMINDER_BODY = fs.readFileSync('./email/confirmationSecondReminder.txt', 'utf-8');
const PARTICIPANT_CONFIRMED_BODY = fs.readFileSync('./email/participantConfirmed.txt', 'utf-8');
const ROOMMATE_CANCELLED_PARTICIPATION_BODY = fs.readFileSync('./email/roommateCancelledParticipation.txt', 'utf-8');
const INVITATION_NO_LONGER_VALID_BODY = fs.readFileSync('./email/invitationNoLongerValid.txt', 'utf-8');
const ROOM_SHARING_INVITATION_BY_PARTICIPANT_BODY =
    fs.readFileSync('./email/sendInvitationFromParticipant.txt', 'utf-8');
process.chdir(dir);
const INVITATION_ACCEPTED_BODY = fs.readFileSync('./email/invitationAccepted.txt', 'utf-8');
const INVITATION_DECLINED_BODY = fs.readFileSync('./email/invitationDeclined.txt', 'utf-8');


export default class Notification extends Service {
    _email: Email;

    constructor(email: Email, eventDispatcher: EventDispatcher) {
      super(eventDispatcher);
      this._email = email;
      eventDispatcher.subscribe(INTERESTED_PERSON_CREATED, this._handleInterestedPersonCreated);
      eventDispatcher.subscribe(INTERESTED_PERSON_CONFIRMED, this._handleInterestedPersonConfirmed);
      eventDispatcher.subscribe(APPLICATION_ADDED, this._handleApplicationAdded);
      eventDispatcher.subscribe(APPLICATION_REGISTERED, this._handleApplicationRegistered);
      eventDispatcher.subscribe(ONETIME_PASSWORD_CREATED, this._handleOneTimePasswordGenerated);
      eventDispatcher.subscribe(REGISTRATION_DELETED, this._handleRegistrationDeleted);
      eventDispatcher.subscribe(SEND_CONFIRMATION_REMINDER, this._handleConfirmationReminder);
      eventDispatcher.subscribe(PARTICIPANT_CONFIRMED, this._handleParticipantConfirmed);
      eventDispatcher.subscribe(INVITATION_CREATED_BY_PARTICIPANT, this._handleInvitationCreatedByParticipant);
      eventDispatcher.subscribe(INVITATION_ACCEPTED, this._handleInvitationAccepted);
      eventDispatcher.subscribe(INVITATION_DECLINED, this._handleInvitationDeclined);
      eventDispatcher.subscribe(APPLICATION_DELETED, this._handleApplicationDeleted);
      eventDispatcher.subscribe(INVITATIONS_CANCELLED_FOR_CANCELLED_PARTICIPANT,
        this._handleInvitationsCancelledForParticipant);
    }

    _handleInterestedPersonConfirmed = (ev: Event) => {
      const {nickname, email} = ev.payload;
      const body = INTERESTED_PERSON_CONFIRMED_BODY
        // eslint-disable-next-line no-template-curly-in-string
        .replace('${name}', nickname)
      // eslint-disable-next-line no-template-curly-in-string
        .replace('${email}', email);
      this._email.send('Newsletter subscription confirmed', body, email)
        .then(result => console.debug('Subscription confirmed notification sent successfully: ', result))
        .catch(error => console.error('Subscription confirmed notification failed to send: %o', error.mailOptions,
          error.error));
    };

    _handleInterestedPersonCreated = (ev: Event) => {
      const {name, email} = ev.payload.person;
      const {consentKey} = ev.payload;
      const body = INTERESTED_PERSON_CREATED_BODY
        // eslint-disable-next-line no-template-curly-in-string
        .replace('${name}', name)
      // eslint-disable-next-line no-template-curly-in-string
        .replace('${email}', email)
      // eslint-disable-next-line no-template-curly-in-string
        .replace('${consentKey}', consentKey);
      this._email.send('Newsletter subscription confirmation', body, email)
        .then(result => console.debug('Subscription notification sent successfully: ', result))
        .catch(error => console.error('Subscription notification failed to send: %o', error.mailOptions, error.error));
    };

    _handleApplicationAdded = (ev: Event) => {
      const {nickname: name, email} = ev.payload;
      const body = APPLICATION_ADDED_BODY
        // eslint-disable-next-line no-template-curly-in-string
        .replace('${name}', name)
      // eslint-disable-next-line no-template-curly-in-string
        .replace('${email}', email);
      this._email.send('You\'re in the Lottery!', body, email)
        .then(result => console.debug('Lottery application notification sent successfully: ', result))
        .catch(error => console.error('Application notification failed to send: %o', error.mailOptions, error.error));
    };

    _handleApplicationRegistered = (ev: Event) => {
      const registrationData: RegistrationData = ev.payload;
      const body = APPLICATION_REGISTERED_BODY
        // eslint-disable-next-line no-template-curly-in-string
        .replace('${name}', registrationData.name)
      // eslint-disable-next-line no-template-curly-in-string
        .replace('${email}', registrationData.email)
      // eslint-disable-next-line no-template-curly-in-string
        .replace('${confirmationKey}', registrationData.confirmationKey)
      // eslint-disable-next-line no-template-curly-in-string
        .replace('${password}',
          (registrationData.oneTimePassword.trim().length > 0 ?
            registrationData.oneTimePassword :
            'Use your password as usual'))
      // eslint-disable-next-line no-template-curly-in-string
        .replace('${room}', humanizeRoomType(registrationData.roomType));
      this._email.send('SoCraTes Unconference. You are in :-)\n', body, registrationData.email)
        .then(result =>
          console.debug('Registration confirmation notification sent successfully: ', result))
        .catch(error =>
          console.error('Registration confirmation notification failed to send: %o', error.mailOptions, error.error));
    };

    _handleOneTimePasswordGenerated = (ev: Event) => {
      const {user, newPassword} = ev.payload;
      const body = PASSWORD_GENERATED_BODY
        // eslint-disable-next-line no-template-curly-in-string
        .replace('${name}', user.name)
      // eslint-disable-next-line no-template-curly-in-string
        .replace('${password}', newPassword);
      this._email.send('SoCraTes Unconference. New password requested', body, user.email)
        .then(result =>
          console.debug('Registration confirmation notification sent successfully: ', result))
        .catch(error =>
          console.error('Registration confirmation notification failed to send: %o', error.mailOptions, error.error));
    };

    _handleRegistrationDeleted = (ev: Event) => {
      const {nickname, email} = ev.payload;
      const body = REGISTRATION_DELETED_BODY
        // eslint-disable-next-line no-template-curly-in-string
        .replace('${name}', nickname);
      this._email.send('SoCraTes Unconference. Registration cancelled.', body, email)
        .then(result =>
          console.debug('Registration cancelled sent successfully: ', result))
        .catch(error =>
          console.error('Registration cancelled failed to send: %o', error.mailOptions, error.error));
    };

    _handleApplicationDeleted = (ev: Event) => {
      const {nickname, email} = ev.payload;
      const body = APPLICATION_DELETED_BODY
        // eslint-disable-next-line no-template-curly-in-string
        .replace('${name}', nickname);
      this._email.send('SoCraTes Unconference. Application cancelled.', body, email)
        .then(result =>
          console.debug('Application cancelled sent successfully: ', result))
        .catch(error =>
          console.error('Application cancelled failed to send: %o', error.mailOptions, error.error));
    };

    _getConfirmationReminderInfo = (payload: Object) => {
      const {name, email, date, confirmationReminderCounter, confirmationLastReminder} = payload;
      let to = null, subject = null, body = null;
      switch (confirmationReminderCounter) {
        case 0:
          to = email;
          subject = 'SoCraTes Registration confirmation reminder';
          // eslint-disable-next-line no-template-curly-in-string
          body = CONFIRMATION_FIRST_REMINDER_BODY.replace('${name}', name)
          // eslint-disable-next-line no-template-curly-in-string
            .replace('${registrationDate}', moment(date).format('MMM Do YYYY'));
          break;
        case 1:
          to = email;
          subject = 'SoCraTes Registration confirmation LAST reminder';
          // eslint-disable-next-line no-template-curly-in-string
          body = CONFIRMATION_SECOND_REMINDER_BODY.replace('${name}', name)
          // eslint-disable-next-line no-template-curly-in-string
            .replace('${registrationDate}', moment(date).format('MMM Do YYYY'))
          // eslint-disable-next-line no-template-curly-in-string
            .replace('${reminderDate}', moment(confirmationLastReminder).format('MMM Do YYYY'))
          // eslint-disable-next-line no-template-curly-in-string
            .replace('${endDate}', moment().add(3, 'days').format('MMM Do YYYY'));
          break;
        default:
          to = 'registration@socrates-conference.de';
          subject = 'Participant has not reacted to confirmation reminders';
          body = 'The Participant ignored the confirmation reminders. Please cancel the participation for: \n' +
                    JSON.stringify(payload);
          break;
      }
      return {to, subject, body};
    };

    _handleConfirmationReminder = (ev: Event) => {
      const {to, subject, body} = this._getConfirmationReminderInfo(ev.payload);
      if (body && to && subject) {
        this._email.send(subject, body, to)
          .then(result =>
            console.debug('Confirmation reminder sent successfully: ', result))
          .catch(error =>
            console.error('Confirmation reminder failed to send: %o', error.mailOptions, error.error));
      }
    };

    _handleParticipantConfirmed = (ev: Event) => {
      const {nickname, email} = ev.payload;
      const body = PARTICIPANT_CONFIRMED_BODY
        // eslint-disable-next-line no-template-curly-in-string
        .replace('${name}', nickname);
      this._email.send('SoCraTes: You have confirmed your participation', body, email)
        .then(result => console.debug('Participation confirmed notification sent successfully: ', result))
        .catch(error => console.error('Participation confirmed notification failed to send: %o', error.mailOptions,
          error.error));
    };


    _handleInvitationCreatedByParticipant = (ev: Event) => {
      const {recipientName, invitee2, invitee1, key} = ev.payload;
      const body = ROOM_SHARING_INVITATION_BY_PARTICIPANT_BODY
        // eslint-disable-next-line no-template-curly-in-string
        .replace('${name}', recipientName)
      // eslint-disable-next-line no-template-curly-in-string
        .replace(/\${invitee1Email}/g, invitee1.email)
      // eslint-disable-next-line no-template-curly-in-string
        .replace(/\${key}/g, key);
      this._email.send('SoCraTes: Room sharing invitation', body, invitee2.email)
        .then(result => console.debug('Room sharing invitation sent successfully: ', result))
        .catch(error => console.error('Room sharing invitation failed to send: %o', error.mailOptions,
          error.error));
    };
    _handleInvitationAccepted = (ev: Event) => {
      const {recipientName, emailToSentTo, emailRoommate} = ev.payload;
      const body = INVITATION_ACCEPTED_BODY
        // eslint-disable-next-line no-template-curly-in-string
        .replace('${name}', recipientName)
      // eslint-disable-next-line no-template-curly-in-string
        .replace(/\${email}/g, emailRoommate);
      this._email.send('SoCraTes: Room sharing invitation accepted', body, emailToSentTo)
        .then(result => console.debug('Room sharing invitation accepted sent successfully: ', result))
        .catch(error => console.error('Room sharing invitation accepted failed to send: %o', error.mailOptions,
          error.error));
    };
    _handleInvitationDeclined = (ev: Event) => {
      const {recipientName, emailToSentTo} = ev.payload;
      const body = INVITATION_DECLINED_BODY
        // eslint-disable-next-line no-template-curly-in-string
        .replace('${name}', recipientName);
      this._email.send('SoCraTes: Room sharing invitation declined', body, emailToSentTo)
        .then(result => console.debug('Room sharing invitation declined sent successfully: ', result))
        .catch(error => console.error('Room sharing invitation declined failed to send: %o', error.mailOptions,
          error.error));
    };

    _handleInvitationsCancelledForParticipant = (ev: Event) => {
      const {cancelledEmail, cancelledInvitations} = ev.payload;
      cancelledInvitations.forEach(invitation => {
        const {email, body, title} = this.getMailDataForInvitationCancelled(invitation, cancelledEmail);
        this._email.send(title, body, email)
          .then(result => console.debug('Room sharing invitation cancelled sent successfully: ', result))
          .catch(error => console.error('Room sharing invitation cancelled failed to send: %o', error.mailOptions,
            error.error));
      });
    };

    getMailDataForInvitationCancelled(invitation: CancelledInvitation, cancelledEmail: string) {
      const {nickname, email, status} = invitation;
      let body;
      let title;
      if (status === 'ACCEPTED') {
        body = ROOMMATE_CANCELLED_PARTICIPATION_BODY;
        title = 'SoCraTes: Your roommate has cancelled their participation.';
      } else {
        body = INVITATION_NO_LONGER_VALID_BODY;
        title = 'SoCraTes: Room sharing invitation is no longer valid.';
      }
      // eslint-disable-next-line no-template-curly-in-string
      body = body.replace('${name}', nickname).replace('${email}', cancelledEmail);
      return {email, body, title};
    }
}

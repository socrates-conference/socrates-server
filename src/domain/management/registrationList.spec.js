import DomainEventDispatcher from '../domainEventDispatcher';
import MySql from '../../db/MySql';
import TestHelper from '../../db/SqlTestHelper';
import RegistrationListRepository from '../../db/registrationList/registrationListRepository';
import RegistrationList from './registrationList';

jest.mock('../../db/registrationList/registrationListRepository');

describe('Registration list should', () => {
  const dispatcher = new DomainEventDispatcher();
  const testHelper = new TestHelper();
  let mySql, registrationList, registrationListRepository;
  beforeEach(() => {
    mySql = new MySql(testHelper.testConfig);
    registrationListRepository = new RegistrationListRepository(mySql);
    registrationList = new RegistrationList(registrationListRepository, dispatcher);
  });

  describe('read all registrations', () => {
    beforeEach(() => {
      registrationListRepository.readAll.mockImplementation(() => {
        return [{
          personId: 4711,
          participantId: 5483,
          date: '2018-03-01T15:00:00.000Z',
          roomType: 'single',
          diversityReason: 'no'
        }];
      });
    });

    afterEach(() => {
      registrationListRepository.readAll.mockReset();
    });

    it('without error', async () => {
      const app = await registrationList.read();
      expect(app).toBeDefined();
      expect(registrationListRepository.readAll.mock.calls.length).toBe(1);
    });

    it('dispatch REGISTRATIONS_READ', async (done) => {
      dispatcher.subscribeOnce('REGISTRATIONS_READ', () => done());
      await registrationList.read();
    });
  });
  describe('throw error when read registrations', () => {
    beforeEach(() => {
      registrationListRepository.readAll.mockImplementation(() => {
        return null;
      });
    });

    afterEach(() => {
      registrationListRepository.readAll.mockReset();
    });

    it('return null', () => {
      registrationList.read().catch((error) => {
        expect(error).toBeDefined();
        expect(error.message).toBe('Read registrations failed.');
      });
    });
  });
  describe('delete registration', () => {
    beforeEach(() => {
      registrationListRepository.deleteRegistration.mockImplementation(() => true);
      registrationListRepository.readPerson.mockImplementation(() => ({id: 4711}));
    });

    afterEach(() => {
      registrationListRepository.deleteRegistration.mockReset();
      registrationListRepository.readPerson.mockReset();
    });

    it('without error', async () => {
      await registrationList.deleteRegistration(4711);
      expect(registrationListRepository.readPerson.mock.calls.length).toBe(1);
      expect(registrationListRepository.deleteRegistration.mock.calls.length).toBe(1);
    });

    it('dispatch REGISTRATION_DELETED', async (done) => {
      dispatcher.subscribeOnce('REGISTRATION_DELETED', () => done());
      await registrationList.deleteRegistration(4711);
    });
  });
  describe('throw on delete', () => {
    const registration = {
      personId: 4711,
      applicantId: 5483,
      date: '2018-03-01T15:00:00.000Z',
      nickname: 'Nickname',
      email: 'email@valid.de',
      roomTypes: ['single', 'bedInDouble', 'juniorShared'],
      diversityReason: 'description'
    };
    let deletePromise, readPromise;
    beforeEach(() => {
      registrationListRepository.deleteRegistration.mockImplementation(() => deletePromise);
      registrationListRepository.readPerson.mockImplementation(() => readPromise);
    });

    afterEach(() => {
      registrationListRepository.deleteRegistration.mockReset();
      registrationListRepository.readPerson.mockReset();
    });

    it('when cannot read person', (done) => {
      readPromise = Promise.resolve(null);
      registrationList.deleteRegistration(registration).catch(err => {
        expect(err.message).toEqual('Delete registration failed. Person not found.');
        expect(registrationListRepository.readPerson.mock.calls.length).toBe(1);
        expect(registrationListRepository.deleteRegistration.mock.calls.length).toBe(0);
        done();
      });
    });
    it('when delete fails', (done) => {
      readPromise = Promise.resolve({id: 4711});
      deletePromise = Promise.resolve(false);
      registrationList.deleteRegistration(registration).catch(err => {
        expect(err.message).toEqual('Delete registration failed.');
        expect(registrationListRepository.readPerson.mock.calls.length).toBe(1);
        expect(registrationListRepository.deleteRegistration.mock.calls.length).toBe(1);
        done();
      });
    });
  });

  describe('send reminder email to person', () => {
    let readRegistrationPromise;
    let updateReminderDataPromise;
    beforeEach(() => {
      registrationListRepository.readRegistration.mockImplementation(() => readRegistrationPromise);
      registrationListRepository.updateReminderData.mockImplementation(() => updateReminderDataPromise);
    });

    afterEach(() => {
      registrationListRepository.readRegistration.mockReset();
      registrationListRepository.updateReminderData.mockReset();
    });

    it('updates registration data without error, when person available and update successful', async () => {
      readRegistrationPromise = Promise.resolve({id: 4711});
      updateReminderDataPromise = Promise.resolve(true);
      await registrationList.sendConfirmationReminder(4711);
      expect(registrationListRepository.readRegistration.mock.calls.length).toBe(1);
      expect(registrationListRepository.updateReminderData.mock.calls.length).toBe(1);
    });

    it('dispatches SEND_CONFIRMATION_REMINDER, when person available and update successful', async (done) => {
      readRegistrationPromise = Promise.resolve({id: 4711});
      updateReminderDataPromise = Promise.resolve(true);
      dispatcher.subscribeOnce('SEND_CONFIRMATION_REMINDER', () => done());
      await registrationList.sendConfirmationReminder(4711);
    });

    it('throws, when person not available', (done) => {
      readRegistrationPromise = Promise.resolve(null);
      updateReminderDataPromise = Promise.resolve(true);
      registrationList.sendConfirmationReminder(4711).catch((error) => {
        expect(error.message).toEqual('Send confirmation reminder failed. Person not found.');
        expect(registrationListRepository.readRegistration.mock.calls.length).toBe(1);
        expect(registrationListRepository.updateReminderData.mock.calls.length).toBe(0);
        done();
      });
    });

    it('dispatches UPDATE_REMINDER_DATA_FAILED, when person not available', (done) => {
      let subscriptionCalled = false;
      readRegistrationPromise = Promise.resolve(null);
      updateReminderDataPromise = Promise.resolve(true);
      dispatcher.subscribeOnce('UPDATE_REMINDER_DATA_FAILED', () => subscriptionCalled = true);
      registrationList.sendConfirmationReminder(4711).catch(() =>
        subscriptionCalled ? done() : done('subscription not called')
      );
    });

    it('throws, when update fails', (done) => {
      readRegistrationPromise = Promise.resolve({id: 4711});
      updateReminderDataPromise = Promise.resolve(false);
      registrationList.sendConfirmationReminder(4711).catch((error) => {
        expect(error.message).toEqual('Update reminder data failed. No email sent.');
        expect(registrationListRepository.readRegistration.mock.calls.length).toBe(1);
        expect(registrationListRepository.updateReminderData.mock.calls.length).toBe(1);
        done();
      });
    });

    it('dispatches UPDATE_REMINDER_DATA_FAILED, when update fails', (done) => {
      let subscriptionCalled = false;
      readRegistrationPromise = Promise.resolve({id: 4711});
      updateReminderDataPromise = Promise.resolve(false);
      dispatcher.subscribeOnce('UPDATE_REMINDER_DATA_FAILED', () => subscriptionCalled = true);
      registrationList.sendConfirmationReminder(4711).catch(() =>
        subscriptionCalled ? done() : done('subscription not called')
      );
    });
  });
  describe('send reminder email to all non confirmed people', () => {
    let readRegistrationsPromise;
    let updateReminderDataPromise;
    beforeEach(() => {
      registrationListRepository.readNonConfirmedRegistrationsToBeReminded
        .mockImplementation(() => readRegistrationsPromise);
      registrationListRepository.updateReminderData.mockImplementation(() => updateReminderDataPromise);
    });

    afterEach(() => {
      registrationListRepository.readNonConfirmedRegistrationsToBeReminded.mockReset();
      registrationListRepository.updateReminderData.mockReset();
    });

    it('updates registration data without error, when person available and update successful', async () => {
      readRegistrationsPromise = Promise.resolve([{id: 4711}, {id: 4712}]);
      updateReminderDataPromise = Promise.resolve(true);
      await registrationList.sendAllConfirmationReminders();
      expect(registrationListRepository.readNonConfirmedRegistrationsToBeReminded.mock.calls.length).toBe(1);
      expect(registrationListRepository.updateReminderData.mock.calls.length).toBe(2);
    });

    it('dispatches SEND_CONFIRMATION_REMINDER, when person available and update successful', async (done) => {
      let subscriptionCalledTimes = 0;
      readRegistrationsPromise = Promise.resolve([{id: 4711}, {id: 4712}]);
      updateReminderDataPromise = Promise.resolve(true);
      dispatcher.subscribe('SEND_CONFIRMATION_REMINDER', () => subscriptionCalledTimes++);
      await registrationList.sendAllConfirmationReminders();
      expect(subscriptionCalledTimes).toBe(2);
      done();
    });

    it('dispatches UPDATE_REMINDER_DATA_FAILED, when update fails', (done) => {
      let subscriptionCalled = false;
      readRegistrationsPromise = Promise.resolve([{id: 4711}]);
      updateReminderDataPromise = Promise.resolve(false);
      dispatcher.subscribe('UPDATE_REMINDER_DATA_FAILED', () => subscriptionCalled = true);
      registrationList.sendAllConfirmationReminders().then(() =>
        subscriptionCalled ? done() : done('subscription not called')
      );
    });
  });

  describe('update room type', () => {
    let updateRoomTypePromise;
    beforeEach(() => {
      registrationListRepository.updateRoomType.mockImplementation(() => updateRoomTypePromise);
    });

    afterEach(() => {
      registrationListRepository.updateRoomType.mockReset();
    });

    it('without error', async () => {
      updateRoomTypePromise = Promise.resolve(true);
      await registrationList.updateRoomType(4711, 'single');
      expect(registrationListRepository.updateRoomType.mock.calls.length).toBe(1);
    });

    it('dispatches UPDATE_ROOM_TYPE_SUCCEED', async (done) => {
      updateRoomTypePromise = Promise.resolve(true);
      dispatcher.subscribeOnce('UPDATE_ROOM_TYPE_SUCCEED', () => done());
      await registrationList.updateRoomType(4711, 'single');
    });

    it('throws, when update fails', (done) => {
      updateRoomTypePromise = Promise.resolve(false);
      registrationList.updateRoomType(4711, 'single').catch((error) => {
        expect(error.message).toEqual('Update room type failed.');
        expect(registrationListRepository.updateRoomType.mock.calls.length).toBe(1);
        done();
      });
    });

    it('dispatches UPDATE_ROOM_TYPE_FAILED, when update fails', (done) => {
      let subscriptionCalled = false;
      updateRoomTypePromise = Promise.resolve(false);
      dispatcher.subscribeOnce('UPDATE_ROOM_TYPE_FAILED', () => subscriptionCalled = true);
      registrationList.updateRoomType(4711, 'single').catch(() =>
        subscriptionCalled ? done() : done('subscription not called')
      );
    });
  });

});

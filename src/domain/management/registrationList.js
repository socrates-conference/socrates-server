// @flow
import Aggregate from '../aggregate';
import {Event, EventDispatcher} from '../eventDispatcher';
import RegistrationListRepository from '../../db/registrationList/registrationListRepository';

export type Registration = {
  id: number,
  personId: number,
  date: Date,
  name: string,
  email: string,
  roomType: string,
  lengthOfStay: string,
  diversityReason: string,
  isConfirmed: boolean
}

export const REGISTRATION_DELETED = 'REGISTRATION_DELETED';
export const SEND_CONFIRMATION_REMINDER = 'SEND_CONFIRMATION_REMINDER';
export const UPDATE_REMINDER_DATA_FAILED = 'UPDATE_REMINDER_DATA_FAILED';
export const UPDATE_ROOM_TYPE_SUCCEED = 'UPDATE_ROOM_TYPE_SUCCEED';
export const LOG_UPDATE_ROOM_TYPE_SUCCEED = 'LOG_UPDATE_ROOM_TYPE_SUCCEED';
export const UPDATE_ROOM_TYPE_FAILED = 'UPDATE_ROOM_TYPE_FAILED';
export const PARTICIPANT_CANCELED = 'PARTICIPANT_CANCELED';

export default class RegistrationList extends Aggregate {
  _registrationListRepository: RegistrationListRepository;

  constructor(registrationListRepository: RegistrationListRepository, dispatcher: EventDispatcher) {
    super(dispatcher);
    dispatcher.subscribe(PARTICIPANT_CANCELED, this._handleParticipantCanceled);
    this._registrationListRepository = registrationListRepository;
  }

  async read(): Promise<Registration[]> {
    const registrations = await this._registrationListRepository.readAll();
    if (registrations) {
      this.dispatch({type: 'REGISTRATIONS_READ', payload: null});
      return registrations;
    } else {
      throw new Error('Read registrations failed.');
    }
  }

  async getRoomTypeByPersonId(personId: number): Promise<string> {
    const registration = await this._registrationListRepository.readRegistration(personId);
    if (registration) {
      return registration.roomType;
    }
    return '';
  }

  async deleteRegistration(personId: number): Promise<void> {
    const person = await this._registrationListRepository.readPerson(personId);
    if (person) {
      const deleted = await this._registrationListRepository.deleteRegistration(personId);
      if (deleted) {
        this.dispatch({type: REGISTRATION_DELETED, payload: person});
      } else {
        throw new Error('Delete registration failed.');
      }
    } else {
      throw new Error('Delete registration failed. Person not found.');
    }
  }

  async sendConfirmationReminder(personId: number): Promise<void> {
    const registration = await this._registrationListRepository.readRegistration(personId);
    if (registration) {
      const updated = await this._registrationListRepository.updateReminderData(personId);
      if (updated) {
        this.dispatch({type: SEND_CONFIRMATION_REMINDER, payload: registration});
      } else {
        this.dispatch({type: UPDATE_REMINDER_DATA_FAILED, payload: registration});
        throw new Error('Update reminder data failed. No email sent.');
      }
    } else {
      this.dispatch({type: UPDATE_REMINDER_DATA_FAILED, payload: registration});
      throw new Error('Send confirmation reminder failed. Person not found.');
    }
  }

  async updateRoomType(personId: number, roomType: string): Promise<void> {
    const oldValue = await this._registrationListRepository.readRegistration(personId);
    const updated = await this._registrationListRepository.updateRoomType(personId, roomType);
    if (updated) {
      const newValue = await this._registrationListRepository.readRegistration(personId);
      this.dispatch({type: UPDATE_ROOM_TYPE_SUCCEED, payload: {personId, roomType}});
      this.dispatch({type: LOG_UPDATE_ROOM_TYPE_SUCCEED, payload:{oldValue: oldValue, newValue: newValue}});
    } else {
      this.dispatch({type: UPDATE_ROOM_TYPE_FAILED, payload: {personId, roomType}});
      throw new Error('Update room type failed.');
    }
  }

  async sendAllConfirmationReminders(): Promise<void> {
    const registrations: ?Array<Object> =
      await this._registrationListRepository.readNonConfirmedRegistrationsToBeReminded();
    if (registrations) {
      registrations.forEach(async (registration) => {
        const updated = await this._registrationListRepository.updateReminderData(registration.personId);
        if (updated) {
          this.dispatch({type: SEND_CONFIRMATION_REMINDER, payload: registration});
        } else {
          this.dispatch({type: UPDATE_REMINDER_DATA_FAILED, payload: registration});
        }
      });
    }
  }

  _handleParticipantCanceled = (ev: Event): void => {
    const personId = ev.payload.id;
    this._registrationListRepository.readPerson(personId).then(async person => {
      if (person) {
        const deleted = await this._registrationListRepository.deleteRegistration(personId);
        if (deleted) {
          this.dispatch({type: REGISTRATION_DELETED, payload: person});
        }
      }
    });
  };
}

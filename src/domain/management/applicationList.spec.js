import DomainEventDispatcher from '../domainEventDispatcher';
import MySql from '../../db/MySql';
import TestHelper from '../../db/SqlTestHelper';
import ApplicationListRepository from '../../db/applicationList/applicationListRepository';
import ApplicationList from './applicationList';

jest.mock('../../db/applicationList/applicationListRepository');

describe('Application list should', () => {
  const dispatcher = new DomainEventDispatcher();
  const testHelper = new TestHelper();
  let mySql, applicationList, applicationListRepository;
  beforeEach(() => {
    mySql = new MySql(testHelper.testConfig);
    applicationListRepository = new ApplicationListRepository(mySql);
    applicationList = new ApplicationList(applicationListRepository, dispatcher);
  });

  describe('read all applications', () => {
    beforeEach(() => {
      applicationListRepository.readAll.mockImplementation(() => {
        return [{
          personId: 4711,
          applicantId: 5483,
          date: '2018-03-01T15:00:00.000Z',
          nickname: 'Nickname',
          email: 'email@valid.de',
          roomTypes: ['single', 'bedInDouble', 'juniorShared'],
          diversityReason: 'description'
        }];
      });
    });

    afterEach(() => {
      applicationListRepository.readAll.mockReset();
    });

    it('without error', async () => {
      const app = await applicationList.read();
      expect(app).toBeDefined();
      expect(applicationListRepository.readAll.mock.calls.length).toBe(1);
    });

    it('dispatch APPLICANT_ADDED', async (done) => {
      dispatcher.subscribeOnce('APPLICANTS_READ', () => done());
      await applicationList.read();
    });
  });
  describe('read all application for single room', () => {
    beforeEach(() => {
      applicationListRepository.readByRoom.mockImplementation(() => {
        return [{
          personId: 4711,
          applicantId: 5483,
          date: '2018-03-01T15:00:00.000Z',
          nickname: 'Nickname',
          email: 'email@valid.de',
          roomTypes: ['single'],
          diversityReason: 'description'
        }];
      });
    });

    afterEach(() => {
      applicationListRepository.readByRoom.mockReset();
    });

    it('without error', async () => {
      const app = await applicationList.readByRoom('single');
      expect(app).toBeDefined();
      expect(applicationListRepository.readByRoom.mock.calls.length).toBe(1);
    });

    it('dispatch APPLICANT_ADDED', async (done) => {
      dispatcher.subscribeOnce('APPLICANTS_READ_BY_ROOM', () => done());
      await applicationList.readByRoom('single');
    });
  });
  describe('throw error when read applications', () => {
    beforeEach(() => {
      applicationListRepository.readAll.mockImplementation(() => {
        return null;
      });
      applicationListRepository.readByRoom.mockImplementation(() => {
        return null;
      });
    });

    afterEach(() => {
      applicationListRepository.readAll.mockReset();
      applicationListRepository.readByRoom.mockReset();
    });

    it('return null', () => {
      applicationList.read().catch((error) => {
        expect(error).toBeDefined();
        expect(error.message).toBe('Read applications failed.');
      });
    });
    it('by room return null', () => {
      applicationList.read().catch((error) => {
        expect(error).toBeDefined();
        expect(error.message).toBe('Read applications failed.');
      });
    });
  });
  describe('update application', () => {
    const application = {
      personId: 4711,
      applicantId: 5483,
      date: '2018-03-01T15:00:00.000Z',
      name: 'Nickname',
      email: 'email@valid.de',
      roomTypes: ['single', 'bedInDouble', 'juniorShared'],
      diversityReason: 'description'
    };
    beforeEach(() => {
      applicationListRepository.updateApplication.mockImplementation(() => {
        return application;
      });
    });

    afterEach(() => {
      applicationListRepository.updateApplication.mockReset();
    });

    it('without error', async () => {
      const app = await applicationList.updateApplication(application);
      expect(app).toEqual(application);
      expect(applicationListRepository.updateApplication.mock.calls.length).toBe(1);
    });

    it('dispatch APPLICATION_UPDATED', async (done) => {
      dispatcher.subscribeOnce('APPLICATION_UPDATED', () => done());
      await applicationList.updateApplication(application);
    });
  });
  describe('throw on update', () => {
    const application = {
      personId: 4711,
      applicantId: 5483,
      date: '2018-03-01T15:00:00.000Z',
      nickname: 'Nickname',
      email: 'email@valid.de',
      roomTypes: ['single', 'bed_in_double', 'junior_shared'],
      diversityReason: 'description'
    };
    beforeEach(() => {
      applicationListRepository.updateApplication.mockImplementation(() => {
        return undefined;
      });
    });

    afterEach(() => {
      applicationListRepository.updateApplication.mockReset();
    });

    it('when update application fails', (done) => {

      applicationList.updateApplication(application).catch(err => {
        expect(err.message).toEqual('Update application failed.');
        expect(applicationListRepository.updateApplication.mock.calls.length).toBe(1);
        done();
      });
    });
  });
  describe('delete application', () => {
    beforeEach(() => {
      applicationListRepository.deleteApplication.mockImplementation(() => true);
      applicationListRepository.readPerson.mockImplementation(() => ({id: 4711}));
    });

    afterEach(() => {
      applicationListRepository.deleteApplication.mockReset();
      applicationListRepository.readPerson.mockReset();
    });

    it('without error', async () => {
      await applicationList.deleteApplication(4711);
      expect(applicationListRepository.readPerson.mock.calls.length).toBe(1);
      expect(applicationListRepository.deleteApplication.mock.calls.length).toBe(1);
    });

    it('dispatch APPLICATION_UPDATED', async (done) => {
      dispatcher.subscribeOnce('APPLICATION_DELETED', () => done());
      await applicationList.deleteApplication(4711);
    });
  });
  describe('throw on delete', () => {
    const application = {
      personId: 4711,
      applicantId: 5483,
      date: '2018-03-01T15:00:00.000Z',
      nickname: 'Nickname',
      email: 'email@valid.de',
      roomTypes: ['single', 'bedInDouble', 'juniorShared'],
      diversityReason: 'description'
    };
    let deletePromise, readPromise;
    beforeEach(() => {
      applicationListRepository.deleteApplication.mockImplementation(() => deletePromise);
      applicationListRepository.readPerson.mockImplementation(() => readPromise);
    });

    afterEach(() => {
      applicationListRepository.deleteApplication.mockReset();
      applicationListRepository.readPerson.mockReset();
    });

    it('when cannot read person', (done) => {
      readPromise = Promise.resolve(null);
      applicationList.deleteApplication(application).catch(err => {
        expect(err.message).toEqual('Delete application failed. Person not found.');
        expect(applicationListRepository.readPerson.mock.calls.length).toBe(1);
        expect(applicationListRepository.deleteApplication.mock.calls.length).toBe(0);
        done();
      });
    });

    it('when delete fails', (done) => {
      readPromise = Promise.resolve({id: 4711});
      deletePromise = Promise.resolve(false);
      applicationList.deleteApplication(application).catch(err => {
        expect(err.message).toEqual('Delete application failed.');
        expect(applicationListRepository.readPerson.mock.calls.length).toBe(1);
        expect(applicationListRepository.deleteApplication.mock.calls.length).toBe(1);
        done();
      });
    });
  });
  describe('register application', () => {
    const application = {
      personId: 4711,
      applicantId: 5483,
      date: '2018-03-01T15:00:00.000Z',
      name: 'Nickname',
      email: 'email@valid.de',
      roomTypes: ['single'],
      diversityReason: 'description'
    };
    beforeEach(() => {
      applicationListRepository.register.mockImplementation(() => {
        return true;
      });
    });

    afterEach(() => {
      applicationListRepository.register.mockReset();
    });

    it('without error', async () => {
      await applicationList.register(application);
      expect(applicationListRepository.register.mock.calls.length).toBe(1);
    });

    it('dispatch APPLICATION_REGISTERED', async (done) => {
      dispatcher.subscribeOnce('APPLICATION_REGISTERED', () => done());
      await applicationList.register(application);
    });
  });
  describe('throw on register', () => {
    const application = {
      personId: 4711,
      applicantId: 5483,
      date: '2018-03-01T15:00:00.000Z',
      nickname: 'Nickname',
      email: 'email@valid.de',
      roomTypes: ['single'],
      diversityReason: 'description'
    };
    beforeEach(() => {
      applicationListRepository.register.mockImplementation(() => {
        return undefined;
      });
    });

    afterEach(() => {
      applicationListRepository.register.mockReset();
    });

    it('when register application fails', (done) => {
      applicationList.register(application).catch(err => {
        expect(err.message).toEqual('Register application failed.');
        expect(applicationListRepository.register.mock.calls.length).toBe(1);
        done();
      });
    });
    it('when application has more than one room type', (done) => {
      application.roomTypes = ['single', 'bedInDouble'];
      applicationList.register(application).catch(err => {
        expect(err.message).toEqual('Register application failed. More than one room type.');
        expect(applicationListRepository.register.mock.calls.length).toBe(0);
        done();
      });
    });
  });
});

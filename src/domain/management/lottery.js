// @flow

import {EventDispatcher} from '../eventDispatcher';
import LotteryRepository from '../../db/lottery/lotteryRepository';
import Aggregate from '../aggregate';

export default class Lottery extends Aggregate {
  _repository: LotteryRepository;

  constructor(repository: LotteryRepository, dispatcher: EventDispatcher) {
    super(dispatcher);
    this._repository = repository;
  }

  readApplicationsByRoomType = async() => {
    const data: Object[] = await this._repository.readApplicationsByRoomType();
    if (data) {
      return data;
    } else {
      throw new Error('Read applications by room type for conference failed.');
    }

  };
  readRegistrationsByRoomType = async (): Promise<Object[]> => {
    const data: Object[] = await this._repository.readRegistrationsByRoomType();
    if (data) {
      return data;
    } else {
      throw new Error('Read registrations by room type for conference failed.');
    }
  };

}
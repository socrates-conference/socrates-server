// @flow
import Aggregate from '../aggregate';
import {EventDispatcher} from '../eventDispatcher';
import DashboardRepository from '../../db/dashboard/dashboardRepository';

export const READ_LENGTHS_OF_STAY_SUCCEEDED = 'READ_LENGTHS_OF_STAY_SUCCEEDED';

export default class Dashboard extends Aggregate {
  _repository: DashboardRepository;

  constructor(repository: DashboardRepository, dispatcher: EventDispatcher) {
    super(dispatcher);
    this._repository = repository;
  }

  readRegistrationsByRoomType = async (): Promise<Object[]> => {
    const data: Object[] = await this._repository.readRegistrationsByRoomType();
    if (data && data.length > 0) {
      this.dispatch({type: READ_LENGTHS_OF_STAY_SUCCEEDED, payload: data});
      return data;
    } else {
      throw new Error('Read registrations by room type for conference failed.');
    }
  };
}

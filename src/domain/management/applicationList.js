// @flow
import Aggregate from '../aggregate';
import {EventDispatcher} from '../eventDispatcher';
import ApplicationListRepository from '../../db/applicationList/applicationListRepository';

export type Application = {
  personId: number,
  applicantId: number,
  date: Date,
  name: string,
  email: string,
  roomTypes: Array<string>,
  diversityReason: string
}

export type RegistrationData = {
  personId: number,
  name: string,
  email: string,
  roomType: string,
  confirmationKey: string,
  confirmationDeadline: string,
  oneTimePassword: string
}

export const APPLICATION_REGISTERED = 'APPLICATION_REGISTERED';
export const APPLICATION_DELETED = 'APPLICATION_DELETED';

export default class ApplicationList extends Aggregate {
  _applicationListRepository: ApplicationListRepository;

  constructor(applicationListRepository: ApplicationListRepository, dispatcher: EventDispatcher) {
    super(dispatcher);
    this._applicationListRepository = applicationListRepository;
  }

  async read(): Promise<Application[]> {
    const applications = await this._applicationListRepository.readAll();
    if (applications) {
      this.dispatch({type: 'APPLICANTS_READ', payload: null});
      return applications;
    } else {
      throw new Error('Read applications failed.');
    }
  }

  async updateApplication(data: Application): Promise<?Application> {
    const application = await this._applicationListRepository.updateApplication(data);
    if (application) {
      this.dispatch({type: 'APPLICATION_UPDATED', payload: data});
      return application;
    } else {
      throw new Error('Update application failed.');
    }
  }

  async deleteApplication(personId: number): Promise<void> {
    const person = await this._applicationListRepository.readPerson(personId);
    if (person) {
      const deleted = await this._applicationListRepository.deleteApplication(personId);
      if (deleted) {
        this.dispatch({type: APPLICATION_DELETED, payload: person});
      } else {
        throw new Error('Delete application failed.');
      }
    } else {
      throw new Error('Delete application failed. Person not found.');
    }
  }

  async register(data: Application): Promise<void> {
    if(data.roomTypes.length === 1) {
      const registration = await this._applicationListRepository.register(data);
      if (registration) {
        this.dispatch({type: 'APPLICATION_REGISTERED', payload: registration});
      } else {
        throw new Error('Register application failed.');
      }
    } else {
      throw new Error('Register application failed. More than one room type.');
    }
  }

  async readByRoom(roomType: string): Promise<Application[]> {
    const applications = await this._applicationListRepository.readByRoom(roomType);
    if (applications) {
      this.dispatch({type: 'APPLICANTS_READ_BY_ROOM', payload: null});
      return applications;
    } else {
      throw new Error('Read applications failed.');
    }
  }
}

import DomainEventDispatcher from '../domainEventDispatcher';
import MySql from '../../db/MySql';
import TestHelper from '../../db/SqlTestHelper';
import LotteryRepository from '../../db/lottery/lotteryRepository';
import Lottery from './lottery';

jest.mock('../../db/lottery/lotteryRepository');

describe('Lottery should', () => {
  const dispatcher = new DomainEventDispatcher();
  const testHelper = new TestHelper();
  let mySql, lottery, lotteryRepository;
  beforeEach(() => {
    mySql = new MySql(testHelper.testConfig);
    lotteryRepository = new LotteryRepository(mySql);
    lottery = new Lottery(lotteryRepository, dispatcher);
  });

  describe('read the applications by room type', () => {
    beforeEach(() => {
      lotteryRepository.readApplicationsByRoomType.mockImplementation(() => {
        return Promise.resolve([{}]);
      });
    });

    afterEach(() => {
      lotteryRepository.readApplicationsByRoomType.mockReset();
    });

    it('without error', async () => {
      const roomTypes = await lottery.readApplicationsByRoomType();
      expect(roomTypes).toBeDefined();
      expect(lotteryRepository.readApplicationsByRoomType.mock.calls.length).toBe(1);
    });
  });
  describe('throw error when read applications by room type', () => {
    beforeEach(() => {
      lotteryRepository.readApplicationsByRoomType.mockImplementation(() => {
        return Promise.resolve(undefined);
      });
    });

    afterEach(() => {
      lotteryRepository.readApplicationsByRoomType.mockReset();
    });

    it('returns undefined', () => {
      lottery.readApplicationsByRoomType()
        .then(() => fail('expected to throw error.'))
        .catch((error) => {
          expect(error).toBeDefined();
          expect(error.message).toBe('Read applications by room type for conference failed.');
        });
    });
  });

  describe('read the registrations by room type', () => {
    beforeEach(() => {
      lotteryRepository.readRegistrationsByRoomType.mockImplementation(() => {
        return Promise.resolve([{}]);
      });
    });

    afterEach(() => {
      lotteryRepository.readRegistrationsByRoomType.mockReset();
    });

    it('without error', async () => {
      const roomTypes = await lottery.readRegistrationsByRoomType();
      expect(roomTypes).toBeDefined();
      expect(lotteryRepository.readRegistrationsByRoomType.mock.calls.length).toBe(1);
    });
  });
  describe('throw error when read registrations by room type', () => {
    beforeEach(() => {
      lotteryRepository.readRegistrationsByRoomType.mockImplementation(() => {
        return Promise.resolve(undefined);
      });
    });

    afterEach(() => {
      lotteryRepository.readRegistrationsByRoomType.mockReset();
    });

    it('returns undefined', () => {
      lottery.readRegistrationsByRoomType()
        .then(() => fail('expected to throw error.'))
        .catch((error) => {
          expect(error).toBeDefined();
          expect(error.message).toBe('Read registrations by room type for conference failed.');
        });
    });
  });
});
import DomainEventDispatcher from '../domainEventDispatcher';
import MySql from '../../db/MySql';
import TestHelper from '../../db/SqlTestHelper';
import Dashboard, {READ_LENGTHS_OF_STAY_SUCCEEDED} from './dashboard';
import DashboardRepository from '../../db/dashboard/dashboardRepository';

jest.mock('../../db/dashboard/dashboardRepository');

describe('Dashboard should', () => {
  const dispatcher = new DomainEventDispatcher();
  const testHelper = new TestHelper();
  let mySql, dashboard, dashboardRepository;
  beforeEach(() => {
    mySql = new MySql(testHelper.testConfig);
    dashboardRepository = new DashboardRepository(mySql);
    dashboard = new Dashboard(dashboardRepository, dispatcher);
  });

  describe('read the registrations by room type data', () => {
    beforeEach(() => {
      dashboardRepository.readRegistrationsByRoomType.mockImplementation(() => {
        return Promise.resolve([{}]);
      });
    });

    afterEach(() => {
      dashboardRepository.readRegistrationsByRoomType.mockReset();
    });

    it('without error', async () => {
      const roomTypes = await dashboard.readRegistrationsByRoomType(2018);
      expect(roomTypes).toBeDefined();
      expect(dashboardRepository.readRegistrationsByRoomType.mock.calls.length).toBe(1);
    });

    it('dispatch READ_ROOM_TYPES_SUCCEEDED', async (done) => {
      dispatcher.subscribeOnce(READ_LENGTHS_OF_STAY_SUCCEEDED, () => done());
      await dashboard.readRegistrationsByRoomType(2018);
    });
  });
  describe('throw error when read registrations by room type', () => {
    beforeEach(() => {
      dashboardRepository.readRegistrationsByRoomType.mockImplementation(() => {
        return Promise.resolve(undefined);
      });
    });

    afterEach(() => {
      dashboardRepository.readRegistrationsByRoomType.mockReset();
    });

    it('returns undefined', () => {
      dashboard.readRegistrationsByRoomType(12018)
        .then(() => fail('expected to throw error.'))
        .catch((error) => {
          expect(error).toBeDefined();
          expect(error.message).toBe('Read registrations by room type for conference failed.');
        });
    });
  });
});
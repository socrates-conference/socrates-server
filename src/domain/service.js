// @flow

import {EventDispatcher} from './eventDispatcher';

export default class Service {
  _dispatcher: EventDispatcher;

  constructor(dispatcher: EventDispatcher) {
    this._dispatcher = dispatcher;
  }
}
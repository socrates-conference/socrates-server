// @flow

import DomainEventDispatcher from './domainEventDispatcher';
import Invitables from './invitables';
import InvitablesMySqlRepository from '../db/invitables/invitablesRepository';
import {PARTICIPANT_CANCELED} from './participant';
import TestHelper from '../db/SqlTestHelper';
import MySql from '../db/MySql';


jest.mock('../db/invitables/invitablesRepository');

const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);
const repository = new InvitablesMySqlRepository(mySql);

const dispatcher = new DomainEventDispatcher();

describe('Invitables:', () => {
  let invitables: Invitables;

  beforeEach(() => {

    invitables = new Invitables(repository, dispatcher);
  });

  describe('on marketplace participant check', () => {
    let promise;
    beforeEach(() => {
      repository.readInvitableByEmail.mockImplementation(() => promise);
    });
    afterEach(() => {
      repository.readInvitableByEmail.mockReset();
    });
    it('returns true if participant takes part in it', async () => {
      promise = Promise.resolve({});
      const isInMarketplace = await invitables.isEmailInMarketplace('test@test.de');
      expect(isInMarketplace).toBe(true);
    });
    it('returns false if participant does not take part in it', async () => {
      promise = Promise.resolve(null);
      const isInMarketplace = await invitables.isEmailInMarketplace('another@test.de');
      expect(isInMarketplace).toBe(false);

    });
  });

  describe('on reading roommate candidates for a participant', () => {
    let getRoomTypeForEmailResponse;
    let getInvitablesForEmailResponse;
    beforeEach(() => {
      repository.getRoomTypeForEmail.mockImplementation(() => getRoomTypeForEmailResponse);
      repository.getInvitablesForEmail.mockImplementation(() => getInvitablesForEmailResponse);
    });
    afterEach(() => {
      repository.getRoomTypeForEmail.mockReset();
      repository.getInvitablesForEmail.mockReset();
    });
    it('returns marketplace participants with same room type and no pending invitations', async () => {
      getRoomTypeForEmailResponse = Promise.resolve('bedInDouble');
      getInvitablesForEmailResponse = Promise.resolve([{}]);
      const results = await invitables.roommateCandidatesForEmail('test@test.de', '5');
      expect(results.length).toBe(1);
    });
  });
  describe('on adding participant to marketplace', () => {
    let getRoomTypeForEmailResponse;
    let addInvitableResponse;
    beforeEach(() => {
      repository.getRoomTypeForEmail.mockImplementation(() => getRoomTypeForEmailResponse);
      repository.addInvitable.mockImplementation(() => addInvitableResponse);
    });
    afterEach(() => {
      repository.getRoomTypeForEmail.mockReset();
      repository.addInvitable.mockReset();
    });
    it('returns true if added', async () => {
      getRoomTypeForEmailResponse = Promise.resolve('bedInDouble');
      addInvitableResponse = Promise.resolve({});
      const successful = await invitables.addInvitable({
        id: 0,
        userName: 'full_name', email: 'test@test.de',
        gender: 'gender', roomType: 'bedInDouble',
        introduction: 'introduction'
      });
      expect(successful).toBe(true);
    });
  });
  describe('on participation cancelled event', () => {
    let deleteByEmailReponse;
    beforeEach(() => {
      repository.deleteByMail.mockImplementation(() => deleteByEmailReponse);
    });
    afterEach(() => {
      repository.deleteByMail.mockReset();
    });
    it('removes the participant from the marketplace', () => {
      deleteByEmailReponse = Promise.resolve({});
      dispatcher.dispatchEvent({type: PARTICIPANT_CANCELED, payload: {id: 42, email: 'test2@test.de'}});
      expect(repository.deleteByMail).toBeCalledWith('test2@test.de');
    });
  });
});

// @flow


import Aggregate from './aggregate';
import type {InvitablesRepository} from '../db/invitables/invitablesRepository';
import type {Event, EventDispatcher} from './eventDispatcher';
import {PARTICIPANT_CANCELED} from './participant';

export type Invitable = {
    id: number,
    email: string,
    userName: string,
    gender: string,
    introduction: string,
    roomType: string
};

export const ALLOW_INVITATION = 'ALLOW_INVITATION';
export const DENY_INVITATION = 'DENY_INVITATION';

export default class Invitables extends Aggregate {
    _repository: InvitablesRepository;

    constructor(repository: InvitablesRepository, dispatcher: EventDispatcher) {
      super(dispatcher);
      this._repository = repository;
      dispatcher.subscribe(PARTICIPANT_CANCELED, this._handleParticipationCanceled);
    }

    async addInvitable(invitable: Invitable): Promise<boolean> {
      try {
        const roomType = await this._repository.getRoomTypeForEmail(invitable.email);
        if (roomType) {
          const result = await this._repository.addInvitable({...invitable, roomType});
          return Boolean(result);
        }
        return false;
      } catch (e) {
        console.log(e);
        return false;
      }
    }

    async roommateCandidatesForEmail(email: string, departure: string): Promise<Array<Invitable>> {
      const roomType = await this._repository.getRoomTypeForEmail(email);
      if (roomType) {
        return await this._repository.getInvitablesForEmail(email, roomType, departure);
      }
      return [];
    }

    async isEmailInMarketplace(email: string): Promise<boolean> {
      const invitable = await this._repository.readInvitableByEmail(email);
      return Boolean(invitable);
    }

    _handleParticipationCanceled = (ev: Event) => {
      this._repository.deleteByMail(ev.payload.email).catch(console.error);
    };
}

// @flow
import DomainEventDispatcher from './domainEventDispatcher';
import MySql from '../db/MySql';
import TestHelper from '../db/SqlTestHelper';
import ConferenceEditionRepository from '../db/conference/conferenceEditionRepository';
import Conferences from './conferences';
import type {Conference, ConferenceEditionSponsor, RoomOption} from './conferenceTypes';
import {
  conferenceArrivalsStubbed,
  conferenceDaysStubbed, conferenceDeparturesStubbed,
  conferenceEditionStubbed, conferenceLengthOfStayStubbed, conferenceRoomTypesStubbed
} from '../db/conference/conferenceTestStubs';

jest.mock('../db/conference/conferenceEditionRepository');


describe('Conferences should', () => {
  const dispatcher = new DomainEventDispatcher();
  const testHelper = new TestHelper();
  let mySql, conference: Conferences, repository: ConferenceEditionRepository;
  beforeEach(() => {
    mySql = new MySql(testHelper.testConfig);
    repository = new ConferenceEditionRepository(mySql);
    conference = new Conferences(repository, dispatcher);
    repository.readConferenceEdition.mockImplementation(() => {
      return Promise.resolve(conferenceEditionStubbed);
    });
    repository.readConferenceDays.mockImplementation(() => {
      return Promise.resolve(conferenceDaysStubbed);
    });
    repository.readConferenceArrivalTimes.mockImplementation(() => {
      return Promise.resolve(conferenceArrivalsStubbed);
    });
    repository.readConferenceDepartureTimes.mockImplementation(() => {
      return Promise.resolve(conferenceDeparturesStubbed);
    });
    repository.readConferenceRoomTypes.mockImplementation(() => {
      return Promise.resolve(conferenceRoomTypesStubbed);
    });
    repository.readConferenceLengthsOfStay.mockImplementation(() => {
      return Promise.resolve(conferenceLengthOfStayStubbed);
    });
  });

  afterEach(() => {
    repository.readConferenceEdition.mockReset();
    repository.readConferenceDays.mockReset();
    repository.readConferenceArrivalTimes.mockReset();
    repository.readConferenceDepartureTimes.mockReset();
    repository.readConferenceRoomTypes.mockReset();
    repository.readConferenceLengthsOfStay.mockReset();
  });

  describe('read conference data', () => {
    it('without error', async () => {
      const conferenceData: ?Conference = await conference.readConferenceData(1);
      expect(conferenceData).toBeDefined();
      expect(conferenceData).not.toBeNull();
      if (conferenceData) {
        expect(conferenceData.conferenceId).toBe(2);
        expect(conferenceData.editionId).toBe(3);
        expect(conferenceData.year).toBe(2019);
        expect(conferenceData.days).toEqual(conferenceDaysStubbed);
        expect(conferenceData.arrivals).toEqual(conferenceArrivalsStubbed);
        expect(conferenceData.departures).toEqual(conferenceDeparturesStubbed);
        expect(conferenceData.roomTypes).toEqual(conferenceRoomTypesStubbed);
        expect(conferenceData.lengthOfStay).toEqual(conferenceLengthOfStayStubbed);
      }
      expect(repository.readConferenceEdition.mock.calls.length).toBe(1);
      expect(repository.readConferenceDays.mock.calls.length).toBe(1);
      expect(repository.readConferenceArrivalTimes.mock.calls.length).toBe(1);
      expect(repository.readConferenceDepartureTimes.mock.calls.length).toBe(1);
      expect(repository.readConferenceRoomTypes.mock.calls.length).toBe(1);
      expect(repository.readConferenceLengthsOfStay.mock.calls.length).toBe(1);
    });
  });
  describe('get room options', () => {
    it('without error', async () => {
      const roomOptions: RoomOption[] = await conference.getRoomOptions(1);
      expect(roomOptions).toBeDefined();
      expect(roomOptions).toHaveLength(2);
      const first = roomOptions[0];
      expect(first.value).toEqual('single');
      expect(first.label).toEqual('Single');

      const second = roomOptions[1];
      expect(second.value).toEqual('shared');
      expect(second.label).toEqual('Shared');
    });
    it('with price calculation for each length of stay', async () => {
      const roomOptions: RoomOption[] = await conference.getRoomOptions(1);
      const first = roomOptions[0];
      expect(first.prices).toHaveLength(2);
    });
    it('room prices are calculated correctly (1)', async () => {
      const roomOptions: RoomOption[] = await conference.getRoomOptions(1);
      const prices = roomOptions[0].prices;
      expect(prices[1].display).toEqual('Thursday afternoon - Sunday morning');
      expect(prices[1].amount).toBe(500);
      expect(prices[1].fees).toBe(275);
      expect(prices[1].roomPrice).toBe(220);
    });
    it('room prices are calculated correctly (2)', async () => {
      const roomOptions: RoomOption[] = await conference.getRoomOptions(1);
      const prices = roomOptions[1].prices;
      expect(prices[0].display).toEqual('Thursday afternoon - Saturday evening');
      expect(prices[0].amount).toBe(385);
      expect(prices[0].fees).toBe(260);
      expect(prices[0].roomPrice).toBe(120);
    });
  });
  describe('read arrivals and departures', () => {
    it('without error', async () => {
      const arrivalsAndDepartures = await conference.readArrivalsAnDeparturesHints(1);
      expect(arrivalsAndDepartures).toEqual(
        {
          arrivals: conferenceArrivalsStubbed,
          departures: conferenceDeparturesStubbed
        });
    });
  });
  describe('get conference days', () => {
    it('without error', async () => {
      const conferenceDays = await conference.readConferenceDays(1);
      expect(conferenceDays).toEqual(conferenceDaysStubbed);
    });
  });
  describe('get sponsors for hotel app', () => {
    const sponsors: ConferenceEditionSponsor[] = [
      {
        sponsorId: 21, name: 'sponsor 1', nameAdditional: 'name additional 1', address1: 'sponsor 1 address 1',
        address2: 'sponsor1 address 2', province: 'sponsor 1 province', postal: 'sponsor 1 postal',
        city: 'sponsor 1 city', country: 'sponsor 1 country', amount: 1000
      },
      {
        sponsorId: 22, name: 'sponsor 2', nameAdditional: 'name additional 2', address1: 'sponsor2 address 1',
        address2: 'sponsor 2 address 2', province: 'sponsor 2 province', postal: 'sponsor 2 postal',
        city: 'sponsor 2 city', country: 'sponsor 2 country', amount: 2000
      }
    ];
    beforeEach(() => {
      repository.readSponsorsForHotel.mockImplementation(() => {
        return Promise.resolve(sponsors);
      });
    });
    afterEach(() => {
      repository.readSponsorsForHotel.mockReset();
    });
    it('without error', async () => {
      const sponsorsResult = await conference.readSponsorsForHotel(1);
      expect(sponsorsResult).toEqual(sponsors);
    });
  });
  describe('get sponsors for hotel app', () => {
    const sponsors: ConferenceEditionSponsor[] = [
      {
        sponsorId: 21, name: 'sponsor 1', nameAdditional: 'name additional 1', address1: 'sponsor 1 address 1',
        address2: 'sponsor1 address 2', province: 'sponsor 1 province', postal: 'sponsor 1 postal',
        city: 'sponsor 1 city', country: 'sponsor 1 country', amount: 1000
      },
      {
        sponsorId: 22, name: 'sponsor 2', nameAdditional: 'name additional 2', address1: 'sponsor2 address 1',
        address2: 'sponsor 2 address 2', province: 'sponsor 2 province', postal: 'sponsor 2 postal',
        city: 'sponsor 2 city', country: 'sponsor 2 country', amount: 2000
      }
    ];
    beforeEach(() => {
      repository.readSponsorsForHotel.mockImplementation(() => {
        return Promise.resolve(sponsors);
      });
    });
    afterEach(() => {
      repository.readSponsorsForHotel.mockReset();
    });
    it('without error', async () => {
      const sponsorsResult = await conference.readSponsorsForHotel(1);
      expect(sponsorsResult).toEqual(sponsors);
    });
  });
});

/* eslint-disable max-nested-callbacks */
import DomainEventDispatcher from './domainEventDispatcher';
import Application, {APPLICATION_ADDED} from './application';
import ApplicationRepository from '../db/application/applicationRepository';

jest.mock('../db/application/applicationRepository');

const dispatcher = new DomainEventDispatcher();
const repo = new ApplicationRepository();

describe('Application:', () => {
  let application;
  let applicationData;
  describe('when adding an application', () => {
    describe('and values are valid', () => {

      beforeEach(() => {

        applicationData = {
          id: 0,
          personId: 47,
          nickname: 'whatever',
          email: 'test@test.de',
          roomTypes: ['single', 'double'],
          diversityReason: 'No'
        };
        repo.create.mockImplementation(() => Promise.resolve({...applicationData, id: 1}));
        application = new Application(repo, dispatcher);
      });

      afterEach(() => {
        repo.create.mockReset();
      });
      describe('and no previous applications exist', () => {
        it('should return the application with new id', async () => {
          const returnedApplication = await application.add(applicationData);
          expect(returnedApplication.id).toEqual(1);
          expect(returnedApplication.nickname).toEqual(applicationData.nickname);
          expect(returnedApplication.email).toEqual(applicationData.email);
        });

        it(`should dispatch ${APPLICATION_ADDED} event`, done => {
          dispatcher.subscribeOnce(APPLICATION_ADDED, () => done());
          application.add(applicationData);
        });
      });

      describe('and a previous application exists', () => {
        beforeEach(() => repo
          .existsPreviousApplication.mockImplementation(() => true));

        afterEach(() => repo.existsPreviousApplication.mockReset());

        it('should throw ALREADY_APPLIED', async () => {
          await application
            .add(applicationData)
            .then(() => fail('Should have thrown an Error'))
            .catch(e => expect(e.message).toEqual('ALREADY_APPLIED'));
        });
      });
    });

    describe('and values are invalid', () => {
      it('should throw a validation error', async () => {
        // $FlowFixMe
        await application.add({...applicationData, nickname: null})
          .then(() => fail('whoops'))
          .catch(e => expect(e).toBeDefined());
        await application.add({...applicationData, nickname: ''})
          .then(() => fail('whoops'))
          .catch(e => expect(e).toBeDefined());
        // $FlowFixMe
        await application.add({...applicationData, email: null})
          .then(() => fail('whoops'))
          .catch(e => expect(e).toBeDefined());
        await application.add({...applicationData, email: ''})
          .then(() => fail('whoops'))
          .catch(e => expect(e).toBeDefined());
        await application.add({...applicationData, email: 'invalid'})
          .then(() => fail('whoops'))
          .catch(e => expect(e).toBeDefined());
        // $FlowFixMe
        await application.add({...applicationData, diversityReason: null})
          .then(() => fail('whoops'))
          .catch(e => expect(e).toBeDefined());
        await application.add({...applicationData, diversityReason: ''})
          .then(() => fail('whoops'))
          .catch(e => expect(e).toBeDefined());
        // $FlowFixMe
        await application.add({...applicationData, roomTypes: null})
          .then(() => fail('whoops'))
          .catch(e => expect(e).toBeDefined());
        await application.add({...applicationData, roomTypes: []})
          .then(() => fail('whoops'))
          .catch(e => expect(e).toBeDefined());
      });
    });
  });
});

// @flow
export interface Event {
  type: string;
  payload: any;
}

export type Listener = Event => void;

export interface EventDispatcher {
  // noinspection JSUnusedLocalSymbols
  dispatchEvent(event: Event): void;
  // noinspection JSUnusedLocalSymbols
  subscribeOnce(type: string, handler: Listener): void; // noinspection JSUnusedLocalSymbols
  // noinspection JSUnusedLocalSymbols
  subscribe(type: string, handler: Listener): void;
}

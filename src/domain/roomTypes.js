// @flow

export function humanizeRoomType(type: string) {
  switch (type) {
    case 'single':
      return 'single';
    case 'bedInDouble':
      return 'bed in a double';
    case 'juniorExclusively':
      return 'junior (exclusively)';
    case 'juniorShared':
      return 'junior (shared)';
    default:
      return '';
  }
}

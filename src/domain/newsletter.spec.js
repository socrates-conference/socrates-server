// @flow

import Newsletter from './newsletter';
import DomainEventDispatcher from './domainEventDispatcher';
import NewsletterRepository from '../db/newsletterRepository';
import MySql from '../db/MySql';

jest.mock('../db/MySql');
import TestHelper from '../db/SqlTestHelper';
import type {NewsletterSubscriber} from './newsletter';

describe('Newsletter should', () => {
  const dispatcher = new DomainEventDispatcher();
  const testHelper = new TestHelper();
  let mySql, newsletter;
  beforeEach(() => {
    mySql = new MySql(testHelper.testConfig);
    newsletter = new Newsletter(new NewsletterRepository(mySql), dispatcher);
  });

  describe('when signing up', () => {
    beforeEach(() => {
      mySql.insert.mockImplementation(() => {
        return Promise.resolve({
          numberOfRowsAffected: 1,
          lastInsertedId: 4711
        });
      });
    });
    afterEach(() => {
      mySql.insert.mockReset();
    });
    describe('and name and email are provided', () => {
      it('create an interested person', () => {
        return newsletter.signup('test', 'test@test.org')
          .then((person: ?NewsletterSubscriber) => {
            expect(person).toBeDefined();
            expect(mySql.insert.mock.calls.length).toBe(1);
          });
      });

      it('dispatch INTERESTED_PERSON_CREATED', async (done) => {
        dispatcher.subscribeOnce('INTERESTED_PERSON_CREATED', () => done());
        await newsletter.signup('test', 'test@test.org');
      });
    });

    describe('and input is invalid', () => {
      it('throw if name is empty', () => {
        return newsletter.signup('', 'name@domain.de')
          .then(() => {
            throw new Error('Promise should not resolve.');
          })
          .catch((err: Error) => {
            expect(err.message).toEqual('The information provided is not valid: The name is missing.');
            expect(mySql.insert.mock.calls.length).toBe(0);
          });
      });

      it('throw if email is empty', () => {
        return newsletter.signup('Name', '')
          .then(() => {
            throw new Error('Promise should not resolve.');
          })
          .catch((err: Error) => {
            expect(err.message).toEqual('The information provided is not valid: The email address is missing.');
            expect(mySql.insert.mock.calls.length).toBe(0);
          });
      });

      it('throw if email is invalid', () => {
        return newsletter.signup('Name', 'invalid email')
          .then(() => {
            throw new Error('Promise should not resolve.');
          })
          .catch((err: Error) => {
            expect(err.message).toEqual('The information provided is not valid: The email address is malformed.');
            expect(mySql.insert.mock.calls.length).toBe(0);
          });
      });
    });
  });
  describe('when confirming', () => {
    describe('and valid consentKey', () => {
      beforeEach(() => {
        mySql.update.mockImplementation(() => {
          return Promise.resolve({
            numberOfRowsAffected: 1,
            lastInsertedId: 0
          });
        });
        mySql.selectWithParams.mockImplementation(() => {
          return Promise.resolve([{id:4711}]);
        });
      });
      afterEach(() => {
        mySql.update.mockReset();
        mySql.selectWithParams.mockReset();
      });
      it('create an interested person', () => {
        return newsletter.confirm('some key')
          .then((success: boolean) => {
            expect(success).toBe(true);
            expect(mySql.update.mock.calls.length).toBe(1);
            expect(mySql.selectWithParams.mock.calls.length).toBe(1);
          });
      });

      it('dispatch INTERESTED_PERSON_CONFIRMED', async (done) => {
        dispatcher.subscribeOnce('INTERESTED_PERSON_CONFIRMED', () => done());
        await newsletter.confirm('some key');
      });
    });

    describe('and consentKey is invalid', () => {
      beforeEach(() => {
        mySql.update.mockImplementation(() => {
          return Promise.resolve({
            numberOfRowsAffected: 0,
            lastInsertedId: 0
          });
        });
        mySql.selectWithParams.mockImplementation(() => {
          return Promise.resolve([]);
        });
      });
      afterEach(() => {
        mySql.update.mockReset();
        mySql.selectWithParams.mockReset();
      });
      it('throw when there is no person for the consentKey', () => {
        return newsletter.confirm('some key')
          .then(() => {
            throw new Error('Promise should not resolve.');
          })
          .catch((err: Error) => {
            expect(err.message).toEqual('Confirmation failed. Interested person could not be found.');
            expect(mySql.update.mock.calls.length).toBe(0);
          });
      });

      it('throw if consentKey is empty', () => {
        return newsletter.confirm('')
          .then(() => {
            throw new Error('Promise should not resolve.');
          })
          .catch((err: Error) => {
            expect(err.message).toEqual('The information provided is not valid. The consent key is missing.');
            expect(mySql.update.mock.calls.length).toBe(0);
          });
      });
    });
  });
  describe('when unsubscribe', () => {
    describe('and name and email are provided', () => {
      beforeEach(() => {
        mySql.update.mockImplementation(() => {
          return Promise.resolve({
            numberOfRowsAffected: 1,
            lastInsertedId: 0
          });
        });
      });
      afterEach(() => {
        mySql.update.mockReset();
      });
      it('create an interested person', () => {
        return newsletter.unsubscribe('test@test.org')
          .then((email: string) => {
            expect(email).toBeDefined();
            expect(email).toEqual('test@test.org');
            expect(mySql.update.mock.calls.length).toBe(1);
          });
      });

      it('dispatch INTERESTED_PERSON_REMOVED', async (done) => {
        dispatcher.subscribeOnce('INTERESTED_PERSON_REMOVED', () => done());
        await newsletter.unsubscribe('test@test.org');
      });
    });
  });
});
// @flow


import Aggregate from './aggregate';
import type {EventDispatcher} from './eventDispatcher';
import DataChangesRepository from '../db/dataChanges/dataChangesRepository';

export default class DataChanges extends Aggregate {
    _repository: DataChangesRepository;

    constructor(repository: DataChangesRepository, dispatcher: EventDispatcher) {
      super(dispatcher);
      this._repository = repository;
    }

    async readChangesFrom(dateFrom: string): Promise<Array<Object>> {
      return this._repository.readChangesFrom(dateFrom);
    }
}

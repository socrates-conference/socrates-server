// @flow
import App from './app';
import axios from 'axios';
import MySql from './db/MySql';
import type Config from './config';
import DomainEventDispatcher from './domain/domainEventDispatcher';
import type {EventDispatcher} from './domain/eventDispatcher';
import defaultHeaders from './routes/defaultHeaders';
import bodyParsers from './routes/bodyParsers';
import authenticationMiddleware from './routes/authenticationMiddleware';
import createApplicationListServer from './routes/management/applicationListServer';
import * as bcryptjs from 'bcryptjs';
import jwt from 'jsonwebtoken';
import config from './config';

jest.mock('./db/MySql');

describe('Authentication:', () => {
  let app: App, dispatcher: EventDispatcher;
  let selectWithParamsPromise, updatePromise;
  const defaultConfiguration: Config = {
    'environment': 'test',
    'database': {
      'host': 'localhost',
      'port': 3306,
      'name': 'socrates',
      'user': '',
      'password': ''
    },
    'server': {
      'port': 9998
    }
  };
  const sqlHandler = new MySql(defaultConfiguration);
  sqlHandler.initialize();

  beforeAll(async () => {
    dispatcher = new DomainEventDispatcher();
    app = new App(defaultConfiguration);
    app.applyMiddleware(bodyParsers);
    app.applyMiddleware(defaultHeaders);
    app.applyMiddleware(authenticationMiddleware(sqlHandler, dispatcher));
    app.applyMiddleware(createApplicationListServer(sqlHandler, dispatcher));
    await app.run();
  });

  afterAll(async () => {
    if (app) {
      await app.stop();
    }
  });

  describe('Authentication middleware:', () => {
    describe('post request to login', () => {
      beforeEach(() => {
        sqlHandler.selectWithParams.mockImplementation(() => selectWithParamsPromise);
      });

      afterEach(() => {
        sqlHandler.selectWithParams.mockReset();
      });

      it('returns 200 status with empty token on error', (done) => {
        const requestToken = jwt.sign({email: 'name@domain.de', hash: 'password'}, config.jwtSecret);
        selectWithParamsPromise = Promise.resolve([{
          name: 'name',
          email: 'name@domain.de',
          password: 'someHash',
          isAdministrator: true
        }]);
        axios.post('http://localhost:9998/api/v1/login', {requestToken}).then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toEqual({token: ''});
          done();
        });
      });
    });
    describe('post request to update password', () => {
      beforeEach(() => {
        sqlHandler.selectWithParams.mockImplementation(() => selectWithParamsPromise);
        sqlHandler.update.mockImplementation(() => updatePromise);

      });

      afterEach(() => {
        sqlHandler.selectWithParams.mockReset();
        sqlHandler.update.mockReset();
      });

      it('return status 200 and a generated token on success', (done) => {
        const requestToken = jwt.sign({
          email: 'name@domain.de',
          oldPassword: 'password',
          oldPasswordHash: bcryptjs.hashSync('password', 10),
          newPassword: 'newPassword'
        }, config.jwtSecret);
        selectWithParamsPromise = Promise.resolve([{
          name: 'name',
          email: 'name@domain.de',
          password: '$2a$10$nrapppt.y2T25Ier4E5RYeLJZOT78Ajj6BzvBljibhbqWl7es3izK',
          isAdministrator: true,
          isOneTimePassword: true
        }]);
        updatePromise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
        axios.post('http://localhost:9998/api/v1/update-password', {requestToken}).then(response => {
          expect(response.status).toBe(200);
          expect(response.data.token !== '').toBe(true);
          done();
        });
      });

      it('returns 200 status with empty token on password error', (done) => {
        const requestToken = jwt.sign({
          email: 'name@domain.de',
          oldPassword: 'password',
          oldPasswordHash: bcryptjs.hashSync('other', 10),
          newPassword: 'newPassword'
        }, config.jwtSecret);
        selectWithParamsPromise = Promise.resolve([{
          name: 'name',
          email: 'name@domain.de',
          password: 'someHash',
          isAdministrator: true
        }]);
        axios.post('http://localhost:9998/api/v1/update-password', {requestToken}).then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toEqual({token: ''});
          done();
        });
      });

      it('returns 200 status with empty token on user error', (done) => {
        const requestToken = jwt.sign({
          email: 'name@domain.de',
          oldPassword: 'password',
          oldPasswordHash: bcryptjs.hashSync('password', 10),
          newPassword: 'newPassword'
        }, config.jwtSecret);
        selectWithParamsPromise = Promise.resolve([]);
        axios.post('http://localhost:9998/api/v1/update-password', {requestToken}).then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toEqual({token: ''});
          done();
        });
      });
    });
    describe('post request to revoke password', () => {
      beforeEach(() => {
        sqlHandler.update.mockImplementation(() => updatePromise);

      });

      afterEach(() => {
        sqlHandler.update.mockReset();
      });

      it('return status 200 on success', (done) => {
        const requestToken = jwt.sign({
          email: 'name@domain.de'
        }, config.jwtSecret);
        updatePromise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
        axios.post('http://localhost:9998/api/v1/revoke-password', {requestToken}).then(response => {
          expect(response.status).toBe(200);
          done();
        });
      });

      it('returns 500 status on error', (done) => {
        const requestToken = jwt.sign({
          email: 'name@domain.de'
        }, config.jwtSecret);
        updatePromise = Promise.resolve({numberOfRowsAffected: 0, lastInsertedId: 0});
        axios.post('http://localhost:9998/api/v1/revoke-password', {requestToken}).catch(err => {
          expect(err.response.status).toBe(500);
          done();
        });
      });

    });
    describe('post request to generate password', () => {
      const requestToken = jwt.sign({
        email: 'name@domain.de',
        emailHash: bcryptjs.hashSync('name@domain.de', 10)
      }, config.jwtSecret);

      beforeEach(() => {
        sqlHandler.selectWithParams.mockImplementation(() => selectWithParamsPromise);
        sqlHandler.update.mockImplementation(() => updatePromise);

      });

      afterEach(() => {
        sqlHandler.selectWithParams.mockReset();
        sqlHandler.update.mockReset();
      });

      it('return status 200 on success', (done) => {
        selectWithParamsPromise = Promise.resolve([{
          name: 'name',
          email: 'name@domain.de',
          password: '',
          isAdministrator: false,
          isOneTimePassword: false
        }]);
        updatePromise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
        axios.post('http://localhost:9998/api/v1/generate-password', {requestToken}).then(response => {
          expect(response.status).toBe(200);
          done();
        });
      });

      it('returns 500 status on update error', (done) => {
        selectWithParamsPromise = Promise.resolve([{
          name: 'name',
          email: 'name@domain.de',
          password: '',
          isAdministrator: false,
          isOneTimePassword: false
        }]);
        updatePromise = Promise.resolve({numberOfRowsAffected: 0, lastInsertedId: 0});
        axios.post('http://localhost:9998/api/v1/generate-password', {requestToken}).catch(err => {
          expect(err.response.status).toBe(500);
          done();
        });
      });
      it('returns 500 status on hash error', (done) => {
        const falseRequestToken = jwt.sign({
          email: 'name@domain.de',
          emailHash: 'falseHash'
        }, config.jwtSecret);
        axios.post('http://localhost:9998/api/v1/generate-password', {falseRequestToken}).catch(err => {
          expect(err.response.status).toBe(500);
          done();
        });
      });

    });
    describe('request to secured uri', () => {
      beforeEach(() => {
        sqlHandler.selectWithParams.mockImplementation(() => selectWithParamsPromise);

      });

      afterEach(() => {
        sqlHandler.selectWithParams.mockReset();
      });

      it('returns status 401 on non authenticated request', (done) => {
        axios.post('http://localhost:9998/api/v1/management')
          .catch(err => {
            expect(err.response.status).toBe(401);
            done();
          });
      });
    });
  });

});

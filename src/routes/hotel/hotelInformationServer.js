// @flow
import express from 'express';
import MySql from '../../db/MySql';
import type {EventDispatcher} from '../../domain/eventDispatcher';
import type {Middleware} from '../../middleware';
import HotelInformationApiVersion1 from '../api/v1/hotel/hotelInformationApi';
import Conferences from '../../domain/conferences';
import ConferenceEditionRepository from '../../db/conference/conferenceEditionRepository';
import Participants from '../../domain/participant';
import ParticipantRepository from '../../db/participation/participantRepository';
import InvitationsMySqlRepository from '../../db/invitations/invitationsRepository';
import Invitations from '../../domain/invitations';
import DataChangesRepository from '../../db/dataChanges/dataChangesRepository';
import DataChanges from '../../domain/dataChanges';

export default function createHotelInformationServer(sqlHandler: MySql, dispatcher: EventDispatcher): Middleware {
  const conferences =
    new Conferences(new ConferenceEditionRepository(sqlHandler), dispatcher);
  const participants =
    new Participants(new ParticipantRepository(sqlHandler), dispatcher);
  const invitations =
    new Invitations(new InvitationsMySqlRepository(sqlHandler), dispatcher);
  const dataChanges =
    new DataChanges(new DataChangesRepository(sqlHandler), dispatcher);
  return (app: express.Application) => {
    app.use('/api/v1/hotel', new HotelInformationApiVersion1(conferences, participants, invitations, dataChanges));
  };
}

// @flow
import express from 'express';
import AuthenticationRepository from '../db/authenticationRepository';
import Authenticator from '../domain/authenticator';
import AuthenticationApiVersion1 from './api/v1/authenticationApi';
import type {Middleware} from '../middleware';
import MySql from '../db/MySql';
import type {EventDispatcher} from '../domain/eventDispatcher';
import type {AccessCheckResult} from '../domain/authenticator';
import {populateRequestUser} from '../utilities/authenticationHelper';

export default function authenticationMiddleware(sqlHandler: MySql, dispatcher: EventDispatcher): Middleware {
  const authenticator = new Authenticator(new AuthenticationRepository(sqlHandler), dispatcher);
  return (app: express.Application) => {
    app.use(async (req: express.Request, res: express.Response, next: express.NextFunction) => {
      if (req.method === 'OPTIONS') {
        return next();
      }
      populateRequestUser(req);
      const access: AccessCheckResult = await authenticator.checkAccess(req.url, req.user);
      if (access.allowed) {
        return next();
      } else {
        res.status(401).send(access.error);
      }
    });
    app.use('/api/v1', new AuthenticationApiVersion1(authenticator));
  };
}
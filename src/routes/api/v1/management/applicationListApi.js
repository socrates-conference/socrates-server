// @flow

import express from 'express';
import ApplicationList from '../../../../domain/management/applicationList';

export default class ApplicationListApiVersion1 extends express.Router {
  applicationList: ApplicationList;

  constructor(applicationList: ApplicationList) {
    super();
    this.applicationList = applicationList;

    this.get('/', (req: express.Request, res: express.Response) => {
      this.applicationList.read()
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.put('/:applicationId', (req: express.Request, res: express.Response) => {
      this.applicationList.updateApplication(req.body)
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.delete('/:personId', (req: express.Request, res: express.Response) => {
      this.applicationList.deleteApplication(req.params.personId)
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.post('/:personId', (req: express.Request, res: express.Response) => {
      this.applicationList.register(req.body)
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.get('/by-room/:roomType', (req: express.Request, res: express.Response) => {
      this.applicationList.readByRoom(req.params.roomType)
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
  }
}




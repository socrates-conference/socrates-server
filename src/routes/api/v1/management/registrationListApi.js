// @flow

import express from 'express';
import RegistrationList from '../../../../domain/management/registrationList';

export default class RegistrationListApiVersion1 extends express.Router {
  registrationList: RegistrationList;

  constructor(registrationList: RegistrationList) {
    super();
    this.registrationList = registrationList;

    this.get('/', (req: express.Request, res: express.Response) => {
      this.registrationList.read()
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.delete('/:personId', (req: express.Request, res: express.Response) => {
      this.registrationList.deleteRegistration(req.params.personId)
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.put('/confirmation-reminder/', async (req: express.Request, res: express.Response) => {
      this.registrationList.sendAllConfirmationReminders()
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.put('/confirmation-reminder/:personId', async (req: express.Request, res: express.Response) => {
      this.registrationList.sendConfirmationReminder(req.params.personId)
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.post('/:personId', async (req: express.Request, res: express.Response) => {
      this.registrationList.updateRoomType(req.params.personId, req.body.roomType)
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
  }
}




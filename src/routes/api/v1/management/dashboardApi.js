// @flow

import express from 'express';
import Dashboard from '../../../../domain/management/dashboard';
import Conferences, {conferenceEditionIdForLegacyCalls} from '../../../../domain/conferences';

export default class DashboardApiVersion1 extends express.Router {
  _dashboard: Dashboard;
  _conferences: Conferences;

  constructor(dashboard: Dashboard, conferences: Conferences) {
    super();
    this._dashboard = dashboard;
    this._conferences = conferences;

    this.get('/dashboard/registrations/by-room-type', (req: express.Request, res: express.Response) => {
      this._dashboard.readRegistrationsByRoomType()
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });

    this.get('/conference', (req: express.Request, res: express.Response) => {
      this._conferences.readConferenceData(conferenceEditionIdForLegacyCalls)
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });

  }
}




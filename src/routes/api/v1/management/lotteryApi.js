// @flow

import express from 'express';
import Lottery from '../../../../domain/management/lottery';

export default class LotteryApiVersion1 extends express.Router {
  _lottery: Lottery;

  constructor(lottery: Lottery) {
    super();
    this._lottery = lottery;

    this.get('/lottery/applications/by-room-type', (req: express.Request, res: express.Response) => {
      this._lottery.readApplicationsByRoomType()
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.get('/lottery/registrations/by-room-type', (req: express.Request, res: express.Response) => {
      this._lottery.readRegistrationsByRoomType()
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
  }
}
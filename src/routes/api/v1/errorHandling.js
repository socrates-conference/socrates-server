// @flow

export const throwError = (status: number, message: string) => {
  // This ugly workaround is brought to you by Babel's inability to extend the built-in Error class.
  throw new Error(JSON.stringify({status, message}));
};

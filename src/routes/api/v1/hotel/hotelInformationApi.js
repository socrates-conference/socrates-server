// @flow
import express from 'express';
import Conferences, {conferenceEditionIdForLegacyCalls} from '../../../../domain/conferences';
import Participants from '../../../../domain/participant';
import Invitations from '../../../../domain/invitations';
import DataChanges from '../../../../domain/dataChanges';
import fs from 'fs';

export default class HotelInformationApiVersion1 extends express.Router {
  conference: Conferences;
  participants: Participants;
  invitations: Invitations;
  dataChanges: DataChanges;

  constructor(conference: Conferences, participants: Participants,
    invitations: Invitations, dataChanges: DataChanges) {
    super();
    this.conference = conference;
    this.participants = participants;
    this.invitations = invitations;
    this.dataChanges = dataChanges;

    this.get('/arrival-departure-hints', (req: express.Request, res: express.Response) => {
      this.conference.readArrivalsAnDeparturesHints(conferenceEditionIdForLegacyCalls)
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.get('/room-types', (req: express.Request, res: express.Response) => {
      this.conference.readRoomTypes(conferenceEditionIdForLegacyCalls)
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.get('/sponsors-list', (req: express.Request, res: express.Response) => {
      this.conference.readSponsorsForHotel(conferenceEditionIdForLegacyCalls)
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.get('/total-sponsored', (req: express.Request, res: express.Response) => {
      this.conference.getTotalSponsoring(conferenceEditionIdForLegacyCalls)
        .then(data => {
          res.status(200).send(data.toString());
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.get('/arrivals-departures', (req: express.Request, res: express.Response) => {
      this.participants.getArrivalsAndDepartures()
        .then(data => {
          res.status(200).send(data);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.get('/total-participants', (req: express.Request, res: express.Response) => {
      this.participants.getTotalAmountOfParticipants()
        .then(data => {
          res.status(200).send(data.toString());
        })
        .catch(err => {
          console.log(err);
          res.status(500).send(err);
        });
    });
    this.get('/fees-per-day', async (req: express.Request, res: express.Response) => {
      const conferenceDays = await this.conference.readConferenceDays(conferenceEditionIdForLegacyCalls);
      const participantsDeparture =
        await this.participants.readParticipantsDepartures(conferenceEditionIdForLegacyCalls);
      if(conferenceDays && participantsDeparture){
        const result = this.participants.calculateDailyConferenceFees(conferenceDays, participantsDeparture);
        res.status(200).send(result);
      } else {
        res.status(500).send('Cannot calculate conference amount of conference daily fees');
      }
    });
    this.get('/accommodated-in/:roomType', (req: express.Request, res: express.Response) => {
      this.participants.readByRoomType(req.params.roomType)
        .then(async participantsByRoomType => {
          res.status(200).send(participantsByRoomType);
        });
    });

    this.get('/room-sharing', (req: express.Request, res: express.Response) => {
      this.invitations.getAllRoomSharingData()
        .then(async data => {
          res.status(200).send(data);
        });
    });
    this.get('/changes', (req: express.Request, res: express.Response) => {
      const fromDate = req.query.from ? req.query.from : '2000-01-01 00:00:00';
      this.dataChanges.readChangesFrom(fromDate)
        .then(async data => {
          res.status(200).send(data);
        });
    });

    this.get('/download/:roomType', (req: express.Request, res: express.Response) => {
      this.participants.readByRoomType(req.params.roomType)
        .then(async participantsByRoomType => {
          if(!fs.existsSync('./tmp')) {
            fs.mkdirSync('./tmp');
          }
          const fields = [{id: 'firstname', title: 'FirstName'}, {id: 'lastname', title: 'LastName'},
            {id: 'company', title: 'Company'}, {id: 'address1', title: 'Address1'},
            {id: 'address2', title: 'Address2'}, {id: 'province', title: 'Province'},
            {id: 'postal', title: 'ZIP'}, {id: 'city', title: 'City'},
            {id: 'country', title: 'Country'}, {id: 'arrival', title: 'ArrivalTime'},
            {id: 'departure', title: 'DepartureTime'}, {id: 'family', title: 'HasFamily'},
            {id: 'familyInfo', title: 'FamilyInfo'}, {id: 'dietary', title: 'DietaryNeeds'},
            {id: 'dietaryInfo', title: 'DietaryInfo'}
          ];
          let content = fields.map(function(field){
            return field.title;
          }).join('\t');
          content += '\n';
          participantsByRoomType.forEach(participant => {
            fields.forEach((field, index) => {
              if (participant.hasOwnProperty(field.id)) {
                const value = participant[field.id];
                if (value) {
                  const preparedString = value.toString().trim()
                    .replace('\r', '')
                    .replace('\n', '');
                  content += preparedString;
                }
              }
              if(index < fields.length - 1) {
                content += '\t';
              }
            });
            content += '\n';
          });
          res.status(200).send(content);
        });
    });
  }
}

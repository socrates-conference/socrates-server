// @flow

import express from 'express';
import Newsletter from '../../../domain/newsletter';
import NewsletterReadModel from '../../../domain/newsletterReadModel';

export default class NewsletterApiVersion1 extends express.Router {
  newsletter: Newsletter;
  readModel: NewsletterReadModel;

  constructor(newsletter: Newsletter, readModel: NewsletterReadModel) {
    super();
    this.newsletter = newsletter;
    this.readModel = readModel;

    this.post('/interested-people', (req: express.Request, res: express.Response) => {
      this.newsletter.signup(req.body.name, req.body.email)
        .then(person => {
          res.status(200).send(person);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.post('/interested-people/confirm', (req: express.Request, res: express.Response) => {
      this.newsletter.confirm(req.body.consentKey)
        .then(person => {
          res.status(200).send(person);
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
    this.delete('/interested-people/:email', (req: express.Request, res: express.Response) => {
      this.newsletter.unsubscribe(req.params.email)
        .then(() => {
          res.status(200).send();
        })
        .catch(err => {
          res.status(500).send(err);
        });
    });
  }
}




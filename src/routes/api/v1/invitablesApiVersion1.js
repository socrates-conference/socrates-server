// @flow

import * as express from 'express';
import Invitables from '../../../domain/invitables';
import type {EventDispatcher} from '../../../domain/eventDispatcher';

export default class InvitablesApiVersion1 extends express.Router {
  _invitables: Invitables;
  _dispatcher: EventDispatcher;

  constructor(invitables: Invitables, dispatcher: EventDispatcher) {
    super();
    this._invitables = invitables;
    this._dispatcher = dispatcher;
    this._initRoutes();
  }

  _initRoutes = () => {

    this.post('/invitables', async (req: express.Request, res: express.Response) => {
      const successful = await this._invitables.addInvitable(req.body);
      res.status(200).send(successful).end();
    });
    this.post('/invitables/byEmail', async (req: express.Request, res: express.Response) => {
      const invitables = await this._invitables.roommateCandidatesForEmail(req.body.email, req.body.departure);
      res.status(200).send(invitables).end();
    });
    this.post('/invitables/isInMarketplace', async (req: express.Request, res: express.Response) => {
      const invitables = await this._invitables.isEmailInMarketplace(req.body.email);
      res.status(200).send(invitables).end();
    });
  };
}



// @flow

import * as express from 'express';
import Invitations, {differentRoomTypes, hasAlreadyRoommateError} from '../../../domain/invitations';
import type {EventDispatcher} from '../../../domain/eventDispatcher';
import {throwError} from './errorHandling';

export default class InvitationsApiVersion1 extends express.Router {
  _invitations: Invitations;
  _dispatcher: EventDispatcher;

  constructor(invitations: Invitations, dispatcher: EventDispatcher) {
    super();
    this._invitations = invitations;
    this._dispatcher = dispatcher;
    this._initRoutes();
  }

  _initRoutes = () => {
    this.get('/invitations', async (req: express.Request, res: express.Response) => {
      const invitations = await this._invitations.list();
      res.status(200).send(invitations).end();
    });

    this.get('/invitations/byKey/:key', async (req: express.Request, res: express.Response) => {
      const invitation = await this._invitations.readByKey(req.params.key);
      if (invitation) {
        res.status(200).send(invitation).end();
      }
      res.status(404).end();
    });

    this.get('/invitations/sentBy/:personId', async (req: express.Request, res: express.Response) => {
      const invitation = await this._invitations.readSentInvitationByPersonId(req.params.personId);
      res.status(200).send(invitation).end();
    });

    this.get('/invitations/receivedBy/:personId', async (req: express.Request, res: express.Response) => {
      const invitations = await this._invitations.readReceivedInvitationsByPersonId(req.params.personId);
      res.status(200).send(invitations).end();
    });

    this.post('/invitations', async (req: express.Request, res: express.Response) => {
      let errorStatus;
      try {
        const invitation = await this._invitations.send(req.body);
        const event = {type: 'SEND_INVITATION', payload: invitation};
        this._dispatcher.dispatchEvent(event);
        res.status(200).send(invitation).end();
      } catch (err) {
        if (err.message === differentRoomTypes || err.message === hasAlreadyRoommateError) {
          errorStatus = 400;
        } else {
          errorStatus = 500;
        }
        res.status(errorStatus).send(err.message).end();
      }
    });

    this.put('/invitations/:action', async (req: express.Request, res: express.Response) => {
      try {
        InvitationsApiVersion1._checkActionValues(req.params.action, req.body);
        if (req.params.action === 'accept') {
          const invitation = await this._invitations.accept(req.body.key, req.body.inviteeEmail);
          if (invitation) {
            res.status(200).send(invitation).end();
          } else {
            res.status(400).end();
          }
        }
        if (req.params.action === 'decline') {
          const invitation = await this._invitations.decline(req.body.key, req.body.inviteeEmail);
          if (invitation) {
            res.status(200).send(invitation).end();
          } else {
            res.status(400).end();
          }
        }
      } catch (err) {
        res.status(400).send(err.message).end();
      }
    });
  };

  static _checkActionValues(action: string, data: { inviteeEmail: string, key: string }) {
    if (!data.inviteeEmail) {
      return throwError(400, 'Missing sender');
    } else if (!data.key) {
      return throwError(400, 'Missing key');
    } else if (action !== 'accept' && action !== 'decline') {
      return throwError(400, 'Action not available');
    }

  }
}

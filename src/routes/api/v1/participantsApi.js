// @flow
import express from 'express';
import type {Participant, PriceCalculationData} from '../../../domain/participant';
import Participants from '../../../domain/participant';
import Conferences, {conferenceEditionIdForLegacyCalls} from '../../../domain/conferences';
import RegistrationList from '../../../domain/management/registrationList';
import type {RoomOption, StayPrice} from '../../../domain/conferenceTypes';

export default class ParticipantsApiVersion1 extends express.Router {
  _participants: Participants;
  _conferences: Conferences;
  _registrations: RegistrationList;

  constructor(participants: Participants, registrations: RegistrationList, conferences: Conferences) {
    super();
    this._participants = participants;
    this._conferences = conferences;
    this._registrations = registrations;

    const calculatePrices = async(personId: number, arrival: string, departure: string) => {
      const roomType = await this._participants.getRoomTypeByPersonId(personId);
      const roomOptions = await this._conferences.getRoomOptions(conferenceEditionIdForLegacyCalls);
      let currentPrice: ?StayPrice = null;
      let pricePerNight = 0;
      if (roomOptions) {
        const room: ?RoomOption = roomOptions.find((roomOption: RoomOption) =>
          roomOption.value === roomType);
        if (room) {
          pricePerNight = room.roomPricePerNight;
          currentPrice = room.prices.find((p: StayPrice) =>
            p.legacyArrivalId === arrival && p.legacyDepartureId === departure);
        }
      }
      const totalSponsoring = await this._conferences.getTotalSponsoring(conferenceEditionIdForLegacyCalls);
      if(currentPrice) {
        const calculationParameters: PriceCalculationData = {
          defaultPrice: currentPrice,
          pricePerNight,
          roomOptions,
          roomType,
          totalSponsoring
        };
        //const price = await this._participants.calculatePrice(roomOptions, roomType, pricePerNight, currentPrice);
        return await this._participants.calculatePrice(calculationParameters);
      }
      return null;
    };

    this.get('/participants', (req: express.Request, res: express.Response) => {
      if (req.query.email) {
        this._participants.findByEmail(req.query.email)
          .then(async participant => {
            if (participant) {
              participant.price =
                await calculatePrices(participant.personId, participant.arrival, participant.departure);
              res.status(200).send(participant);
            } else {
              res.status(404).send('NOT_FOUND');
            }
          })
          .catch(err => {
            console.error('Error while retrieving participant %s:', req.query.email, err);
            res.status(500).send('FAIL');
          });
      } else {
        this._participants.list()
          .then(list => res.status(200).send(list))
          .catch(err => {
            console.error('Error while retrieving participant list:', err);
            res.status(500).send('FAIL');
          });
      }
    });

    this.get('/participants/:id', (req: express.Request, res: express.Response) => {
      this._participants.find(req.params.id)
        .then(async participant => {
          if (participant) {
            participant.price =
              await calculatePrices(participant.personId, participant.arrival, participant.departure);
            res.status(200).send(participant);
          } else {
            res.status(404).send('NOT_FOUND');
          }
        })
        .catch(err => {
          console.error('Error while retrieving participant:', err);
          res.status(500).send('FAIL');
        });
    });

    this.get('/participants/calculate-price/:personId', async (req: express.Request, res: express.Response) => {
      let personId: ?number = Number(req.params.personId);
      if ((!personId || personId === 0) && req.query.email) {
        personId = await this._participants.findPersonIdByEmail(req.query.email);
      }
      if(personId) {
        const calculation = await calculatePrices(personId, req.query.arrival, req.query.departure);
        if (calculation) {
          res.status(200).send(calculation);
        } else {
          console.error('Error while calculating prices');
          res.status(500).send('FAIL: Calculation error');
        }
      } else {
        console.error('Error while calculating prices : Person cannot be found.');
        res.status(500).send('FAIL: Person not found');
      }
    });

    this.put('/participants', (req: express.Request, res: express.Response) => {
      const participant: Participant = {...req.body};
      this._participants.confirm(participant)
        .then(confirmedParticipant => {
          res.status(200).send(confirmedParticipant);
        })
        .catch(err => {
          console.error('Error while confirming participant:', err);
          res.status(500).send('FAIL');
        });
    });

    this.post('/participants', (req: express.Request, res: express.Response) => {
      const participant: Participant = {...req.body};
      this._participants.update(participant)
        .then(updatedParticipant => {
          res.status(200).send(updatedParticipant);
        })
        .catch(err => {
          console.error('Error while updating participant:', err);
          res.status(500).send('FAIL');
        });
    });

    this.delete('/participants/:id', (req: express.Request, res: express.Response) => {
      const id = req.params.id;
      this._participants.cancel(id)
        .then(success => {
          if (success) {
            res.status(200).end();
          } else {
            res.status(500).send('FAIL');
          }
        })
        .catch(err => {
          console.error('Error while deleting participant:', err);
          res.status(500).send('FAIL');
        });
    });

    this.get('/registrations/by-key/:confirmationKey', (req: express.Request, res: express.Response) => {
      const key = req.params.confirmationKey;
      this._participants.getRegistrationEmailByConfirmationKey(key)
        .then(email => {
          res.status(200).send({email});
        })
        .catch(err => {
          console.error('Error while retrieving email for key %s:', key, err);
          res.status(500).send('FAIL');
        });
    });
    this.delete('/registrations/by-key/:confirmationKey', (req: express.Request, res: express.Response) => {
      const key = req.params.confirmationKey;
      this._participants.deleteConfirmationKey(key)
        .then(wasSuccessful => {
          res.status(wasSuccessful ? 200 : 500).send();
        })
        .catch(err => {
          console.error('Error while deleting key %s:', key, err);
          res.status(500).send();
        });
    });
  }
}

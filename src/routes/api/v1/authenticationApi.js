// @flow

import express from 'express';
import Authenticator from '../../../domain/authenticator';

export default class AuthenticationApiVersion1 extends express.Router {
  authenticator: Authenticator;

  constructor(authenticator: Authenticator) {
    super();
    this.authenticator = authenticator;

    this.post('/login', (req: express.Request, res: express.Response) => {
      this.authenticator.login(req.body.requestToken)
        .then(result => {
          res.status(200).send(result);
        })
        .catch((e) => {
          console.error(e);
          res.status(200).send({success: false, token: '', isAdministrator: false});
        });
    });
    this.post('/update-password', (req: express.Request, res: express.Response) => {
      this.authenticator.updatePassword(req.body.requestToken)
        .then(result => {
          res.status(200).send(result);
        })
        .catch(() => {
          res.status(200).send({success: false, token: '', isAdministrator: false});
        });
    });
    this.post('/revoke-password', (req: express.Request, res: express.Response) => {
      this.authenticator.revokePassword(req.body.requestToken)
        .then(result => {
          res.status(200).send(result);
        })
        .catch((error) => {
          res.status(500).send(error);
        });
    });
    this.post('/generate-password', (req: express.Request, res: express.Response) => {
      this.authenticator.generateOneTimePassword(req.body.requestToken)
        .then(result => {
          res.status(200).send(result);
        })
        .catch((error) => {
          res.status(500).send(error);
        });
    });
  }
}




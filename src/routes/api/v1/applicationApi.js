// @flow
import express from 'express';
import Application from '../../../domain/application';
import type {ApplicationData} from '../../../domain/application';
import Conferences, {conferenceEditionIdForLegacyCalls} from '../../../domain/conferences';

export default class ApplicationApiVersion1 extends express.Router {
  _application: Application;

  _conferences: Conferences;

  constructor(application: Application, conferences: Conferences) {
    super();
    this._application = application;
    this._conferences = conferences;

    this.post('/', (req: express.Request, res: express.Response) => {
      const applicationData: ApplicationData = {
        personId: 0,
        nickname: req.body.nickname,
        email: req.body.email,
        roomTypes: req.body.roomTypes,
        diversityReason: req.body.diversity
      };
      this._application.add(applicationData)
        .then(addedApplicationData => {
          res.status(200).send(addedApplicationData);
        })
        .catch(err => {
          if (err.message === 'ALREADY_APPLIED') {
            res.status(400).send(err.message);
          } else {
            res.status(500).send('FAIL');
          }
        });
    });
    this.get('/options/', (req: express.Request, res: express.Response) => {
      this._conferences.getRoomOptions(conferenceEditionIdForLegacyCalls)
        .then(roomOptions => res.status(200).send(roomOptions))
        .catch(() => {
          res.status(500).send('FAILED');
        });
    });
  }
}

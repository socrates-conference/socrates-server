// @flow

import express from 'express';
import Invitations from '../domain/invitations';
import type {EventDispatcher} from '../domain/eventDispatcher';
import type {Middleware} from '../middleware';
import InvitationsApiVersion1 from './api/v1/invitationsApiVersion1';
import InvitationsMySqlRepository from '../db/invitations/invitationsRepository';
import MySql from '../db/MySql';

export default function createInvitationsServer(sqlHandler: MySql, dispatcher: EventDispatcher): Middleware {
  const invitations = new Invitations(new InvitationsMySqlRepository(sqlHandler), dispatcher);
  return (app: express.Application) => app.use('/api/v1/',
    new InvitationsApiVersion1(invitations, dispatcher));
}

// @flow

import express from 'express';
import Invitables from '../domain/invitables';
import type {EventDispatcher} from '../domain/eventDispatcher';
import type {Middleware} from '../middleware';
import InvitablesApiVersion1 from './api/v1/invitablesApiVersion1';
import MySql from '../db/MySql';
import InvitablesMySqlRepository from '../db/invitables/invitablesRepository';

export default function createInvitablesServer(sqlHandler: MySql, dispatcher: EventDispatcher): Middleware {
  const invitations = new Invitables(new InvitablesMySqlRepository(sqlHandler), dispatcher);
  return (app: express.Application) => app.use('/api/v1/',
    new InvitablesApiVersion1(invitations, dispatcher));
}


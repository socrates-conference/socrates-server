// @flow
import App from '../../app';
import axios from 'axios';
import MySql from '../../db/MySql';
import type Config from '../../config';
import DomainEventDispatcher from '../../domain/domainEventDispatcher';
import type {EventDispatcher} from '../../domain/eventDispatcher';
import bodyParsers from '../../routes/bodyParsers';
import createDashboardServer from '../../routes/management/dashboardServer';
import defaultHeaders from '../defaultHeaders';

jest.mock('../../db/MySql');
describe('Application:', () => {
  let app: App, dispatcher: EventDispatcher;
  let selectWithParamsPromise;
  const defaultConfiguration: Config = {
    'environment': 'test',
    'database': {
      'host': 'localhost',
      'port': 3306,
      'name': 'socrates',
      'user': '',
      'password': ''
    },
    'server': {
      'port': 9999
    }
  };
  const sqlHandler = new MySql(defaultConfiguration);

  beforeAll(async () => {
    dispatcher = new DomainEventDispatcher();
    app = new App(defaultConfiguration);
    app.applyMiddleware(bodyParsers);
    app.applyMiddleware(defaultHeaders);
    app.applyMiddleware(createDashboardServer(sqlHandler, dispatcher));
    await app.run();
  });

  afterAll(async () => {
    if (app) {
      await app.stop();
    }
  });

  beforeEach(() => {
    sqlHandler.select.mockImplementation(() => {
      return selectWithParamsPromise;
    });
  });
  afterEach(() => {
    sqlHandler.select.mockReset();
  });

  describe('dashboard router api get requests for registrations by room type', () => {
    it('returns status 200 when available', () => {
      selectWithParamsPromise = Promise.resolve([{}]);
      return axios.get('http://localhost:9999/api/v1/management/dashboard/registrations/by-room-type')
        .then((response) => {
          expect(response.status).toBe(200);
        });
    });
    it('returns status 500 when not available', () => {
      selectWithParamsPromise = Promise.resolve([]);
      return axios.get('http://localhost:9999/api/v1/management/dashboard/registrations/by-room-type')
        .catch((error) => {
          expect(error.response.status).toBe(500);
        });
    });
  });
});

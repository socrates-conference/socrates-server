// @flow
import express from 'express';
import MySql from '../../db/MySql';
import type {EventDispatcher} from '../../domain/eventDispatcher';
import type {Middleware} from '../../middleware';
import DashboardApiVersion1 from '../api/v1/management/dashboardApi';
import Dashboard from '../../domain/management/dashboard';
import DashboardRepository from '../../db/dashboard/dashboardRepository';
import Conferences from '../../domain/conferences';
import ConferenceEditionRepository from '../../db/conference/conferenceEditionRepository';

export default function createDashboardServer(sqlHandler: MySql, dispatcher: EventDispatcher): Middleware {
  const dashboard =
    new Dashboard(new DashboardRepository(sqlHandler), dispatcher);
  const conferences =
    new Conferences(new ConferenceEditionRepository(sqlHandler), dispatcher);
  return (app: express.Application) => {
    app.use('/api/v1/management', new DashboardApiVersion1(dashboard, conferences));
  };
}

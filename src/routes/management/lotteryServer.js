// @flow

import type {Middleware} from '../../middleware';
import type {EventDispatcher} from '../../domain/eventDispatcher';
import MySql from '../../db/MySql';
import express from 'express';
import Lottery from '../../domain/management/lottery';
import LotteryRepository from '../../db/lottery/lotteryRepository';
import LotteryApiVersion1 from '../api/v1/management/lotteryApi';

export default function createLotteryServer(sqlHandler: MySql, dispatcher: EventDispatcher): Middleware {
  const lottery =
    new Lottery(new LotteryRepository(sqlHandler), dispatcher);
  return (app: express.Application) => {
    app.use('/api/v1/management', new LotteryApiVersion1(lottery));
  };

}

// @flow
import express from 'express';
import MySql from '../../db/MySql';
import RegistrationList from '../../domain/management/registrationList';
import RegistrationListRepository from '../../db/registrationList/registrationListRepository';
import RegistrationListApiVersion1 from '../api/v1/management/registrationListApi';
import type {EventDispatcher} from '../../domain/eventDispatcher';
import type {Middleware} from '../../middleware';

export default function createRegistrationListServer(sqlHandler: MySql, dispatcher: EventDispatcher): Middleware {
  const registrationList =
    new RegistrationList(new RegistrationListRepository(sqlHandler), dispatcher);
  return (app: express.Registration) => {
    app.use('/api/v1/management/registration-list', new RegistrationListApiVersion1(registrationList));
  };
}
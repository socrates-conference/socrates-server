// @flow
import express from 'express';
import MySql from '../../db/MySql';
import ApplicationList from '../../domain/management/applicationList';
import ApplicationListRepository from '../../db/applicationList/applicationListRepository';
import ApplicationListApiVersion1 from '../api/v1/management/applicationListApi';
import type {EventDispatcher} from '../../domain/eventDispatcher';
import type {Middleware} from '../../middleware';

export default function createApplicationListServer(sqlHandler: MySql, dispatcher: EventDispatcher): Middleware {
  const applicationList =
    new ApplicationList(new ApplicationListRepository(sqlHandler), dispatcher);
  return (app: express.Application) => {
    app.use('/api/v1/management/application-list', new ApplicationListApiVersion1(applicationList));
  };
}
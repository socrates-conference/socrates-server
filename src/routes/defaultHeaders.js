// @flow
import express from 'express';

export default function defaultHeaders(app: express.Application): void {
  app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, ' +
      'Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization');
    res.setHeader('Cache-Control', 'no-cache');
    next();
  });
}
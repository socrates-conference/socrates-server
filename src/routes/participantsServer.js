// @flow
import express from 'express';
import MySql from '../db/MySql';
import type {EventDispatcher} from '../domain/eventDispatcher';
import type {Middleware} from '../middleware';
import Participants from '../domain/participant';
import ParticipantRepository from '../db/participation/participantRepository';
import ParticipantsApiVersion1 from './api/v1/participantsApi';
import Conferences from '../domain/conferences';
import RegistrationList from '../domain/management/registrationList';
import ConferenceEditionRepository from '../db/conference/conferenceEditionRepository';
import RegistrationListRepository from '../db/registrationList/registrationListRepository';

export default function createParticipantsServer(sqlHandler: MySql, dispatcher: EventDispatcher): Middleware {
  const participants = new Participants(new ParticipantRepository(sqlHandler), dispatcher);
  const conferences = new Conferences(new ConferenceEditionRepository(sqlHandler), dispatcher);
  const registrations = new RegistrationList(new RegistrationListRepository(sqlHandler), dispatcher);
  return (app: express.Application) => {
    app.use('/api/v1/', new ParticipantsApiVersion1(participants, registrations, conferences));
  };
}

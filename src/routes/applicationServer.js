// @flow
import express from 'express';
import MySql from '../db/MySql';
import type {EventDispatcher} from '../domain/eventDispatcher';
import type {Middleware} from '../middleware';
import Application from '../domain/application';
import ApplicationRepository from '../db/application/applicationRepository';
import ApplicationApiVersion1 from './api/v1/applicationApi';
import Conferences from '../domain/conferences';
import ConferenceEditionRepository from '../db/conference/conferenceEditionRepository';

export default function createApplicationServer(sqlHandler: MySql, dispatcher: EventDispatcher): Middleware {
  const application = new Application(new ApplicationRepository(sqlHandler), dispatcher);
  const conferences = new Conferences(new ConferenceEditionRepository(sqlHandler), dispatcher);
  return (app: express.Application) => {
    app.use('/api/v1/applications/', new ApplicationApiVersion1(application, conferences));
  };
}

import {populateRequestUser} from './authenticationHelper';
import config from '../config';
import {sign} from 'jsonwebtoken';

const testToken = sign({
  email: 'alex@email.de',
  name: 'Alex',
  isAdministrator: 0
}, config.jwtSecret);

const bogusToken = 'bogusToken';


describe('authentication helper', () => {
  it('add user to request when header available', () => {
    const request = {headers: {authorization: `Bearer ${testToken}`}};
    populateRequestUser(request);
    expect(request.user).toBeDefined();
    expect(request.user.email).toBe('alex@email.de');
    expect(request.user.name).toBe('Alex');
    expect(request.user.isAdministrator).toBe(0);
  });
  it('doesn\'t add user to request when authorization header the wrong key', () => {
    const request = {headers: {authorization: `JWT ${testToken}`}};
    populateRequestUser(request);
    expect(request.user).toBeUndefined();
  });
  it('doesn\'t add user to request when authorization header contains wrong token', () => {
    const request = {headers: {authorization: `Bearer ${bogusToken}`}};
    populateRequestUser(request);
    expect(request.user).toBeUndefined();
  });
});
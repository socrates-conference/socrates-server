// @flow

import {verify} from 'jsonwebtoken';
import express from 'express';
import config from '../config';

function isJWTAuthorizationAvailable(req: express.Request ) {
  return req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer';
}

function extractJWTToken(req: express.Request) {
  return req.headers.authorization.split(' ')[1];
}

export function populateRequestUser(req: express.Request) {
  if (isJWTAuthorizationAvailable(req)) {
    verify(extractJWTToken(req), config.jwtSecret, (err, decode) => {
      req.user = err ? undefined : decode;
    });
  } else {
    req.user = undefined;
  }
}

// @flow

import {arrivalToText, departureToText, makeRandomPass, validateEmail} from './Utilities';

describe('validateEmail should', () => {
  it('accept valid address', () => {
    expect(validateEmail('name@domain.de')).toBeTruthy();
  });

  it('accept german characters in local part', () => {
    expect(validateEmail('äö-üß.german@domain.de')).toBeTruthy();
  });

  it('accept subdomains', () => {
    expect(validateEmail('name@sub-domain.domain.de')).toBeTruthy();
  });

  it('refuse empty string', () => {
    expect(validateEmail('')).toBeFalsy();
  });

  it('refuse address with forbidden characters', () => {
    expect(validateEmail('name\\@domain.de')).toBeFalsy();
    expect(validateEmail('.name@domain.de')).toBeFalsy();
    expect(validateEmail('na,me@domain.de')).toBeFalsy();
    expect(validateEmail('name@äää.de')).toBeFalsy();
    expect(validateEmail('name@dom ain.äö')).toBeFalsy();
    expect(validateEmail('na me@domain.äö')).toBeFalsy();
  });

  it('refuse malformed address', () => {
    expect(validateEmail('name.domain.de')).toBeFalsy();
    expect(validateEmail('@name.domain.de')).toBeFalsy();
    expect(validateEmail('name.domain.de@')).toBeFalsy();
    expect(validateEmail('name@domain')).toBeFalsy();
    expect(validateEmail('name@domain.a')).toBeFalsy();
    expect(validateEmail('name@domain@domain.fr')).toBeFalsy();
  });
});

describe('make random password', () => {
  it('generates a string with the specified length', () => {
    expect(makeRandomPass(10).length).toBe(10);
    expect(makeRandomPass(5).length).toBe(5);
    expect(makeRandomPass(25).length).toBe(25);
  });
});
describe('arrival to text', () => {
  it('generates a string with the name of the arrival', () => {
    expect(arrivalToText('0')).toEqual('Thursday afternoon');
    expect(arrivalToText('1')).toEqual('Thursday evening');
    expect(arrivalToText('2')).toEqual('unknown arrival day');
  });
});
describe('departure to text', () => {
  it('generates a string with the name of the departure', () => {
    expect(departureToText('0')).toEqual('Saturday afternoon');
    expect(departureToText('1')).toEqual('Saturday evening');
    expect(departureToText('2')).toEqual('Sunday morning');
    expect(departureToText('3')).toEqual('Sunday afternoon');
    expect(departureToText('4')).toEqual('Sunday evening');
    expect(departureToText('5')).toEqual('Monday');
    expect(departureToText('6')).toEqual('unknown departure day');
  });
});


// @flow
import generateGuid from 'uuid/v4';

export function validateEmail(email: string): boolean {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // eslint-disable-line
  return re.test(String(email).toLowerCase());
}

export function makeRandomPass(length: number) {
  const text = [];
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!"§$%()=,.-;:_#+*@';

  for (let i = 0; i < length; i++) {
    text.push(possible.charAt(Math.floor(Math.random() * possible.length)));
  }

  return text.join('');
}
export function makeRandomKey() {
  return generateGuid();
}

export function borderedLog(message: any) {
  console.log('#######################################');
  console.log(message);
  console.log('#######################################');
}

export const arrivalToText = (arrival: string): string => {
  switch (arrival) {
    case '0':
      return 'Thursday afternoon';
    case '1':
      return 'Thursday evening';
    default:
      return 'unknown arrival day';
  }
};
export const departureToText = (arrival: string): string => {
  switch (arrival) {
    case '0':
      return 'Saturday afternoon';
    case '1':
      return 'Saturday evening';
    case '2':
      return 'Sunday morning';
    case '3':
      return 'Sunday afternoon';
    case '4':
      return 'Sunday evening';
    case '5':
      return 'Monday';
    default:
      return 'unknown departure day';
  }
};

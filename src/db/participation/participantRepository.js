// @flow
import MySql from '../MySql';
import type {
  Participant, ParticipantArrival, ParticipantDeparture, ParticipantsDeparture,
  RoomInformation
} from '../../domain/participant';
import type {QueryResult} from '../MySql';
import type {Registration} from '../../domain/management/registrationList';
import type {PersonResult} from '../application/personMySQL';
import {arrivalToText, departureToText} from '../../utilities/Utilities';

export default class ParticipantRepository {

  _mySql: MySql;

  constructor(mySql: MySql) {
    this._mySql = mySql;
  }

  async create(participant: Participant): Promise<Participant> {
    const insertParticipantSql = 'INSERT INTO participants ' +
      '(person_id, firstname, lastname, company, address1, address2, province, ' +
      'postal, city, country, arrival, departure, dietary, dietary_info, ' +
      'family, family_info, gender, tshirt, labelname, social, pronoun) VALUES ' +
      '(?,  ?,  ?,  ?,  ?,  ?,  ?,  ' +
      '?,  ?,  ?,  ?,  ?,  ?,  ?, ' +
      '?,  ?,  ?,  ?,  ?,  ?,  ?)';
    const {
      email, firstname, lastname, company, address1, address2, province, postal, city, country,
      arrival, departure, dietary, dietaryInfo, family, familyInfo, gender, tshirt, labelname, social,
      pronoun, personId, nickname
    } = participant;
    const values = [
      personId, firstname, lastname, company, address1, address2, province, postal, city, country,
      arrival, departure, dietary, dietaryInfo, family, familyInfo, gender, tshirt, labelname, social,
      pronoun
    ];
    const queryResult = await this._mySql.insert(insertParticipantSql, values);
    return {
      id: queryResult.lastInsertedId,
      personId, email, nickname,
      firstname, lastname, company, address1, address2, province, postal, city, country,
      arrival, departure, dietary, dietaryInfo, family, familyInfo, gender,
      tshirt, labelname, social, pronoun
    };
  }

  async deleteParticipant(id: number): Promise<boolean> {
    const deleteQuery = 'DELETE FROM participants WHERE id=?';
    const result = await this._mySql.delete(deleteQuery, [id]);
    return result.numberOfRowsAffected > 0;
  }

  async update(participant: Participant): Promise<Participant> {
    const updateParticipantSql = 'UPDATE participants SET ' +
      'person_id=?, firstname=?, lastname=?, company=?, address1=?, address2=?, province=?, ' +
      'postal=?, city=?, country=?, arrival=?, departure=?, dietary=?, dietary_info=?, ' +
      'family=?, family_info=?, gender=?, tshirt=?, labelname=?, social=?, pronoun=? WHERE id=?';
    const {
      personId, email, nickname, firstname, lastname, company, address1, address2, province, postal, city, country,
      arrival, departure, dietary, dietaryInfo, family, familyInfo, gender, tshirt, labelname, social,
      pronoun, id
    } = participant;
    const values = [
      personId, firstname, lastname, company, address1, address2, province, postal, city, country,
      arrival, departure, dietary, dietaryInfo, family, familyInfo, gender, tshirt, labelname, social,
      pronoun, id
    ];
    const queryResult = await this._mySql.update(updateParticipantSql, values);
    if (queryResult.numberOfRowsAffected !== 1) {
      throw new Error('Update failed.');
    }
    return {
      personId,
      email,
      nickname,
      firstname,
      lastname,
      company,
      address1,
      address2,
      province,
      postal,
      city,
      country,
      arrival,
      departure,
      dietary,
      dietaryInfo,
      family,
      familyInfo,
      gender,
      tshirt,
      labelname,
      social,
      pronoun,
      id
    };
  }

  async readPersonByEmail(theEmail: string): Promise<?PersonResult> {
    const readPersonSql = 'SELECT * FROM person WHERE email = ?;';
    const values = [theEmail];
    const results = await this._mySql.selectWithParams(readPersonSql, values);
    if (results && results.length === 1) {
      return results[0];
    }
    return null;
  }

  async readPersonById(id: number): Promise<?PersonResult> {
    const readPersonSql = 'SELECT * FROM person WHERE id = ?;';
    const values = [id];
    const results = await this._mySql.selectWithParams(readPersonSql, values);
    if (results && results.length === 1) {
      return results[0];
    }
    return null;
  }

  async readParticipant(id: number): Promise<?Participant> {
    const readParticipantSql = 'SELECT * FROM participants WHERE id = ?';
    const values = [id];
    const results = await this._mySql.selectWithParams(readParticipantSql, values);
    if (results && results.length === 1) {
      return rowResultToParticipant(results[0]);
    }
    return null;
  }

  async _readParticipantByPersonId(personId: number): Promise<?Participant> {
    const readParticipantSql = 'SELECT * FROM participants WHERE person_id = ?';
    const values = [personId];
    const results = await this._mySql.selectWithParams(readParticipantSql, values);
    if (results && results.length === 1) {
      return rowResultToParticipant(results[0]);
    }
    return null;
  }

  async readParticipantByEmail(theEmail: string): Promise<?Participant> {
    const person: ?PersonResult = await this.readPersonByEmail(theEmail);
    if (person) {
      const {id: personId, nickname, email} = person;
      const registration: ?Registration = await this._readRegistrationByPersonId(`${personId}`);
      const participant = await this._readParticipantByPersonId(personId);
      if (participant) {
        return {
          ...participant,
          confirmed: Boolean(registration && registration.isConfirmed),
          sharedRoom: Boolean(registration
            && (registration.roomType === 'bedInDouble' || registration.roomType === 'juniorShared')),
          nickname,
          email
        };
      }
    }
    return null;
  }

  async readAll(): Promise<Array<Participant>> {
    const readParticipantsSql = 'SELECT * FROM participants';
    const results: Array<Object> = await this._mySql.select(readParticipantsSql);
    if (results && results.length > 0) {
      return results.map(rowResultToParticipant);
    }
    return [];
  }
  async readByRoomType(roomTypeId: string): Promise<Array<Participant>> {
    const readParticipantsSql = 'SELECT p.* FROM participants as p ' +
      'INNER JOIN registration as r ON r.person_id = p.person_id WHERE r.room_type = ?';
    const results: Array<Object> = await this._mySql.selectWithParams(readParticipantsSql, [roomTypeId]);
    if (results) {
      return results.map(createHotelParticipantData);
    }
    return [];
  }

  async readAllRoomInformation(): Promise<Array<RoomInformation>> {
    const readRoomTypeSql = 'SELECT r.person_id AS personId, r.room_type AS room, p.arrival, p.departure FROM ' +
      'registration as r LEFT OUTER JOIN participants as p ON r.person_id = p.person_id ORDER BY r.person_id';
    const results: Array<Object> = await this._mySql.select(readRoomTypeSql);
    if (results && results.length > 0) {
      return results.map(rowResultToRoomInformation);
    }
    return [];
  }

  async readRegistrationEmailByConfirmationKey(key: string): Promise<string> {
    const readEmailSql = 'SELECT p.email FROM registration as r INNER JOIN person as p ON r.person_id = p.id ' +
      'WHERE r.confirmation_key = ?';
    const results: Array<Object> = await this._mySql.selectWithParams(readEmailSql, [key]);
    if (results && results.length === 1) {
      return results[0].email;
    }
    return '';
  }

  async getRoomTypeByPersonId(personId: number): Promise<string> {
    const readRoomTypeSql = 'SELECT room_type as roomType FROM registration WHERE person_id=?';
    const results: Array<Object> = await this._mySql.selectWithParams(readRoomTypeSql, [personId]);
    if (results && results.length === 1) {
      return results[0].roomType;
    }
    return '';
  }

  async _readRegistrationByPersonId(personId: string): Promise<?Registration> {
    const readEmailSql = 'SELECT * FROM registration where person_id = ?';
    const results: Array<Object> = await this._mySql.selectWithParams(readEmailSql, [personId]);
    if (results && results.length === 1) {
      return rowResultToRegistration(results[0]);
    }
  }

  async deleteConfirmationKey(key: string): Promise<boolean> {
    const deleteKeySql = 'UPDATE registration SET confirmation_key = \'\' ' +
      'WHERE confirmation_key = ?';
    const result: QueryResult = await this._mySql.update(deleteKeySql, [key]);
    return Boolean(result && result.numberOfRowsAffected === 1);

  }

  async confirmRegistration(personId: number): Promise<boolean> {
    const confirmRegistrationSql = 'UPDATE registration SET confirmed = 1,  confirmation_date= NOW()' +
      'WHERE person_id = ?';
    const result: QueryResult = await this._mySql.update(confirmRegistrationSql, [personId]);
    return Boolean(result && result.numberOfRowsAffected === 1);
  }

  async readParticipantsDepartures(conferenceEditionId: number): Promise<ParticipantsDeparture[]> {
    const confirmRegistrationSql = 'SELECT count(p.id) as amount, d2.conferenceDayNumber as departureDayNumber, ' +
      'dtime.hasToPayConferenceDayFee as hasToPayDepartureDayConferenceFee ' +
      'FROM participants as p ' +
      'INNER JOIN departure_time as dtime ON dtime.legacyId = p.departure ' +
      'INNER JOIN conference_day as d2 ON dtime.conferenceDayId = d2.id ' +
      'WHERE dtime.conferenceEditionId = ? AND d2.conferenceEditionId = ? ' +
      'GROUP BY d2.conferenceDayNumber, dtime.hasToPayConferenceDayFee  ' +
      'ORDER BY d2.conferenceDayNumber';
    try {
      const result: Array<Object> =
        await this._mySql.selectWithParams(confirmRegistrationSql, [conferenceEditionId, conferenceEditionId]);
      return result ? result : [];
    } catch (e) {
      console.log(e);
      return [];
    }
  }
  async readArrivalsForConfirmedParticipants(): Promise<ParticipantArrival[]> {
    const sql = 'SELECT arrival, count(p.id) as count, ' +
      'case arrival ' +
      'when 0 then \'Thursday afternoon\' ' +
      'when 1 then \'Thursday evening\' ' +
      'else \'Undefined arrival point in time\' end as arrivalText ' +
      'FROM registration as r ' +
      'INNER JOIN participants as p ' +
      'ON p.person_id = r.person_id  ' +
      'WHERE r.confirmed = 1 ' +
      'GROUP BY arrival';

    return await this._mySql.select(sql);
  }

  async readDeparturesForConfirmedParticipants(): Promise<ParticipantDeparture[]> {
    const sql = 'SELECT count(p.id) as count, departure, ' +
      'case departure ' +
      'when 0 then \'Saturday afternoon\' ' +
      'when 1 then \'Saturday evening\' ' +
      'when 2 then \'Sunday morning\' ' +
      'when 3 then \'Sunday afternoon\' ' +
      'when 4 then \'Sunday evening\' ' +
      'when 5 then \'Monday\' ' +
      'else \'Undefined departure point in time\' end as departureText ' +
      'FROM registration as r ' +
      'INNER JOIN participants as p ' +
      'ON p.person_id = r.person_id ' +
      'WHERE r.confirmed = 1 ' +
      'GROUP BY departure ORDER BY departure;';

    return await this._mySql.select(sql);
  }

  async getTotalAmountOfParticipants(): Promise<number> {
    const sql = 'select count(p.id) as amount from participants as p ' +
      'inner join registration as r on p.person_id = r.person_id ' +
      'where r.confirmed = 1';
    try {
      const result: Array<Object> = await this._mySql.select(sql);
      return result && result.length === 1 ? result[0].amount : 0;
    } catch(err) {
      console.log(err);
      return 0;
    }
  }
}

const rowResultToRoomInformation = (result: any): RoomInformation => {
  const {personId, room, arrival = '0', departure = '3'} = result;
  return {personId, room, arrival: parseInt(arrival), departure: parseInt(departure)};
};


const createHotelParticipantData = (result: any): Participant => ({
  ...result,
  personId: result.person_id,
  family: Boolean(result.family),
  familyInfo: result.family_info,
  dietary: Boolean(result.dietary),
  dietaryInfo: result.dietary_info,
  arrival: arrivalToText(result.arrival),
  departure: departureToText(result.departure)
});

const rowResultToParticipant = (result: any): Participant => ({
  ...result,
  personId: result.person_id,
  family: Boolean(result.family),
  familyInfo: result.family_info,
  dietary: Boolean(result.dietary),
  dietaryInfo: result.dietary_info
});

const rowResultToRegistration = (result: any): Registration => ({
  ...result,
  isConfirmed: Boolean(result.confirmed),
  personId: result.person_id,
  roomType: result.room_type,
  diversityReason: result.diversity_reason
});

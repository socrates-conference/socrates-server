// @flow

import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import ParticipantRepository from './participantRepository';
import type {
  Participant,
  ParticipantArrival,
  ParticipantDeparture,
  ParticipantsDeparture
} from '../../domain/participant';

jest.mock('../MySql');

let participantRepository: ParticipantRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);


const participantArrivals: ParticipantArrival[] = [
  {arrival: '0', arrivalText: 'arrival One', count: 20},
  {arrival: '1', arrivalText: 'arrival Two', count: 10}
];
const participantDepartures: ParticipantDeparture[] = [
  {departure: '0', departureText: 'arrival One', count: 13},
  {departure: '1', departureText: 'arrival Two', count: 17},
  {departure: '1', departureText: 'arrival Two', count: 55}
];

describe('Participant repository:', () => {

  beforeEach(() => {
    participantRepository = new ParticipantRepository(mySql);
  });

  describe('deletes confirmation key', () => {
    let promise;
    beforeEach(() => {
      mySql.update.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.update.mockReset();
    });

    it('return true, when key exists', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      const result = await participantRepository.deleteConfirmationKey('4711');
      expect(result).toBe(true);
      expect(mySql.update.mock.calls.length).toBe(1);
    });
    it('return false, when key not exists', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 0, lastInsertedId: 0});
      const result = await participantRepository.deleteConfirmationKey('4711');
      expect(result).toBe(false);
      expect(mySql.update.mock.calls.length).toBe(1);
    });
    it('return false, when error', (done) => {
      promise = Promise.reject(new Error());
      participantRepository.deleteConfirmationKey('4711').catch(() => {
        expect(mySql.update.mock.calls.length).toBe(1);
        done();
      });
    });
  });

  describe('confirms registration', () => {
    let promise;
    beforeEach(() => {
      mySql.update.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.update.mockReset();
    });

    it('return true, when key exists', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      const result = await participantRepository.confirmRegistration(1);
      expect(result).toBe(true);
      expect(mySql.update.mock.calls.length).toBe(1);
    });
    it('return false, when key not exists', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 0, lastInsertedId: 0});
      const result = await participantRepository.confirmRegistration(1);
      expect(result).toBe(false);
      expect(mySql.update.mock.calls.length).toBe(1);
    });
    it('return false, when error', (done) => {
      promise = Promise.reject(new Error());
      participantRepository.confirmRegistration(1).catch(() => {
        expect(mySql.update.mock.calls.length).toBe(1);
        done();
      });
    });
  });

  describe('gets email for confirmation key', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('return an email, when key exists', async () => {
      promise = Promise.resolve([{email: 'someEmail@valid.de'}]);
      const result = await participantRepository.readRegistrationEmailByConfirmationKey('4711');
      expect(result).toBe('someEmail@valid.de');
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('return empty string, when key not exists', async () => {
      promise = Promise.resolve([]);
      const result = await participantRepository.readRegistrationEmailByConfirmationKey('4711');
      expect(result).toBe('');
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('return empty string, when error', (done) => {
      promise = Promise.reject(new Error());
      participantRepository.readRegistrationEmailByConfirmationKey('4711').catch(() => {
        expect(mySql.selectWithParams.mock.calls.length).toBe(1);
        done();
      });
    });
  });

  describe('reads all participants', () => {
    let promise;
    beforeEach(() => {
      mySql.select.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.select.mockReset();
    });

    it('without errors', async () => {
      promise = Promise.resolve([{'person_id': 4711}]);
      const result: Array<Participant> = await participantRepository.readAll();
      expect(result).toHaveLength(1);
      expect(result[0].personId).toBe(4711);
      expect(mySql.select.mock.calls.length).toBe(1);
    });
    it('returns empty array, when no data available', async () => {
      promise = Promise.resolve([]);
      const result: Array<Participant> = await participantRepository.readAll();
      expect(result).toHaveLength(0);
      expect(mySql.select.mock.calls.length).toBe(1);
    });
    it('returns empty string, when error', (done) => {
      promise = Promise.reject(new Error());
      participantRepository.readAll().catch(() => {
        expect(mySql.select.mock.calls.length).toBe(1);
        done();
      });
    });
  });

  describe('reads participants by room type', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('without errors', async () => {
      promise = Promise.resolve([{'person_id': 4711}]);
      const result: Array<Participant> = await participantRepository.readByRoomType('single');
      expect(result).toHaveLength(1);
      expect(result[0].personId).toBe(4711);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('returns empty array, when no data available', async () => {
      promise = Promise.resolve([]);
      const result: Array<Participant> = await participantRepository.readByRoomType('single');
      expect(result).toHaveLength(0);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('returns empty string, when error', (done) => {
      promise = Promise.reject(new Error());
      participantRepository.readByRoomType('single').catch(() => {
        expect(mySql.selectWithParams.mock.calls.length).toBe(1);
        done();
      });
    });
  });

  describe('reads participants departures', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('without errors', async () => {
      promise = Promise.resolve([
        {amount: 10, departureDayNumber: 1, hasToPayDepartureDayConferenceFee: true},
        {amount: 5, departureDayNumber: 1, hasToPayDepartureDayConferenceFee: false},
        {amount: 30, departureDayNumber: 2, hasToPayDepartureDayConferenceFee: true},
        {amount: 15, departureDayNumber: 2, hasToPayDepartureDayConferenceFee: false},
        {amount: 8, departureDayNumber: 3, hasToPayDepartureDayConferenceFee: false}
      ]);
      const result: ParticipantsDeparture[] = await participantRepository.readParticipantsDepartures(1);
      expect(result).toHaveLength(5);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('returns empty array, when no data available', async () => {
      promise = Promise.resolve([]);
      const result: ParticipantsDeparture[] = await participantRepository.readParticipantsDepartures(1);
      expect(result).toHaveLength(0);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('returns empty array, when error', async() => {
      promise = Promise.reject(new Error());
      const result: ParticipantsDeparture[] = await participantRepository.readParticipantsDepartures(1);
      expect(result).toHaveLength(0);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
  });
  describe('reading participant arrivals', () => {
    let promise;
    beforeEach(() => {
      mySql.select.mockImplementation(() => promise);
    });
    afterEach(() => {
      mySql.select.mockReset();
    });
    it('gets the arrivals', async() => {
      promise = Promise.resolve(participantArrivals);
      const arrivals = await participantRepository.readArrivalsForConfirmedParticipants();
      expect(arrivals).toEqual(participantArrivals);
    });
  });
  describe('reading participant departures', () => {
    let promise;
    beforeEach(() => {
      mySql.select.mockImplementation(() => promise);
    });
    afterEach(() => {
      mySql.select.mockReset();
    });
    it('gets the departures', async() => {
      promise = Promise.resolve(participantDepartures);
      const arrivals = await participantRepository.readDeparturesForConfirmedParticipants();
      expect(arrivals).toEqual(participantDepartures);
    });
  });
  describe('reading total amount of participants', () => {
    let promise;
    beforeEach(() => {
      mySql.select.mockImplementation(() => promise);
    });
    afterEach(() => {
      mySql.select.mockReset();
    });
    it('gets the amount of confirmed participants', async() => {
      promise = Promise.resolve([{amount: 222}]);
      const arrivals = await participantRepository.getTotalAmountOfParticipants();
      expect(arrivals).toEqual(222);
    });
    it('gets zero on empty result', async() => {
      promise = Promise.resolve([]);
      const arrivals = await participantRepository.getTotalAmountOfParticipants();
      expect(arrivals).toEqual(0);
    });
    it('gets zero on error', async() => {
      promise = Promise.reject(new Error('Error'));
      const arrivals = await participantRepository.getTotalAmountOfParticipants();
      expect(arrivals).toEqual(0);
    });
  });
});

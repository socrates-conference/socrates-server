// @flow
import MySql from '../MySql';
import type {
  ArrivalTime,
  ConferenceDay,
  ConferenceEdition, ConferenceEditionSponsor,
  DepartureTime,
  LengthOfStay,
  RoomType
} from '../../domain/conferenceTypes';

export default class ConferenceEditionRepository {

  _mySql: MySql;

  constructor(mySql: MySql) {
    this._mySql = mySql;
  }

  async readConferenceEdition(conferenceEditionId: number): Promise<?ConferenceEdition> {
    const sql = 'SELECT c.name, c.longName, ce.* FROM conference_edition as ce ' +
      'INNER JOIN conference as c ON ce.conferenceId = c.id WHERE ce.id = ?';
    const data = await this._mySql.selectWithParams(sql, [conferenceEditionId]);
    if (data && data.length === 1) {
      return data[0];
    } else {
      return null;
    }
  }

  async readConferenceDays(conferenceEditionId: number): Promise<ConferenceDay[]> {
    const sql = 'SELECT * FROM conference_day WHERE conferenceEditionId = ?';
    const data = await this._mySql.selectWithParams(sql, [conferenceEditionId]);
    if (data) {
      return data;
    } else {
      return [];
    }
  }

  async readConferenceArrivalTimes(conferenceEditionId: number): Promise<ArrivalTime[]> {
    const sql = 'SELECT * FROM arrival_time WHERE conferenceEditionId = ?';
    const data = await this._mySql.selectWithParams(sql, [conferenceEditionId]);
    if (data) {
      return data;
    } else {
      return [];
    }
  }

  async readConferenceDepartureTimes(conferenceEditionId: number): Promise<DepartureTime[]> {
    const sql = 'SELECT * FROM departure_time WHERE conferenceEditionId = ?';
    const data = await this._mySql.selectWithParams(sql, [conferenceEditionId]);
    if (data) {
      return data;
    } else {
      return [];
    }
  }

  async readConferenceRoomTypes(conferenceEditionId: number): Promise<RoomType[]> {
    const sql = 'SELECT *, name as display FROM room_type WHERE conferenceEditionId = ?';
    const data = await this._mySql.selectWithParams(sql, [conferenceEditionId]);
    if (data) {
      return data;
    } else {
      return [];
    }
  }

  async readConferenceLengthsOfStay(conferenceEditionId: number): Promise<LengthOfStay[]> {
    const sql = 'SELECT sl.*, a.name as `from`, a.conferenceDayId as arrivalDayId,  ' +
      'd.name as `to`, d.conferenceDayId as departureDayId, ' +
      'd.hasToPayConferenceDayFee as hasDepartureDayFee ' +
      'FROM stay_length as sl ' +
      'INNER JOIN arrival_time as a ON sl.arrivalId = a.id ' +
      'INNER JOIN departure_time as d ON sl.departureId = d.id ' +
      'WHERE a.conferenceEditionId = ? AND d.conferenceEditionId = ?';
    const data = await this._mySql.selectWithParams(sql, [conferenceEditionId, conferenceEditionId]);
    if (data) {
      return data;
    } else {
      return [];
    }
  }

  async getTotalSponsoring(conferenceEditionId: number): Promise<number> {
    const sql = 'SELECT SUM(sp.price-se.amountForOrganisationalStuff) as totalSponsoring ' +
      'FROM sponsor_conference_event as se ' +
      'INNER JOIN sponsoring_package as sp ON se.packageId = sp.id ' +
      'WHERE se.conferenceEditionId = ?';
    const data = await this._mySql.selectWithParams(sql, [conferenceEditionId]);
    if (data && data.length === 1) {
      return data[0].totalSponsoring;
    }
    return 0;
  }

  async readSponsorsForHotel(conferenceEditionId: number): Promise<ConferenceEditionSponsor[]> {
    const sql = 'SELECT sce.sponsorId, s.name, s.nameAdditional, s.address1, s.address2, s.province, ' +
      's.postal, s.city, s.country, (sp.price-sce.amountForOrganisationalStuff) as amount ' +
      'FROM sponsor_conference_event as sce ' +
      'INNER JOIN sponsor as s ON sce.sponsorId = s.id ' +
      'INNER JOIN sponsoring_package as sp ON sce.packageId = sp.id ' +
      'WHERE sce.conferenceEditionId = ? AND sce.stateOfRequestForSponsoring = 3 ' +
      'AND sp.conferenceEditionId = ? AND (sp.price - sce.amountForOrganisationalStuff) > 0';
    const result = await this._mySql.selectWithParams(sql, [conferenceEditionId, conferenceEditionId]);
    if (result) {
      return result;
    }
    return [];
  }

}


import moment from 'moment';

const now = '2019-04-01T00:00:00';

export const conferenceEditionStubbed = {
  id: 3,
  conferenceId: 2,
  name: 'Test-Conf',
  longName: 'Test conference',
  year: 2019,
  startDate: moment(now),
  endDate: moment(now),
  startApplications: moment(now),
  endApplications: moment(now),
  lotteryDay: moment(now),
  lunchPrice: 10,
  dinnerPrice: 15
};

export const conferenceDaysStubbed = [
  {id: 21, conferenceEditionId: 1, name: 'Thursday', conferenceDayNumber: 1, fee: 20},
  {id: 22, conferenceEditionId: 1, name: 'Friday', conferenceDayNumber: 2, fee: 90},
  {id: 23, conferenceEditionId: 1, name: 'Saturday', conferenceDayNumber: 3, fee: 65},
  {id: 24, conferenceEditionId: 1, name: 'Sunday', conferenceDayNumber: 4, fee: 44}
];

export const conferenceArrivalsStubbed = [
  {
    id: 4, legacyId: '0', conferenceEditionId: 1, conferenceDayId: 21, name: 'Day One Morning',
    hint: 'Very Important Info to share with the hotel.'
  },
  {
    id: 5, legacyId: '1', conferenceEditionId: 1, conferenceDayId: 21, name: 'Day One Evening',
    hint: 'Another Important Info to share with the hotel.'
  }
];

export const conferenceDeparturesStubbed = [
  {
    id: 66, legacyId: '0', conferenceEditionId: 1, conferenceDayId: 23, name: 'Day Three Evening',
    hint: 'Very Important Info to share with the hotel.', hasToPayConferenceDayFee: true
  },
  {
    id: 76, legacyId: '1', conferenceEditionId: 1, conferenceDayId: 23, name: 'Day Four Morning',
    hint: 'Another Important Info to share with the hotel.', hasToPayConferenceDayFee: false
  }
];

export const conferenceRoomTypesStubbed = [
  {
    id: 'single', conferenceEditionId: 1, display: 'Single', nightPricePerPerson: 55,
    numberOfRooms: 100, peoplePerRoom: 1, reservedOrganization: 10, reservedSponsors: 5,
    lotterySortOrder: 2
  },
  {
    id: 'shared', conferenceEditionId: 1, display: 'Shared', nightPricePerPerson: 40,
    numberOfRooms: 30, peoplePerRoom: 2, reservedOrganization: 5, reservedSponsors: 5,
    lotterySortOrder: 1
  }
];

export const conferenceLengthOfStayStubbed = [
  {
    id: 1, arrivalId: 4, departureId: 66, from: 'Thursday afternoon',
    to: 'Saturday evening', nights: 3, lunches: 4, dinners: 3,
    arrivalDayId: 21, departureDayId: 23, hasDepartureDayFee: true
  },
  {
    id: 2, arrivalId: 4, departureId: 76, from: 'Thursday afternoon',
    to: 'Sunday morning', nights: 4, lunches: 4, dinners: 4,
    arrivalDayId: 21, departureDayId: 24, hasDepartureDayFee: false
  }
];


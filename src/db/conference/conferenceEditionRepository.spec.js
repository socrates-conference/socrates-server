// @flow

import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import ConferenceEditionRepository from './conferenceEditionRepository';
import type {ConferenceEditionSponsor} from '../../domain/conferenceTypes';

jest.mock('../MySql');

let repository: ConferenceEditionRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('Conferences edition repository', () => {

  beforeEach(() => {
    repository = new ConferenceEditionRepository(mySql);
  });

  describe('reading conference edition for conference', () => {
    let promise;
    const conferenceEditionStubbed = {
      id: 1,
      conferenceId: 1,
      year: 2019
    };
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('returns conference edition if found', async () => {
      promise = Promise.resolve([conferenceEditionStubbed]);
      const conferenceEdition = await repository.readConferenceEdition(1);
      expect(conferenceEdition).toBeDefined();
      expect(conferenceEdition).toEqual(conferenceEditionStubbed);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('returns null if not found', async () => {
      promise = Promise.resolve(null);
      const conferenceEdition = await repository.readConferenceEdition(1);
      expect(conferenceEdition).toBeNull();
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
  });
  describe('reading conference days for conference edition', () => {
    let promise;
    const conferenceDaysStubbed = [
      {id: 21, conferenceEditionId: 1, name: 'Day One', conferenceDayNumber: 1, fee: 20},
      {id: 22, conferenceEditionId: 1, name: 'Day Two', conferenceDayNumber: 2, fee: 90},
      {id: 23, conferenceEditionId: 1, name: 'Day Three', conferenceDayNumber: 3, fee: 65},
      {id: 24, conferenceEditionId: 1, name: 'Day Four', conferenceDayNumber: 4, fee: 44}
    ];

    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('returns days if found', async () => {
      promise = Promise.resolve(conferenceDaysStubbed);
      const conferenceDays = await repository.readConferenceDays(1);
      expect(conferenceDays).toBeDefined();
      expect(conferenceDays).toEqual(conferenceDaysStubbed);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('returns empty array if not found', async () => {
      promise = Promise.resolve([]);
      const conferenceDays = await repository.readConferenceDays(1);
      expect(conferenceDays).toBeDefined();
      expect(conferenceDays).toEqual([]);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('returns empty array if not found', async () => {
      promise = Promise.resolve(null);
      const conferenceDays = await repository.readConferenceDays(1);
      expect(conferenceDays).toBeDefined();
      expect(conferenceDays).toEqual([]);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
  });
  describe('reading arrival times for conference edition', () => {
    let promise;
    const conferenceArrivalsStubbed = [
      {
        id: 4, conferenceEditionId: 1, conferenceDayId: 5, name: 'Day One Morning',
        hint: 'Very Important Info to share with the hotel.'
      },
      {
        id: 5, conferenceEditionId: 1, conferenceDayId: 5, name: 'Day One Evening',
        hint: 'Another Important Info to share with the hotel.'
      }
    ];
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('returns days if found', async () => {
      promise = Promise.resolve(conferenceArrivalsStubbed);
      const conferenceDays = await repository.readConferenceArrivalTimes(1);
      expect(conferenceDays).toBeDefined();
      expect(conferenceDays).toEqual(conferenceArrivalsStubbed);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('returns empty array if not found', async () => {
      promise = Promise.resolve(null);
      const conferenceDays = await repository.readConferenceArrivalTimes(1);
      expect(conferenceDays).toBeDefined();
      expect(conferenceDays).toEqual([]);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
  });
  describe('reading departure times for conference edition', () => {
    let promise;
    const conferenceDeparturesStubbed = [
      {
        id: 66, conferenceEditionId: 1, conferenceDayId: 33, name: 'Day Three Evening',
        hint: 'Very Important Info to share with the hotel.', hasToPayConferenceDayFee: true
      },
      {
        id: 76, conferenceEditionId: 1, conferenceDayId: 34, name: 'Day Four Morning',
        hint: 'Another Important Info to share with the hotel.', hasToPayConferenceDayFee: false
      }
    ];
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('returns days if found', async () => {
      promise = Promise.resolve(conferenceDeparturesStubbed);
      const conferenceDays = await repository.readConferenceDepartureTimes(1);
      expect(conferenceDays).toBeDefined();
      expect(conferenceDays).toEqual(conferenceDeparturesStubbed);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('returns empty array if not found', async () => {
      promise = Promise.resolve(null);
      const conferenceDays = await repository.readConferenceDepartureTimes(1);
      expect(conferenceDays).toBeDefined();
      expect(conferenceDays).toEqual([]);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
  });
  describe('reading room types for conference edition', () => {
    let promise;
    const conferenceRoomTypesStubbed = [
      {
        id: 'single', conferenceEditionId: 1, display: 'Single', nightPricePerPerson: 55,
        numberOfRooms: 100, peoplePerRoom: 1, reservedOrganization: 10, reservedSponsors: 5,
        lotterySortOrder: 2
      },
      {
        id: 'shared', conferenceEditionId: 1, display: 'Shared', nightPricePerPerson: 40,
        numberOfRooms: 30, peoplePerRoom: 2, reservedOrganization: 5, reservedSponsors: 5,
        lotterySortOrder: 1
      }
    ];
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('returns days if found', async () => {
      promise = Promise.resolve(conferenceRoomTypesStubbed);
      const conferenceDays = await repository.readConferenceRoomTypes(1);
      expect(conferenceDays).toBeDefined();
      expect(conferenceDays).toEqual(conferenceRoomTypesStubbed);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('returns empty array if not found', async () => {
      promise = Promise.resolve(null);
      const conferenceDays = await repository.readConferenceRoomTypes(1);
      expect(conferenceDays).toBeDefined();
      expect(conferenceDays).toEqual([]);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
  });
  describe('reading lengths of stay for conference edition', () => {
    let promise;
    const lengthOfStayStubbed = [
      {
        id: 1, arrivalId: 6, departureId: 12, from: 'First Day in the morning',
        to: ' Fourth day in the evening', nights: 3, lunches: 4, dinners: 4,
        arrivalDayId: 5, departureDayId: 9, hasDepartureDayFee: true
      },
      {
        id: 2, arrivalId: 6, departureId: 11, from: 'First Day in the morning',
        to: ' Fourth day in the afternoon', nights: 3, lunches: 4, dinners: 3,
        arrivalDayId: 5, departureDayId: 9, hasDepartureDayFee: true
      }
    ];
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('returns days if found', async () => {
      promise = Promise.resolve(lengthOfStayStubbed);
      const conferenceDays = await repository.readConferenceLengthsOfStay(1);
      expect(conferenceDays).toBeDefined();
      expect(conferenceDays).toEqual(lengthOfStayStubbed);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('returns empty array if not found', async () => {
      promise = Promise.resolve(null);
      const conferenceDays = await repository.readConferenceLengthsOfStay(1);
      expect(conferenceDays).toBeDefined();
      expect(conferenceDays).toEqual([]);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
  });
  describe('reading total sponsoring amount for conference edition', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementationOnce(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('returns value if found', async () => {
      promise = Promise.resolve([{totalSponsoring: 350}]);
      const totalSponsoring = await repository.getTotalSponsoring(1);
      expect(totalSponsoring).toBeDefined();
      expect(totalSponsoring).toEqual(350);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('returns zero if not found', async () => {
      promise = Promise.resolve(null);
      const totalSponsoring = await repository.getTotalSponsoring(1);
      expect(totalSponsoring).toBeDefined();
      expect(totalSponsoring).toEqual(0);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
  });
  describe('reading the sponsors list for the hotel information', () => {
    let promise;
    const sponsors: ConferenceEditionSponsor[] = [
      {
        sponsorId: 21, name: 'sponsor 1', nameAdditional: 'name additional 1', address1: 'sponsor 1 address 1',
        address2: 'sponsor1 address 2', province: 'sponsor 1 province', postal: 'sponsor 1 postal',
        city: 'sponsor 1 city', country: 'sponsor 1 country', amount: 1000
      },
      {
        sponsorId: 22, name: 'sponsor 2', nameAdditional: 'name additional 2', address1: 'sponsor2 address 1',
        address2: 'sponsor 2 address 2', province: 'sponsor 2 province', postal: 'sponsor 2 postal',
        city: 'sponsor 2 city', country: 'sponsor 2 country', amount: 2000
      }
    ];
    beforeEach(() => {
      mySql.selectWithParams.mockImplementationOnce(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('returns sponsors if found', async () => {
      promise = Promise.resolve(sponsors);
      const sponsorsResult = await repository.readSponsorsForHotel(1);
      expect(sponsorsResult).toBeDefined();
      expect(sponsorsResult).toHaveLength(2);
      expect(sponsorsResult).toEqual(sponsors);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
    it('returns empty array if not found', async () => {
      promise = Promise.resolve(null);
      const sponsorsResult = await repository.readSponsorsForHotel(1);
      expect(sponsorsResult).toBeDefined();
      expect(sponsorsResult).toEqual([]);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
  });
});

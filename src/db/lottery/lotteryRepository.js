// @flow

import MySql from '../MySql';

export default class LotteryRepository {
  _mySql: MySql;

  _applicationsByRoomType = 'SELECT count(id) as counter, a.room_type as roomType ' +
    'FROM socrates_db.application as a GROUP BY a.room_type;';

  _registrationsByRoomType = 'SELECT count(id) as counter, room_type as roomType, ' +
    'diversity_reason as diversityReason ' +
    'FROM registration as r group by room_type, diversity_reason;';

  constructor(mySql: MySql) {
    this._mySql = mySql;
  }

  async readApplicationsByRoomType(): Promise<Array<Object>> {
    const data: Array<Object> = await this._mySql.select(this._applicationsByRoomType);
    if (data && data.length > 0) {
      return data.map(item => {
        return {
          counter: item.counter,
          roomType: item.roomType
        };
      });
    } else {
      return [];
    }
  }

  async readRegistrationsByRoomType(): Promise<Object[]> {
    const data: Array<Object> = await this._mySql.select(this._registrationsByRoomType);
    if (data && data.length > 0) {
      return data.map(item => {
        return {
          counter: item.counter,
          roomType: item.roomType,
          diversityReason: item.diversityReason
        };
      });
    } else {
      return [];
    }
  }
}

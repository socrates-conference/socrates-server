// @flow

import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import LotteryRepository from './lotteryRepository';

jest.mock('../MySql');

let lotteryRepository: LotteryRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('Lottery repository', () => {

  beforeEach(() => {
    lotteryRepository = new LotteryRepository(mySql);
  });

  describe('can read applications by room type', () => {
    beforeEach(() => {
      mySql.select.mockImplementation(() => {
        return Promise.resolve([]);
      });
    });
    afterEach(() => {
      mySql.select.mockReset();
    });

    it('without errors, when no data available', async () => {
      const applications = await lotteryRepository.readApplicationsByRoomType();
      expect(applications).toBeDefined();
      expect(applications).toEqual([]);
      expect(mySql.select.mock.calls.length).toBe(1);
    });
  });
  describe('can read applications by room type data', () => {
    beforeEach(() => {
      mySql.select.mockImplementation(() => {
        return Promise.resolve([{counter: 3, roomType: 'single'}]);
      });
    });
    afterEach(() => {
      mySql.select.mockReset();
    });

    it('without errors, when data available', async () => {
      const applications = await lotteryRepository.readApplicationsByRoomType();
      expect(applications).toBeDefined();
      expect(applications).toEqual([{counter: 3, roomType: 'single'}]);
      expect(mySql.select.mock.calls.length).toBe(1);
    });
  });
  describe('can read registrations by room type', () => {
    beforeEach(() => {
      mySql.select.mockImplementation(() => {
        return Promise.resolve([]);
      });
    });
    afterEach(() => {
      mySql.select.mockReset();
    });

    it('without errors, when no data available', async () => {
      const applications = await lotteryRepository.readRegistrationsByRoomType();
      expect(applications).toBeDefined();
      expect(applications).toEqual([]);
      expect(mySql.select.mock.calls.length).toBe(1);
    });
  });
  describe('can read registrations by room type data', () => {
    beforeEach(() => {
      mySql.select.mockImplementation(() => {
        return Promise.resolve([{counter: 3, roomType: 'single'}]);
      });
    });
    afterEach(() => {
      mySql.select.mockReset();
    });

    it('without errors, when data available', async () => {
      const applications = await lotteryRepository.readRegistrationsByRoomType();
      expect(applications).toBeDefined();
      expect(applications).toEqual([{counter: 3, roomType: 'single'}]);
      expect(mySql.select.mock.calls.length).toBe(1);
    });
  });
});
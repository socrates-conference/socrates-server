/* eslint-disable camelcase */
// @flow

import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import InvitationsMySqlRepository from './invitationsRepository';

jest.mock('../MySql');

let invitationsRepository: InvitationsMySqlRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

const rawInvitationData = {
  id: 4711,
  inviter_person_id: 1,
  inviter_email: 'test@test.de',
  invitee1_person_id: 1,
  invitee1_email: 'test@test.de',
  invitee2_person_id: 2,
  invitee2_email: 'test2@test.de',
  status: 'OPEN',
  key: 'key'
};
const rawRoomSharingData = [
  {
    P1Id: 1,
    P1FirstName: 'P1FirstName',
    P1LastName: 'P1LastName',
    P1FamilyInfo: 'P1FamilyInfo',
    P2Id: 2,
    P2FirstName: 'P2FirstName',
    P2LastName: 'P2LastName',
    P2FamilyInfo: 'P2FamilyInfo',
    RoomType: 'roomType'
  },
  {
    P1Id: 3,
    P1FirstName: 'P3FirstName',
    P1LastName: 'P3LastName',
    P1FamilyInfo: 'P3FamilyInfo',
    P2Id: 4,
    P2FirstName: 'P4FirstName',
    P2LastName: 'P4LastName',
    P2FamilyInfo: 'P4FamilyInfo',
    RoomType: 'roomType'
  }
];

const invitationStub = {
  id: 4711,
  inviter: {id: 1, email: 'test@test.de'},
  invitee1: {id: 1, email: 'test@test.de'},
  invitee2: {id: 2, email: 'test2@test.de'},
  status: 'OPEN',
  key: 'key'
};

const invitationRequestStub = {
  inviterId: 1,
  inviterEmail: 'test@test.de',
  invitee1Id: 1,
  invitee1Email: 'test@test.de',
  invitee2Id: 0,
  invitee2Email: 'test2@test.de'
};

describe('Invitations repository:', () => {

  beforeEach(() => {
    invitationsRepository = new InvitationsMySqlRepository(mySql);
  });

  describe('when searching invitation', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });
    describe('by id', () => {
      it('returns invitation if it exists', async () => {
        promise = Promise.resolve([rawInvitationData]);
        const invitation = await invitationsRepository.find(4711);
        expect(invitation).toBeDefined();
        if (invitation) {
          expect(invitation.id).toBe(4711);
          expect(invitation.inviter).toEqual({id: 1, email: 'test@test.de'});
          expect(invitation.invitee1).toEqual({id: 1, email: 'test@test.de'});
          expect(invitation.invitee2).toEqual({id: 2, email: 'test2@test.de'});
        }
      });
      it('returns null if it does not exist', async () => {
        promise = Promise.resolve([]);
        const invitation = await invitationsRepository.find(4711);
        expect(invitation).toBeNull();
      });
    });
    describe('by key', () => {
      it('returns invitation if it exists', async () => {
        promise = Promise.resolve([rawInvitationData]);
        const invitation = await invitationsRepository.findByKey('key');
        expect(invitation).toBeDefined();
        if (invitation) {
          expect(invitation.id).toBe(4711);
          expect(invitation.inviter).toEqual({id: 1, email: 'test@test.de'});
          expect(invitation.invitee1).toEqual({id: 1, email: 'test@test.de'});
          expect(invitation.invitee2).toEqual({id: 2, email: 'test2@test.de'});
        }
      });
      it('returns null if it does not exist', async () => {
        promise = Promise.resolve([]);
        const invitation = await invitationsRepository.findByKey('anotherKey');
        expect(invitation).toBeNull();
      });
    });
  });
  describe('when searching all invitations', () => {
    let promise;
    beforeEach(() => {
      mySql.select.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.select.mockReset();
    });
    it('returns the invitations', async () => {
      promise = Promise.resolve([rawInvitationData]);
      const invitations = await invitationsRepository.findAll();
      expect(invitations).toBeDefined();
      expect(invitations.length).toBe(1);
    });
    it('returns empty array if no invitations available', async () => {
      promise = Promise.resolve([]);
      const invitations = await invitationsRepository.findAll();
      expect(invitations).toBeDefined();
      expect(invitations.length).toBe(0);
    });
  });
  describe('when updating an invitation', () => {
    let promise;
    beforeEach(() => {
      mySql.update.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.update.mockReset();
    });
    it('returns the invitations', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 1});
      const result = await invitationsRepository.update(invitationStub);
      expect(result).toBeDefined();
    });
  });

  describe('when creating an invitation', () => {
    let promise;
    beforeEach(() => {
      mySql.insert.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.insert.mockReset();
    });
    it('returns the created invitation', async () => {
      promise = Promise.resolve({lastInsertedId: 4711, numberOfRowsAffected: 1});
      const result = await invitationsRepository.create(invitationRequestStub, 'key', 'OPEN');
      expect(result).toBeDefined();
      if (result) {
        expect(result.id).toBe(4711);
      }
    });
  });
  describe('when getting person info by email', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });
    it('returns person data', async () => {
      const person = {
        id: 1,
        email: 'test@test.de'
      };
      promise = Promise.resolve([person]);
      const result = await invitationsRepository.readPersonByEmail('test@test.de');
      expect(result).toBeDefined();
      if (result) {
        expect(result.id).toBe(1);
        expect(result.email).toEqual('test@test.de');
      }
    });
  });
  describe('when getting sent invitation', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });
    it('returns invitation if available', async () => {
      promise = Promise.resolve([rawInvitationData]);
      const result = await invitationsRepository.readSentInvitationByPersonId(1);
      expect(result).toBeDefined();
      if (result) {
        expect(result.id).toBe(4711);
        expect(result.inviter).toEqual({id: 1, email: 'test@test.de'});
        expect(result.invitee1).toEqual({id: 1, email: 'test@test.de'});
        expect(result.invitee2).toEqual({id: 2, email: 'test2@test.de'});
      }
    });
    it('returns null if not available', async () => {
      promise = Promise.resolve([]);
      const result = await invitationsRepository.readSentInvitationByPersonId(1);
      expect(result).toBeNull();
    });
  });

  describe('when getting received invitations', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });
    it('returns invitations if available', async () => {
      promise = Promise.resolve([rawInvitationData]);
      const results = await invitationsRepository.readReceivedInvitationsByPersonId(1);
      expect(results).toBeDefined();
      expect(results.length).toBe(1);
      expect(results[0].id).toBe(4711);
      expect(results[0].inviter).toEqual({id: 1, email: 'test@test.de'});
      expect(results[0].invitee1).toEqual({id: 1, email: 'test@test.de'});
      expect(results[0].invitee2).toEqual({id: 2, email: 'test2@test.de'});
    });
    it('returns empty array if not available', async () => {
      promise = Promise.resolve([]);
      const result = await invitationsRepository.readReceivedInvitationsByPersonId(1);
      expect(result.length).toBe(0);
    });
  });

  describe('when checking participants room types', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });
    it('returns true if same room type', async () => {
      promise = Promise.resolve([{sameRoomType: 1}]);
      const result = await invitationsRepository.haveParticipantsSameRoomType(1, 2);
      expect(result).toBe(true);
    });
    it('returns false if different room type', async () => {
      promise = Promise.resolve([{sameRoomType: 0}]);
      const result = await invitationsRepository.haveParticipantsSameRoomType(1, 6);
      expect(result).toBe(false);
    });
  });

  describe('when checking if a participant has already a roommate', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });
    it('returns true if they have', async () => {
      promise = Promise.resolve([{hasRoommate: 1}]);
      const result = await invitationsRepository.hasRoommate(2);
      expect(result).toBe(true);
    });
    it('returns false if they do not have', async () => {
      promise = Promise.resolve([{hasRoommate: 0}]);
      const result = await invitationsRepository.hasRoommate(6);
      expect(result).toBe(false);
    });
  });

  describe('when deleting sent invitations by email', () => {
    let promise;
    beforeEach(() => {
      mySql.delete.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.delete.mockReset();
    });
    it('calls mysql delete', async () => {
      promise = Promise.resolve();
      await invitationsRepository.deleteSentInvitationsByMail('test@test.de');
      expect(mySql.delete).toBeCalledWith('DELETE FROM invitations WHERE invitee1_email = ?', ['test@test.de']);
    });

  });

  describe('when deleting received invitations by email', () => {
    let promise;
    beforeEach(() => {
      mySql.delete.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.delete.mockReset();
    });
    it('calls mysql delete', async () => {
      promise = Promise.resolve();
      await invitationsRepository.deleteReceivedInvitationsByMail('test@test.de');
      expect(mySql.delete).toBeCalledWith('DELETE FROM invitations WHERE invitee2_email = ?', ['test@test.de']);
    });

  });

  describe('when reading all room mates', () => {
    let promise;
    beforeEach(() => {
      mySql.select.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.select.mockReset();
    });
    it('returns the sharing information', async () => {
      promise = Promise.resolve(rawRoomSharingData);
      const roomSharing = await invitationsRepository.getAllRoomSharingData();
      expect(roomSharing).toBeDefined();
      expect(roomSharing.length).toBe(2);
    });
    it('returns empty array if no sharing data available', async () => {
      promise = Promise.resolve([]);
      const roomSharing = await invitationsRepository.getAllRoomSharingData();
      expect(roomSharing).toBeDefined();
      expect(roomSharing.length).toBe(0);
    });
  });

});

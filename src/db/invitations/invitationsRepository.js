//@flow

import type {Invitation, InvitationRequest, InvitationStatus, RoomSharing} from '../../domain/invitations';
import MySql from '../MySql';
import type {PersonResult} from '../application/personMySQL';

export interface InvitationsRepository {
  find(id: number): Promise<?Invitation>;

  findByKey(key: string): Promise<?Invitation>;

  findAll(): Promise<Array<Invitation>>;

  update(update: Invitation): Promise<Invitation>;

  create(invitationRequest: InvitationRequest, generatedKey: string, status: InvitationStatus): Promise<Invitation>;

  readPersonByEmail(theEmail: string): Promise<?PersonResult>;

  readSentInvitationByPersonId(personId: number): Promise<?Invitation>;

  readReceivedInvitationsByPersonId(personId: number): Promise<Invitation[]>;

  haveParticipantsSameRoomType(firstId: number, secondId: number): Promise<boolean>;

  hasRoommate(id: number): Promise<boolean>;

  deleteSentInvitationsByMail(email: string): Promise<void>;

  deleteReceivedInvitationsByMail(email: string): Promise<void>;

  getAllRoomSharingData(): Promise<RoomSharing[]>
}

type InsertResult = { error: Error } | { numberOfRowsAffected: number, lastInsertedId: number }
type UpdateResult = { error: Error } | { numberOfRowsAffected: number }

function invitationsEntryToInvitation(selected): Invitation {
  return {
    id: selected.id, status: selected.status,
    inviter: {id: selected.inviter_person_id, email: selected.inviter_email},
    invitee1: {id: selected.invitee1_person_id, email: selected.invitee1_email},
    invitee2: {id: selected.invitee2_person_id, email: selected.invitee2_email},
    key: selected.invitation_key
  };
}

const findSelect = 'SELECT inviter_person_id, inviter_email,' +
  'invitee1_person_id, invitee1_email,' +
  'invitee2_person_id, invitee2_email,' +
  'status, id, invitation_key FROM invitations';

export default class InvitationsMySqlRepository implements InvitationsRepository {
  _mysql: MySql;

  constructor(mysql: MySql) {
    this._mysql = mysql;
  }

  async find(id: number): Promise<?Invitation> {
    const selectStatement = `${findSelect} WHERE id=?`;
    const selected = await this._mysql.selectWithParams(selectStatement, [id]);
    return selected && selected.length === 1 ? invitationsEntryToInvitation(selected[0]) : null;
  }

  async findByKey(key: string): Promise<?Invitation> {
    const selectStatement = `${findSelect} WHERE invitation_key=?`;
    const selected = await this._mysql.selectWithParams(selectStatement, [key]);
    return selected && selected.length === 1 ? invitationsEntryToInvitation(selected[0]) : null;
  }

  async deleteSentInvitationsByMail(email: string): Promise<void> {
    const deleteStatement = 'DELETE FROM invitations WHERE invitee1_email = ?';
    await this._mysql.delete(deleteStatement, [email]);
  }

  async deleteReceivedInvitationsByMail(email: string): Promise<void> {
    const deleteStatement = 'DELETE FROM invitations WHERE invitee2_email = ?';
    await this._mysql.delete(deleteStatement, [email]);
  }

  async findAll(): Promise<Array<Invitation>> {
    const selected = await this._mysql.select(findSelect);
    return selected && selected.length > 0
      ? selected.map(invitationsEntryToInvitation)
      : [];
  }

  async update(invitation: Invitation): Promise<Invitation> {
    const values = 'inviter_person_id=?, inviter_email=?, ' +
      'invitee1_person_id=?, invitee1_email=?, ' +
      'invitee2_person_id=?, invitee2_email=?, ' +
      'status=?';
    const updateStatement = `UPDATE invitations SET ${values} WHERE id=?`;
    const queryResult: UpdateResult = await this._mysql
      .update(updateStatement,
        [invitation.inviter.id, invitation.inviter.email,
          invitation.invitee1.id, invitation.invitee1.email,
          invitation.invitee2.id, invitation.invitee2.email,
          invitation.status, invitation.id])
      .catch(error => ({error}));

    if (queryResult.error) {
      throw new Error(queryResult.error);
    }

    return {...invitation};
  }

  async create
  (invitationRequest: InvitationRequest, generatedKey: string, status: InvitationStatus): Promise<Invitation> {
    const fieldNames = 'inviter_person_id, inviter_email, ' +
      'invitee1_person_id, invitee1_email, ' +
      'invitee2_person_id, invitee2_email, ' +
      'status, invitation_key';
    const insertStatement = `INSERT INTO invitations (${fieldNames}) VALUES (?,?,?,?,?,?,?,?)`;
    const queryResult: InsertResult = await this._mysql
      .insert(insertStatement,
        [invitationRequest.inviterId, invitationRequest.inviterEmail,
          invitationRequest.invitee1Id, invitationRequest.invitee1Email,
          invitationRequest.invitee2Id, invitationRequest.invitee2Email, status,
          generatedKey
        ])
      .catch(error => ({error}));

    if (queryResult.error) {
      throw new Error(queryResult.error);
    }

    return {
      id: queryResult.lastInsertedId,
      inviter: {id: invitationRequest.inviterId, email: invitationRequest.inviterEmail},
      invitee1: {id: invitationRequest.invitee1Id, email: invitationRequest.invitee1Email},
      invitee2: {id: invitationRequest.invitee2Id, email: invitationRequest.invitee2Email},
      status: status, key: generatedKey
    };
  }

  async readPersonByEmail(theEmail: string): Promise<?PersonResult> {
    const readPersonSql = 'SELECT * FROM person WHERE email = ?';
    const values = [theEmail];
    const results = await this._mysql.selectWithParams(readPersonSql, values);
    if (results && results.length === 1) {
      return results[0];
    }
    return null;
  }

  async readSentInvitationByPersonId(personId: number): Promise<?Invitation> {
    const readParticipantSql = 'SELECT * FROM invitations WHERE status <> "DECLINED" AND invitee1_person_id = ?';
    const values = [personId];
    const results = await this._mysql.selectWithParams(readParticipantSql, values);
    if (results && results.length === 1) {
      return invitationsEntryToInvitation(results[0]);
    }
    return null;
  }

  async readReceivedInvitationsByPersonId(personId: number): Promise<Invitation[]> {
    const readParticipantSql = 'SELECT * FROM invitations WHERE status <> "DECLINED" AND invitee2_person_id = ?';
    const values = [personId];
    const results = await this._mysql.selectWithParams(readParticipantSql, values);
    if (results && results.length > 0) {
      const invitations = [];
      for (let index = 0; index < results.length; index++) {
        invitations.push(invitationsEntryToInvitation(results[index]));
      }
      return invitations;
    }
    return [];
  }

  async haveParticipantsSameRoomType(firstId: number, secondId: number): Promise<boolean> {
    const haveSameRoomType = 'SELECT count(r1.id) as sameRoomType FROM registration r1 INNER JOIN registration r2 ' +
      'ON r1.room_type = r2.room_type WHERE r1.id <> r2.id and r1.person_id=? and r2.person_id=?;';
    const values = [firstId, secondId];
    const results = await this._mysql.selectWithParams(haveSameRoomType, values);
    return Boolean(results && results.length === 1 && results[0].sameRoomType === 1);
  }

  async hasRoommate(id: number): Promise<boolean> {
    const hasRoommate = 'SELECT count(id) as hasRoommate FROM socrates_db.invitations WHERE status=\'ACCEPTED\' ' +
      'AND (invitee1_person_id=? OR invitee2_person_id =?);';
    const values = [id, id];
    const results = await this._mysql.selectWithParams(hasRoommate, values);
    return Boolean(results && results.length === 1 && results[0].hasRoommate === 1);
  }

  async getAllRoomSharingData(): Promise<Object[]> {
    const roomSharingSql = 'SELECT p1.firstname as P1FirstName, p1.lastname as P1LastName, r1.room_type as RoomType, ' +
      'p2.firstname as P2FirstName, p2.lastname as P2LastName, p1.family_info as P1FamilyInfo, ' +
      'p2.family_info as P2FamilyInfo, i.invitee1_person_id  as P1Id, i.invitee2_person_id  as P2Id, ' +
      'p1.address1 as P1Address1, p1.address2 as P1Address2, p1.company as P1Company, p1.province as P1Province, ' +
      'p1.city as P1City, p1.postal as P1Postal, p1.country as P1Country, ' +
      'p2.address1 as P2Address1, p2.address2 as P2Address2, p2.company as P2Company, p2.province as P2Province, ' +
      'p2.city as P2City, p2.postal as P2Postal, p2.country as P2Country ' +
      'FROM socrates_db.invitations as i ' +
      'INNER JOIN participants as p1 ON p1.person_id = i.invitee1_person_id ' +
      'INNER JOIN registration as r1 ON r1.person_id = i.invitee1_person_id ' +
      'INNER JOIN participants as p2 ON p2.person_id = i.invitee2_person_id ' +
      'where i.status=\'ACCEPTED\';';
    const results = await this._mysql.select(roomSharingSql);
    return results ? results : [];
  }
}

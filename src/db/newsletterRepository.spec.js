// @flow

import MySql from './MySql';
import TestHelper from './SqlTestHelper';
import NewsletterRepository from './newsletterRepository';
import type {NewsletterSubscriber} from '../domain/newsletter';

jest.mock('./MySql');

let interestedPeopleHandler: NewsletterRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('Newsletter repository should', () => {

  let selectPromise, selectWithParamsPromise, updatePromise, insertPromise, deletePromise;
  beforeEach(() => {
    interestedPeopleHandler = new NewsletterRepository(mySql);
    mySql.selectWithParams.mockImplementation(() => selectWithParamsPromise);
    mySql.select.mockImplementation(() => selectPromise);
    mySql.update.mockImplementation(() => updatePromise);
    mySql.insert.mockImplementation(() => insertPromise);
    mySql.delete.mockImplementation(() => deletePromise);
  });
  afterEach(() => {
    mySql.selectWithParams.mockReset();
    mySql.select.mockReset();
    mySql.update.mockReset();
    mySql.insert.mockReset();
    mySql.delete.mockReset();
  });


  describe('read person id', () => {
    it('gets the id, if person exists', (done) => {
      selectWithParamsPromise = Promise.resolve([{id: 4711}]);
      interestedPeopleHandler.readPersonId('test@email.de')
        .then((id: number) => {
          expect(id).toBe(4711);
          expect(mySql.selectWithParams.mock.calls.length).toBe(1);
          done();
        });
    });

    it('gets id = 0, if person does not exists', (done) => {
      selectWithParamsPromise = Promise.resolve([]);
      interestedPeopleHandler.readPersonId('test@email.de')
        .then((id: number) => {
          expect(id).toBe(0);
          expect(mySql.selectWithParams.mock.calls.length).toBe(1);
          done();
        });
    });
  });

  describe('read person by consent key', () => {
    it('gets the subscriber, if person exists', (done) => {
      selectWithParamsPromise = Promise.resolve([{id: 4711}]);
      interestedPeopleHandler.readPersonByConsentKey('someKey')
        .then((subscriber: ?NewsletterSubscriber) => {
          expect(subscriber).toBeDefined();
          expect(mySql.selectWithParams.mock.calls.length).toBe(1);
          done();
        });
    });

    it('gets null, if person does not exists', (done) => {
      selectWithParamsPromise = Promise.resolve([]);
      interestedPeopleHandler.readPersonByConsentKey('someKey')
        .then((subscriber: ?NewsletterSubscriber) => {
          expect(subscriber).toBeNull();
          expect(mySql.selectWithParams.mock.calls.length).toBe(1);
          done();
        });
    });
  });

  describe('delete person by email', () => {
    it('works', (done) => {
      deletePromise = Promise.resolve({numberOfRowsAffected: 1});
      interestedPeopleHandler.deletePersonByEmail('email@valid.de')
        .then(() => {
          expect(mySql.delete.mock.calls.length).toBe(1);
          done();
        });
    });
  });

  describe('read all', () => {
    it('interested people', (done) => {
      selectPromise = Promise.resolve([]);
      return interestedPeopleHandler.readAll()
        .then((interestedPeople: NewsletterSubscriber[]) => {
          expect(interestedPeople.length).toBe(0);
          expect(mySql.select.mock.calls.length).toBe(1);
          done();
        });
    });
  });

  describe('unsubscribe', () => {
    it('an interested person', (done) => {
      updatePromise = Promise.resolve({
        numberOfRowsAffected: 1
      });
      interestedPeopleHandler.unsubscribe('name@domain.de')
        .then((email: ?string) => {
          expect(email).toEqual('name@domain.de');
          expect(mySql.update.mock.calls.length).toBe(1);
          done();
        });
    });

    it('a not interested person without error', (done) => {
      updatePromise = Promise.resolve({
        numberOfRowsAffected: 0
      });
      interestedPeopleHandler.unsubscribe('name@domain.de')
        .then((email: ?string) => {
          expect(email).toEqual('name@domain.de');
          done();
          expect(mySql.update.mock.calls.length).toBe(1);
        });
    });
  });

  describe('sign up', () => {
    it('an existent person', (done) => {
      selectWithParamsPromise = Promise.resolve([{id: 4711}]);
      updatePromise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      insertPromise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 5678});
      interestedPeopleHandler.signup('name', 'name@domain.de', 'some key')
        .then((person: ?NewsletterSubscriber) => {
          expect(person).toBeDefined();
          if (person) {
            expect(person.email).toEqual('name@domain.de');
          }
          expect(mySql.selectWithParams.mock.calls.length).toBe(1);
          expect(mySql.insert.mock.calls.length).toBe(0);
          expect(mySql.update.mock.calls.length).toBe(1);
          expect(mySql.update.mock.calls[0][1]).toContain('some key');
          expect(mySql.update.mock.calls[0][1]).toContain('name');
          expect(mySql.update.mock.calls[0][1]).toContain(4711);
          done();
        });
    });

    it('a non existent person', (done) => {
      selectWithParamsPromise = Promise.resolve([]);
      interestedPeopleHandler.signup('name', 'name@domain.de', 'some key')
        .then((person: ?NewsletterSubscriber) => {
          expect(person).toBeDefined();
          if (person) {
            expect(person.email).toEqual('name@domain.de');
          }
          expect(mySql.selectWithParams.mock.calls.length).toBe(1);
          expect(mySql.insert.mock.calls.length).toBe(1);
          expect(mySql.update.mock.calls.length).toBe(0);
          done();
        });
    });
  });

  describe('confirm', () => {
    it('a potential subscriber without errors', (done) => {
      updatePromise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      interestedPeopleHandler.confirm('some key')
        .then((success: boolean) => {
          expect(success).toBe(true);
          expect(mySql.update.mock.calls.length).toBe(1);
          expect(mySql.update.mock.calls[0][1]).toContain('some key');
          done();
        });
    });
    it('using a non existent confirmation key, returns false', (done) => {
      updatePromise = Promise.resolve({numberOfRowsAffected: 0, lastInsertedId: 0});
      interestedPeopleHandler.confirm('some key')
        .then((success: boolean) => {
          expect(success).toBe(false);
          expect(mySql.update.mock.calls.length).toBe(1);
          expect(mySql.update.mock.calls[0][1]).toContain('some key');
          done();
        });
    });
  });
});
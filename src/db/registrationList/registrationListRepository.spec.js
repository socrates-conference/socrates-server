import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import RegistrationListRepository from './registrationListRepository';

jest.mock('../MySql');

let registrationListRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('RegistrationMySQL:', () => {
  let promise;
  beforeEach(() => registrationListRepository = new RegistrationListRepository(mySql));

  describe('reads person', () => {
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => promise);
    });

    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('without errors if it exist', async () => {
      promise = Promise.resolve([{id: 4711, nickname: 'Test', email: 'test@valid.de'}]);
      const person = await registrationListRepository.readPerson(4711);
      expect(person).toBeDefined();
    });
  });

  describe('reads registrations', () => {
    beforeEach(() => {
      mySql.select.mockImplementation(() => promise);
    });

    afterEach(() => {
      mySql.select.mockReset();
    });

    it('without errors if it exist', async () => {
      promise = Promise.resolve([{id: 4711, nickname: 'Test', email: 'test@valid.de'}]);
      const rows = await registrationListRepository.readAll();
      expect(rows).toBeDefined();
    });
  });

  describe('reads non confirmed registrations', () => {
    beforeEach(() => {
      mySql.select.mockImplementation(() => promise);
    });

    afterEach(() => {
      mySql.select.mockReset();
    });

    it('without errors if it exist', async () => {
      promise = Promise.resolve([{personId: 4711}]);
      const rows = await registrationListRepository.readAll();
      expect(rows).toBeDefined();
    });
  });

  describe('on delete registration', () => {
    beforeEach(() => {
      mySql.delete.mockImplementation(() => promise);
    });

    afterEach(() => {
      mySql.delete.mockReset();
    });

    it('without errors if it exist', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      const success = await registrationListRepository.deleteRegistration(4711);
      expect(success).toBe(true);
    });
  });

  describe('on update reminder data', () => {
    beforeEach(() => {
      mySql.update.mockImplementation(() => promise);
    });

    afterEach(() => {
      mySql.update.mockReset();
    });

    it('without errors if it exist', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      const success = await registrationListRepository.updateReminderData(4711);
      expect(success).toBe(true);
    });
  });
  describe('on update room type', () => {
    beforeEach(() => {
      mySql.update.mockImplementation(() => promise);
    });

    afterEach(() => {
      mySql.update.mockReset();
    });

    it('without errors if it exist', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      const success = await registrationListRepository.updateRoomType(4711, 'single');
      expect(success).toBe(true);
    });
  });
});

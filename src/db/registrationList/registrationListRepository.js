// @flow
import MySql from '../MySql';
import type {Registration} from '../../domain/management/registrationList';
import {RegistrationMySQL} from './registrationMySQL';
import type {PersonResult} from '../application/personMySQL';

export default class RegistrationListRepository {

  _mySql: MySql;
  _registrationList: RegistrationMySQL;

  constructor(mySql: MySql) {
    this._mySql = mySql;
    this._registrationList = new RegistrationMySQL(mySql);
  }

  async readPerson(id: number): Promise<?PersonResult> {
    return await this._registrationList.readPerson(id);
  }

  async readAll(): Promise<Registration[]> {
    return await this._registrationList.read();
  }

  async deleteRegistration(personId: number): Promise<boolean> {
    return await this._registrationList.delete(personId);
  }

  async updateReminderData(personId: number): Promise<boolean> {
    return await this._registrationList.updateReminderData(personId);
  }

  async updateRoomType(personId: number, roomType: string): Promise<boolean> {
    return await this._registrationList.updateRoomType(personId, roomType);
  }

  async readNonConfirmedRegistrationsToBeReminded(): Promise<Array<Object>> {
    return await this._registrationList.readNonConfirmedRegistrationsToBeReminded();
  }

  async readRegistration(personId: number): Promise<Object> {
    return await this._registrationList.readRegistration(personId);
  }
}

// @flow
import MySql from '../MySql';
import type {Registration} from '../../domain/management/registrationList';
import type {PersonResult} from '../application/personMySQL';
import moment from 'moment';


export class RegistrationMySQL {
  _mySql: MySql;

  _selectAll = 'SELECT r.id as participantId, r.person_id as personId, r.room_type as roomType, ' +
    'diversity_reason as diversityReason, r.date, r.confirmed as isConfirmed, p.nickname as name, p.email, ' +
    'pa.firstname as firstName, pa.lastname as lastName, pa.company, pa.address1, pa.address2, pa.province, ' +
    'pa.postal, pa.city, pa.country, pa.arrival, pa.departure, pa.dietary_info as dietaryInfo, ' +
    'pa.family_info as familyInfo, pa.tshirt as shirt, pa.labelname as labelName, pa.social, pa.pronoun, ' +
    'r.confirmation_deadline as confirmationDeadline, ' +
    'r.confirmation_reminder_counter as confirmationReminderCounter, ' +
    'r.confirmation_last_reminder as confirmationLastReminder ' +
    'FROM registration as r ' +
    'JOIN person as p ON p.id = r.person_id ' +
    'LEFT JOIN participants as pa ON pa.person_id = r.person_id ;';

  _deleteRegistrationSql = 'DELETE FROM registration WHERE person_id = ?';
  _updateReminderDataSql = 'UPDATE registration SET ' +
    'confirmation_reminder_counter = confirmation_reminder_counter + 1, ' +
    'confirmation_last_reminder = ? WHERE person_id = ?';
  _selectAllNonConfirmed = 'SELECT p.id as personId, p.nickname as name, p.email as email, r.date as date, ' +
    'r.confirmation_reminder_counter as confirmationReminderCounter, ' +
    'confirmation_last_reminder as confirmationLastReminder ' +
    'FROM registration as r ' +
    'JOIN person as p ON p.id = r.person_id ' +
    'WHERE r.confirmed = 0';
  _selectNonConfirmedPerson = 'SELECT p.nickname as name, p.email as email, r.date as date, ' +
    'r.confirmation_reminder_counter as confirmationReminderCounter, r.room_type as roomType, ' +
    'confirmation_last_reminder as confirmationLastReminder, pa.firstname, pa.lastname ' +
    'FROM registration as r ' +
    'JOIN person as p ON p.id = r.person_id ' +
    'LEFT JOIN participants as pa ON pa.person_id = r.person_id ' +
    'WHERE r.person_id = ?';
  _updateRoomTypeSql = 'UPDATE registration SET room_type = ? ' +
    'WHERE person_id = ?';

  constructor(mysql: MySql) {
    this._mySql = mysql;
  }

  async readPerson(id: number): Promise<?PersonResult> {
    const readPersonSql = 'SELECT * FROM person WHERE id = ?;';
    const values = [id];
    const results = await this._mySql.selectWithParams(readPersonSql, values);
    if (results && results.length === 1) {
      return results[0];
    }
    return null;
  }

  async read(): Promise<Registration[]> {
    return await this._mySql.select(this._selectAll)
      .then(rows => rows.map(row => toRegistration(row)));
  }

  delete = async (personId: number): Promise<boolean> => {
    const result = await this._mySql.delete(this._deleteRegistrationSql, [personId]);
    return (result && result.numberOfRowsAffected === 1);
  };

  updateReminderData = async(personId: number): Promise<boolean> => {
    const values = [moment().format('YYYY-MM-DD HH:mm:ss'), personId];
    const result = await this._mySql.update(this._updateReminderDataSql, values);
    return (result && result.numberOfRowsAffected === 1);
  };

  updateRoomType = async(personId: number, roomType: string): Promise<boolean> => {
    const values = [roomType, personId];
    const result = await this._mySql.update(this._updateRoomTypeSql, values);
    return (result && result.numberOfRowsAffected === 1);
  };

  async readNonConfirmedRegistrationsToBeReminded(): Promise<Array<Object>> {
    return await this._mySql.select(this._selectAllNonConfirmed);
  }

  async readRegistration(personId: number): Promise<?Object> {
    const values = [personId];
    const results = await this._mySql.selectWithParams(this._selectNonConfirmedPerson, values);
    if (results && results.length === 1) {
      return results[0];
    }
    return null;
  }

}

function toRegistration(row: Object) {
  return {...row, isConfirmed: Boolean(row.isConfirmed)};
}

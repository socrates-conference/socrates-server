import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import {RegistrationMySQL} from './registrationMySQL';

jest.mock('../MySql');

let registrationMySQL;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('RegistrationMySQL:', () => {
  let promise;
  beforeEach(() => registrationMySQL = new RegistrationMySQL(mySql));

  describe('reads person', () => {
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => promise);
    });

    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('without errors if it exist', async () => {
      promise = Promise.resolve([{id: 4711, nickname: 'Test', email: 'test@valid.de'}]);
      const person = await registrationMySQL.readPerson(4711);
      expect(person).toBeDefined();
    });
    it('return null if non existent', async () => {
      promise = Promise.resolve([]);
      const person = await registrationMySQL.readPerson(4711);
      expect(person).toBeNull();
    });
    it('throws if error', (done) => {
      promise = Promise.reject(new Error());
      registrationMySQL.readPerson(4711).catch(() => done());
    });
  });

  describe('reads registrations', () => {
    beforeEach(() => {
      mySql.select.mockImplementation(() => promise);
    });

    afterEach(() => {
      mySql.select.mockReset();
    });

    it('without errors if it exist', async () => {
      promise = Promise.resolve([{id: 4711, nickname: 'Test', email: 'test@valid.de'}]);
      const rows = await registrationMySQL.read();
      expect(rows).toBeDefined();
    });
    it('throws if error', (done) => {
      promise = Promise.reject(new Error());
      registrationMySQL.read().catch(() => done());
    });
  });

  describe('reads non confirmed registrations', () => {
    beforeEach(() => {
      mySql.select.mockImplementation(() => promise);
    });

    afterEach(() => {
      mySql.select.mockReset();
    });

    it('without errors if it exist', async () => {
      promise = Promise.resolve([{personId: 4711}]);
      const rows = await registrationMySQL.readNonConfirmedRegistrationsToBeReminded();
      expect(rows).toBeDefined();
    });
    it('throws if error', (done) => {
      promise = Promise.reject(new Error());
      registrationMySQL.readNonConfirmedRegistrationsToBeReminded().catch(() => done());
    });
  });

  describe('on delete registration', () => {
    beforeEach(() => {
      mySql.delete.mockImplementation(() => promise);
    });

    afterEach(() => {
      mySql.delete.mockReset();
    });

    it('without errors if it exist', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      const success = await registrationMySQL.delete(4711);
      expect(success).toBe(true);
    });
    it('return null if non existent', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 0, lastInsertedId: 0});
      const success = await registrationMySQL.delete(4711);
      expect(success).toBe(false);
    });
    it('throws if error', (done) => {
      promise = Promise.reject(new Error());
      registrationMySQL.delete(4711).catch(() => done());
    });
  });
  describe('on update reminder data', () => {
    beforeEach(() => {
      mySql.update.mockImplementation(() => promise);
    });

    afterEach(() => {
      mySql.update.mockReset();
    });

    it('without errors if it exist', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      const success = await registrationMySQL.updateReminderData(4711);
      expect(success).toBe(true);
    });
    it('return null if non existent', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 0, lastInsertedId: 0});
      const success = await registrationMySQL.updateReminderData(4711);
      expect(success).toBe(false);
    });
    it('throws if error', (done) => {
      promise = Promise.reject(new Error());
      registrationMySQL.updateReminderData(4711).catch(() => done());
    });
  });
  describe('on update room type', () => {
    beforeEach(() => {
      mySql.update.mockImplementation(() => promise);
    });

    afterEach(() => {
      mySql.update.mockReset();
    });

    it('without errors if it exist', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      const success = await registrationMySQL.updateRoomType(4711);
      expect(success).toBe(true);
    });
    it('return null if non existent', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 0, lastInsertedId: 0});
      const success = await registrationMySQL.updateRoomType(4711);
      expect(success).toBe(false);
    });
    it('throws if error', (done) => {
      promise = Promise.reject(new Error());
      registrationMySQL.updateRoomType(4711).catch(() => done());
    });
  });
});

// @flow
import MySql from '../MySql';
import type {Application} from '../../domain/management/applicationList';

export class ApplicationMySQL {
  _mySql: MySql;
  _deleteQuery: string = 'DELETE FROM application WHERE person_id = ?';
  _insertQuery: string = 'INSERT INTO application (person_id, room_type, diversity_reason) VALUES ';

  constructor(mysql: MySql) {
    this._mySql = mysql;
  }

  async insert(application: Application): Promise<Application> {
    const addPersonSql = this._buildInsertQuery(application);
    const values: Array<any> = this._buildValues(application);
    const result = await this._mySql.insert(addPersonSql, values);
    if (result.numberOfRowsAffected === application.roomTypes.length) {
      return application;
    }
    throw new Error('Fatal error: Number of roomTypes and number of inserts did not match.');
  }

  async delete(personId: number): Promise<boolean> {
    const result = await this._mySql.delete(this._deleteQuery, [personId]);
    return result.numberOfRowsAffected > 0;
  }

  _buildInsertQuery({roomTypes}: Application): string {
    // noinspection JSUnusedAssignment
    return roomTypes
      .reduce(acc => acc += '(?, ?, ?), ', this._insertQuery)
      .slice(0, -2);
  }

  _buildValues({personId, roomTypes, diversityReason}: Application): Array<any> {
    return roomTypes
      .reduce((acc, roomType) => acc.concat([personId, roomType, diversityReason]), []);
  }
}

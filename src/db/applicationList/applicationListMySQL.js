// @flow
import MySql from '../MySql';
import type {PersonResult} from '../application/personMySQL';

export class ApplicationListMySQL {
  _mySql: MySql;

  _allApplicationsQuery = 'SELECT app.person_id, app.id as applicant_id, p.nickname, p.email, app.date, ' +
    'GROUP_CONCAT(room_type) as room_types, app.diversity_reason ' +
    'FROM application as app INNER JOIN person as p ON app.person_id = p.id GROUP BY app.person_id ' +
    'ORDER BY app.date;';

  _applicationsByRoomQuery = 'SELECT app.person_id, app.id as applicant_id, p.nickname, p.email, app.date, ' +
    'room_type as room_types, app.diversity_reason ' +
    'FROM application as app INNER JOIN person as p ON app.person_id = p.id ' +
    'WHERE room_type = ?' +
    'GROUP BY app.person_id ' +
    'ORDER BY app.date;';

  constructor(mysql: MySql) {
    this._mySql = mysql;
  }

  async readPerson(id: number): Promise<?PersonResult> {
    const readPersonSql = 'SELECT * FROM person WHERE id = ?;';
    const values = [id];
    const results = await this._mySql.selectWithParams(readPersonSql, values);
    if (results && results.length === 1) {
      return results[0];
    }
    return null;
  }

  read = async (): Promise<[]> => {
    return await this._mySql.select(this._allApplicationsQuery);
  };

  readByRoomType = async (roomType: string): Promise<[]> => {
    return await this._mySql.selectWithParams(this._applicationsByRoomQuery, [roomType]);
  };
}

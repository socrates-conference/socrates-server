// @flow
import MySql from '../MySql';
import type {Application, RegistrationData} from '../../domain/management/applicationList';
import uuidv1 from 'uuid/v1';
import moment from 'moment';


const createRegistrationDataFromApplication = (application: Application): RegistrationData => {
  return {
    personId: application.personId,
    name: application.name,
    email: application.email,
    roomType: application.roomTypes[0],
    confirmationKey: uuidv1(),
    confirmationDeadline: moment().add(30, 'days').format('YYYY-MM-DD HH:mm:ss'),
    oneTimePassword: ''
  };
};

export class RegistrationMySQL {
  _mySql: MySql;
  _insertQuery: string =
    'INSERT INTO registration (person_id, room_type, diversity_reason, stay_length, ' +
    'confirmation_key, confirmation_deadline) ' +
    'VALUES (?, ?, ?, \'\', ?, ?)';

  constructor(mysql: MySql) {
    this._mySql = mysql;
  }

  async insert(application: Application): Promise<?RegistrationData> {
    const data = createRegistrationDataFromApplication(application);
    const params = [data.personId, data.roomType, application.diversityReason,
      data.confirmationKey, data.confirmationDeadline];
    const result = await this._mySql.insert(this._insertQuery, params);
    if (result.numberOfRowsAffected === 1 && result.lastInsertedId > 0) {
      return data;
    }
  }
}
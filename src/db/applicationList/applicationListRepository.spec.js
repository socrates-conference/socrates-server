/* eslint-disable camelcase */
import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import ApplicationListRepository from './applicationListRepository';

jest.mock('../MySql');

let applicationListRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('Application list repository', () => {

  beforeEach(() => {
    applicationListRepository = new ApplicationListRepository(mySql);
  });

  describe('get all applications', () => {
    beforeEach(() => {
      mySql.select.mockImplementation(() => {
        return Promise.resolve([{
          person_id: 4711,
          applicant_id: 5483,
          date: '2018-03-01T15:00:00.000Z',
          nickname: 'Nickname',
          email: 'email@valid.de',
          room_types: 'single,bedInDouble,juniorShared',
          diversity_reason: 'description'
        }]);
      });
    });

    afterEach(() => {
      mySql.select.mockReset();
    });

    it('works and gets data', async () => {
      const result = await applicationListRepository.readAll();
      expect(result.length).toBe(1);
      const item = result[0];
      expect(item.personId).toBe(4711);
      expect(item.applicantId).toBe(5483);
      expect(item.date).toBe('2018-03-01T15:00:00.000Z');
      expect(item.name).toBe('Nickname');
      expect(item.email).toBe('email@valid.de');
      expect(item.roomTypes[0]).toBe('single');
      expect(item.roomTypes[1]).toBe('bedInDouble');
      expect(item.roomTypes[2]).toBe('juniorShared');
      expect(item.diversityReason).toBe('description');
    });
  });
  describe('get applications by room', () => {
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return Promise.resolve([{
          person_id: 4711,
          applicant_id: 5483,
          date: '2018-03-01T15:00:00.000Z',
          nickname: 'Nickname',
          email: 'email@valid.de',
          room_types: 'single',
          diversity_reason: 'description'
        }]);
      });
    });

    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('works and gets data', async () => {
      const result = await applicationListRepository.readByRoom('single');
      expect(result.length).toBe(1);
      const item = result[0];
      expect(item.personId).toBe(4711);
      expect(item.applicantId).toBe(5483);
      expect(item.date).toBe('2018-03-01T15:00:00.000Z');
      expect(item.name).toBe('Nickname');
      expect(item.email).toBe('email@valid.de');
      expect(item.roomTypes[0]).toBe('single');
      expect(item.diversityReason).toBe('description');
    });
  });
  describe('update application', () => {
    const application = {
      personId: 4711,
      date: '2018-03-01T15:00:00.000Z',
      name: 'Nickname',
      email: 'email@valid.de',
      roomTypes: ['single', 'bedInDouble'],
      diversityReason: 'description'
    };

    beforeEach(() => {
      mySql.delete.mockImplementation(() => {
        return Promise.resolve({numberOfRowsAffected: 3, lastInsertedId: 0});
      });
      mySql.insert.mockImplementation(() => {
        return Promise.resolve({numberOfRowsAffected: 2, lastInsertedId: 0});
      });
    });

    afterEach(() => {
      mySql.delete.mockReset();
      mySql.insert.mockReset();
    });


    it('works and gets data', async () => {
      const item = await applicationListRepository.updateApplication(application);
      expect(item).toEqual(application);
    });
  });
  describe('delete application', () => {
    beforeEach(() => {
      mySql.delete.mockImplementation(() => {
        return Promise.resolve({numberOfRowsAffected: 3, lastInsertedId: 0});
      });
    });

    afterEach(() => {
      mySql.delete.mockReset();
    });

    it('works and gets data', async () => {
      expect(await applicationListRepository.deleteApplication(4711)).toBe(true);
    });
  });
  describe('register application', () => {
    const application = {
      personId: 4711,
      date: '2018-03-01T15:00:00.000Z',
      name: 'Nickname',
      email: 'email@valid.de',
      roomTypes: ['single'],
      diversityReason: 'no'
    };
    beforeEach(() => {
      mySql.delete.mockImplementation(() => {
        return Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      });
      mySql.insert.mockImplementation(() => {
        return Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 5555});
      });
      mySql.selectWithParams.mockImplementation(() => {
        return Promise.resolve([{count: 0}]);
      });
      mySql.update.mockImplementation(() => {
        return Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      });
    });

    afterEach(() => {
      mySql.delete.mockReset();
      mySql.insert.mockReset();
      mySql.selectWithParams.mockReset();
      mySql.update.mockReset();
    });

    it('works and gets data', async () => {
      const registrationData = await applicationListRepository.register(application);
      expect(registrationData).toBeDefined();
      expect(registrationData.personId).toBe(4711);
      expect(registrationData.name).toBe('Nickname');
      expect(registrationData.email).toBe('email@valid.de');
      expect(registrationData.roomType).toBe('single');
      expect(registrationData.confirmationKey.length).toBe(36);
      expect(registrationData.oneTimePassword.length).toBeGreaterThan(0);
    });
  });
});
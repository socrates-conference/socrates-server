/* eslint-disable camelcase */
import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import {PersonMySQL} from './personMySQL';

jest.mock('../MySql');

let personMySql;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('Application list person mysql', () => {

  beforeEach(() => {
    personMySql = new PersonMySQL(mySql);
  });

  describe('can perform a password existence check and', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });

    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('returns true when the person has a password', async () => {
      promise = Promise.resolve([{count: 1}]);
      expect(await personMySql.hasPassword(4711)).toBe(true);
    });
    it('returns false when the person\'s password is empty', async () => {
      promise = Promise.resolve([{count: 0}]);
      expect(await personMySql.hasPassword(4711)).toBe(false);
    });
  });

  describe('can update password and', () => {
    let promise;
    beforeEach(() => {
      mySql.update.mockImplementation(() => {
        return promise;
      });
    });

    afterEach(() => {
      mySql.update.mockReset();
    });

    it('returns the new generated password saving it in the database', async () => {
      promise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      const result = await personMySql.updateWithOneTimePassword(4711);
      expect(result === '').toBe(false);
      expect(mySql.update.mock.calls.length).toBe(1);
    });
  });

  describe('updates person\'s password if needed, meaning', () => {
    let selectPromise;
    let updatePromise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return selectPromise;
      });
      mySql.update.mockImplementation(() => {
        return updatePromise;
      });
    });

    afterEach(() => {
      mySql.update.mockReset();
      mySql.selectWithParams.mockReset();
    });

    it('there is an update and a new password when the person has no password', async () => {
      selectPromise = Promise.resolve([{count: 0}]);
      updatePromise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      const result = await personMySql.updatePersonWithOneTimePasswordIfNeeded(4711);
      expect(result === '').toBe(false);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
      expect(mySql.update.mock.calls.length).toBe(1);
    });
    it('there is no update and no new password when the person already has a password', async () => {
      selectPromise = Promise.resolve([{count: 1}]);
      updatePromise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 0});
      const result = await personMySql.updatePersonWithOneTimePasswordIfNeeded(4711);
      expect(result === '').toBe(true);
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
      expect(mySql.update.mock.calls.length).toBe(0);
    });
  });

});
// @flow
import MySql from '../MySql';
import type {Application, RegistrationData} from '../../domain/management/applicationList';
import {ApplicationMySQL} from './applicationMySQL';
import {ApplicationListMySQL} from './applicationListMySQL';
import {RegistrationMySQL} from './registrationMySQL';
import {PersonMySQL} from './personMySQL';
import type {PersonResult} from '../application/personMySQL';

export default class ApplicationListRepository {

  _applicationList: ApplicationListMySQL;
  _applications: ApplicationMySQL;
  _registration: RegistrationMySQL;
  _person: PersonMySQL;

  constructor(mySql: MySql) {
    this._applicationList = new ApplicationListMySQL(mySql);
    this._applications = new ApplicationMySQL(mySql);
    this._registration = new RegistrationMySQL(mySql);
    this._person = new PersonMySQL(mySql);
  }

  async readPerson(id: number): Promise<?PersonResult> {
    return await this._applicationList.readPerson(id);
  }

  async readAll(): Promise<Application[]> {
    const applications = await this._applicationList.read();
    return this._mapResult(applications);
  }

  async updateApplication(data: Application): Promise<?Application> {
    if (await this._applications.delete(data.personId)) {
      if (await this._applications.insert(data)) {
        return data;
      }
    }
  }

  async deleteApplication(personId: number): Promise<boolean> {
    return await this._applications.delete(personId);
  }

  async register(application: Application): Promise<?RegistrationData> {
    const registration = await this._registration.insert(application);
    if (registration) {
      const oneTimePassword = await this._person.updatePersonWithOneTimePasswordIfNeeded(application.personId);
      const deleted = await this.deleteApplication(application.personId);
      if (deleted) {
        return {...registration, oneTimePassword: oneTimePassword};
      }
    }
  }

  async readByRoom(roomType: string): Promise<Application[]> {
    const applications = await this._applicationList.readByRoomType(roomType);
    return this._mapResult(applications);
  }

  _mapResult = (people: [] | Array<Object>): Application[] => {
    return people.map(p => {
      let roomTypes = [];
      if (p.room_types) {
        roomTypes = p.room_types.split(',');
      }
      roomTypes.forEach((value, index) => roomTypes[index] = value);
      return {
        personId: p.person_id,
        applicantId: p.applicant_id,
        date: p.date,
        name: p.nickname,
        email: p.email,
        roomTypes: roomTypes,
        diversityReason: p.diversity_reason
      };
    });
  };


}

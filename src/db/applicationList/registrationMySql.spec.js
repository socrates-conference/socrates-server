/* eslint-disable camelcase */
import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import {RegistrationMySQL} from './registrationMySQL';

jest.mock('../MySql');

let registrationMySql;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('Application list registration mysql', () => {

  beforeEach(() => {
    registrationMySql = new RegistrationMySQL(mySql);
  });

  describe('adds a new registration ', () => {
    let insertPromise;
    beforeEach(() => {
      mySql.insert.mockImplementation(() => {
        return insertPromise;
      });
    });

    afterEach(() => {
      mySql.insert.mockReset();
    });

    it('in the database', async () => {
      const application = {
        personId: 4711,
        date: '2018-03-01T15:00:00.000Z',
        name: 'Nickname',
        email: 'email@valid.de',
        roomTypes: ['single'],
        diversityReason: 'no'
      };
      insertPromise = Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 5555});
      const registrationData = await registrationMySql.insert(application);
      expect(registrationData).toBeDefined();
      expect(registrationData.personId).toBe(4711);
      expect(registrationData.name).toBe('Nickname');
      expect(registrationData.email).toBe('email@valid.de');
      expect(registrationData.roomType).toBe('single');
      expect(registrationData.confirmationKey.length).toBe(36);
      expect(registrationData.oneTimePassword).toBe('');
    });
  });
});
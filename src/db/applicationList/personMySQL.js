// @flow
import MySql from '../MySql';
import {makeRandomPass} from '../../utilities/Utilities';
import bcryptjs from 'bcryptjs';
import sjcl from 'sjcl';

const PASSWORD_LENGTH = 15;

export class PersonMySQL {
  _mySql: MySql;
  _hasPasswordQuery: string = 'SELECT count(id) as count FROM person WHERE TRIM(password) <> \'\' AND id=?;';
  _updateQuery: string = 'UPDATE person SET password=?, is_onetime_pass=1 WHERE id=?;';

  constructor(mysql: MySql) {
    this._mySql = mysql;
  }

  async hasPassword(personId: number): Promise<boolean> {
    const results: Array<{ count: number }> = await this._mySql.selectWithParams(this._hasPasswordQuery, [personId]);
    return results[0].count === 1;
  }

  async updateWithOneTimePassword(personId: number): Promise<string> {
    const newPass = makeRandomPass(PASSWORD_LENGTH);
    const sha256 = sjcl.hash.sha256.hash(newPass);
    const hash = sjcl.codec.hex.fromBits(sha256);
    const passwordHash = bcryptjs.hashSync(hash, 10);
    const result = await this._mySql.update(this._updateQuery, [passwordHash, personId]);
    if(result.numberOfRowsAffected === 1) {
      return newPass;
    }
    throw new Error('Update password failed.');
  }

  updatePersonWithOneTimePasswordIfNeeded = async (personId: number): Promise<string> => {
    let oneTimePassword: string = '';
    if (!await this.hasPassword(personId)) {
      oneTimePassword = await this.updateWithOneTimePassword(personId);
    }
    return oneTimePassword;
  };


}
// @flow
import MySql from './MySql';
import type {NewsletterSubscriber} from '../domain/newsletter';
import type {QueryResult} from './MySql';
import moment from 'moment';

export default class NewsletterRepository {

  _mySql: MySql;

  constructor(mySql: MySql) {
    this._mySql = mySql;
  }

  async signup(name: string, email: string, consentKey: string): Promise<?NewsletterSubscriber> {
    const personId = await this.readPersonId(email);
    let result: QueryResult;
    const now = moment().format('YYYY-MM-DD HH:mm:ss.SSS');
    if (personId > 0) {
      const updateSubscriberSql = 'UPDATE person SET is_newsletter_consent_pending=1, ' +
        'newsletter_eudatap_consent_key= ?, newsletter_request_date = ?, nickname=? WHERE id=?';
      const values = [consentKey, now, name, personId];
      result = await this._mySql.update(updateSubscriberSql, values);
    } else {
      const addNewSubscriberSql = 'INSERT INTO person (nickname, email, is_newsletter_consent_pending, ' +
        'newsletter_eudatap_consent_key, newsletter_request_date) VALUES (?, ?, 1, ?, ?);';
      const values = [name, email, consentKey, now];
      result = await this._mySql.insert(addNewSubscriberSql, values);
    }
    if (result.numberOfRowsAffected === 1) {
      return {id: result.lastInsertedId, name, email};
    }
  }

  async confirm(consentKey: string): Promise<boolean> {
    const updateSubscriberSql = 'UPDATE person SET is_newsletter_subscriber = 1, is_newsletter_consent_pending = 0, ' +
      'newsletter_eudatap_consent_key= \'\', newsletter_eudatap_consent_date = ? ' +
      'WHERE newsletter_eudatap_consent_key=?';
    const values = [moment().format('YYYY-MM-DD HH:mm:ss.SSS'), consentKey];
    const result = await this._mySql.update(updateSubscriberSql, values);
    return result.numberOfRowsAffected === 1;
  }

  async unsubscribe(email: string): Promise<?string> {
    const canPersonBeDeletedSql = 'SELECT ' +
      '(SELECT count(p.id) FROM person as p ' +
      'JOIN application as a ON a.person_id = p.id WHERE p.email = ?) + ' +
      '(SELECT count(p.id) FROM person as p ' +
      'JOIN registration as r ON r.person_id = p.id ' +
      'WHERE p.email = ?) as counter;';
    const removeSubscriberSql = 'UPDATE person SET is_newsletter_subscriber = 0, is_newsletter_consent_pending = 0, ' +
      'newsletter_eudatap_consent_key= \'\', newsletter_eudatap_consent_date = null, newsletter_request_date = null ' +
      'WHERE email = ?;';
    const personInfo: Array<{ counter: number }> =
      await this._mySql.selectWithParams(canPersonBeDeletedSql, [email, email]);
    const result: { numberOfRowsAffected: number } = await this._mySql.update(removeSubscriberSql, [email]);
    if (result.numberOfRowsAffected < 2) {
      if (personInfo && personInfo.length === 1 && personInfo[0].counter === 0) {
        await this.deletePersonByEmail(email);
      }
      return email;
    }
  }

  async readAll(): Promise<NewsletterSubscriber[]> {
    const readAllSubscribersSql = 'SELECT * FROM person WHERE is_newsletter_subscriber = 1;';
    const people: Array<Object> = await this._mySql.select(readAllSubscribersSql);
    return people.map(p => (p: NewsletterSubscriber));
  }

  async readPersonId(email: string): Promise<number> {
    const readPersonSql = 'SELECT id FROM person WHERE email = ?;';
    const values = [email];
    const results = await this._mySql.selectWithParams(readPersonSql, values);
    return results && results.length === 1 ? results[0].id : 0;
  }

  async deletePersonByEmail(email: string): Promise<void> {
    const deletePersonSql = 'DELETE FROM person WHERE email = ?;';
    const values = [email];
    await this._mySql.delete(deletePersonSql, values);
  }

  async readPersonByConsentKey(consentKey: string): Promise<?NewsletterSubscriber> {
    const readPersonSql = 'SELECT * FROM person WHERE newsletter_eudatap_consent_key = ?;';
    const values = [consentKey];
    const results = await this._mySql.selectWithParams(readPersonSql, values);
    return results && results.length === 1 ? results[0] : null;
  }
}

// @flow

import MySql from './MySql';
import TestHelper from './SqlTestHelper';
import AuthenticationRepository from './authenticationRepository';
import type {User} from '../domain/authenticator';

jest.mock('./MySql');

let authRepository: AuthenticationRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('Authentication repository', () => {

  const rawPerson =
    {nickname: 'test', email: 'email@valid.de', password: 'theHash', 'is_administrator': false, 'is_hotel': false};
  beforeEach(() => {
    authRepository = new AuthenticationRepository(mySql);
  });

  describe('if the email is available', () => {
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return Promise.resolve([{...rawPerson}]);
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('then it finds the user', () => {
      return authRepository.readPersonByEmail('email@valid.de')
        .then((user: ?User) => {
          expect(user).toBeDefined();
          expect(user).toEqual({
            name: 'test', email: 'email@valid.de', password: 'theHash',
            isAdministrator: false, isHotel: false
          });
          expect(mySql.selectWithParams.mock.calls.length).toBe(1);
        });
    });
  });
  describe('if the email is NOT available', () => {
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return Promise.resolve([]);
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('then no user is found', () => {
      return authRepository.readPersonByEmail('email@valid.de')
        .then((user: ?User) => {
          expect(user).toBeNull();
          expect(mySql.selectWithParams.mock.calls.length).toBe(1);
        });
    });
  });
});

// @flow
import MySql from '../MySql';
import type {ApplicationData} from '../../domain/application';

export type PersonResult = {
  id: number,
  nickname: string,
  email: string
}

export class PersonMySQL {
  _mySql: MySql;
  _selectQuery: string = 'SELECT id FROM person WHERE email = ?;';
  _insertQuery: string = 'INSERT INTO person (nickname, email) VALUES (?, ?);';
  _updateQuery: string = 'UPDATE person SET nickname=? WHERE id=?;';

  constructor(mysql: MySql) {
    this._mySql = mysql;
  }

  async read(application: ApplicationData): Promise<?PersonResult> {
    const values = [application.email];
    const results = await this._mySql.selectWithParams(this._selectQuery, values);
    return results && results.length === 1 ? results[0] : null;
  }

  async insert(application: ApplicationData): Promise<number> {
    const values = [application.nickname, application.email];
    const result = await this._mySql.insert(this._insertQuery, values);
    return result.lastInsertedId;
  }

  async update(application: ApplicationData): Promise<void> {
    const values = [application.nickname, application.personId];
    const result = await this._mySql.update(this._updateQuery, values);
    if (result.numberOfRowsAffected !== 1) {
      throw new Error('Update failed.');
    }
  }

  async insertOrUpdate(application: ApplicationData): Promise<number> {
    const person: ?PersonResult = await this.read(application);
    const personId = person ? person.id : await this.insert(application);
    const completeApplicationData = {...application, personId};
    if (person) {
      await this.update(completeApplicationData);
    }
    return personId;
  }
}

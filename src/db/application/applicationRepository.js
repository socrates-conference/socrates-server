// @flow
import MySql from '../MySql';
import type {ApplicationData} from '../../domain/application';
import {PersonMySQL} from './personMySQL';
import {ApplicationMySQL} from './applicationMySQL';
import type {PersonResult} from './personMySQL';
import {RegistrationMySQL} from './registrationMySQL';

export default class ApplicationRepository {
  _persons: PersonMySQL;
  _applications: ApplicationMySQL;
  _registrations: RegistrationMySQL;

  constructor(mySql: MySql) {
    this._persons = new PersonMySQL(mySql);
    this._applications = new ApplicationMySQL(mySql);
    this._registrations = new RegistrationMySQL(mySql);
  }

  async existsPreviousApplication(application: ApplicationData): Promise<boolean> {
    const personId = await this._persons.insertOrUpdate(application);
    return await this._applications.existsForPersonId(personId);
  }

  async existsRegistration(application: ApplicationData): Promise<boolean> {
    const personId = await this._persons.insertOrUpdate(application);
    return await this._registrations.existsForPersonId(personId);
  }

  async create(application: ApplicationData): Promise<ApplicationData> {
    const completeApplication = {...application};
    const person: ?PersonResult = await this._persons.read(application);
    if (person) {
      completeApplication.personId = person.id;
    }
    return await this._applications.insert(completeApplication);
  }
}


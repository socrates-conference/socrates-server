// @flow
import MySql from '../MySql';

export class RegistrationMySQL {
  _mySql: MySql;
  _selectCountQuery: string = 'SELECT COUNT(id) AS registrationCount FROM registration WHERE person_id = ?';

  constructor(mysql: MySql) {
    this._mySql = mysql;
  }

  async existsForPersonId(personId: number): Promise<boolean> {
    const select = this._selectCountQuery;
    const results: Array<{registrationCount: number}> = await this._mySql.selectWithParams(select, [personId]);
    return results[0].registrationCount > 0;
  }
}

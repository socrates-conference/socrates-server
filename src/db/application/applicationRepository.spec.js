import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import ApplicationRepository from './applicationRepository';

jest.mock('../MySql');

let applicationRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('Application repository', () => {

  beforeEach(() => {
    applicationRepository = new ApplicationRepository(mySql);
  });

  describe('on valid data adding application', () => {
    describe('when person exists, but no previous application', () => {
      beforeEach(() => {
        const person = {id: 4711};
        mySql.selectWithParams.mockImplementation(() => Promise.resolve([person]));
        mySql.update.mockImplementation(() => ({numberOfRowsAffected: 1}));
        mySql.insert.mockImplementation(() => {
          return Promise.resolve({numberOfRowsAffected: 2, lastInsertedId: 0});
        });
      });

      afterEach(() => {
        mySql.insert.mockReset();
        mySql.update.mockReset();
        mySql.selectWithParams.mockReset();
      });

      it('application is defined and has the same data', async () => {
        const sourceApplication = {
          personId: 4711,
          nickname: 'whatever',
          email: 'valid@email.de',
          roomTypes: ['single', 'double'],
          diversityReason: 4
        };
        const resultApplication = await applicationRepository.create(sourceApplication);
        expect(resultApplication.personId).toBe(4711);
        expect(resultApplication.nickname).toBe('whatever');
        expect(resultApplication.email).toBe('valid@email.de');
        expect(resultApplication.roomTypes).toContain('single');
        expect(resultApplication.roomTypes).toContain('double');
        expect(resultApplication.diversityReason).toBe(4);
      });

      it('insert called ', async () => {
        const sourceApplication = {
          personId: 4711,
          nickname: 'whatever',
          email: 'valid@email.de',
          roomTypes: ['single', 'double'],
          diversityReason: 4
        };
        await applicationRepository.create(sourceApplication);
        expect(mySql.insert.mock.calls.length).toBe(1);
      });
    });

  });

  describe('when an application exists for a personId', () => {
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => Promise.resolve([{applicationCount: 1}]));
      mySql.update.mockImplementation(() => Promise.resolve({numberOfRowsAffected: 1}));
    });

    afterEach(() => {
      mySql.selectWithParams.mockReset();
      mySql.update.mockReset();
    });

    it('should return true', async () => {
      expect(await applicationRepository.existsPreviousApplication({id: 4711, email: 'test@test.de'})).toBe(true);
    });
  });

  describe('when an application does not exist for a personId', () => {
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => Promise.resolve([{applicationCount: 0}]));
      mySql.update.mockImplementation(() => Promise.resolve({numberOfRowsAffected: 1}));
    });

    afterEach(() => {
      mySql.selectWithParams.mockReset();
      mySql.update.mockReset();
    });

    it('should return false', async () => {
      expect(await applicationRepository.existsPreviousApplication({id: 4711, email: 'test@test.de'})).toBe(false);
    });
  });
});

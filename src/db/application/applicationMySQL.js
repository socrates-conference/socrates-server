// @flow
import MySql from '../MySql';
import type {ApplicationData} from '../../domain/application';

export class ApplicationMySQL {
  _mySql: MySql;
  _selectCountQuery: string = 'SELECT COUNT(id) AS applicationCount FROM application WHERE person_id = ?';
  _insertQuery: string = 'INSERT INTO application (person_id, room_type, diversity_reason) VALUES ';

  constructor(mysql: MySql) {
    this._mySql = mysql;
  }

  async existsForPersonId(personId: number): Promise<boolean> {
    const select = this._selectCountQuery;
    const results: Array<{applicationCount: number}> = await this._mySql.selectWithParams(select, [personId]);
    return results[0].applicationCount > 0;
  }

  async insert(application: ApplicationData): Promise<ApplicationData> {
    const addPersonSql = this._buildInsertQuery(application);
    const values: Array<any> = this._buildValues(application);
    const result = await this._mySql.insert(addPersonSql, values);
    if (result.numberOfRowsAffected === application.roomTypes.length) {
      return application;
    }
    throw new Error('Fatal error: Number of roomTypes and number of inserts did not match.');
  }

  _buildInsertQuery({roomTypes}: ApplicationData): string {
    // noinspection JSUnusedAssignment
    return roomTypes
      .reduce(acc => acc += '(?, ?, ?), ', this._insertQuery)
      .slice(0, -2);
  }

  _buildValues({personId, roomTypes, diversityReason}: ApplicationData): Array<any> {
    return roomTypes
      .reduce((acc, roomType) => acc.concat([personId, roomType, diversityReason]), []);
  }
}
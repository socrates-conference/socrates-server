import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import {PersonMySQL} from './personMySQL';

jest.mock('../MySql');

let personMySQL;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('PersonMySQL:', () => {
  beforeEach(() => personMySQL = new PersonMySQL(mySql));

  describe('on sql error by adding a new person', () => {
    beforeEach(() => {
      mySql.insert.mockImplementation(() => {
        return Promise.resolve({numberOfRowsAffected: 0, lastInsertedId: 0});
      });
    });

    afterEach(() => {
      mySql.insert.mockReset();
    });

    it('personId is zero ', async () => {
      const personId = await personMySQL.insert({nickname: 'whatever', email: 'invalidEmail'});
      expect(personId).toBe(0);
    });
  });

  describe('on valid data adding a new person', () => {
    beforeEach(() => {
      mySql.insert.mockImplementation(() => {
        return Promise.resolve({numberOfRowsAffected: 1, lastInsertedId: 4711});
      });
      mySql.selectWithParams.mockImplementation(() => {
        return Promise.resolve([{id: 4711, nickname: 'whatever', email: 'valid@email.de'}]);
      });
    });

    afterEach(() => {
      mySql.insert.mockReset();
      mySql.selectWithParams.mockReset();
    });

    it('personId is non zero ', async () => {
      const personId = await personMySQL.insert({nickname: 'whatever', email: 'valid@email.de'});
      expect(personId).toBe(4711);
    });
    it('insert called ', async () => {
      await personMySQL.insert({nickname: 'whatever', email: 'valid@email.de'});
      expect(mySql.insert.mock.calls.length).toBe(1);
    });
  });

  describe('if person with given email already exists', () => {
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return Promise.resolve([{id: 4711}]);
      });
    });

    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('the person can be read', async () => {
      const person = await personMySQL.read({nickname: 'whatever', email: 'email@valid.de'});
      expect(person.id).toBe(4711);
    });
    it('selectWithParams is called ', async () => {
      await personMySQL.read({nickname: 'whatever', email: 'email@valid.de'});
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
  });

  describe('if person with given email does not exists', () => {
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return Promise.resolve([]);
      });
    });

    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });

    it('the person is null', async () => {
      const person = await personMySQL.read({id: 0, nickname: 'whatever', email: 'email@valid.de'});
      expect(person).toBe(null);
    });
    it('selectWithParams is called ', async () => {
      await personMySQL.read({id: 0, nickname: 'whatever', email: 'email@valid.de'});
      expect(mySql.selectWithParams.mock.calls.length).toBe(1);
    });
  });

  describe('when a person cannot be updated', () => {
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => Promise.resolve([{applicationCount: 0}]));
      mySql.update.mockImplementation(() => Promise.resolve({numberOfRowsAffected: 0}));
    });

    afterEach(() => {
      mySql.selectWithParams.mockReset();
      mySql.update.mockReset();
    });

    it('should throw an Error', () => {
      return personMySQL
        .insert({
          personId: 4711,
          nickname: 'whatever',
          email: 'valid@email.de',
          roomTypes: ['single', 'double'],
          diversityReason: 4
        })
        .then(() => fail())
        .catch(e => expect(e).toBeDefined());
    });
  });
});

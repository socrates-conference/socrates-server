// @flow

import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import DashboardRepository from './dashboardRepository';

jest.mock('../MySql');

let dashboardRepository: DashboardRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('Dashboard repository', () => {

  beforeEach(() => {
    dashboardRepository = new DashboardRepository(mySql);
  });

  describe('can read registrations by room type data', () => {
    beforeEach(() => {
      mySql.select.mockImplementation(() => {
        return Promise.resolve([]);
      });
    });
    afterEach(() => {
      mySql.select.mockReset();
    });

    it('without errors', async () => {
      const lengthOfStay = await dashboardRepository.readRegistrationsByRoomType();
      expect(lengthOfStay).toBeDefined();
      expect(lengthOfStay).toEqual([]);
      expect(mySql.select.mock.calls.length).toBe(1);
    });
  });
});
// @flow
import MySql from '../MySql';

export default class DashboardRepository {

  _registrationsByRoomType = 'SELECT count(id) as counter, room_type as roomType, ' +
    'diversity_reason as diversityReason ' +
    'FROM registration as r group by room_type, diversity_reason;';

  _mySql: MySql;

  constructor(mySql: MySql) {
    this._mySql = mySql;
  }

  async readRegistrationsByRoomType(): Promise<Object[]> {
    const data: Array<Object> = await this._mySql.select(this._registrationsByRoomType);
    if(data && data.length > 0) {
      return data.map(item => {
        return {
          counter: item.counter,
          roomType: item.roomType,
          diversityReason: item.diversityReason
        };
      });
    } else {
      return [];
    }
  }
}


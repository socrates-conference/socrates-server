// @flow
import MySql from './MySql';
import type {User} from '../domain/authenticator';
import type {Registration} from '../domain/management/registrationList';
import {makeRandomPass} from '../utilities/Utilities';
import bcryptjs from 'bcryptjs';
import sjcl from 'sjcl';

export default class AuthenticationRepository {

  _mySql: MySql;

  constructor(mySql: MySql) {
    this._mySql = mySql;
  }

  async readPersonByEmail(theEmail: string): Promise<?User> {
    const readPersonSql = 'SELECT * FROM person WHERE email = ?;';
    const values = [theEmail];
    const results = await this._mySql.selectWithParams(readPersonSql, values);
    if (results && results.length === 1) {
      const {
        id: personId,
        nickname: name, email, password, is_administrator: isAdministrator,
        is_onetime_pass: isOneTimePassword, is_hotel: isHotel
      } = results[0];
      return {personId, name, email, password, isAdministrator, isHotel, isOneTimePassword};
    }
    return null;
  }

  async readRegistrationByPersonId(theId: string): Promise<?Registration> {
    const readRegistrationSql = 'SELECT * FROM registration WHERE person_id = ?;';
    const values = [theId];
    const results = await this._mySql.selectWithParams(readRegistrationSql, values);
    if (results && results.length === 1) {
      return this._toRegistration(results[0]);
    }
    return null;
  }
  _toRegistration(row: Object) {
    // eslint-disable-next-line camelcase
    return {...row, isConfirmed: Boolean(row.isConfirmed), personId: row.person_id};
  }

  async updatePassword(theEmail: string, theNewPassword: string): Promise<boolean> {
    const updatePasswordSql = 'UPDATE person SET password = ?, is_onetime_pass = 0 WHERE email = ?;';
    const values = [theNewPassword, theEmail];
    const result = await this._mySql.update(updatePasswordSql, values);
    return Boolean(result && result.numberOfRowsAffected === 1);

  }

  async updateWithOneTimePassword(email: string): Promise<string> {
    const updatePasswordSql = 'UPDATE person SET password = ?, is_onetime_pass = 1 WHERE email = ?;';
    const newPass = makeRandomPass(15);
    const sha256 = sjcl.hash.sha256.hash(newPass);
    const hash = sjcl.codec.hex.fromBits(sha256);
    const passwordHash = bcryptjs.hashSync(hash, 10);
    const result = await this._mySql.update(updatePasswordSql, [passwordHash, email]);
    if (result.numberOfRowsAffected === 1) {
      return newPass;
    }
    throw new Error('Update password failed.');
  }

}

//@flow


import MySql from '../MySql';
import type {Invitable} from '../../domain/invitables';

type InsertResult = { error: Error } | { numberOfRowsAffected: number, lastInsertedId: number }

export interface InvitablesRepository {
    addInvitable(invitable: Invitable): Promise<?Invitable>;

    getRoomTypeForEmail(email: string): Promise<?string>;

    getInvitablesForEmail(email: string, roomType: string, departure: string): Promise<Array<Invitable>>;

    readInvitableByEmail(email: string): Promise<?Invitable>;

    deleteByMail(email: string): Promise<void>;
}


export default class InvitablesMySqlRepository implements InvitablesRepository {
  _mysql: MySql;

  constructor(mysql: MySql) {
    this._mysql = mysql;
  }

  _toInvitable = (item: any): Invitable => {
    return {
      id: item.id,
      email: item.email,
      userName: item.full_name,
      roomType: item.room_type,
      gender: item.gender,
      introduction: item.introduction
    };
  };

  _leaveAtTheSameDay = (foundDeparture: string, departureToCompareWith: string) => {
    switch (departureToCompareWith){
      case '0':
      case '1':
        return foundDeparture === '0' || foundDeparture === '1';
      case '2':
      case '3':
      case '4':
        return foundDeparture === '2' || foundDeparture === '3' || foundDeparture === '4';
      case '5':
        return foundDeparture === '5';
      default:
        return false;
    }
  };

  async addInvitable(invitable: Invitable): Promise<?Invitable> {
    const insertStatement = 'INSERT INTO invitables (full_name, room_type, gender, email, introduction) ' +
            'VALUES (?,?,?,?,?)';
    const {userName, gender, introduction, email, roomType} = invitable;
    const queryResult: InsertResult = await this._mysql
      .insert(insertStatement, [userName, roomType, gender, email, introduction])
      .catch(error => ({error}));

    if (queryResult.error) {
      throw new Error(queryResult.error);
    }
    const id: number = queryResult.lastInsertedId;
    return {...invitable, id};
  }

  async getRoomTypeForEmail(email: string): Promise<?string> {
    const queryStatement = 'SELECT room_type as roomType FROM socrates_db.registration as r INNER JOIN person as p ' +
            'ON r.person_id = p.id where p.email =?';
    const values = [email];
    const readValues = await this._mysql.selectWithParams(queryStatement, values);
    if (readValues && readValues.length === 1) {
      return readValues[0].roomType;
    }
    return null;
  }

  async getInvitablesForEmail(email: string, roomType: string, departure: string): Promise<Array<Invitable>> {
    const queryStatement = 'SELECT i.*, p.departure  FROM invitables as i ' +
          'INNER JOIN person as p2 ' +
          'ON p2.email = i.email ' +
          'INNER JOIN participants as p ' +
          'ON p2.id = p.person_id ' +
          'WHERE i.room_type=? ' +
          'AND i.email<>? ' +
          'AND (SELECT count(id) FROM invitations ' +
          'WHERE status<>\'DECLINED\' ' +
          'AND (invitee1_email= i.email OR invitee2_email=i.email)) = 0 ';
    const values = [roomType, email];
    console.log(queryStatement);
    console.log(values);
    const results = await this._mysql.selectWithParams(queryStatement, values);
    if (results) {
      const invitables = results
        .filter(item => this._leaveAtTheSameDay(item.departure, departure))
        .map(item => this._toInvitable(item));
      console.log(invitables);
      return invitables;
    }
    return [];
  }
  async readInvitableByEmail(email: string): Promise<?Invitable> {
    const queryStatement = 'SELECT * FROM socrates_db.invitables WHERE email=?';
    const values = [email];
    const results = await this._mysql.selectWithParams(queryStatement, values);
    return results && results.length === 1 ? this._toInvitable(results[0]) : null;
  }

  async deleteByMail(email: string): Promise<void> {
    const deleteStatement = 'DELETE FROM invitables WHERE email = ?';
    await this._mysql.delete(deleteStatement, [email]);
  }

}

/* eslint-disable camelcase */
// @flow

import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import InvitablesMySqlRepository from './invitablesRepository';
import type {Invitable} from '../../domain/invitables';

jest.mock('../MySql');

let invitablesRepository: InvitablesMySqlRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

const rawInvitableData = {
  id: 4711, full_name: 'Test', email: 'test@test.de',
  gender: 'gender', room_type: 'bedInDouble',
  introduction: 'introduction', departure: '5'
};

const invitableStub: Invitable = {
  id: 4711,
  email: 'test@test.de',
  userName: 'Test',
  gender: 'gender',
  roomType: 'bedInDouble',
  introduction: 'introduction'
};

describe('Invitables repository:', () => {

  beforeEach(() => {
    invitablesRepository = new InvitablesMySqlRepository(mySql);
  });

  describe('when searching an invitable', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });
    describe('by email', () => {
      it('returns invitable if it exists', async () => {
        promise = Promise.resolve([rawInvitableData]);
        const invitable: ?Invitable = await invitablesRepository.readInvitableByEmail('test@test.de');
        expect(invitable).toBeDefined();
        if(invitable) {
          expect(invitable.id).toBe(4711);
          expect(invitable.email).toEqual('test@test.de');
          expect(invitable.userName).toEqual('Test');
          expect(invitable.roomType).toEqual('bedInDouble');
          expect(invitable.gender).toEqual('gender');
          expect(invitable.introduction).toEqual('introduction');
        }
      });
      it('returns null if it does not exist', async () => {
        promise = Promise.resolve([]);
        const invitable = await invitablesRepository.readInvitableByEmail('test@test.de');
        expect(invitable).toBeNull();
      });
    });
  });
  describe('when reading the room type for an email', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });
    it('returns the room type, if email is registered', async () => {
      promise = Promise.resolve([{roomType: 'bedInDouble'}]);
      const roomType = await invitablesRepository.getRoomTypeForEmail('test@test.de');
      expect(roomType).toEqual('bedInDouble');
    });
    it('returns empty string if email is not registered', async () => {
      promise = Promise.resolve([]);
      const roomType = await invitablesRepository.getRoomTypeForEmail('test@test.de');
      expect(roomType).toBeNull();
    });
  });
  describe('when creating an invitable', () => {
    let promise;
    beforeEach(() => {
      mySql.insert.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.insert.mockReset();
    });
    it('returns the created invitable', async () => {
      promise = Promise.resolve({lastInsertedId: 4711, numberOfRowsAffected: 1});
      const result = await invitablesRepository.addInvitable({...invitableStub, id: 0});
      expect(result).toBeDefined();
      if(result) {
        expect(result.id).toBe(4711);
      }
    });
  });
  describe('when reading all possible roommates for a participant', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });
    it('we get all participants that are in the marketplace, have the same room type and has no open invitations',
      async () => {
        promise = Promise.resolve([rawInvitableData]);
        const results = await invitablesRepository.getInvitablesForEmail('test2@test.de', 'bedInDouble', '5');
        expect(results).toBeDefined();
        expect(results.length).toBe(1);
        if (results[0]) {
          expect(results[0].id).toBe(4711);
          expect(results[0].email).toEqual('test@test.de');
        }
      });
  });

  describe('when deleting by email', () => {
    let promise;
    beforeEach(() => {
      mySql.delete.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.delete.mockReset();
    });
    it('calls delete on mysql',
      async () => {
        promise = Promise.resolve();
        await invitablesRepository.deleteByMail('test2@test.de');
        expect(mySql.delete).toBeCalledWith('DELETE FROM invitables WHERE email = ?', ['test2@test.de']);
      });
  });
});

// @flow

import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import EventLogRepository from './eventLogRepository';

jest.mock('../MySql');

let repository: EventLogRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('Event log repository', () => {
  let promise;
  beforeEach(() => {
    repository = new EventLogRepository(mySql);
  });

  describe('can record a new event', () => {
    beforeEach(() => {
      mySql.insert.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.insert.mockReset();
    });
    it('when all fields are filled', async() => {
      promise = Promise.resolve({
        lastInsertedId: 4711,
        numberOfRowsAffected: 1
      });
      const success = await repository.log('EventType', 'OldValue', 'NewValue');
      expect(success).toBe(true);
      expect(mySql.insert.mock.calls).toHaveLength(1);
    });
  });
  describe('does not record the event', () => {
    it('when no event type available', async() => {
      const success = await repository.log('', 'OldValue', 'NewValue');
      expect(success).toBe(false);
    });
  });
});

// @flow
import MySql from '../MySql';

export default class EventLogRepository {

  _mySql: MySql;

  constructor(mySql: MySql) {
    this._mySql = mySql;
  }
  async log(type: string, oldValue: string, newValue: string): Promise<boolean> {
    if(type && type.length > 0) {
      const _insertQuery: string = 'INSERT INTO event_log (type, old_values, new_values) VALUES (?, ?, ?)';
      const values: Array<any> = [type, oldValue, newValue];
      const result = await this._mySql.insert(_insertQuery, values);
      return result.numberOfRowsAffected === 1;
    }
    return false;
  }
}

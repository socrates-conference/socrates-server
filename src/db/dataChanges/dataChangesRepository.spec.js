/* eslint-disable camelcase */
// @flow

import MySql from '../MySql';
import TestHelper from '../SqlTestHelper';
import DataChangesRepository from './dataChangesRepository';

jest.mock('../MySql');

let repository: DataChangesRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

const rawChanges: Object[] = [{
  id: 4711,
  type: 'ChangeType',
  date: '2006-03-94T00:00:00.000Z',
  oldValue: 'oldValue',
  newValue: 'newValue'
}];
describe('Data changes repository:', () => {

  beforeEach(() => {
    repository = new DataChangesRepository(mySql);
  });

  describe('can read changes', () => {
    let promise;
    beforeEach(() => {
      mySql.selectWithParams.mockImplementation(() => {
        return promise;
      });
    });
    afterEach(() => {
      mySql.selectWithParams.mockReset();
    });
    it('and returns a filled array if changes are present', async () => {
      promise = Promise.resolve(rawChanges);
      const changes: Object[] = await repository.readChangesFrom('2005-01-01 00:00:00');
      expect(changes).toBeDefined();
      expect(changes).toHaveLength(1);
      if (changes) {
        const change = changes[0];
        expect(change.id).toBe(4711);
        expect(change.type).toEqual('ChangeType');
        expect(change.date).toEqual('2006-03-94T00:00:00.000Z');
        expect(change.oldValue).toEqual('oldValue');
        expect(change.newValue).toEqual('newValue');
      }
    });
    it('and returns an empty array if no changes are present', async () => {
      promise = Promise.resolve([]);
      const changes: Object[] = await repository.readChangesFrom('2005-01-01 00:00:00');
      expect(changes).toBeDefined();
      expect(changes).toHaveLength(0);
    });
  });
});

//@flow


import MySql from '../MySql';

export default class DataChangesRepository {
  _mysql: MySql;

  constructor(mysql: MySql) {
    this._mysql = mysql;
  }

  async readChangesFrom(fromDate: string): Promise<Array<Object>> {
    const queryStatement =
      'SELECT id, type, date, old_values as oldValue, new_values as newValue FROM socrates_db.event_log ' +
      ' where date >= ?';
    const values = [fromDate];
    const readValues: Array<Object> = await this._mysql.selectWithParams(queryStatement, values);
    if (readValues && readValues.length > 0) {
      return readValues;
    }
    return [];
  }
}

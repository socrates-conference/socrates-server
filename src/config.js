// @flow

import rc from 'rc';
import type {Config} from './ConfigType';

// add your own config in .socrates-serverrc file and don't add it to git if you want to keep your secrets.

const config: Config = {
  'pricingTable': {
    'single': 62.5,
    'juniorExclusively': 62.5,
    'juniorShared': 45.45,
    'bedInDouble': 47.95,
    'Thursday Flat': 11.9,
    'Thursday Dinner': 13.3,
    'Friday Flat': 52.9,
    'Friday Dinner': 13.3,
    'Saturday Flat': 52.9,
    'Saturday Dinner': 13.3,
    'Sunday Flat': 45,
    'Sunday Dinner': 13.3
  },
  'sponsoringAmount': 350,
  'environment': 'dev',
  'jwtSecret': '$lsRTf!gksTRcDWs',
  'database': {
    'host': '',
    'port': 0,
    'name': '',
    'user': '',
    'password': ',',
    'debug': false
  },
  'server': {
    'port': 4444
  },
  'mail': {
    'user': '',
    'password': '',
    'host': ''
  },
  'kafka': {
    host: 'zookeeper',
    port: '2181'
  }
};
export default rc('socrates-server', config);

// @flow
import instana from 'instana-nodejs-sensor';
instana();

import App from './app';
import config from './config';
import rootServer from './routes/rootServer';
import createNewsletterServer from './routes/newsletterServer';
import createApplicationServer from './routes/applicationServer';
import MySql from './db/MySql';
import DomainEventDispatcher from './domain/domainEventDispatcher';
import defaultHeaders from './routes/defaultHeaders';
import bodyParsers from './routes/bodyParsers';
import authenticationMiddleware from './routes/authenticationMiddleware';
import Notification from './domain/notification';
import Email from './adapter/email';
import createApplicationListServer from './routes/management/applicationListServer';
import createRegistrationListServer from './routes/management/registrationListServer';
import createDashboardServer from './routes/management/dashboardServer';
import createLotteryServer from './routes/management/lotteryServer';
import createParticipantsServer from './routes/participantsServer';
import {EventPublisher} from 'socrates-event-stream';
import Eventbus from './adapter/eventbus';
import createInvitationsServer from './routes/invitationsServer';
import createInvitablesServer from './routes/invitablesServer';
import createHotelInformationServer from './routes/hotel/hotelInformationServer';
import DataChangesLogger from './adapter/data_changes_logger';
import EventLogRepository from './db/eventLog/eventLogRepository';

const sqlHandler = new MySql(config);
sqlHandler.initialize();
const dispatcher = new DomainEventDispatcher();
const email = new Email();
// eslint-disable-next-line no-unused-vars
const notification = new Notification( email, dispatcher);

const logger = {
  info: (message, obj) => console.log(message, obj),
  error: (message, obj) => console.error(message, obj),
  trace: (message, obj) => console.log(message, obj)
};
const kafka = new EventPublisher( 'socrates-events', 'zookeeper', '2181', logger);
// eslint-disable-next-line no-unused-vars
const eventbus = new Eventbus(dispatcher, kafka);

// eslint-disable-next-line no-unused-vars
const dataChangesLogger = new DataChangesLogger(new EventLogRepository(sqlHandler), dispatcher);

const app = new App(config);
app.applyMiddleware(bodyParsers);
app.applyMiddleware(defaultHeaders);
app.applyMiddleware(authenticationMiddleware(sqlHandler, dispatcher));
app.applyMiddleware(createNewsletterServer(sqlHandler, dispatcher));
app.applyMiddleware(createParticipantsServer(sqlHandler, dispatcher));
app.applyMiddleware(createApplicationServer(sqlHandler, dispatcher));
app.applyMiddleware(createDashboardServer(sqlHandler, dispatcher));
app.applyMiddleware(createApplicationListServer(sqlHandler, dispatcher));
app.applyMiddleware(createRegistrationListServer(sqlHandler, dispatcher));
app.applyMiddleware(createLotteryServer(sqlHandler, dispatcher));
app.applyMiddleware(createInvitationsServer(sqlHandler, dispatcher));
app.applyMiddleware(createInvitablesServer(sqlHandler, dispatcher));
app.applyMiddleware(createHotelInformationServer(sqlHandler, dispatcher));
app.applyMiddleware(rootServer);

app.run().catch(err => console.error(err));

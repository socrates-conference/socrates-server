// @flow

import express from 'express';

export type Middleware = (app: express.Application) => void;

export function addMiddleware(app: express.Application) {
  return (fn: Middleware) => {
    fn(app);
  };
}
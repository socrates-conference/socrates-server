// @flow
import DomainEventDispatcher from '../domain/domainEventDispatcher';
import {
  LOG_PARTICIPANT_CANCELED, LOG_PARTICIPANT_CONFIRMED,
  LOG_PARTICIPANT_DELETED, LOG_PARTICIPANT_UPDATED
} from '../domain/participant';
import DataChangesLogger from './data_changes_logger';
import EventLogRepository from '../db/eventLog/eventLogRepository';
import TestHelper from '../db/SqlTestHelper';
import MySql from '../db/MySql';
import {LOG_UPDATE_ROOM_TYPE_SUCCEED} from '../domain/management/registrationList';

jest.mock('../db/eventLog/eventLogRepository');

describe('Date changes logger:', () => {
// eslint-disable-next-line no-unused-vars
  let logger, dispatcher, promise;
  let repository: EventLogRepository;
  const testHelper = new TestHelper();
  const mySql = new MySql(testHelper.testConfig);


  beforeEach(() => {
    dispatcher = new DomainEventDispatcher();
    repository = new EventLogRepository(mySql);
    logger = new DataChangesLogger(repository, dispatcher);
    repository.log.mockImplementation(() => {
      return promise;
    });
  });

  afterEach(() => {
    repository.log.mockReset();
  });

  it('logs the change when a participant cancels the participation', () => {
    promise = Promise.resolve(true);
    dispatcher.dispatchEvent({
      type: LOG_PARTICIPANT_CANCELED,
      payload: {oldValue: 'someValue', newValue: 'newValue'}
    });

    expect(repository.log.mock.calls).toHaveLength(1);
  });
  it('logs the change when an admin removes a participant', () => {
    promise = Promise.resolve(true);
    dispatcher.dispatchEvent({
      type: LOG_PARTICIPANT_DELETED,
      payload: {oldValue: 'someValue', newValue: 'newValue'}
    });

    expect(repository.log.mock.calls).toHaveLength(1);
  });
  it('logs the change when a participant confirms', () => {
    promise = Promise.resolve(true);
    dispatcher.dispatchEvent({
      type: LOG_PARTICIPANT_CONFIRMED,
      payload: {oldValue: 'someValue', newValue: 'newValue'}
    });

    expect(repository.log.mock.calls).toHaveLength(1);
  });
  it('logs the change when an administrator changes a participant\'s room type', () => {
    promise = Promise.resolve(true);
    dispatcher.dispatchEvent({
      type: LOG_UPDATE_ROOM_TYPE_SUCCEED,
      payload: {oldValue: 'someValue', newValue: 'newValue'}
    });

    expect(repository.log.mock.calls).toHaveLength(1);
  });
  describe('When a participant changes the profile information', () => {
    const oldValue = {
      'id': 243,
      'person_id': 7,
      'firstname': 'Alexandre',
      'lastname': 'Soler Sanandres',
      'company': '',
      'address1': 'Am Hirschanger 1',
      'address2': '',
      'province': '',
      'postal': '90610',
      'city': 'Winkelhaid',
      'country': 'Deutschland',
      'arrival': '0',
      'departure': '5',
      'dietary': false,
      'dietary_info': '',
      'family': false,
      'family_info': '',
      'gender': '',
      'tshirt': '12',
      'labelname': 'Alex6',
      'social': '@xelamrelos',
      'pronoun': 'he',
      'personId': 7,
      'familyInfo': '',
      'dietaryInfo': ''
    };
    const newValue = {
      'id': 243,
      'person_id': 7,
      'firstname': 'Alexandre',
      'lastname': 'Soler Sanandres',
      'company': '',
      'address1': 'Am Hirschanger 1',
      'address2': '',
      'province': '',
      'postal': '90610',
      'city': 'Winkelhaid',
      'country': 'Deutschland',
      'arrival': '0',
      'departure': '5',
      'dietary': false,
      'dietary_info': '',
      'family': false,
      'family_info': '',
      'gender': '',
      'tshirt': '12',
      'labelname': 'Alex6',
      'social': '@xelamrelos',
      'pronoun': 'he',
      'personId': 7,
      'familyInfo': '',
      'dietaryInfo': ''
    };

    it('does not log the change when nothing changes', () => {
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(0);
    });
    it('logs the change when the first name is changed', () => {
      newValue.firstname = 'changed';
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the last name is changed', () => {
      newValue.lastname = 'changed';
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the company is changed', () => {
      newValue.company = 'changed';
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the address1 is changed', () => {
      newValue.address1 = 'changed';
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the address2 is changed', () => {
      newValue.address2 = 'changed';
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the province is changed', () => {
      newValue.province = 'changed';
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the postal is changed', () => {
      newValue.postal = 'changed';
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the city is changed', () => {
      newValue.city = 'changed';
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the country is changed', () => {
      newValue.country = 'changed';
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the arrival is changed', () => {
      newValue.arrival = 'changed';
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the departure is changed', () => {
      newValue.departure = 'changed';
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the family checkbox is changed', () => {
      newValue.family = true;
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the family info is changed', () => {
      newValue.familyInfo = 'changed';
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the dietary checkbox is changed', () => {
      newValue.dietary = true;
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
    it('logs the change when the dietary info is changed', () => {
      newValue.dietaryInfo = 'changed';
      promise = Promise.resolve(true);
      dispatcher.dispatchEvent({
        type: LOG_PARTICIPANT_UPDATED,
        payload: {oldValue, newValue}
      });

      expect(repository.log.mock.calls).toHaveLength(1);
    });
  });
});

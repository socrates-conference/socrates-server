// @flow

import nodemailer from 'nodemailer';
import config from '../config';

export default class Email {
  transport: $Shape<{sendMail: (mailOptions: any, callback: (error: Error, info: any)=> void) => void}>;

  constructor() {
    this.transport = nodemailer.createTransport({
      host: config.mail.host,
      auth: {
        user: config.mail.user,
        pass: config.mail.password
      }
    });
  }

  send(title: string, body: string, ...email: [string]): Promise<string> {
    const mailOptions = {
      from: config.mail.user,
      to: email[0],
      subject: title,
      text: body
    };

    return new Promise((resolve, reject) => {
      this.transport.sendMail(mailOptions, (error, info) => {
        if (error) {
          reject({error, mailOptions});
        } else {
          resolve('Email sent: ' + info.response);
        }
      });
    });
  }
}
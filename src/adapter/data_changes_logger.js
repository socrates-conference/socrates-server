// @flow

import Service from '../domain/service';
import type {Event, EventDispatcher} from '../domain/eventDispatcher';
import EventLogRepository from '../db/eventLog/eventLogRepository';
import {
  LOG_PARTICIPANT_CANCELED,
  LOG_PARTICIPANT_CONFIRMED,
  LOG_PARTICIPANT_DELETED,
  LOG_PARTICIPANT_UPDATED
} from '../domain/participant';
import {LOG_UPDATE_ROOM_TYPE_SUCCEED} from '../domain/management/registrationList';
import {arrivalToText, departureToText} from '../utilities/Utilities';

export type DataChangeValues = {
  firstname: string,
  lastname: string,
  company?: string,
  address1?: string,
  address2?: string,
  province?: string,
  postal?: string,
  city?: string,
  country?: string,
  arrival?: string,
  departure?: string,
  dietary?: boolean,
  dietaryInfo?: string,
  family?: boolean,
  familyInfo?: string,
}
export default class DataChangesLogger extends Service {
  eventLogger: EventLogRepository;

  constructor(eventLogger: EventLogRepository, eventDispatcher: EventDispatcher) {
    super(eventDispatcher);
    this.eventLogger = eventLogger;
    // catch admin deletes participant
    eventDispatcher.subscribe(LOG_PARTICIPANT_DELETED, this._handleDataChange);
    eventDispatcher.subscribe(LOG_PARTICIPANT_CANCELED, this._handleDataChange);
    eventDispatcher.subscribe(LOG_PARTICIPANT_CONFIRMED, this._handleDataChange);
    eventDispatcher.subscribe(LOG_UPDATE_ROOM_TYPE_SUCCEED, this._handleDataChange);
    eventDispatcher.subscribe(LOG_PARTICIPANT_UPDATED, this._handleProfileUpdate);
  }

  _handleDataChange = (event: Event) => {
    const oldValue = JSON.stringify(event.payload.oldValue || '');
    const newValue = JSON.stringify(event.payload.newValue || '');
    this.eventLogger.log(event.type, oldValue, newValue)
      .then(result => {
        if (!result) {
          throw new Error(`Cannot record event ${event.type}`);
        }
      });
  };
  _handleProfileUpdate = (event: Event) => {
    if (this._relevantDataChanged(event)) {
      this._handleDataChange(this._prepareData(event));
    }
  };

  _prepareData = (event: Event): Event => {
    const {oldValue, newValue} = event.payload;
    const oldData: DataChangeValues = {firstname: oldValue.firstname, lastname: oldValue.lastname};
    const newData: DataChangeValues = {firstname: newValue.firstname, lastname: newValue.lastname};
    if (this._addressDataHasChanged(event)) {
      oldData.company = oldValue.company;
      newData.company = newValue.company;
      oldData.address1 = oldValue.address1;
      newData.address1 = newValue.address1;
      oldData.address2 = oldValue.address2;
      newData.address2 = newValue.address2;
      oldData.province = oldValue.province;
      newData.province = newValue.province;
      oldData.postal = oldValue.postal;
      newData.postal = newValue.postal;
      oldData.city = oldValue.city;
      newData.city = newValue.city;
      oldData.country = oldValue.country;
      newData.country = newValue.country;
    }
    if (this._lengthOfStayHasChanged(event)) {
      oldData.arrival = arrivalToText(oldValue.arrival);
      newData.arrival = arrivalToText(newValue.arrival);
      oldData.departure = departureToText(oldValue.departure);
      newData.departure = departureToText(newValue.departure);
    }
    if (this._dietaryNeedsHasChanged(event)) {
      oldData.dietary = oldValue.dietary;
      newData.dietary = newValue.dietary;
      oldData.dietaryInfo = oldValue.dietaryInfo;
      newData.dietaryInfo = newValue.dietaryInfo;
    }
    if (this._familyInfoHasChanged(event)) {
      oldData.family = oldValue.family;
      newData.family = newValue.family;
      oldData.familyInfo = oldValue.familyInfo;
      newData.familyInfo = newValue.familyInfo;
    }
    event.payload.oldValue = oldData;
    event.payload.newValue = newData;
    return event;
  };

  _relevantDataChanged = (event: Event): boolean => {
    return this._nameDataHasChanged(event) || this._addressDataHasChanged(event) || this._lengthOfStayHasChanged(event)
      || this._dietaryNeedsHasChanged(event) || this._familyInfoHasChanged(event);
  };

  _nameDataHasChanged = (event: Event): boolean => {
    const {oldValue, newValue} = event.payload;
    return oldValue.firstname !== newValue.firstname || oldValue.lastname !== newValue.lastname;
  };

  _addressDataHasChanged = (event: Event): boolean => {
    const {oldValue, newValue} = event.payload;
    return oldValue.company !== newValue.company || oldValue.address1 !== newValue.address1
      || oldValue.address2 !== newValue.address2 || oldValue.province !== newValue.province
      || oldValue.postal !== newValue.postal || oldValue.city !== newValue.city
      || oldValue.country !== newValue.country;
  };

  _lengthOfStayHasChanged = (event: Event): boolean => {
    const {oldValue, newValue} = event.payload;
    return oldValue.arrival !== newValue.arrival || oldValue.departure !== newValue.departure;
  };

  _dietaryNeedsHasChanged = (event: Event): boolean => {
    const {oldValue, newValue} = event.payload;
    return oldValue.dietary !== newValue.dietary || oldValue.dietaryInfo !== newValue.dietaryInfo;
  };

  _familyInfoHasChanged = (event: Event): boolean => {
    const {oldValue, newValue} = event.payload;
    return oldValue.family !== newValue.family || oldValue.familyInfo !== newValue.familyInfo;
  };

}

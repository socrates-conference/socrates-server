// @flow

import type {
  Event,
  EventDispatcher
} from '../domain/eventDispatcher';
import {
  EventPublisher,
  StringifyTransform
} from 'socrates-event-stream';
import config from '../config';

export default class Eventbus {
  _eventDispatcher: EventDispatcher;
  _kafka: EventPublisher;
  _stringify: StringifyTransform;

  constructor(eventDispatcher: EventDispatcher, kafka: EventPublisher) {
    this._eventDispatcher = eventDispatcher;
    this._kafka = kafka;
    this._kafka.on('error', this._handleError);
    this._kafka.on('ready', this._handleReady);
    this._stringify = new StringifyTransform();
  }

  _handleReady = () => {
    console.log('Connected to Kafka on %s:%s', config.kafka.host, config.kafka.port);
    this._stringify.pipe(this._kafka);
    this._eventDispatcher.subscribe('*', this._handleEvent);
  };

  _handleEvent = (ev: Event) => {
    const publishedEvent: {event: string, value: any, timestamp?: Date} = {
      event: ev.type,
      value: ev.payload,
      timestamp: new Date()
    };
    this._stringify.write(publishedEvent);
  };

  _handleError = (err: Error) => {
    console.error(err);
  };

}
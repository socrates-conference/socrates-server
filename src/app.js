// @flow

import express from 'express';
import type {Config} from './ConfigType';
import {addMiddleware} from './middleware';
import type {Middleware} from './middleware';

export default class App {
  config: Config;
  app: express.Application;
  server: express.Server;
  isRunning: boolean;

  constructor(config: Config) {
    this.isRunning = false;
    this.config = config;
    this.app = express();
    this.app.set('env', config.environment);
  }

  applyMiddleware = (middleware: Middleware) => {
    addMiddleware(this.app)(middleware);
  };

  run = (): Promise<void> => {
    return new Promise(resolve => {
      this.isRunning = true;
      this.server = this.app.listen(this.config.server.port, () => {
        console.log(`Server listening on port ${this.config.server.port}`);
        resolve();
      });
    });
  };

  stop = (): Promise<void> => {
    return new Promise(resolve => {
      this.isRunning = false;
      if (this.server) {
        this.server.close(() => {
          console.log('Server shutdown complete.');
          resolve();
        });
      }
    });
  };
}

/* eslint-disable camelcase */
import App from './app';
import axios from 'axios';
import MySql from './db/MySql';
import DomainEventDispatcher from './domain/domainEventDispatcher';
import createNewsletterServer from './routes/newsletterServer';
import rootServer from './routes/rootServer';
import defaultHeaders from './routes/defaultHeaders';
import bodyParsers from './routes/bodyParsers';
import createApplicationServer from './routes/applicationServer';
import createApplicationListServer from './routes/management/applicationListServer';
import createRegistrationListServer from './routes/management/registrationListServer';
import createLotteryServer from './routes/management/lotteryServer';
import {stub} from 'sinon';
import createInvitationsServer from './routes/invitationsServer';
import createInvitablesServer from './routes/invitablesServer';
import createParticipantsServer from './routes/participantsServer';
import createHotelInformationServer from './routes/hotel/hotelInformationServer';
import {
  conferenceArrivalsStubbed,
  conferenceDaysStubbed, conferenceDeparturesStubbed,
  conferenceEditionStubbed, conferenceLengthOfStayStubbed,
  conferenceRoomTypesStubbed
} from './db/conference/conferenceTestStubs';

jest.mock('./db/MySql');
describe('Application:', () => {
  let app, dispatcher;
  const defaultConfiguration = {
    'environment': 'test',
    'database': {
      'host': 'localhost',
      'port': 3306,
      'name': 'socrates',
      'user': '',
      'password': ''
    },
    'server': {
      'port': 7777
    }
  };
  const sqlHandler = new MySql(defaultConfiguration);

  const selectWithParamsStub = stub(sqlHandler, 'selectWithParams');
  const insertStub = stub(sqlHandler, 'insert');
  const updateStub = stub(sqlHandler, 'update');
  const deleteStub = stub(sqlHandler, 'delete');
  const selectStub = stub(sqlHandler, 'select');

  beforeAll(async () => {
    dispatcher = new DomainEventDispatcher();
    app = new App(defaultConfiguration);
    app.applyMiddleware(bodyParsers);
    app.applyMiddleware(defaultHeaders);
    app.applyMiddleware(createNewsletterServer(sqlHandler, dispatcher));
    app.applyMiddleware(createApplicationServer(sqlHandler, dispatcher));
    app.applyMiddleware(createApplicationListServer(sqlHandler, dispatcher));
    app.applyMiddleware(createRegistrationListServer(sqlHandler, dispatcher));
    app.applyMiddleware(createLotteryServer(sqlHandler, dispatcher));
    app.applyMiddleware(createInvitationsServer(sqlHandler, dispatcher));
    app.applyMiddleware(createInvitablesServer(sqlHandler, dispatcher));
    app.applyMiddleware(createParticipantsServer(sqlHandler, dispatcher));
    app.applyMiddleware(createHotelInformationServer(sqlHandler, dispatcher));
    app.applyMiddleware(rootServer);
    await app.run();
  });

  afterAll(async () => {
    if (app) {
      await app.stop();
    }
  });

  afterEach(() => {
    selectStub.reset();
    selectWithParamsStub.reset();
    deleteStub.reset();
    insertStub.reset();
    updateStub.reset();
  });

  it('starts', () => {
    expect(app.isRunning).toBe(true);
  });

  it('accepts get requests to root', () => {
    return axios.get('http://localhost:7777/')
      .then((response) => {
        expect(response.status).toBe(200);
      });
  });

  it('rejects requests to non existent urls', (done) => {
    axios.get('http://localhost:7777/non-existent')
      .catch(err => {
        expect(err.response.status).toBe(404);
        done();
      });
  });

  it('accepts api get requests to root', () => {
    return axios.get('http://localhost:7777/api/v1/')
      .then((response) => {
        expect(response.status).toBe(200);
      });
  });

  describe('Newsletter router:', () => {
    describe('post request to interested-people', () => {

      it('return status 200 on success', (done) => {
        selectWithParamsStub.resolves([]);
        insertStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 888});
        axios.post('http://localhost:7777/api/v1/interested-people', {
          name: 'FirstName LastName',
          email: 'name@domain.de'
        }).then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeTruthy();
          done();
        });
      });

      it('returns 500 status on error', (done) => {
        selectWithParamsStub.resolves([]);
        insertStub.resolves({numberOfRowsAffected: 0, lastInsertedId: 0});
        axios.post('http://localhost:7777/api/v1/interested-people', {
          name: 'FirstName LastName',
          email: 'name@domain.de'
        })
          .then(() => done('Error was expected'))
          .catch((err) => {
            expect(err.response.status).toBe(500);
            expect(err.response.data).toEqual({});
            done();
          });
      });
    });
    describe('post request to interested-people/confirm/:consentKey', () => {

      it('return status 200 on success', (done) => {
        selectWithParamsStub.resolves([{id: 4711}]);
        updateStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 0});
        axios.post('http://localhost:7777/api/v1/interested-people/confirm', {consentKey: 'some key'})
          .then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toBeTruthy();
            done();
          });
      });

      it('returns 500 status on error', (done) => {
        axios.post('http://localhost:7777/api/v1/interested-people/confirm', {consentKey: ''})
          .then(() => done('Error was expected'))
          .catch((err) => {
            expect(err.response.status).toBe(500);
            expect(err.response.data).toEqual({});
            done();
          });
      });
    });
    describe('delete request to interested-people', () => {
      it('return status 200 on success', (done) => {
        updateStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 0});
        axios.delete('http://localhost:7777/api/v1/interested-people/name@domain.de')
          .then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toBe('');
            done();
          });
      });

      it('returns 500 status on error', (done) => {
        updateStub.resolves({numberOfRowsAffected: 2, lastInsertedId: 0});
        axios.delete('http://localhost:7777/api/v1/interested-people/name@domain.de')
          .then(() => done('Error was expected'))
          .catch((err) => {
            expect(err.response.status).toBe(500);
            expect(err.response.data).toEqual({});
            done();
          });
      });
    });
  });

  describe('Application:', () => {
    describe('post request to applications', () => {

      it('return status 200 on success', (done) => {
        selectWithParamsStub.withArgs('SELECT id FROM person WHERE email = ?;', ['test@test.de']).resolves([{id: 47}]);
        selectWithParamsStub.withArgs('SELECT COUNT(id) AS applicationCount FROM application WHERE person_id = ?', [47])
          .resolves([{applicationCount: 0}]);
        selectWithParamsStub
          .withArgs('SELECT COUNT(id) AS registrationCount FROM registration WHERE person_id = ?', [47])
          .resolves([{registrationCount: 0}]);
        updateStub.resolves({numberOfRowsAffected: 1});
        insertStub.resolves({numberOfRowsAffected: 2});
        axios.post('http://localhost:7777/api/v1/applications',
          {
            id: 0,
            personId: 47,
            nickname: 'whatever',
            email: 'test@test.de',
            roomTypes: ['single', 'double'],
            diversity: 'No'
          })
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });

      it('returns 500 status on error', (done) => {
        axios.post('http://localhost:7777/api/v1/applications')
          .catch(err => {
            expect(err.response.status).toBe(500);
            done();
          });
      });
    });
    describe('get request to applications/options', () => {
      it('return status 200 on success', (done) => {
        selectWithParamsStub.onFirstCall().resolves({data: {id: 1, name: 'SoCraTes'}})
          .onSecondCall().resolves({data: {id: 1, year: 2019}})
          .resolves({data: []});
        axios.get('http://localhost:7777/api/v1/applications/options')
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
    });
  });

  describe('Application list:', () => {
    describe('get request to application-list', () => {
      it('return status 200 on success', (done) => {
        selectStub.resolves([]);
        axios.get('http://localhost:7777/api/v1/management/application-list')
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
    });
    describe('get request to application-list/by-room/:roomType', () => {
      it('return status 200 on success', (done) => {
        selectWithParamsStub.onFirstCall().resolves([{
          person_Id: 4711, applicant_id: 815, nickname: 'nickname', email: 'test@test.de',
          date: '', room_types: 'single', diversity_reason: 'no'
        }]);
        axios.get('http://localhost:7777/api/v1/management/application-list/by-room/single')
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
    });
    describe('put request to application-list/:applicationId', () => {
      it('return status 200 on success', (done) => {
        deleteStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 0});
        insertStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 888});
        axios.put('http://localhost:7777/api/v1/management/application-list/47', {
          personId: 47,
          roomTypes: ['single'],
          diversityReason: 'no'
        })
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
      it('return status 500 on failure', (done) => {
        deleteStub.resolves({numberOfRowsAffected: 0, lastInsertedId: 0});
        insertStub.resolves({numberOfRowsAffected: 0, lastInsertedId: 0});
        axios.put('http://localhost:7777/api/v1/management/application-list/47', {
          personId: 47,
          roomTypes: ['single'],
          diversityReason: 'no'
        })
          .catch(err => {
            expect(err.response.status).toBe(500);
            done();
          });
      });
    });

    describe('delete request to application-list/:personId', () => {
      it('return status 200 on success', (done) => {
        selectWithParamsStub.resolves([{id: 47}]);
        deleteStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 0});
        axios.delete('http://localhost:7777/api/v1/management/application-list/47')
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
      it('return status 500 on failure', (done) => {
        deleteStub.resolves({numberOfRowsAffected: 0, lastInsertedId: 0});
        axios.put('http://localhost:7777/api/v1/management/application-list/47')
          .catch(err => {
            expect(err.response.status).toBe(500);
            done();
          });
      });
    });
  });

  describe('Lottery:', () => {
    describe('get request to lottery application by room type', () => {
      it('return status 200 on success', (done) => {
        selectStub.resolves([]);
        axios.get('http://localhost:7777/api/v1/management/lottery/applications/by-room-type')
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
    });
    describe('get request to lottery registrations by room type', () => {
      it('return status 200 on success', (done) => {
        selectStub.resolves([]);
        axios.get('http://localhost:7777/api/v1/management/lottery/registrations/by-room-type')
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
    });
  });

  describe('Registration list:', () => {
    describe('get request to registration-list', () => {
      it('return status 200 on success', (done) => {
        selectStub.resolves([{}]);
        axios.get('http://localhost:7777/api/v1/management/registration-list')
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
    });

    describe('delete request to registration-list/:personId', () => {
      it('return status 200 on success', (done) => {
        selectWithParamsStub.resolves([{id: 47}]);
        deleteStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 0});
        axios.delete('http://localhost:7777/api/v1/management/registration-list/47')
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
      it('return status 500 on failure', (done) => {
        selectWithParamsStub.resolves([{id: 47}]);
        deleteStub.resolves({numberOfRowsAffected: 0, lastInsertedId: 0});
        axios.delete('http://localhost:7777/api/v1/management/registration-list/47')
          .catch(err => {
            expect(err.response.status).toBe(500);
            done();
          });
      });
    });
    describe('put request to registration-list/send-confirmation/:personId', () => {
      it('return status 200 on success, when personId is present', (done) => {
        selectWithParamsStub.resolves([{id: 47}]);
        updateStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 0});
        axios.put('http://localhost:7777/api/v1/management/registration-list/confirmation-reminder/47')
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
      it('return status 500 on failure, when personId is present', (done) => {
        selectWithParamsStub.resolves([{id: 47}]);
        updateStub.resolves({numberOfRowsAffected: 0, lastInsertedId: 0});
        axios.put('http://localhost:7777/api/v1/management/registration-list/confirmation-reminder/47')
          .catch(err => {
            expect(err.response.status).toBe(500);
            done();
          });
      });
    });
    describe('put request to registration-list/send-confirmation/', () => {
      it('return status 200', (done) => {
        selectStub.resolves([{id: 47}]);
        updateStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 0});
        axios.put('http://localhost:7777/api/v1/management/registration-list/confirmation-reminder/')
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
    });
    describe('post request to registration-list/send-confirmation/:personId', () => {
      it('return status 200 on success, when personId is present', (done) => {
        updateStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 0});
        axios.post('http://localhost:7777/api/v1/management/registration-list/47')
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
      it('return status 500 on failure, when personId is present', (done) => {
        updateStub.resolves({numberOfRowsAffected: 0, lastInsertedId: 0});
        axios.post('http://localhost:7777/api/v1/management/registration-list/47')
          .catch(err => {
            expect(err.response.status).toBe(500);
            done();
          });
      });
    });
  });

  describe('Participants', () => {
    describe('get request to participants without email in query', () => {
      it('return status 200 on success', (done) => {
        selectStub.resolves([{}]);
        axios.get('http://localhost:7777/api/v1/participants').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeTruthy();
          done();
        });
      });

      it('return status 500 on failure', (done) => {
        selectStub.rejects();
        axios.get('http://localhost:7777/api/v1/participants').then(() => {
          done('Exception expected!');
        }).catch((err) => {
          expect(err.response.status).toBe(500);
          done();
        });
      });
    });

    describe('get request to participants with email in query', () => {
      it('return status 200', (done) => {
        selectWithParamsStub.onCall(0).resolves([{}]);
        selectWithParamsStub.onCall(1).resolves([{}]);
        selectWithParamsStub.onCall(2).resolves([
          {person_id: 1, arrival: '0', departure: '1', nickname: 'Test', email: 'test@test.de'}
        ]);
        selectWithParamsStub.onCall(3).resolves([{roomType: 'single'}]);
        selectWithParamsStub.onCall(4).resolves([conferenceEditionStubbed]);
        selectWithParamsStub.onCall(5).resolves(conferenceDaysStubbed);
        selectWithParamsStub.onCall(6).resolves(conferenceRoomTypesStubbed);
        selectWithParamsStub.onCall(7).resolves(conferenceArrivalsStubbed);
        selectWithParamsStub.onCall(8).resolves(conferenceDeparturesStubbed);
        selectWithParamsStub.onCall(9).resolves(conferenceLengthOfStayStubbed);
        selectStub.onCall(0).resolves([
          {personId: 1, room: 'single', arrival: 0, departure: 1}]);
        selectWithParamsStub.onCall(10).resolves(350);
        axios.get('http://localhost:7777/api/v1/participants?email=test@test.de').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeTruthy();
          done();
        });
      });
      it('return status 404 when participant cannot be found', (done) => {
        selectWithParamsStub.onCall(0).resolves([{}]);
        selectWithParamsStub.onCall(1).resolves([{}]);
        selectWithParamsStub.onCall(2).resolves([]);
        axios.get('http://localhost:7777/api/v1/participants?email=test@test.de').then(() => {
          done('Exception expected!');
        }).catch((err) => {
          expect(err.response.status).toBe(404);
          done();
        });
      });

      it('return status 500 when fails', (done) => {
        selectWithParamsStub.onCall(0).resolves([{}]);
        selectWithParamsStub.onCall(1).resolves([{}]);
        selectWithParamsStub.onCall(2).rejects();
        axios.get('http://localhost:7777/api/v1/participants?email=test@test.de').then(() => {
          done('Exception expected!');
        }).catch((err) => {
          expect(err.response.status).toBe(500);
          done();
        });
      });

    });

    describe('get request to /participants/calculate-price/:personId', () => {
      it('return status 200 on success', (done) => {
        selectWithParamsStub.onCall(0).resolves([{roomType: 'single'}]);
        selectWithParamsStub.onCall(1).resolves([conferenceEditionStubbed]);
        selectWithParamsStub.onCall(2).resolves(conferenceDaysStubbed);
        selectWithParamsStub.onCall(3).resolves(conferenceRoomTypesStubbed);
        selectWithParamsStub.onCall(4).resolves(conferenceArrivalsStubbed);
        selectWithParamsStub.onCall(5).resolves(conferenceDeparturesStubbed);
        selectWithParamsStub.onCall(6).resolves(conferenceLengthOfStayStubbed);
        selectStub.onCall(0).resolves([
          {personId: 1, room: 'single', arrival: 0, departure: 1}]);

        axios.get('http://localhost:7777/api/v1/participants/calculate-price/1',
          {params: {arrival: '0', departure: '1', email: 'test@email.de'}})
          .then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toBeTruthy(); done();
          });
      });
    });

    describe('get request to /participants/:personId', () => {
      it('return status 200 on success', (done) => {
        selectWithParamsStub.resolves([{}]);
        axios.get('http://localhost:7777/api/v1/participants/1')
          .then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toBeTruthy();
            done();
          });
      });
      it('return status 404 when participant not found', (done) => {
        selectWithParamsStub.resolves([]);
        axios.get('http://localhost:7777/api/v1/participants/1')
          .catch(error => {
            expect(error.response.status).toBe(404);
            expect(error.response.data).toBeTruthy();
            done();
          });
      });
      it('return status 500 when failure', (done) => {
        selectWithParamsStub.rejects();
        axios.get('http://localhost:7777/api/v1/participants/1')
          .catch(error => {
            expect(error.response.status).toBe(500);
            expect(error.response.data).toBeTruthy();
            done();
          });
      });
    });

    describe('put request to /participants', () => {
      it('return status 200 on success', (done) => {
        selectWithParamsStub.resolves([{id:4711}]);
        updateStub.resolves({numberOfRowsAffected: 1});
        insertStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 333});
        axios.put('http://localhost:7777/api/v1/participants', {email: 'test@test.de'})
          .then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toBeTruthy();
            done();
          });
      });
      it('return status 500 when failure', (done) => {
        selectWithParamsStub.resolves([{id:4711}]);
        updateStub.rejects();
        axios.put('http://localhost:7777/api/v1/participants', {email: 'test@test.de'})
          .catch(error => {
            expect(error.response.status).toBe(500);
            expect(error.response.data).toBeTruthy();
            done();
          });
      });
      it('return status 200 with empty data when no participant data (email) in body', (done) => {
        selectWithParamsStub.resolves([]);
        updateStub.rejects();
        axios.put('http://localhost:7777/api/v1/participants', {})
          .then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toEqual('');
            done();
          });
      });
    });

    describe('post request to /participants', () => {
      it('return status 200 on success', (done) => {
        selectWithParamsStub.resolves([{id:4711}]);
        updateStub.resolves({numberOfRowsAffected: 1});
        axios.post('http://localhost:7777/api/v1/participants', {personId: 4711})
          .then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toBeTruthy();
            done();
          });
      });
      it('return status 500 when failure', (done) => {
        selectWithParamsStub.resolves([{id:4711}]);
        updateStub.rejects();
        axios.post('http://localhost:7777/api/v1/participants', {personId: 4711})
          .catch(error => {
            expect(error.response.status).toBe(500);
            expect(error.response.data).toBeTruthy();
            done();
          });
      });
    });

    describe('delete request to /participants/:personId', () => {

      it('return status 200 on success', (done) => {
        selectWithParamsStub.onCall(0).resolves([{}]);
        selectWithParamsStub.onCall(1).resolves([{}]);
        deleteStub.resolves({numberOfRowsAffected: 1});
        axios.delete('http://localhost:7777/api/v1/participants/1')
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
      it('return status 500 when failure', (done) => {
        selectWithParamsStub.onCall(0).resolves([{}]);
        selectWithParamsStub.onCall(1).resolves([{}]);
        deleteStub.resolves({numberOfRowsAffected: 0});
        axios.delete('http://localhost:7777/api/v1/participants/1')
          .catch(error => {
            expect(error.response.status).toBe(500);
            done();
          });
      });
    });

    describe('get request to /registrations/by-key/:confirmationKey', () => {
      it('return status 200 on success', (done) => {
        selectWithParamsStub.resolves([{email:'test@email.de'}]);
        axios.get('http://localhost:7777/api/v1/registrations/by-key/xyz')
          .then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toBeTruthy();
            done();
          });
      });
      it('return status 200 with empty email when cannot find confirmation key ', (done) => {
        selectWithParamsStub.resolves([]);
        axios.get('http://localhost:7777/api/v1/registrations/by-key/xyz')
          .then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toEqual({email: ''});
            done();
          });
      });
      it('return status 500 when failure', (done) => {
        selectWithParamsStub.rejects();
        axios.get('http://localhost:7777/api/v1/registrations/by-key/xyz')
          .catch(error => {
            expect(error.response.status).toBe(500);
            expect(error.response.data).toBeTruthy();
            done();
          });
      });
    });

    describe('delete request to /registrations/by-key/:confirmationKey', () => {
      it('return status 200 on success', (done) => {
        updateStub.resolves({numberOfRowsAffected: 1});
        axios.delete('http://localhost:7777/api/v1/registrations/by-key/xyz')
          .then(response => {
            expect(response.status).toBe(200);
            done();
          });
      });
      it('return status 500 when cannot update', (done) => {
        selectWithParamsStub.resolves({numberOfRowsAffected: 0});
        axios.delete('http://localhost:7777/api/v1/registrations/by-key/xyz')
          .catch(error => {
            expect(error.response.status).toBe(500);
            done();
          });
      });
      it('return status 500 when failure', (done) => {
        selectWithParamsStub.rejects();
        axios.delete('http://localhost:7777/api/v1/registrations/by-key/xyz')
          .catch(error => {
            expect(error.response.status).toBe(500);
            done();
          });
      });
    });
  });

  describe('invitations', () => {
    describe('post request to invitations', () => {
      it('return status 200 on success', (done) => {
        selectWithParamsStub.onFirstCall().resolves([{id: 2, email: 'test2@test.de'}]);
        selectWithParamsStub.onSecondCall().resolves([{hasRoommate: 0}]);
        selectWithParamsStub.onThirdCall().resolves([{sameRoomType: 1}]);
        insertStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 888});
        axios.post('http://localhost:7777/api/v1/invitations', {
          inviterId: 1,
          inviterEmail: 'test@test.de',
          invitee1Id: 1,
          invitee1Email: 'test@test.de',
          invitee2Email: 'test2@test.de'
        }).then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeTruthy();
          done();
        });
      });

      it('return status 400 when invitee has already a roommate', (done) => {
        selectWithParamsStub.onFirstCall().resolves([{id: 2, email: 'test2@test.de'}]);
        selectWithParamsStub.onSecondCall().resolves([{hasRoommate: 1}]);
        selectWithParamsStub.onThirdCall().resolves([{sameRoomType: 1}]);
        insertStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 888});
        axios.post('http://localhost:7777/api/v1/invitations', {
          inviterId: 1,
          inviterEmail: 'test@test.de',
          invitee1Id: 1,
          invitee1Email: 'test@test.de',
          invitee2Email: 'test2@test.de'
        }).then(() => done('Error was expected'))
          .catch((err) => {
            expect(err.response.status).toBe(400);
            done();
          });
      });

      it('return status 400 when invitees have different room types', (done) => {
        selectWithParamsStub.onFirstCall().resolves([{id: 2, email: 'test2@test.de'}]);
        selectWithParamsStub.onSecondCall().resolves([{hasRoommate: 0}]);
        selectWithParamsStub.onThirdCall().resolves([{sameRoomType: 0}]);
        insertStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 888});
        axios.post('http://localhost:7777/api/v1/invitations', {
          inviterId: 1,
          inviterEmail: 'test@test.de',
          invitee1Id: 1,
          invitee1Email: 'test@test.de',
          invitee2Email: 'test2@test.de'
        }).then(() => done('Error was expected'))
          .catch((err) => {
            expect(err.response.status).toBe(400);
            done();
          });
      });

      it('returns 500 status on error', (done) => {
        selectWithParamsStub.resolves([]);
        insertStub.resolves({numberOfRowsAffected: 0, lastInsertedId: 0});
        axios.post('http://localhost:7777/api/v1/invitations', {})
          .then(() => done('Error was expected'))
          .catch((err) => {
            expect(err.response.status).toBe(500);
            done();
          });
      });
    });
    describe('get request to invitations', () => {
      it('return status 200 on success', (done) => {
        selectStub.resolves([{
          // eslint-disable-next-line camelcase
          inviter_id: 1,
          // eslint-disable-next-line camelcase
          inviter_email: 'test@test.de',
          // eslint-disable-next-line camelcase
          invitee1_id: 1,
          // eslint-disable-next-line camelcase
          invitee1_email: 'test@test.de',
          // eslint-disable-next-line camelcase
          invitee2_id: 2,
          // eslint-disable-next-line camelcase
          invitee2_email: 'test2@test.de',
          id: 4711,
          key: 'key'
        }]);
        axios.get('http://localhost:7777/api/v1/invitations').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeTruthy();
          done();
        });
      });
    });
    describe('put request to invitations', () => {
      it('throws error if invitee email not present', (done) => {
        axios.put('http://localhost:7777/api/v1/invitations/accept', {
          key: 'key'
        }).then(() => done('error expected'))
          .catch(error => {
            expect(error.response.status).toBe(400);
            done();
          });
      });
      it('throws error if key not present', (done) => {
        axios.put('http://localhost:7777/api/v1/invitations/accept', {
          inviteeEmail: 'test@test.de'
        }).then(() => done('error expected'))
          .catch(error => {
            expect(error.response.status).toBe(400);
            done();
          });
      });
      it('throws error if invalid action', (done) => {
        axios.put('http://localhost:7777/api/v1/invitations/invalid', {
          key: 'key',
          inviteeEmail: 'test@test.de'
        }).then(() => done('error expected'))
          .catch(error => {
            expect(error.response.status).toBe(400);
            done();
          });
      });
      describe(' if data ist ok', () => {
        it('return status 200 on accept success', (done) => {
          selectWithParamsStub.resolves([{
            id: 4711, inviter_person_id: 1, inviter_email: 'test@test.de',
            invitee1_person_id: 1, invitee1_email: 'test@test.de',
            invitee2_person_id: 2, invitee2_email: 'test2@test.de',
            key: 'key', status: 'OPEN'
          }]);
          updateStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 0});
          axios.put('http://localhost:7777/api/v1/invitations/accept', {
            key: 'key',
            inviteeEmail: 'test@test.de'
          }).then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toBeTruthy();
            done();
          });
        });
        it('return status 200 on decline success', (done) => {
          selectWithParamsStub.resolves([{
            id: 4711, inviter_person_id: 1, inviter_email: 'test@test.de',
            invitee1_person_id: 1, invitee1_email: 'test@test.de',
            invitee2_person_id: 2, invitee2_email: 'test2@test.de',
            key: 'key', status: 'OPEN'
          }]);
          updateStub.resolves({numberOfRowsAffected: 1, lastInsertedId: 0});
          axios.put('http://localhost:7777/api/v1/invitations/decline', {
            key: 'key',
            inviteeEmail: 'test@test.de'
          }).then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toBeTruthy();
            done();
          });
        });
      });
    });
    describe('get request to invitations sent by person', () => {
      it('returns status 200 on success', (done) => {
        selectWithParamsStub.resolves([{
          // eslint-disable-next-line camelcase
          inviter_id: 1,
          // eslint-disable-next-line camelcase
          inviter_email: 'test@test.de',
          // eslint-disable-next-line camelcase
          invitee1_id: 1,
          // eslint-disable-next-line camelcase
          invitee1_email: 'test@test.de',
          // eslint-disable-next-line camelcase
          invitee2_id: 2,
          // eslint-disable-next-line camelcase
          invitee2_email: 'test2@test.de',
          id: 4711,
          key: 'key'
        }]);
        axios.get('http://localhost:7777/api/v1/invitations/sentBy/1').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeTruthy();
          done();
        });
      });
      it('returns no invitation if not found', (done) => {
        selectWithParamsStub.resolves([]);
        axios.get('http://localhost:7777/api/v1/invitations/sentBy/1').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeFalsy();
          done();
        });
      });
    });
    describe('get request to invitations received by person', () => {
      it('return status 200 on success', (done) => {
        selectWithParamsStub.resolves([{
          // eslint-disable-next-line camelcase
          inviter_id: 1,
          // eslint-disable-next-line camelcase
          inviter_email: 'test@test.de',
          // eslint-disable-next-line camelcase
          invitee1_id: 1,
          // eslint-disable-next-line camelcase
          invitee1_email: 'test@test.de',
          // eslint-disable-next-line camelcase
          invitee2_id: 2,
          // eslint-disable-next-line camelcase
          invitee2_email: 'test2@test.de',
          id: 4711,
          key: 'key'
        }]);
        axios.get('http://localhost:7777/api/v1/invitations/receivedBy/2').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeTruthy();
          done();
        });
      });
      it('return empty array if not found', (done) => {
        selectWithParamsStub.resolves([]);
        axios.get('http://localhost:7777/api/v1/invitations/receivedBy/2').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeTruthy();
          expect(response.data.length).toBe(0);
          done();
        });
      });
    });
    describe('get request by key', () => {
      it('return status 200 on success', (done) => {
        selectWithParamsStub.resolves([{
          // eslint-disable-next-line camelcase
          inviter_id: 1,
          // eslint-disable-next-line camelcase
          inviter_email: 'test@test.de',
          // eslint-disable-next-line camelcase
          invitee1_id: 1,
          // eslint-disable-next-line camelcase
          invitee1_email: 'test@test.de',
          // eslint-disable-next-line camelcase
          invitee2_id: 2,
          // eslint-disable-next-line camelcase
          invitee2_email: 'test2@test.de',
          id: 4711,
          key: 'key'
        }]);
        axios.get('http://localhost:7777/api/v1/invitations/byKey/key').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeTruthy();
          done();
        });
      });
      it('return status 404 if not found', (done) => {
        selectWithParamsStub.resolves([]);
        axios.get('http://localhost:7777/api/v1/invitations/byKey/key').then(() => {
          done('error expected');
        }).catch(error => {
          expect(error.response.status).toBe(404);
          done();
        });
      });
    });
    describe('get request to room sharing data', () => {
      it('return status 200 on success', (done) => {
        selectStub.resolves([
          {
            P1Id: 1,
            P1FirstName: 'P1FirstName',
            P1LastName: 'P1LastName',
            P1FamilyInfo: 'P1FamilyInfo',
            P2Id: 2,
            P2FirstName: 'P2FirstName',
            P2LastName: 'P2LastName',
            P2FamilyInfo: 'P2FamilyInfo',
            RoomType: 'roomType'
          },
          {
            P1Id: 3,
            P1FirstName: 'P3FirstName',
            P1LastName: 'P3LastName',
            P1FamilyInfo: 'P3FamilyInfo',
            P2Id: 4,
            P2FirstName: 'P4FirstName',
            P2LastName: 'P4LastName',
            P2FamilyInfo: 'P4FamilyInfo',
            RoomType: 'roomType'
          }
        ]);
        axios.get('http://localhost:7777/api/v1/hotel/room-sharing').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeTruthy();
          done();
        });
      });
    });
  });

  describe('invitables', () => {
    describe('post request to /invitables/byEmail', () => {
      it('return status 200 on accept success', (done) => {
        selectWithParamsStub.resolves([{
          id: 4711, full_name: 'full_name', email: 'test@test.de',
          gender: 'gender', room_type: 'bedInDouble',
          introduction: 'introduction'
        }]);
        axios.post('http://localhost:7777/api/v1/invitables/byEmail', {
          email: 'test2@test.de'
        }).then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeTruthy();
          done();
        });
      });
    });
    describe('post request to /invitables/isInMarketplace', () => {
      it('return status 200 with true in data if in marketplace', (done) => {
        selectWithParamsStub.resolves([{
          id: 4711, full_name: 'full_name', email: 'test@test.de',
          gender: 'gender', room_type: 'bedInDouble',
          introduction: 'introduction'
        }]);
        axios.post('http://localhost:7777/api/v1/invitables/isInMarketplace', {
          email: 'test2@test.de'
        }).then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBe(true);
          done();
        });
      });
      it('return status 200 with false in data if not in marketplace', (done) => {
        selectWithParamsStub.resolves([]);
        axios.post('http://localhost:7777/api/v1/invitables/isInMarketplace', {
          email: 'test2@test.de'
        }).then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBe(false);
          done();
        });
      });
    });
    describe('post request to /invitables', () => {
      it('return status 200 with true in data if can add invitable', (done) => {
        selectWithParamsStub.resolves([{roomType: 'bedInDouble'}]);
        insertStub.resolves({lastInsertedId: 4711, numberOfRowsAffected: 1});
        axios.post('http://localhost:7777/api/v1/invitables', {
          userName: 'full_name', email: 'test@test.de',
          gender: 'gender', roomType: '',
          introduction: 'introduction'
        }).then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBe(true);
          done();
        });
      });
      it('return status 200 with false in data if cannot add invitable', (done) => {
        insertStub.throws('Some kind of error');
        axios.post('http://localhost:7777/api/v1/invitables', {
          email: 'test2@test.de'
        }).then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBe(false);
          done();
        });
      });
    });
  });

  describe('Hotel information', () => {
    describe('get request to /hotel/arrival-departure-hints', () => {
      it('return status 200 with non empty data', (done) => {
        selectWithParamsStub.resolves([{}]);
        axios.get('http://localhost:7777/api/v1/hotel/arrival-departure-hints').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeDefined();
          expect(response.data).not.toBeNull();
          done();
        });
      });
    });
    describe('get request to /hotel/arrivals-departures', () => {
      it('return status 200 with non empty data', (done) => {
        selectStub.onCall(0).resolves([
          {arrival: '0', arrivalText: 'arrival One', count: 20},
          {arrival: '1', arrivalText: 'arrival Two', count: 10}
        ]);
        selectStub.onCall(1).resolves([
          {departure: '0', departureText: 'arrival One', count: 13},
          {departure: '1', departureText: 'arrival Two', count: 17},
          {departure: '1', departureText: 'arrival Two', count: 55}
        ]);
        axios.get('http://localhost:7777/api/v1/hotel/arrivals-departures').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeDefined();
          expect(response.data).not.toBeNull();
          done();
        });
      });
      it('return status 500 on error', (done) => {
        selectStub.throws('Error');
        axios.get('http://localhost:7777/api/v1/hotel/arrivals-departures')
          .then(() => done('Exception expected.'))
          .catch((error) => {
            expect(error.response.status).toBe(500);
            done();
          });
      });
    });
    describe('get request to /hotel/total-participants', () => {
      it('return status 200 with non empty data', (done) => {
        selectStub.onCall(0).resolves([{amount: 222}]);
        axios.get('http://localhost:7777/api/v1/hotel/total-participants').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeDefined();
          expect(response.data).toBe(222);
          done();
        });
      });
      it('return status 200 with zero value on error', (done) => {
        selectStub.throws('Error');
        axios.get('http://localhost:7777/api/v1/hotel/total-participants').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeDefined();
          expect(response.data).toBe(0);
          done();
        });
      });
    });
    describe('get request to /hotel/sponsors-list', () => {
      it('return status 200 with non empty data', (done) => {
        selectWithParamsStub.resolves([{}]);
        axios.get('http://localhost:7777/api/v1/hotel/sponsors-list').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeDefined();
          expect(response.data).not.toBeNull();
          done();
        });
      });
    });
    describe('get request to /hotel/total-sponsored', () => {
      it('return status 200 with non empty data', (done) => {
        selectWithParamsStub.resolves([{totalSponsoring: 3000}]);
        axios.get('http://localhost:7777/api/v1/hotel/total-sponsored').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).not.toBeNull();
          done();
        });
      });
    });
    describe('get request to /hotel/fees-per-day', () => {
      const participantsDepartures = [
        {amount: 10, departureDayNumber: 2, hasToPayDepartureDayConferenceFee: true},
        {amount: 5, departureDayNumber: 2, hasToPayDepartureDayConferenceFee: false},
        {amount: 30, departureDayNumber: 3, hasToPayDepartureDayConferenceFee: true},
        {amount: 15, departureDayNumber: 3, hasToPayDepartureDayConferenceFee: false},
        {amount: 8, departureDayNumber: 4, hasToPayDepartureDayConferenceFee: true}
      ];
      it('return status 200 with non empty data', (done) => {
        selectWithParamsStub.onCall(0).resolves(conferenceDaysStubbed);
        selectWithParamsStub.onCall(1).resolves(participantsDepartures);
        axios.get('http://localhost:7777/api/v1/hotel/fees-per-day').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).not.toBeNull();
          expect(response.data).toHaveLength(conferenceDaysStubbed.length);
          done();
        });
      });
    });
    describe('get request to /hotel/accommodated-in/:roomType', () => {
      it('return status 200 on success', (done) => {
        selectWithParamsStub.resolves([{}]);
        axios.get('http://localhost:7777/api/v1/hotel/accommodated-in/single')
          .then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toBeTruthy();
            done();
          });
      });
      it('return status 200 with empty array when nothing is found', (done) => {
        selectWithParamsStub.resolves(null);
        axios.get('http://localhost:7777/api/v1/hotel/accommodated-in/single')
          .then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toEqual([]);
            done();
          });
      });
      it('return status 200 with empty array when failure', (done) => {
        selectWithParamsStub.rejects();
        axios.get('http://localhost:7777/api/v1/hotel/accommodated-in/single')
          .then(response => {
            expect(response.status).toBe(200);
            expect(response.data).toEqual([]);
            done();
          });
      });
    });
    describe('get request to /hotel/changes', () => {
      it('return status 200 with non empty data', (done) => {
        selectWithParamsStub.resolves([{}]);
        axios.get('http://localhost:7777/api/v1/hotel/changes').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeDefined();
          expect(response.data).not.toBeNull();
          done();
        });
      });
    });
    describe('get request to /hotel/download/:roomType', () => {
      it('returns a csv (tabbed) formatted string', (done) => {
        const expected = 'FirstName\tLastName\tCompany\tAddress1\tAddress2\tProvince\tZIP\tCity\tCountry' +
          '\tArrivalTime\tDepartureTime\tHasFamily\tFamilyInfo\tDietaryNeeds\tDietaryInfo\nPeter\tMuster\t' +
          'My Company GmbH\tThomasstraße 666\t\t\t99999\tcity\tcountry\tThursday afternoon\tMonday\ttrue\t' +
          'family info\ttrue\tDietaryNeeds. With two lines.\n';
        selectWithParamsStub.resolves([{
          firstname: 'Peter', lastname: 'Muster', company: 'My Company GmbH', address1: 'Thomasstraße 666',
          address2: '', province: '', postal: '99999', city: 'city', country: 'country', arrival: '0',
          departure: '5', family: true, family_info: 'family info', dietary: true,
          dietary_info: 'DietaryNeeds. \nWith two lines.'
        }]);
        axios.get('http://localhost:7777/api/v1/hotel/download/single').then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toBeDefined();
          expect(response.data).not.toBeNull();
          expect(response.data).toEqual(expected);
          done();
        });
      });
    });
  });
});
